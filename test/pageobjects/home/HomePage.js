// @flow
import BasePage from '../BasePage'

const VIDEO_HOME_PAGE = 'vjs_video_3_html5_api'
const VIDEO_HOME_PAGE_VALUE = '//div[@id="vjs_video_3"]'
const HOME_PAGE_TEXT_BELOW_VIDEO = '//div[@class="content"]/p[contains(text(),"At")]'
const HOME_PAGE_TEXT_UNDER_WHAT_MATTERS_MOST_SECTION_LIST = '//section[@class="section section-4 three_image_banner clearfix"]//div[@class="details"]/p/strong'
const HOME_PAGE_PAY_IT_FORWARD_TEXT = '//h2[text()="PAY IT FORWARD"]'
const HOME_PAGE_LEARN_MORE_BUTTON = '//a[text()="LEARN MORE"]'
const HOME_PAGE_OWL_CAROUSEL_TEXT = '//div[@class="owl-item active"]//i'
const HOME_PAGE_HERO_COPY_TEXT = '//div[@class="hero__copy"]/div'
const HOME_PAGE_CAROUSEL_DOT_LIST = '//div[@class="owl-dot"]/span'
const HOME_PAGE_BANNER = '//*[@id="content"]/section[1]/section/div'
const HOME_PAGE_US_BANNER_TEXT = '//*[@id="content"]/section[1]/section/div/div/div/div'
const HOME_PAGE_US_LEARN_MORE = '//*[@id="content"]/section[1]/section/div/div/a'
const HOME_PAGE_PROACTIVE_COMM_TEXT = '//*[@id="content"]/section[3]/div'
const HOME_PAGE_US_CARROUSEL = '//*[@id="content"]/div[3]/div[1]'
const HOME_PAGE_US_GLOBAL_FLOATING = '//*[@id="page"]/div[2]/div'
const HOME_PAGE_US_START_TODAY_BUTTON = '//*[@id="page"]/div[2]/div/div/div/div/a'
const HOME_PAGE_WHAT_MATTERS_MOST_TEXT = '//h1[@id="customhide" and text()=" What matters most to you"]'

export default class HomePage extends BasePage {
  async verifyPageIsLoaded () {
    let usBanner = await this.locatorFound(HOME_PAGE_BANNER)
    usBanner ? await this.waitForDisplayed(HOME_PAGE_BANNER, 10) : await this.waitForDisplayed(VIDEO_HOME_PAGE, 10)
  }

  async getPageTitle () {
    return this.getCurrentPageTitle()
  }

  async getPageUrl () {
    return this.getCurrentPageUrl()
  }

  async getVideoAttribute (attrib: string) {
    return this.getAttributesValue(VIDEO_HOME_PAGE_VALUE, attrib)
  }

  async verifyTextBelowVideoIsLoaded () {
    await this.waitForDisplayed(HOME_PAGE_TEXT_BELOW_VIDEO)
  }

  async getWhatMattersMostSectionTextList () {
    await this.waitForDisplayed(HOME_PAGE_TEXT_UNDER_WHAT_MATTERS_MOST_SECTION_LIST)
    return this.getElementsTextArrayList(HOME_PAGE_TEXT_UNDER_WHAT_MATTERS_MOST_SECTION_LIST, 3)
  }

  async isWhatMattersMostSectionTextDisplayed () {
    return this.locatorFound(HOME_PAGE_WHAT_MATTERS_MOST_TEXT)
  }

  async verifyPayItForwardTextIsDisplayed () {
    await this.waitForDisplayed(HOME_PAGE_PAY_IT_FORWARD_TEXT)
  }

  async verifyLearnMoreButtonIsDisplayed () {
    await this.waitForDisplayed(HOME_PAGE_LEARN_MORE_BUTTON)
  }

  async verifyCarouselSectionTextIsDisplayed () {
    await this.waitForDisplayed(HOME_PAGE_OWL_CAROUSEL_TEXT)
  }

  async clickCarouselDotByIndex (index: any) {
    return this.clickByIndex(HOME_PAGE_CAROUSEL_DOT_LIST, index)
  }

  async getCarouselSectionText () {
    await this.browser.pause(3000)
    return this.getText(HOME_PAGE_OWL_CAROUSEL_TEXT)
  }

  async getHeroCopySectionText () {
    return this.getText(HOME_PAGE_HERO_COPY_TEXT)
  }

  async getHomeBannerText () {
    return this.getText(HOME_PAGE_US_BANNER_TEXT)
  }

  async isUSLearnMoreButtonDisplayed () {
    return this.locatorFound(HOME_PAGE_US_LEARN_MORE)
  }

  async getUsLearnMoreLink () {
    return this.getAttributesValue(HOME_PAGE_US_LEARN_MORE, 'href')
  }

  async getProactiveCommText () {
    return this.getText(HOME_PAGE_PROACTIVE_COMM_TEXT)
  }

  async getPromoSlotText (index: string) {
    let promoSlot = '//*[@id="content"]/section[4]/section[1]/div/div[' + index + ']/div[2]/h4/strong'
    return this.getText(promoSlot)
  }

  async isPromoSlotButtonDisplayedAndRedirectPage (index: string) {
    let promoSlotButton = '//*[@id="content"]/section[4]/section[1]/div/div[' + index + ']/div[2]/p/a'
    return {isSlotDisplayed: await this.locatorFound(promoSlotButton),
      hrefValue: await this.getAttributesValue(promoSlotButton, 'href')}
  }

  async isCarrouselLoaded () {
    return this.locatorFound(HOME_PAGE_US_CARROUSEL)
  }

  async isGlobalFloatingLoaded () {
    return this.locatorFound(HOME_PAGE_US_GLOBAL_FLOATING)
  }

  async isStartTodayButtonDisplayed () {
    return this.locatorFound(HOME_PAGE_US_START_TODAY_BUTTON)
  }

  async getStartTodayLink () {
    return this.getAttributesValue(HOME_PAGE_US_START_TODAY_BUTTON, 'href')
  }
}
