// @flow
import BasePage from '../BasePage'

const MARKET_SELECTOR_POPUP = '#cboxContent'
const POPUP_BODY = '//*[@id="colorbox"]'
// const CHANNEL_SWITCH_POPUP_BODY_TITLE = `//*[@id="channelswitchpopup"]/h2[.="Are you sure you want to switch your account and subscription from Medifast Direct to OPTAVIA?"]`)
const POPUP_BODY_TITLE = '//*[@id="home-market_siteRedirect"]/h6[starts-with(text(),"YOUR")]'
const POPUP_BODY_TITLE_REPLICATED_SITE = '(//*[@id="home-market_popup"]/h6)[2]'
const POPUP_TEXT = '//div[@id="home-market_siteRedirect"]/p'
const RETURN_TO_MY_HOMEPAGE_BTN = '#return-homeMarket'
const REDIRECT_TO_MY_HOME_PAGE_BTN = '//div[contains(@style,"visibility")]//*[@id="redirectHome"]'
const SWITCH_CHANNEL_BUTTON_YES = '#continue-switch'
const SWITCH_CHANNEL_BUTTON_NO = '//*[@id="return-medifast"]'
const CLOSE_BUTTON = '//button[@id="cboxClose"]'

export default class PopupMarketSwitch extends BasePage {
  async verifyPageIsLoaded () {
    await this.waitForDisplayed(MARKET_SELECTOR_POPUP)
    await this.waitForDisplayed(POPUP_BODY_TITLE)
  }

  async verifyReplicatedSiteIsLoaded () {
    await this.waitForDisplayed(MARKET_SELECTOR_POPUP)
    await this.waitForDisplayed(POPUP_BODY_TITLE_REPLICATED_SITE)
  }

  async verifyChannelSwitchPopupIsLoaded () {
    await this.waitForDisplayed(MARKET_SELECTOR_POPUP)
  }

  async getPopupBodyTitle () {
    return this.getText(POPUP_BODY_TITLE)
  }

  async getPopupText () {
    return this.getText(POPUP_TEXT)
  }

  async isPopupDisplayed () {
    let status = await this.getAttributesValue(POPUP_BODY, 'style')
    return status.includes('display: block;')
  }

  async isReturnToMyHomepageButtonPresent () {
    return this.locatorFound(RETURN_TO_MY_HOMEPAGE_BTN)
  }

  async clickReturnToMyHomepageButton () {
    await this.click(RETURN_TO_MY_HOMEPAGE_BTN)
  }

  async clickRedirectToMyHomepageButton () {
    await this.click(REDIRECT_TO_MY_HOME_PAGE_BTN)
  }

  async clickOnSwitchChannel () {
    await this.moveToLocatorPosition(SWITCH_CHANNEL_BUTTON_YES)
    await this.click(SWITCH_CHANNEL_BUTTON_YES)
  }

  async clickOnReturnToMedifast () {
    await this.waitForDisplayed(SWITCH_CHANNEL_BUTTON_NO)
    await this.click(SWITCH_CHANNEL_BUTTON_NO)
  }

  async clickCloseButton () {
    await this.moveToLocatorPosition(CLOSE_BUTTON)
    await this.click(CLOSE_BUTTON)
  }
}
