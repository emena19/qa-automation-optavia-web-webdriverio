// @flow
import { By } from 'selenium-webdriver'
import BasePage from '../BasePage'
import CommonUtils from '../../lib/CommonUtils'

const CUSTOMIZE_YOUR_KIT_BODY_TITLE = By.css('div[id="content"] h1')
const SELECTED_KIT_ITEMS_LIST = By.xpath('//li[contains(@class,"selected")]/descendant::strong')
const DECREASE_ITEM_BUTTON_LIST = By.css('div[class="info-container"] button:nth-child(1)')
const INCREASE_ITEM_BUTTON_LIST = By.css('div[class="info-container"] button:nth-child(3)')
const ADD_TO_CART_BUTTON = By.xpath('//*[@id="addKitToCartForm"]/button')
const KIT_PRICE = By.xpath('//span[@class="product-price"]/span')
const CUSTOMIZE_YOUR_KIT_ADD_TO_CART_BUTTON = By.id('customKitAddButton')
export default class CustomizeYourKitPage extends BasePage {
  async verifyPageIsLoaded () {
    await this.waitForDisplayed(CUSTOMIZE_YOUR_KIT_BODY_TITLE)
  }

  async getPageTitle () {
    return this.getCurrentPageTitle()
  }

  async getPageUrl () {
    return this.getCurrentPageUrl()
  }

  async getItemsKitList () {
    await this.waitForDisplayed(SELECTED_KIT_ITEMS_LIST)
    return this.getElementsTextArrayList(SELECTED_KIT_ITEMS_LIST)
  }

  async getKitPrice () {
    await this.waitForDisplayed(KIT_PRICE)
    return this.getText(KIT_PRICE)
  }

  async addItemFromProductList (index) {
    await this.clickByIndex(INCREASE_ITEM_BUTTON_LIST, index)
  }

  async removeItemFromProductList (index) {
    await this.clickByIndex(DECREASE_ITEM_BUTTON_LIST, index)
  }

  async getAddToCartBtnDisabledAttributeValue () {
    await this.waitForDisplayed(ADD_TO_CART_BUTTON)
    return this.getAttributesValue(ADD_TO_CART_BUTTON, 'disabled')
  }

  async clickAddToCartButton () {
    await this.waitForDisplayed(ADD_TO_CART_BUTTON, 5)
    await this.click(ADD_TO_CART_BUTTON)
  }

  async getProductIsUnavailable (product: string) {
    let unavailableProductXpath = By.xpath(`//*[@data-bind="text: name" and contains(text(),"${product}")]/parent::div//following-sibling::div/button[@class="button button-tertiary outOfStock"]`)
    await this.moveToLocatorPosition(unavailableProductXpath)
    await this.waitForDisplayed(unavailableProductXpath, 10)
    return this.getText(unavailableProductXpath)
  }

  async isQtyStepperDisplayed (product: string) {
    let unavailableProductXpath = By.xpath(`//*[@data-bind="text: name" and contains(text(),"${product}")]/parent::div//following-sibling::div/span[@class="qty"]`)
    return this.locatorFound(unavailableProductXpath)
  }

  async clickAddToCartButtonFromCustomizeYourKit () {
    await this.waitForDisplayed(CUSTOMIZE_YOUR_KIT_ADD_TO_CART_BUTTON, 5)
    await this.click(CUSTOMIZE_YOUR_KIT_ADD_TO_CART_BUTTON)
  }

  async addRemoveSpecificItemFromCustomizeKit (action, product) {
    let baseItemXpath = By.xpath('//*[@class="product-name"]')
    let allItems = await this.getElementsTextArrayList(baseItemXpath)
    let index = await CommonUtils.getIndexOfItemFromArray(allItems, product)
    action === 'add' ? await this.addItemFromProductList(index) : await this.removeItemFromProductList(index)
  }
}
