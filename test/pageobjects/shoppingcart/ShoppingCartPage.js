// @flow
import BasePage from '../BasePage'

const AUTOSHIP_TEMPLATE_GLOBAL_MESSAGE = '//*[@class="success-msg"]/ul/li'
const SHOPPING_CART_BODY_TITLE = '//*[@class="col-md-12"]/h1'
const REMOVE_PRODUCT_ICON = '//*[@id="RemoveProduct_0"]/i'
const REMOVE_PRODUCT_ICON_ARRAY = '//*[@id="cartItems"]/table/descendant::p[contains (text(),"")]//ancestor::tr/td[1]/a'
const REMOVE_PAID_PRODUCT_ICON = '//*[@id="cartItems"]/table/tbody/tr/td[@class="item-remove"]/descendant::a[@class="submitRemoveProduct"]'
// const PRODUCT_NAME = '(//p[@class="product-name"])[last()]')
const PRODUCT_NAME = '//*[@id="cartItems"]/table/descendant::p[@class="product-name"]'
const PRODUCT_UNIT_PRICE = '//*[@id="cartItems"]/table/tbody/tr[1]/td[4]'
const PRODUCT_UNIT_PRICE_ARRAY = '//*[@id="cartItems"]/table/descendant::td[@class="item-price"]'
// const PRODUCT_QTY = '//*[@id="cartItems"]/table/tbody/tr[1]/td[5]/form/input[4]')
const PRODUCT_QTY = '//*[@class="qty input-text"][not(@disabled)]'
const LAST_PRODUCT_QTY = '(//*[@class="qty input-text"])[last()]'
const PRICE_TOTAL = '//*[@id="cartItems"]/table/tbody/tr[1]/td[6]'
const OPTAVIA_PREMIER_PRODUCT_PRICE = `//*[@id="cartItems"]/table/tbody/tr/td[@data-header="Subtotal"]`
const JOIN_PREMIER_CHECKBOX = '#my-med-advantage'
const JOIN_PREMIER_CONTAINER = '//div[@class="item_container"]'
const FREE_SHIPPING_MESSAGES = '//*[@class="cart-promotions-applied"]'
const UPSELL_MESSAGES = '//*[@class="upsells__title"]'
const CONGRATULATIONS_MESSAGES = '//*[@class="optaviaMessage"]'
const PROCEED_TO_CHECKOUT_BTN = '#checkoutButtonBottom'
const PRODUCT_LINE_FIRST = '#updateCartForm0'
const ORDER_SUBTOTAL = '//*[@class="subtotalAmt"]'
const PRODUCT_REMOVED_MESSAGE = '//*[@id="content"]/descendant::li[contains(text(), "Product has been removed from your cart.")]'
const PREMIER_PRODUCT_REMOVAL_ERROR_MSG = '//*[@id="content"]//ul[@class="messages"]/li[@class="error-msg"]/ul/li[1]'
const PREMIER_PRODUCT_REMOVAL_NOTE_MSG = '//*[@id="content"]//ul[@class="messages"]/li[@class="note-msg"]/ul/li[1]'
const CONTINUE_SHOPPING_BUTTON = '//a[text()="Continue Shopping"]'
const PRODUCT_NAME_ARRAY_LIST = '//*[@id="cartItems"]/table/descendant::p[@class="product-name"]'
const PREMIER_PRODUCT_UNIT_PRICE_ARRAY_LIST = `//*[@id="cartItems"]/table/tbody/tr/td[4]`
const OPTAVIA_PREMIER_PRODUCT_QTY_ARRAY_LIST = '//*[@class="qty input-text"]'
const ENTER_PROMO_CODE = '//*[@id="promoCode"]/input[1]'
const APPLY_CODE_BUTTON = '//*[@id="promoCode"]/descendant::button[contains (text(),"Apply Code")]'
const APPLY_CODE_ARROW = '//*[@id="promoCode"]/button/i'
const APPLIED_PROMO_CODE_TEXT = '//*[@class="promo-code"]//following-sibling::ul'
const OPTAVIA_BLACK_PROMO_HEADER_MESSAGE = '//div[@class="optaviaMessage"]/h6'
const OPTAVIA_BLACK_PROMO_PARA_MESSAGE = '//div[@class="optaviaMessage"]/p'
const OPTAVIA_LEARN_HOW_LINK = '//div[@class="optaviaMessage"]/p/a'
const OPTAVIA_GUIDE = '//div[@class="product-info"]/p[contains(text(),"37883")]/../p'
const JOIN_PREMIER_TERMS_AND_CONDITIONS_LINK = '//*[@id="command"]/div[1]/div/span/span/a'
const JOIN_PREMIER_TERMS_AND_CONDITIONS_TITLE = '//div[@class="content"]/h1[1]'
const JOIN_PREMIER_TERMS_AND_CONDITIONS_CLOSE_BUTTON = '//*[@id="cboxClose"]'
const COMMISSIONABLE_RADIO_BUTTON = '//*[@id="promoCode"]/div[1]/div[1]'
const NON_COMMISSIONABLE_RADIO_BUTTON = '//*[@id="promoCode"]/div[1]/div[2]'

// Remove All Products
const REMOVE_ALL_PRODUCTS_LINK = '//a[contains(text(),"Remove All Products")]'
const REMOVE_ALL_POPUP_TITLE = '//h3[contains(text(),"Remove All Products From Cart?")]'
const REMOVE_ALL_POPUP_CANCEL_BUTTON = '//a[contains(text(),"Cancel")]'
const REMOVE_ALL_POPUP_REMOVE_ALL_BUTTON = '//button[contains(text(),"Remove All Products")]'
const EMPTY_CART_TITLE = '//*[@class="empty-cart__title"]'
const PREMIER_ORDER_EMPTY_CART_TITLE_1 = '//*[@class="empty-cart__line1"]'
const PREMIER_ORDER_EMPTY_CART_TITLE_2 = '//*[@class="empty-cart__line2"]'
const CONTINUE_SHOPPING_ON_EMPTY_CART_BUTTON = '//*[@class="empty-cart__title"]//following-sibling::a'
const CONTINUE_SHOPPING_ON_PREMIER_EMPTY_CART_BUTTON = '//*[@class="empty-cart__line2"]//following-sibling::a'

// Coach Kit Upsell
const COACH_UPSELL_BANNER = '//*[@id="content"]/div/div[4]/div[1]/div/div[2]/ul/div[1]'
const DR_A_HOH_BUNDLE_ADD_TO_CART_BTN = '//*[@id="addToCartFormDrAHabitsOfHealthKit_Product"]/button'
const OPTAVIA_FUELING_EXPERIENCE_PACK_ADD_TO_CART_BTN = '//*[@id="addToCartFormFuelingExperienceUpsell_Product"]/button'
const HEALTH_PROFESSIONAL_MARKETING_PACK_ADD_TO_CART_BTN = '//*[@id="addToCartFormHPMarketingKit_Product"]/button'
const OPTAVIA_COACH_BUSINESS_KIT_SPANISH_ADD_TO_CART_BTN = '//*[@id="addToCartFormoptavia-coach-business-kit-spanish-kt"]/button'
const COACH_UPSELL_BANNER_ITEMS = '//*[@id="content"]//*[@class="details upsell__content-trigger"]'
const COACH_UPSELL_BANNER_HABITS_OF_TRANSFORMATION = '//*[@id="addToCartFormDrAHabitsOfHealthKit_Product"]/button'
const COACH_UPSELL_OPTAVIA_FUELINGS_DISCOUNT_PRICE_TEXT = '(//div[@class="details upsell__content-trigger"])[4]/following-sibling::div/span[@class="product-price sales"]'
// Exclusive OPTAVIA Premier offers at bottom of page
const LIVING_A_LONGER_HEALTHIER_LIFE_ADD_TO_CART_BTN = '//*[@id="addToCartFormdra-habits-companion-guide-ea"]/button'
const DISCOVER_YOUR_OPTIMAL_HEALTH_ADD_TO_CART_BTN = '//*[@id="addToCartFormdiscover-optimal-health-ea"]/button'
const DR_A_HABITS_OF_HEALTH_ADD_TO_CART_BTN = '//*[@id="addToCartFormdra-habits-health-book-ea"]/button'

// Exclusive OPTAVIA Premier Upsell kits
const EXCLUSIVE_USELL_BANNER = '//*[@id="content"]/div/div[4]/div[1]/div'
const EXCLUSIVE_UPSELL_ULTIMATE_HEALTHY_HYDRATION_PACK_ADD_AND_JOIN_BTN = '//*[@id="addToCartFormPHUltimatePackUpsell_Product"]/button'
const EXCLUSIVE_UPSELL_VERY_CHEESY_PACK_ADD_AND_JOIN_BTN = '//*[@id="addToCartFormVeryCheesyUpsell_Product"]/button'
const EXCLUSIVE_UPSELL_HEALTHY_HYDRATION_STARTER_PACK_ADD_AND_JOIN_BTN = '//*[@id="addToCartFormPHStarterPackUpsell_Product"]/button'
const EXCLUSIVE_UPSELL_HABITS_OF_HEALTH_TRANSFORMATIONAL_ADD_AND_JOIN_BTN = '//*[@id="addToCartFormDrAHabitsOfHealthKit_Product"]/button'
const EXCLUSIVE_UPSELLS_TEXT_BY_INDEX = '//*[@class="details upsell__content-trigger"]'
const EXCLUSIVE_UPSELL_OPTAVIA_FUELING_ADD_AND_JOIN_BTN = '//*[@id="addToCartFormFuelingExperienceUpsell_Product"]/button'
const UPSELL_KIT_PRODUCT_NAME = '//*[@id="cartItems"]/table/tbody/tr[14]/td[3]/div/p[1]'
const NO_EXCLUSIVE_UPSELL_BANNER = '//*[@id="content"]/div/div[3]'

// Acquisition Offer - Free Meal and Free Items
// const FREE_MEAL_LIST = '//*[@class="applied-promotions"]/parent::td/*[@class="styled-list"]/li')
const FREE_OPTAVIA_GUIDE = '//*[@class="freeItemWrapper"]/descendant::p[contains(., "OPTAVIA Guide")]'
const FREE_OPTAVIA_GUIDE_CHINESE = '//*[@class="freeItemWrapper"]/descendant::p[contains(., "(Traditional Chinese)")]'
const FREE_JOURNEY_KICKOFF_CARD_INSERT = '//*[@class="freeItemWrapper"]/descendant::p[contains(., "Journey Kickoff Card")]'
const FREE_OPTAVIA_GUIDE_TOP_TIPS_INSERT = '//*[@class="freeItemWrapper"]/descendant::p[contains(., "Tip")]'
const FREE_GET_1_WEEK_FREE = '//*[@class="freeItemWrapper"]/descendant::p[.="Get 1 Week FREE with your OPTAVIA Premier order (up to $121.25 Value)!"]'
const FREE_BLENDER_BOTTLE = '//*[@class="freeItemWrapper"]/descendant::p[contains(., "OPTAVIA BlenderBottle")]'
const FREE_ITEMS_LIST = '//*[@id="cartItems"]/table/tbody/*[@class="freeItemWrapper"]'
const FREE_ITEMS_NAMES = '//*[@class="freeItemWrapper"]//p[@class="product-name"]'
const FREE_CREAMY_CHOCOLATE_SHAKE = '//*[@class="freeItemWrapper"]/descendant::p[contains(., "Essential Creamy Chocolate Shake")]'
const FREE_TROPICAL_FRUIT_FRUIT_SMOOTHIE = '//*[@class="freeItemWrapper"]/descendant::p[contains(., "Tropical Fruit Smoothie")]'
const FREE_ZESTY_CHEDDAR_ITALIAN_HERB_FLAVORED_CRUNCHERS = '//*[@class="freeItemWrapper"]/descendant::p[contains(., "Zesty Cheddar")]'
const FREE_ZESTY_LEMON_CRISP_BAR = '//*[@class="freeItemWrapper"]/descendant::p[contains(., "Zesty Lemon Crisp Bar")]'
const FREE_CREAMY_DOUBLE_PEANUT_BUTTER_CRISP_BAR = '//*[@class="freeItemWrapper"]/descendant::p[contains(., "Creamy Double Peanut Butter")]'
const FREE_MEALS_INCLUDED = '//*[@id="cartItems"]/descendant::ul[contains(@class,"styled-list")]'
const EDIT_FREE_MEAL_BUTTON = '//*[@id="cartItems"]/descendant::a[contains(text(),"Free Meal")]'
const EDIT_KIT_PAID_MEAL_BUTTON = '//a[@class="button mealButton-alternate"]'
const EDIT_KIT_PAID_MEAL_MESSAGE = 'li[class="note-msg"] li'
const FREE_EGG_FLOWER_MUSHROOM_SOUP = '//*[@class="freeItemWrapper"]/descendant::p[contains(., "Essential Egg Flower & Mushroom Soup")]'
const FREE_GREEN_TEA_SHAKE = '//*[@class="freeItemWrapper"]/descendant::p[contains(., "Essential Green Tea Shake")]'
const FREE_SPICY_POPPERS = '//*[@class="freeItemWrapper"]/descendant::p[contains(., "Essential Spicy Poppers")]'
const FREE_CRANBERRY_HONEY_NUT = '//*[@class="freeItemWrapper"]/descendant::p[contains(., "Essential Cranberry Honey Nut Granola Bar")]'
const FREE_GET_35_MEALS = '//*[@class="freeItemWrapper"]/descendant::p[contains(., "Get 35 FREE meals with your")]'
const DRAS_HABITS_OF_HEALTH_BUNDLE = '//*[@id="cartItems"]/table/descendant::p[contains(text(),"Dr. A’s Habits of Health Bundle")]'
// Updating the quantity of a product from shopping cart page
const UPDATE_PRODUCT_LINK = '#QuantityProduct_0'
const UPDATE_LAST_PRODUCT_LINK = '(//*[@class="updateQuantityProduct"])[last()]'
const SAVE_ORDER_LINK = '//a[contains(@class,"button button-default save")]'
const SAVE_ORDER_DISABLED_LINK = '//button[contains(@class,"button button-default save")]'
// const UPDATE_PRODUCT_QUANTITY_INPUT = By.id('quantity0')

// ORDER SUMMARY
const DISCOUNTS = '//*[@class="order-totals details-block"]//td[@class="savingsAmt"]'
const GST_AMT = '//*[@class="order-totals details-block"]//td[@class="total taxAmt"]'

// Optavia Premier Template Shopping cart page
const TEMPLATE_EDIT_MESSAGE = '//*[@id="content"]/div/descendant::li[contains(text(), "You are now editing your ")]'
const UPSELL_KIT_PRODUCT_NAME_PREMIER_TEMPLATE = '//*[@id="cartItems"]/table/tbody/tr[1]/td[3]/div/p[1]'
const OPTAVIA_PREMIER_APPLIED_REWARDS = '//*[@id="orderTotals"]/tbody/descendant::td[contains(text(),"Rewards:")]/following-sibling::td'
const OPTAVIA_PREMIER_DELIVERY_CHARGE = `//*[@id="orderTotals"]/tbody/descendant::td[contains(text(),"Delivery Charge:")]/following-sibling::td`
const OPTAVIA_PREMIER_TAX = `//*[@id="orderTotals"]/tbody/descendant::td[contains(text(),"Estimated tax:")]/following-sibling::td`
const OPTAVIA_PREMIER_ORDER_TOTAL = `//*[@id="orderTotals"]/tfoot/descendant::td[contains(text(),"Order Total")]/following-sibling::td`
const SAVE_SUBSCRIPTION_BTN = '//*[@class="button-wrapper"]/descendant::a[contains(text(),"Save")]'
const CANCEL_SUBSCRIPTION_BTN = '//*[@class="button-wrapper"]/descendant::a[contains(text(),"Cancel")]'

const CV_TOTAL_TEXT = '//*[@id="orderTotals"]/tbody/descendant::td[contains(text(),"CV")]/following-sibling::td'
const ORDER_SUMMARY_WELLNESS_CREDIT_TEXT = '//*[@id="orderTotals"]/tbody/descendant::td[contains(text(),"Wellness Credit:")]'

// Wellnesscredit
const WELLNESSCREDIT_CUST_USERNAME_CLIENTID_LIST = '//*[@id="cartItems"]/table/descendant::p[@class="client-info"]'

const SHOPPING_CART_VIEW_ITEMS_IN_KIT = '//*[@id="cartItems"]/descendant::a[contains(text(),"View Items")]'
const SHOPPING_CART_ITEMS_KIT_LIST = 'div[class="modal-content"] ul li'
const SHOPPING_CART_CLOSE_POPUP_BUTTON = 'div[class="modal-content"] a[data-target="#kit-description"]'
const SHOPPING_CART_SPINNER = '//*[@id="add-to-cart-spinner"]'

// Upsells offers

const ULTIMATE_HEALTHY_HYDRATION_PACK_QTY = '//*[@id="qtyPHUltimatePackUpsell_Product"]'
const VERY_CHEESY_PACK_QTY = '//*[@id="qtyVeryCheesyUpsell_Product"]'
const HEALTHY_HYDRATION_STARTER_QTY = '//*[@id="qtyPHStarterPackUpsell_Product"]'
const HOH_TRANSFORMATIONAL_QTY = '//*[@id="qtyDrAHabitsOfHealthKit_Product"]'

export default class ShoppingCartPage extends BasePage {
  async verifyPageIsLoaded () {
    await this.waitForDisplayed(SHOPPING_CART_BODY_TITLE)
  }

  async getPageTitle () {
    return this.getCurrentPageTitle()
  }

  async getPageUrl () {
    return this.getCurrentPageUrl()
  }

  async getYourShoppingCartTitle () {
    return this.getText(SHOPPING_CART_BODY_TITLE)
  }

  async isJoinPremierChecked () {
    return this.hasAttributePresent(JOIN_PREMIER_CHECKBOX, 'checked')
  }

  async isJoinPremierContainerDisplayed () {
    return this.locatorFound(JOIN_PREMIER_CONTAINER, 1)
  }

  async isProceedToCheckoutBtnDisplayed () {
    return this.locatorFound(PROCEED_TO_CHECKOUT_BTN, 1)
  }

  async clickToCheckJoinPremierCheckBox () {
    await this.click(JOIN_PREMIER_CHECKBOX)
  }

  async clickContinueShoppingBtn () {
    await this.click(CONTINUE_SHOPPING_BUTTON)
  }

  async getFreeCartItemsArrayList () {
    return this.getElementsTextArrayList(FREE_ITEMS_LIST)
  }

  async verifyFreeOptaviaGuideIsLoaded () {
    await this.waitForDisplayed(FREE_OPTAVIA_GUIDE)
  }

  async verifyFreeOptaviaGuideChineseIsLoaded () {
    await this.waitForDisplayed(FREE_OPTAVIA_GUIDE_CHINESE)
  }

  async verifyFreeJourneyKickoffCardInsertIsLoaded () {
    await this.waitForDisplayed(FREE_JOURNEY_KICKOFF_CARD_INSERT)
  }

  async verifyFreeOptaviaGuideTopTipsInsertIsLoaded () {
    await this.waitForDisplayed(FREE_OPTAVIA_GUIDE_TOP_TIPS_INSERT)
  }

  async verifyFree1WeekMealIsLoaded () {
    await this.waitForDisplayed(FREE_GET_1_WEEK_FREE)
  }

  async verifyFreeBlenderBottleIsLoaded () {
    await this.waitForDisplayed(FREE_BLENDER_BOTTLE)
  }

  async verifyDrAsHabitofHealthBundleIsLoaded () {
    await this.waitForDisplayed(DRAS_HABITS_OF_HEALTH_BUNDLE)
  }

  async getFreeItemsNames () {
    return this.getElementsTextArrayList(FREE_ITEMS_NAMES)
  }

  // MESSAGES
  async getMessageAutoshipTemplateCancel () {
    return this.getText(AUTOSHIP_TEMPLATE_GLOBAL_MESSAGE)
  }

  async getUpsellsMessage () {
    await this.waitForDisplayed(UPSELL_MESSAGES)
    return this.getText(UPSELL_MESSAGES)
  }

  async getCongratulationsEnrollMessage () {
    await this.driver.sleep(10000)
    return this.getTextByIndex(CONGRATULATIONS_MESSAGES, 0)
  }

  async getCongratulationsOfferMessage () {
    return this.getTextByIndex(CONGRATULATIONS_MESSAGES, 1)
  }

  async getShippingFreeTopMessage () {
    await this.driver.sleep(10000)
    return this.getTextByIndex(FREE_SHIPPING_MESSAGES, 0)
  }

  async getShippingFreeReceivedPromotionsMessage () {
    return this.getTextByIndex(FREE_SHIPPING_MESSAGES, 1)
  }

  async clickRemoveProductIcon () {
    await this.moveToLocatorPositionAndClick(REMOVE_PRODUCT_ICON)
  }

  async isLoadedProductRemovedMessage () {
    return this.locatorFound(PRODUCT_REMOVED_MESSAGE)
  }

  async clickRemovePaidProductIcon () {
    await this.moveToLocatorPositionAndClick(REMOVE_PAID_PRODUCT_ICON)
  }

  async getPaidCartItemsArrayListCount () {
    return this.getCountOfArrayList(REMOVE_PAID_PRODUCT_ICON)
  }

  async getPremierTempErrorMessageText () {
    await this.waitForDisplayed(PREMIER_PRODUCT_REMOVAL_ERROR_MSG)
    return this.getText(PREMIER_PRODUCT_REMOVAL_ERROR_MSG)
  }

  async getPremierTempNoteMessageText () {
    await this.waitForDisplayed(PREMIER_PRODUCT_REMOVAL_NOTE_MSG)
    return this.getText(PREMIER_PRODUCT_REMOVAL_NOTE_MSG)
  }

  async getProductNameText () {
    await this.waitForDisplayed(PRODUCT_NAME)
    return this.getText(PRODUCT_NAME)
  }

  async getOptaviaGuideText () {
    await this.waitForDisplayed(OPTAVIA_GUIDE)
    return this.getText(OPTAVIA_GUIDE)
  }

  async getProductNameTextArray (num: any) {
    await this.waitForDisplayed(PRODUCT_NAME_ARRAY_LIST, 5)
    return this.getTextByIndex(PRODUCT_NAME_ARRAY_LIST, num)
  }

  async getProductUnitPrice () {
    return this.getText(PRODUCT_UNIT_PRICE)
  }

  async getProductUnitPriceArray (num: any) {
    return this.getTextByIndex(PRODUCT_UNIT_PRICE_ARRAY, num)
  }

  async getProductQty () {
    return this.getAttributesValue(PRODUCT_QTY, 'value')
  }

  async updateProductQTY (keys: any) {
    await this.waitForDisplayed(PRODUCT_QTY, 5)
    await this.moveToLocatorPositionAndClick(PRODUCT_QTY)
    await this.sendKeys(PRODUCT_QTY, keys)
  }

  async clickUpdateProductLink () {
    await this.click(UPDATE_PRODUCT_LINK)
  }

  async clickSaveOrderLink () {
    await this.click(SAVE_ORDER_LINK)
  }

  async isSaveOrderLinkDisabled () {
    return this.hasAttributePresent(SAVE_ORDER_LINK, 'disabled')
  }

  async isSaveOrderLinkDisabledOnEmptyCart () {
    return this.hasAttributePresent(SAVE_ORDER_DISABLED_LINK, 'disabled')
  }

  async isSaveOrderLinkDisplayed () {
    return this.locatorFound(SAVE_ORDER_LINK)
  }

  async getProductTotalPrice () {
    return this.getText(PRICE_TOTAL)
  }

  async getOrderNumber () {
    return this.getAttributesValue(PRODUCT_LINE_FIRST, 'data-cart')
  }

  async clickProceedToCheckoutBtn () {
    await this.waitForDisplayed(PROCEED_TO_CHECKOUT_BTN)
    await this.click(PROCEED_TO_CHECKOUT_BTN)
  }

  async isCheckoutBtnDisplayed () {
    return this.locatorFound(PROCEED_TO_CHECKOUT_BTN)
  }

  async getDiscountAmt () {
    return this.getText(DISCOUNTS)
  }

  async getGSTAmt () {
    return this.getText(GST_AMT)
  }

  async verifyGSTIsLoaded () {
    await this.waitForDisplayed(GST_AMT)
  }

  async verifyDiscountIsLoaded () {
    await this.waitForDisplayed(DISCOUNTS)
  }

  async isDiscountNotDisplayed () {
    return this.locatorNotFound(DISCOUNTS)
  }

  // Upsell Kits

  async isCoachUpSellBannerLoaded () {
    await this.moveToLocatorPosition(COACH_UPSELL_BANNER)
    return this.locatorFound(COACH_UPSELL_BANNER, '')
  }

  async clickDrAHOHBundleAddToCartBtn () {
    await this.click(DR_A_HOH_BUNDLE_ADD_TO_CART_BTN)
  }

  async clickOptaviaFuelingsExperiencePackAddToCartBtn () {
    await this.click(OPTAVIA_FUELING_EXPERIENCE_PACK_ADD_TO_CART_BTN)
  }

  async clickHealthProfessionalMarketingPackAddToCartBtn () {
    await this.click(HEALTH_PROFESSIONAL_MARKETING_PACK_ADD_TO_CART_BTN)
  }

  async clickOptaviaCoachBusinessKitSpanishAddToCartBtn () {
    await this.click(OPTAVIA_COACH_BUSINESS_KIT_SPANISH_ADD_TO_CART_BTN)
  }

  // EXCLUSIVE OPTAVIA UPSELLS

  async isUpSellBannerLoaded () {
    return this.hasAttributePresent(EXCLUSIVE_USELL_BANNER, '')
  }

  async isUpSellBannerDisplayed () {
    return this.locatorFound(EXCLUSIVE_USELL_BANNER)
  }

  async isUpSellBannerNotLoaded () {
    return this.hasAttributePresent(NO_EXCLUSIVE_UPSELL_BANNER, '')
  }

  async clickUpSellUltimateHealthyHydrationPackAddAndJoinBtn () {
    await this.waitForDisplayed(EXCLUSIVE_UPSELL_ULTIMATE_HEALTHY_HYDRATION_PACK_ADD_AND_JOIN_BTN)
    await this.click(EXCLUSIVE_UPSELL_ULTIMATE_HEALTHY_HYDRATION_PACK_ADD_AND_JOIN_BTN)
  }

  async clickUpSellVeryCheesyPackAddAndJoinBtn () {
    await this.waitForDisplayed(EXCLUSIVE_UPSELL_VERY_CHEESY_PACK_ADD_AND_JOIN_BTN)
    await this.click(EXCLUSIVE_UPSELL_VERY_CHEESY_PACK_ADD_AND_JOIN_BTN)
  }

  async clickUpSellHydrationStarterPackAddAndJoinBtn () {
    await this.click(EXCLUSIVE_UPSELL_HEALTHY_HYDRATION_STARTER_PACK_ADD_AND_JOIN_BTN)
  }

  async clickUpSellSelectHabitsOfHealthTransPackAddAndJoinBtn () {
    await this.click(EXCLUSIVE_UPSELL_HABITS_OF_HEALTH_TRANSFORMATIONAL_ADD_AND_JOIN_BTN)
  }

  async getUpsellTextByIndex (index: string) {
    let upsell = '//*[@class="upsell__content"]//*[@class="details upsell__content-trigger"]'
    return this.getTextByIndex(upsell, index)
  }

  async getUpsellQtyTextUltimateHealthyHydrationPack () {
    return this.getAttributesValue(ULTIMATE_HEALTHY_HYDRATION_PACK_QTY, 'value')
  }

  async getUpsellQtyTextCheesyPack () {
    return this.getAttributesValue(VERY_CHEESY_PACK_QTY, 'value')
  }

  async getUpsellQtyHealthyHydrationPack () {
    return this.getAttributesValue(HEALTHY_HYDRATION_STARTER_QTY, 'value')
  }

  async getUpsellQtyHoHTransformational () {
    return this.getAttributesValue(HOH_TRANSFORMATIONAL_QTY, 'value')
  }

  // Exclusive OPTAVIA Premier offers
  async clickLivingALongerHealthierLifeAddToCartBtn () {
    await this.click(LIVING_A_LONGER_HEALTHIER_LIFE_ADD_TO_CART_BTN)
  }

  async clickDiscoverYourOptimalHealthAddToCartBtn () {
    await this.click(DISCOVER_YOUR_OPTIMAL_HEALTH_ADD_TO_CART_BTN)
  }

  async clickDrAHabitsOfHealthAddToCartBtn () {
    await this.click(DR_A_HABITS_OF_HEALTH_ADD_TO_CART_BTN)
  }

  async getUpsellKitName () {
    await this.waitForDisplayed(UPSELL_KIT_PRODUCT_NAME)
    return this.getText(UPSELL_KIT_PRODUCT_NAME)
  }

  async getUpsellKitNameInPremierTemplateShoppingCart () {
    await this.waitForDisplayed(UPSELL_KIT_PRODUCT_NAME_PREMIER_TEMPLATE)
    return this.getText(UPSELL_KIT_PRODUCT_NAME_PREMIER_TEMPLATE)
  }

  async getOrderSubtotal () {
    return this.getTextByIndex(ORDER_SUBTOTAL, 0)
  }

  async getOrderTotal () {
    await this.waitForDisplayed(OPTAVIA_PREMIER_ORDER_TOTAL)
    return this.getTextByIndex(OPTAVIA_PREMIER_ORDER_TOTAL, 0)
  }

  async getTextUltimateHealthyHydrationPackAddBtn () {
    return this.getText(EXCLUSIVE_UPSELL_ULTIMATE_HEALTHY_HYDRATION_PACK_ADD_AND_JOIN_BTN)
  }

  async getTextVeryCheesyAddBtn () {
    return this.getText(EXCLUSIVE_UPSELL_VERY_CHEESY_PACK_ADD_AND_JOIN_BTN)
  }

  async getTextHydrationStarterPackAddBtn () {
    return this.getText(EXCLUSIVE_UPSELL_HEALTHY_HYDRATION_STARTER_PACK_ADD_AND_JOIN_BTN)
  }

  async getTextSelectHabitsOfHealthTransPackAddBtn () {
    return this.getText(EXCLUSIVE_UPSELL_HABITS_OF_HEALTH_TRANSFORMATIONAL_ADD_AND_JOIN_BTN)
  }

  async getTextWellnessCreditClientIDUserNameByIndex (index: any) {
    return this.getTextByIndex(WELLNESSCREDIT_CUST_USERNAME_CLIENTID_LIST, index)
  }

  async isWellnessCreditTextDisplayed () {
    return this.locatorFound(ORDER_SUMMARY_WELLNESS_CREDIT_TEXT)
  }

  // Subscription template
  async getSubscriptionEditMessageText () {
    return this.getText(TEMPLATE_EDIT_MESSAGE)
  }

  async getCartItemsArrayList () {
    return this.getElementsTextArrayList(PRODUCT_NAME_ARRAY_LIST)
  }

  async getCartItemsArrayListCount () {
    return this.getCountOfArrayList(PRODUCT_NAME)
  }

  async getCartUnitPriceArrayList () {
    return this.getElementsTextArrayList(PREMIER_PRODUCT_UNIT_PRICE_ARRAY_LIST)
  }

  async getCartQtyArrayList () {
    return this.getElementsAttributeValueArrayList(OPTAVIA_PREMIER_PRODUCT_QTY_ARRAY_LIST, 'value')
  }

  async getCartPriceArrayList () {
    return this.getElementsTextArrayList(OPTAVIA_PREMIER_PRODUCT_PRICE)
  }

  async getPremierAppliedRewardsText () {
    return this.getText(OPTAVIA_PREMIER_APPLIED_REWARDS)
  }

  async isPremierAppliedRewardsPresent () {
    return this.locatorFound(OPTAVIA_PREMIER_APPLIED_REWARDS, 1)
  }

  async clickSaveSubscriptionBtn () {
    await this.click(SAVE_SUBSCRIPTION_BTN)
  }

  async clickCancelSubscriptionBtn () {
    await this.click(CANCEL_SUBSCRIPTION_BTN)
  }

  async getPremierDeliveryChargeText () {
    return this.getText(OPTAVIA_PREMIER_DELIVERY_CHARGE)
  }

  async getPremierTaxText () {
    return this.getText(OPTAVIA_PREMIER_TAX)
  }

  async getPremierOrderTotalText () {
    return this.getText(OPTAVIA_PREMIER_ORDER_TOTAL)
  }

  async clickRemoveIcon () {
    await this.waitForDisplayed(REMOVE_PRODUCT_ICON_ARRAY, 3)
    return this.click(REMOVE_PRODUCT_ICON_ARRAY)
  }

  async clickRemoveIconByIndexForSpecificProduct (key: number) {
    await this.clickByIndex(REMOVE_PRODUCT_ICON_ARRAY, key)
  }

  async verifyFreeCreamyChocolateShakeIsDisplayed () {
    await this.waitForDisplayed(FREE_CREAMY_CHOCOLATE_SHAKE)
  }

  async verifyFreeTropicalFruitSmoothieIsDisplayed () {
    await this.waitForDisplayed(FREE_TROPICAL_FRUIT_FRUIT_SMOOTHIE)
  }

  async verifyFreeZestyCheddarCrunchersIsDisplayed () {
    await this.waitForDisplayed(FREE_ZESTY_CHEDDAR_ITALIAN_HERB_FLAVORED_CRUNCHERS)
  }

  async verifyFreeZestyLemonCrispBarIsDisplayed () {
    await this.waitForDisplayed(FREE_ZESTY_LEMON_CRISP_BAR)
  }

  async verifyFreeCreamyDoublePeanutButterCrispBarIsDisplayed () {
    await this.waitForDisplayed(FREE_CREAMY_DOUBLE_PEANUT_BUTTER_CRISP_BAR)
  }

  async verifyFreeEggFlowerAndMushroomSoupIsDisplayed () {
    await this.waitForDisplayed(FREE_EGG_FLOWER_MUSHROOM_SOUP)
  }

  async verifyFreeGreenTeaShakeIsDisplayed () {
    await this.waitForDisplayed(FREE_GREEN_TEA_SHAKE)
  }

  async verifyFreeSpicyPoppersIsDisplayed () {
    await this.waitForDisplayed(FREE_SPICY_POPPERS)
  }

  async verifyFreeCranberryHoneyNutIsDisplayed () {
    await this.waitForDisplayed(FREE_CRANBERRY_HONEY_NUT)
  }

  async verifyGet35FreeMealsIsDisplayed () {
    await this.waitForDisplayed(FREE_GET_35_MEALS)
  }

  async updateProductQty (productName: string, keys: any) {
    const productQTY = `//div/p[contains(text(),"${productName}")]//..//..//..//td[5]//input[@name="quantity"]`
    await this.moveToLocatorPosition(productQTY)
    await this.sendKeys(productQTY, keys)
  }

  async clickUpdateProductButton (productName) {
    const updateBTN =
      `//td[3]/div/p[contains(text(),'${productName}')]//..//..//..//td[5]//a`
    await this.click(updateBTN)
  }

  async clickRemoveProduct (productName) {
    const removeIcon =
      `//td[3]/div/p[contains(text(),'${productName}')]//..//..//..//td/a`
    await this.click(removeIcon)
  }

  async clickEditFreeMealButton () {
    await this.click(EDIT_FREE_MEAL_BUTTON)
  }

  async getFreeMealsIncluded () {
    await this.waitForDisplayed(FREE_MEALS_INCLUDED)
    return this.getText(FREE_MEALS_INCLUDED)
  }

  async clickEditKitPaidMealButton () {
    await this.click(EDIT_KIT_PAID_MEAL_BUTTON)
  }

  async getKitEditedMessage () {
    await this.waitForDisplayed(EDIT_KIT_PAID_MEAL_MESSAGE)
    return this.getText(EDIT_KIT_PAID_MEAL_MESSAGE)
  }

  async clickViewItemsInKit () {
    await this.waitForDisplayed(SHOPPING_CART_VIEW_ITEMS_IN_KIT, 5)
    await this.moveToLocatorPosition(SHOPPING_CART_VIEW_ITEMS_IN_KIT)
    await this.click(SHOPPING_CART_VIEW_ITEMS_IN_KIT)
  }

  async getItemsKitList () {
    await this.waitForDisplayed(SHOPPING_CART_ITEMS_KIT_LIST)
    return this.getElementsTextArrayList(SHOPPING_CART_ITEMS_KIT_LIST)
  }

  async closeViewItemsPopup () {
    await this.waitForDisplayed(SHOPPING_CART_CLOSE_POPUP_BUTTON)
    await this.click(SHOPPING_CART_CLOSE_POPUP_BUTTON)
  }

  async sendKeysToPromoCodeTextField (keys: string) {
    await this.waitForDisplayed(ENTER_PROMO_CODE, 3)
    await this.sendKeys(ENTER_PROMO_CODE, keys)
  }

  async clickOnApplyCodeButton () {
    await this.waitForDisplayed(APPLY_CODE_BUTTON, 3)
    await this.click(APPLY_CODE_BUTTON)
  }

  async getCVTextValue () {
    await this.waitForDisplayed(CV_TOTAL_TEXT, 3)
    return this.getText(CV_TOTAL_TEXT)
  }

  async getPromoCodeText () {
    await this.waitForDisplayed(APPLIED_PROMO_CODE_TEXT)
    return this.getText(APPLIED_PROMO_CODE_TEXT)
  }

  async isPromoCodeApplied () {
    return this.locatorFound(APPLIED_PROMO_CODE_TEXT)
  }

  async getOptaviaBlackPromoHeaderText () {
    return this.getText(OPTAVIA_BLACK_PROMO_HEADER_MESSAGE)
  }

  async isOptaviaBlackPromoDisplayed () {
    return this.locatorFound(OPTAVIA_BLACK_PROMO_HEADER_MESSAGE)
  }

  async getOptaviaBlackPromoParaText () {
    return this.getText(OPTAVIA_BLACK_PROMO_PARA_MESSAGE)
  }

  async clickOnLearnHowLink () {
    await this.waitForDisplayed(OPTAVIA_LEARN_HOW_LINK, 3)
    await this.click(OPTAVIA_LEARN_HOW_LINK)
  }

  async getCoachUpsellsArrayList () {
    return this.getElementsTextArrayList(COACH_UPSELL_BANNER_ITEMS)
  }

  async getCoachUpsellsITemsTotal () {
    return this.getCountOfArrayList(COACH_UPSELL_BANNER_ITEMS)
  }

  async getUpsellButtonText () {
    return [
      await this.getText(COACH_UPSELL_BANNER_HABITS_OF_TRANSFORMATION),
      await this.getText(HEALTH_PROFESSIONAL_MARKETING_PACK_ADD_TO_CART_BTN),
      await this.getText(OPTAVIA_COACH_BUSINESS_KIT_SPANISH_ADD_TO_CART_BTN),
      await this.getText(OPTAVIA_FUELING_EXPERIENCE_PACK_ADD_TO_CART_BTN)
    ]
  }

  async removeItemFromCartByIndex (index: any) {
    await this.clickByIndex(REMOVE_PRODUCT_ICON_ARRAY, index)
  }

  async getProductText (sku: string) {
    let PRODUCT = `//div[@class="product-info"]/p[contains(text(),"${sku}")]/../p`
    await this.waitForDisplayed(PRODUCT)
    return this.getText(PRODUCT)
  }

  async isSpinnerPresent () {
    return this.locatorFound(SHOPPING_CART_SPINNER)
  }

  async getElementAttribute () {
    return this.getAttributesValue(SHOPPING_CART_SPINNER, 'style')
  }

  async clickOnTermsAndConditionsFromJoinPremierBox () {
    await this.waitForDisplayed(JOIN_PREMIER_TERMS_AND_CONDITIONS_LINK, 3)
    await this.click(JOIN_PREMIER_TERMS_AND_CONDITIONS_LINK)
  }

  async isJoinPremierTermsAndConditionsLoaded () {
    await this.waitForDisplayed(JOIN_PREMIER_TERMS_AND_CONDITIONS_TITLE, 3)
    return this.locatorFound(JOIN_PREMIER_TERMS_AND_CONDITIONS_TITLE)
  }

  async closeTermsAndConditionsFromJoinPremier () {
    await this.waitForDisplayed(JOIN_PREMIER_TERMS_AND_CONDITIONS_CLOSE_BUTTON, 3)
    await this.click(JOIN_PREMIER_TERMS_AND_CONDITIONS_CLOSE_BUTTON)
  }

  async leaveOneProductOnCart () {
    let totalItems = await this.getCountOfArrayList(REMOVE_PRODUCT_ICON_ARRAY)
    while (totalItems !== 1) {
      await this.clickRemoveProductIcon()
      totalItems--
    }
  }

  async clickOnCommissionableRadioButton () {
    await this.waitForDisplayed(COMMISSIONABLE_RADIO_BUTTON, 5)
    await this.click(COMMISSIONABLE_RADIO_BUTTON)
  }

  async clickOnNonCommissionableRadioButton () {
    await this.waitForDisplayed(NON_COMMISSIONABLE_RADIO_BUTTON, 5)
    await this.click(NON_COMMISSIONABLE_RADIO_BUTTON)
  }

  async setCommissionableVoucher (option: boolean) {
    if (option) {
      await this.clickOnCommissionableRadioButton()
    } else {
      await this.clickOnNonCommissionableRadioButton()
    }
  }

  async getShortNamesListFromCart () {
    let productsShortNames = []
    let totalItems = await this.getCartItemsArrayListCount()
    for (let i = 0; i < totalItems; i++) {
      let PRODUCTS_SHORT_NAMES_LIST = '//*[@id="updateCartForm' + i + '"]/input[2]'
      productsShortNames.push(await this.getAttributesValue(PRODUCTS_SHORT_NAMES_LIST, 'value'))
    }
    return productsShortNames
  }

  async clickOnApplyCodeArrow () {
    await this.waitForDisplayed(APPLY_CODE_ARROW, 3)
    await this.click(APPLY_CODE_ARROW)
  }

  async isProductNameDisplayed () {
    return this.locatorFound(PRODUCT_NAME)
  }

  // REMOVE ALL PRODUCTS
  async isRemoveAllProductLinkDisplayed () {
    return this.locatorFound(REMOVE_ALL_PRODUCTS_LINK)
  }

  async clickOnRemoveAllProductLink () {
    await this.waitForDisplayed(REMOVE_ALL_PRODUCTS_LINK)
    await this.click(REMOVE_ALL_PRODUCTS_LINK)
  }

  async isRemoveAllPopupDisplayed () {
    return this.locatorFound(REMOVE_ALL_POPUP_TITLE)
  }

  async isRemoveAllPopupCancelButtonDisplayed () {
    return this.locatorFound(REMOVE_ALL_POPUP_CANCEL_BUTTON)
  }

  async isRemoveAllPopupRemoveAllButtonDisplayed () {
    return this.locatorFound(REMOVE_ALL_POPUP_REMOVE_ALL_BUTTON)
  }

  async clickOnCancelButtonOnRemoveAllPopup () {
    await this.waitForDisplayed(REMOVE_ALL_POPUP_CANCEL_BUTTON)
    await this.click(REMOVE_ALL_POPUP_CANCEL_BUTTON)
  }

  async clickOnRemoveAllButton () {
    await this.waitForDisplayed(REMOVE_ALL_POPUP_REMOVE_ALL_BUTTON)
    await this.click(REMOVE_ALL_POPUP_REMOVE_ALL_BUTTON)
  }

  async isShoppingCartEmptyMessageDisplayed () {
    return this.locatorFound(EMPTY_CART_TITLE)
  }

  async isPremierShoppingCartEmptyMessageLine1Displayed () {
    return this.locatorFound(PREMIER_ORDER_EMPTY_CART_TITLE_1)
  }

  async isPremierShoppingCartEmptyMessageLine2Displayed () {
    return this.locatorFound(PREMIER_ORDER_EMPTY_CART_TITLE_2)
  }

  async clickOnContinueShoppingOnEmptyCart () {
    await this.waitForDisplayed(CONTINUE_SHOPPING_ON_EMPTY_CART_BUTTON)
    await this.click(CONTINUE_SHOPPING_ON_EMPTY_CART_BUTTON)
  }

  async clickOnContinueShoppingOnPremierEmptyCart () {
    await this.waitForDisplayed(CONTINUE_SHOPPING_ON_PREMIER_EMPTY_CART_BUTTON)
    await this.click(CONTINUE_SHOPPING_ON_PREMIER_EMPTY_CART_BUTTON)
  }
  async isUpsellsAddToCartButtonsDisplayed () {
    return [
      await this.locatorFound(COACH_UPSELL_BANNER_HABITS_OF_TRANSFORMATION),
      await this.locatorFound(HEALTH_PROFESSIONAL_MARKETING_PACK_ADD_TO_CART_BTN),
      await this.locatorFound(OPTAVIA_COACH_BUSINESS_KIT_SPANISH_ADD_TO_CART_BTN),
      await this.locatorFound(OPTAVIA_FUELING_EXPERIENCE_PACK_ADD_TO_CART_BTN)
    ]
  }

  async getUpsellsTextList () {
    return this.getElementsTextArrayList(EXCLUSIVE_UPSELLS_TEXT_BY_INDEX)
  }

  async getFreeItemsCount () {
    await this.waitForDisplayed(FREE_ITEMS_LIST)
    return this.getCountOfArrayList(FREE_ITEMS_LIST)
  }

  async areFreeItemsDisplayed () {
    return this.locatorFound(FREE_ITEMS_LIST)
  }

  async getOptaviaFuelingDiscountedText () {
    await this.waitForDisplayed(COACH_UPSELL_OPTAVIA_FUELINGS_DISCOUNT_PRICE_TEXT)
    return this.getText(COACH_UPSELL_OPTAVIA_FUELINGS_DISCOUNT_PRICE_TEXT)
  }

  async clickUpSellSelectOptFuelingExpPackAddAndJoinBtn () {
    await this.click(EXCLUSIVE_UPSELL_OPTAVIA_FUELING_ADD_AND_JOIN_BTN)
  }

  async updateProductQtyByIndex (qty) {
    await this.sendKeys(LAST_PRODUCT_QTY, qty)
  }

  async clickUpdateQtyLinkOfLastProduct () {
    await this.click(UPDATE_LAST_PRODUCT_LINK)
  }
}
