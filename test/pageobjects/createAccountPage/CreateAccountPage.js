// @flow
import BasePage from '../BasePage'
const TITLE = '//div[@class="create-account__header"]/h1'
const FULL_NAME = '#fullName'
const EMAIL_ADDRESS = '#emailId'
const PASSWORD = '#password'
const PHONE = '[name = "custom-mobileno"]'
const USER_POLICY = '//input[@name="userPolicyAccepted" and @type="checkbox"]'
const CREATE_ACCOUNT_BUTTON = '//button[@id="create_account" and @type="submit"]'
const FULL_NAME_ERROR = '#fullName-error'
const EMAIL_ERROR = '#emailId-error'
const PASSWORD_ERROR = '#password-error'
const PHONE_ERROR = '#custom-mobileno-error'
const EMAIL_ALREADY_EXISTING_ERROR = '#email.errors'
const GLOBAL_ERROR_MESSAGE = '//li[@class="error-msg"]//li'
const COUNTRY_CODE_DROP_DOWN = '.dropdown-selection'
const PHONE_NUMBER_TEXT = '#mobileNo'
const LOGIN_LINK = '//*[@id="content"]//a[text()="Login"]'
const CREATE_AN_ACCOUNT_TITLE = '//*[@class="create-account__header"]/h1'
const COUNTRY_NAME = '[name = "country"]'
const PHONE_NUMBER_PLACEHOLDER = '//*[@class="validate-required newThemePhoneNovalidate"]'
const EXISTING_EMAIL_ERROR = '#email.errors'

export default class CreateAccountPage extends BasePage {
  async verifyPageIsLoaded () {
    await this.waitForDisplayed(TITLE, 7)
  }

  async getPageTitle () {
    return this.getCurrentPageTitle()
  }

  async getPageUrl () {
    return this.getCurrentPageUrl()
  }

  async getBodyTitle () {
    return this.getText(TITLE)
  }

  async sendKeysPassword (keys: string) {
    return this.sendKeys(PASSWORD, keys)
  }

  async sendKeysEmailAddress (keys: string) {
    return this.sendKeys(EMAIL_ADDRESS, keys)
  }

  async sendKeysFullName (keys: string) {
    return this.sendKeys(FULL_NAME, keys)
  }

  async sendKeysPhone (keys: string) {
    return this.sendKeys(PHONE, keys)
  }

  async clickUserPolictyCheck () {
    await this.click(USER_POLICY)
  }

  async clickCreateAccountBtn () {
    await this.click(CREATE_ACCOUNT_BUTTON)
  }

  async isCreateAcctButtonDisabled () {
    await this.waitForDisplayed(CREATE_ACCOUNT_BUTTON, 10)
    return this.hasAttributePresent(CREATE_ACCOUNT_BUTTON, 'disabled')
  }

  async getFullNameErrorMessage () {
    return this.getText(FULL_NAME_ERROR)
  }

  async getEmailErrorMessage () {
    return this.getText(EMAIL_ERROR)
  }

  async getEmailExistingErrorMessage () {
    return this.getText(EMAIL_ALREADY_EXISTING_ERROR)
  }

  async getPasswordErrorMessage () {
    return this.getText(PASSWORD_ERROR)
  }

  async getPhoneErrorMessage () {
    return this.getText(PHONE_ERROR)
  }

  async createAnAccount (fullname: string, email: string, password: string, phone: string) {
    await this.sendKeys(FULL_NAME, fullname)
    await this.sendKeys(EMAIL_ADDRESS, email)
    await this.waitForDisplayed(PASSWORD, 10)
    await this.sendKeys(PASSWORD, password)
    await this.sendKeys(PHONE, phone)
    await browser.pause(1000)
    await this.clickUserPolicy()
    await browser.pause(1000)
    await this.click(CREATE_ACCOUNT_BUTTON)
  }

  async getCreateAccFieldsErrorMessage () {
    let addressFieldsUS = [FULL_NAME_ERROR, EMAIL_ERROR, PASSWORD_ERROR, PHONE_ERROR]
    let errorMessages = []
    for (let i = 0; i < addressFieldsUS.length; i++) {
      errorMessages.push(await this.getText(addressFieldsUS[i]))
    }
    return errorMessages
  }

  async getGlobalErrorMessage () {
    return this.getText(GLOBAL_ERROR_MESSAGE)
  }

  async selectOptionFromDropdown (keys: string) {
    await this.click(COUNTRY_CODE_DROP_DOWN)
    const countryCode = `//div[@class="dropdown-options"]/ul/li[@data-value='${keys}']`
    await this.moveToLocatorPositionAndClick(countryCode)
  }

  async getPhoneNumberText () {
    return this.getAttributesValue(PHONE_NUMBER_TEXT, 'value')
  }

  async clickOnLoginLink () {
    await this.click(LOGIN_LINK)
  }

  async getCreateAnAccountTitleText () {
    return this.getAttributesValue(CREATE_AN_ACCOUNT_TITLE, 'innerText')
  }

  async getCountryPlaceHolderValue () {
    return this.getAttributesValue(COUNTRY_NAME, 'value')
  }

  async getFullNamePlaceholderValue () {
    return this.getAttributesValue(FULL_NAME, 'placeholder')
  }

  async getEmailPlaceHolderValue () {
    return this.getAttributesValue(EMAIL_ADDRESS, 'placeholder')
  }

  async getPasswordPlaceHolderValue () {
    return this.getAttributesValue(PASSWORD, 'placeholder')
  }

  async isCreateAccountButtonDisabled () {
    let value = await this.getAttributesValue(CREATE_ACCOUNT_BUTTON, 'disabled')
    let boolValue = (value === 'true')
    return boolValue
  }

  async getPhonePlaceHolderValue () {
    return this.getAttributesValue(PHONE_NUMBER_PLACEHOLDER, 'placeholder')
  }

  async getExistingEmailErrorMessage () {
    await this.waitForDisplayed(EXISTING_EMAIL_ERROR)
    return this.getText(EXISTING_EMAIL_ERROR)
  }

  async clickUserPolicy () {
    await this.click(USER_POLICY)
  }
}
