// @flow
import BasePage from '../BasePage'

const TITLE = '//*[@class="account-login_title"]'
const LOGIN_EMAIL_ADDRESS = '#okta-signin-username'
const LOGIN_PASSWORD = '//*[@data-test="signin-input-password"]'
const LOGIN_BUTTON = '#okta-signin-submit'
const LOGIN_ERROR_MESSAGE = '//*[@id="form1"]//p'
const LOGIN_CREATE_ACCOUNT_LINK = '.account-login_alternate_option_link'
const LOGIN_GENDER = '#gender'
const LOGIN_DOB = '#register.dob'
const LOGIN_COMMON_ERROR = '//*[@class="okta-form-infobox-error infobox infobox-error"]/p'
const LOGIN_FORGOT_YOUR_PASSWORD = '//*[@class="account-login_forgot_pasword"]/a'
const USER_OR_PASSWORD_ERROR = '//*[@id="form19"]/div[1]/div[1]/div/div/p'
const WELCOME_TO_MEDIFAST_TITLE = '//*[@id="content"]/h1'
// Reset Password
const RESET_PASSWORD_EMAIL = '#forgottenPwd.email'
const RESET_PASSWORD_TITLE = '//*[@class="headline"]'
const RESET_SEND_EMAIL_BUTTON = '//*[@id="forgottenPwdForm"]/button'
const RESET_PASSWORD_GLOBAL_MESSAGE = '//*[@id="validEmail"]/ul/li'
const RESET_PASSWORD_EMAIL_ERROR = '#forgottenPwd.email-error'
const LOGIN_OKTA_ERROR_MESSAGE = '//*[@class="error-msg"]//div[2]'
export default class LoginPage extends BasePage {
  async verifyPageIsLoaded () {
    await this.waitForDisplayed(TITLE)
  }

  async loginAsExistingCustomer (username: string, password: string, error: boolean = false) {
    await this.sendKeysUsername(username)
    await this.sendKeysLoginPassword(password)
    // Add this line to verify the error in Login scenarios
    if (error) {
      await browser.pause(7500)
    }
    await this.click(LOGIN_BUTTON)
    await browser.pause(5000)
  }

  async getLoginErrorMessage () {
    return this.getText(LOGIN_ERROR_MESSAGE)
  }

  async getOKTALoginErrorMessage () {
    return this.getText(LOGIN_OKTA_ERROR_MESSAGE)
  }

  async getPageUrl () {
    return this.getCurrentPageUrl()
  }
  async getPageTitle () {
    return this.getCurrentPageTitle()
  }

  async clickCreateAccountLink () {
    await this.click(LOGIN_CREATE_ACCOUNT_LINK)
  }

  async isUserNotAddedToOkta () {
    let errorStatus = await this.locatorFound(USER_OR_PASSWORD_ERROR)
    let message = await this.getText(USER_OR_PASSWORD_ERROR)
    return !(errorStatus && message === 'Your username or password was incorrect.')
  }

  async isWelcomeToMedifastDisplayed () {
    return this.locatorFound(WELCOME_TO_MEDIFAST_TITLE)
  }

  async getEmailPlaceHolderValue () {
    return this.getAttributesValue(LOGIN_EMAIL_ADDRESS, 'placeholder')
  }

  async getPasswordPlaceHolderValue () {
    return this.getAttributesValue(LOGIN_PASSWORD, 'placeholder')
  }

  async isGenderFieldDisplayed () {
    return this.locatorFound(LOGIN_GENDER)
  }

  async isDOBFieldDisplayed () {
    return this.locatorFound(LOGIN_DOB)
  }

  async getLoginUsernamePlaceholderValue () {
    return this.getAttributesValue(LOGIN_EMAIL_ADDRESS, 'placeholder')
  }

  async getLoginPasswordPlaceholderValue () {
    return this.getAttributesValue(LOGIN_PASSWORD, 'placeholder')
  }

  async getLoginCommonErrorMessage () {
    await this.waitForDisplayed(LOGIN_COMMON_ERROR)
    return this.getText(LOGIN_COMMON_ERROR)
  }

  async clickForgotYourPassword () {
    return this.click(LOGIN_FORGOT_YOUR_PASSWORD)
  }

  async getResetPasswordEmailPlaceholderValue () {
    return this.getAttributesValue(RESET_PASSWORD_EMAIL, 'placeholder')
  }

  async sendKeysResetPasswordEmailAddress (keys: string) {
    await this.sendKeys(RESET_PASSWORD_EMAIL, keys)
  }

  async verifyResetPasswordPopupIsLoaded () {
    await this.waitForDisplayed(RESET_PASSWORD_TITLE)
  }

  async clickResetPasswordSendEmailButton () {
    await this.click(RESET_SEND_EMAIL_BUTTON)
  }

  async getResetPasswordEmailErrorMessage () {
    return this.getText(RESET_PASSWORD_EMAIL_ERROR)
  }

  async verifyResetPasswordGlobalMessageIsLoaded () {
    await this.waitForDisplayed(RESET_PASSWORD_GLOBAL_MESSAGE)
  }

  async getResetPasswordGlobalMessage () {
    return this.getText(RESET_PASSWORD_GLOBAL_MESSAGE)
  }

  async isSignInButtonDisabled () {
    await this.waitForDisplayed(LOGIN_BUTTON, 10)
    return this.hasAttributePresent(LOGIN_BUTTON, 'disabled')
  }

  async sendKeysUsername (keys: string) {
    await this.sendKeys(LOGIN_EMAIL_ADDRESS, keys)
  }

  async sendKeysLoginPassword (keys: string) {
    await this.sendKeys(LOGIN_PASSWORD, keys)
  }

  async isLoginErrorDisplayed () {
    return this.locatorFound(LOGIN_COMMON_ERROR)
  }
}
