// @flow
import BasePage from '../BasePage'

const ADDED_TO_YOUR_SHOPPING_CART_POPUP_TITLE = '//*[@id="addto-cart-layer"]/div/div[1]'
const ADDED_TO_YOUR_SHOPPING_CART_POPUP_TITLE_HK = '//*[@id="kitModalWindow"]/div/div[1]/h1'
// const ADDED_TO_YOUR_SHOPPING_CART_POPUP_QTY_BOX = '//*[@id="quantity0"]')
// const ADDED_TO_YOUR_SHOPPING_CART_POPUP_UPDATE_LINK = '//*[@id="QuantityProduct_0"]')
const ADDED_TO_YOUR_SHOPPING_CART_POPUP_QTY_BOX = '[name = "quantity"]'
const ADDED_TO_YOUR_SHOPPING_CART_POPUP_UPDATE_LINK = '//*[@class="updateQuantityProduct"]'
const ADDED_TO_YOUR_SHOPPING_CART_POPUP_CHECKOUT_BTN = '//*[@class="button button-default showLoaderOnClickCheckout"][text()="Checkout"]'
// const ADDED_TO_YOUR_SHOPPING_CART_POPUP_BEGIN_CHECKOUT_BTN_HK = '//*[contains(@class,"Checkout")]')
const ADDED_TO_YOUR_SHOPPING_CART_POPUP_BEGIN_CHECKOUT_BTN_HK = '//*[@id="kit-checkout-button"]'
const ADDED_TO_YOUR_SHOPPING_CART_POPUP_CONTINUE_BTN = '//*[@id="addto-cart-layer"]/div/div[3]/a[2]'
// Monetate add to cart pop up for kits
const MONETATE_ADD_TO_YOUR_SHOPPING_CART_POP_UP = `//*[@id="kitModalWindow"]/div`
const MONETATE_ADD_TO_YOUR_SHOPPING_CART_POP_UP_TITLE = `//*[@id="kitModalWindow"]/div/descendant::h1[contains (text(), "Added to Your Shopping Cart:")]`
// const MONETATE_ADD_ADDITIONAL_ITEM_TO_CART_POP_UP_TITLE = '//*[@id="kitModalWindow"]/div/descendant::h1[contains (text(), "Additional Tools for Success:")]')
const MONETATE_BEGIN_CHECKOUT_BTN = '#kit-checkout-button'
const CONTINUE_SHOPPING_KIT_BTN = '//*[@class="button-div non-mobile-display"]//*[@id="kit-continue-shopping-button"]'
const MONETATE_POP_UP_PURPOSEFUL_HYDRATION_BUNDLE = '#suggestedItemName'
const MONETATE_KIT = '#addedKitName'
const MONETATE_ITEM = '#addedItemName'
const ADDED_TO_YOUR_SHOPPING_CART_POPUP_TITLE_ERROR = '.cart_popup_error_msg'

export default class AddedToYourShoppingCartPopup extends BasePage {
  async verifyPageIsLoaded () {
    await this.waitForDisplayed(ADDED_TO_YOUR_SHOPPING_CART_POPUP_TITLE, 10)
    await this.waitForDisplayed(ADDED_TO_YOUR_SHOPPING_CART_POPUP_UPDATE_LINK, 10)
  }

  async verifyPageIsLoadedHK () {
    await this.waitForDisplayed(ADDED_TO_YOUR_SHOPPING_CART_POPUP_TITLE_HK)
  }

  async verifyButtonIsLoaded () {
    await this.waitForDisplayed(ADDED_TO_YOUR_SHOPPING_CART_POPUP_CHECKOUT_BTN)
    await this.waitForDisplayed(ADDED_TO_YOUR_SHOPPING_CART_POPUP_CONTINUE_BTN)
  }

  async verifyButton1IsLoaded () {
    await this.waitForDisplayed(ADDED_TO_YOUR_SHOPPING_CART_POPUP_CHECKOUT_BTN)
    await this.waitForDisplayed(ADDED_TO_YOUR_SHOPPING_CART_POPUP_CONTINUE_BTN)
  }

  async verifyButton2IsLoaded () {
    await this.waitForDisplayed(ADDED_TO_YOUR_SHOPPING_CART_POPUP_CHECKOUT_BTN)
    await this.waitForDisplayed(ADDED_TO_YOUR_SHOPPING_CART_POPUP_CONTINUE_BTN)
  }

  async verifyErrorIsLoaded () {
    await this.waitForDisplayed(ADDED_TO_YOUR_SHOPPING_CART_POPUP_TITLE_ERROR)
  }

  async getAddedToYourShoppingCartPopupTitle () {
    return this.getText(ADDED_TO_YOUR_SHOPPING_CART_POPUP_TITLE)
  }

  async enterQuantity (keys: any) {
    await this.click(ADDED_TO_YOUR_SHOPPING_CART_POPUP_QTY_BOX)
    await this.sendKeys(ADDED_TO_YOUR_SHOPPING_CART_POPUP_QTY_BOX, keys)
  }

  async clickUpdateLink () {
    await this.waitForDisplayed(ADDED_TO_YOUR_SHOPPING_CART_POPUP_UPDATE_LINK, 7)
    await this.click(ADDED_TO_YOUR_SHOPPING_CART_POPUP_UPDATE_LINK)
  }

  async clickCheckoutButton () {
    await this.waitForDisplayed(ADDED_TO_YOUR_SHOPPING_CART_POPUP_CHECKOUT_BTN, 7)
    await this.click(ADDED_TO_YOUR_SHOPPING_CART_POPUP_CHECKOUT_BTN)
  }

  async clickBeginCheckoutButtonHK () {
    await this.waitForDisplayed(ADDED_TO_YOUR_SHOPPING_CART_POPUP_BEGIN_CHECKOUT_BTN_HK)
    await this.click(ADDED_TO_YOUR_SHOPPING_CART_POPUP_BEGIN_CHECKOUT_BTN_HK)
  }

  async clickContinueButton () {
    await this.waitForDisplayed(ADDED_TO_YOUR_SHOPPING_CART_POPUP_CONTINUE_BTN)
    await this.click(ADDED_TO_YOUR_SHOPPING_CART_POPUP_CONTINUE_BTN)
  }

  async verifyMonetatePopUpIsLoaded () {
    await this.waitForDisplayed(MONETATE_ADD_TO_YOUR_SHOPPING_CART_POP_UP)
    await this.waitForDisplayed(MONETATE_ADD_TO_YOUR_SHOPPING_CART_POP_UP_TITLE)
  }

  async clickBeginCheckoutButton () {
    await this.waitForDisplayed(MONETATE_BEGIN_CHECKOUT_BTN)
    await this.click(MONETATE_BEGIN_CHECKOUT_BTN)
  }

  async isBeginCheckoutButtonDisplayed () {
    return this.locatorFound(MONETATE_BEGIN_CHECKOUT_BTN)
  }

  async clickContinueShoppingKit () {
    await this.waitForDisplayed(CONTINUE_SHOPPING_KIT_BTN, 3)
    await this.click(CONTINUE_SHOPPING_KIT_BTN)
  }

  async isContinueShoppingButtonDisplayed () {
    return this.locatorFound(CONTINUE_SHOPPING_KIT_BTN)
  }

  async verifyMonetatePopUpIsLoadedOtherMarkets () {
    await this.waitForDisplayed(MONETATE_ADD_TO_YOUR_SHOPPING_CART_POP_UP)
    await this.waitForDisplayed(MONETATE_ADD_TO_YOUR_SHOPPING_CART_POP_UP_TITLE)
  }

  async getPopUpQtyText () {
    return this.getAttributesValue(ADDED_TO_YOUR_SHOPPING_CART_POPUP_QTY_BOX, 'value')
  }

  async isPurposefulHydrationBundleDisplayed () {
    return this.locatorFound(MONETATE_POP_UP_PURPOSEFUL_HYDRATION_BUNDLE)
  }

  async isKitDisplayed () {
    return this.locatorFound(MONETATE_KIT)
  }

  async getKitName () {
    return this.getText(MONETATE_KIT)
  }

  async isItemDisplayed () {
    return this.locatorFound(MONETATE_ITEM)
  }

  async getItemName () {
    return this.getText(MONETATE_ITEM)
  }
}
