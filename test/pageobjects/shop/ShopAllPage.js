// @flow
import BasePage from '../BasePage'

const SHOP_ALL_BODY_TITLE = '//*[@id="content"]/descendant::h1[@class="hero__title"]'
const COACH_STORE_SHOP_BUTTON = '//*[@class="row category-list"]/descendant::a[contains(@href,"/coachstore")][contains(text(), "Shop")]'
const SELECT_FUELINGS_SHOP_BUTTON = '//*[@class="row category-list"]/descendant::a[contains(@href,"/select")][contains(text(), "Shop")]'
const ESSENTIAL_FUELINGS_SHOP_BUTTON = '//*[@class="row category-list"]/descendant::a[contains(@href,"/essential")][contains(text(), "Shop")]'
const TOOLS_AND_ACCESSORIES_SHOP_BUTTON = '//*[@class="row category-list"]/descendant::a[contains(@href,"/tools")][contains(text(), "Shop")]'
const VIEW_ALL_BUTTON = '//*[@class="row category-list"]/descendant::a[contains(@href,"/search?q")][contains(text(), "View All")]'
const CLASSIC_FUELINGS_SHOP_BUTTON = '//*[@class="row category-list"]/descendant::a[contains(@href,"/classic")][contains(text(), "Shop")]'
const TOOLS_AND_ACCESSORIES_CHECKBOX = '//input[@id="subCategory_Tools&Accessories"]/..'
const KITS_CHECKBOX = '//*[@class="facet_block-label"][contains(text(), "Kits")]'
const HEALTH_PROFESSIONAL_MATERIALS_CHECKBOX = '//input[@id="subCategory_HealthProfessionalMaterials"]/..'
const MARKETING_MATERIALS_CHECKBOX = '//input[@id="subCategory_MarketingMaterials"]/..'
const SPANISH_MATERIALS_CHECKBOX = '//input[@id="subCategory_SpanishMaterials"]/..'
const ESSENTIAL_OPTIMAL_KIT_421_ADDTOCART_BTN = '//*[@formid="addToCartForm421EssentialOptimalKit_Product"]'
const OPTAVIA_GUIDE_BTN = '//*[@id="addToCartForm37883-optavia-guide"]/button'
const BUSINESS_RENEWAL_AND_CERTIFICATION_CHECKBOX = '//input[@id="subCategory_BusinessRenewal&Certification"]/..'
const OPTAVIA_CERTIFICATION_COURSE_ADDTOCART_BTN = '//form[@id="addToCartFormcertification-exam"]/button'
const OPTIMAL_KIT_FOR_5_AND_1_PLAN_US_FLAVORS_BTN = '//*[@formid="addToCartForm81805EssentialOptimalHalalKit_Product"]'
const ESSENTIAL_ROASTED_GARLIC_CREAMY_SMASHED_POTATOES = '//*[@id="addToCartForm81006-opt-garlic-potatoes"]/button'
const OPTIMAL_KIT_CUSTOMIZE_KIT_BTN = '//input[@value="EssentialOptimalKit"]/following-sibling::button[@id="customizeKitBtn"]'
const FIRST_KIT_CUSTOMIZE_BTN = '(//button[@id="customizeKitBtn"])[1]'
const FIRST_PRODUCT_ADD_TO_CART_BUTTON = '(//*[@class="add_to_cart_form"]/button)[1]'
const ESSENTIAL_OPTIMAL_KIT_5_1_BUTTON = '//*[@formid="addToCartFormEssentialOptimalKit_Product"]'
const ESSENTIAL_OPTIMAL_KIT_5_1_CUSTOMIZE_KIT_BUTTON = '(//*[@id="customizeKitBtn"])[1]'

export default class ShopAllPage extends BasePage {
  async verifyPageIsLoaded () {
    await this.waitForDisplayed(SHOP_ALL_BODY_TITLE)
  }

  async getPageTitle () {
    return this.getCurrentPageTitle()
  }

  async getPageUrl () {
    return this.getCurrentPageUrl()
  }

  async getShopPageBodyTitle () {
    return this.getText(SHOP_ALL_BODY_TITLE)
  }

  async clickCoachStoreShopBtn () {
    await this.click(COACH_STORE_SHOP_BUTTON)
  }

  async clickSelectFuelingsShopBtn () {
    await this.click(SELECT_FUELINGS_SHOP_BUTTON)
  }

  async clickEssentialFuelingsShopBtn () {
    await this.waitForDisplayed(ESSENTIAL_FUELINGS_SHOP_BUTTON)
    await this.click(ESSENTIAL_FUELINGS_SHOP_BUTTON)
  }

  async clickToolsAndAccessoriesShopBtn () {
    await this.click(TOOLS_AND_ACCESSORIES_SHOP_BUTTON)
  }

  async clickToolsAndAccessoriesCheckbox () {
    await this.click(TOOLS_AND_ACCESSORIES_CHECKBOX)
  }

  async clickKitsCheckbox () {
    await this.waitForDisplayed(KITS_CHECKBOX, 10)
    await this.click(KITS_CHECKBOX)
  }

  async clickEssentialKitAddToCartBtn () {
    await this.waitForDisplayed(ESSENTIAL_OPTIMAL_KIT_421_ADDTOCART_BTN, 5)
    await this.click(ESSENTIAL_OPTIMAL_KIT_421_ADDTOCART_BTN)
  }
  async clickBusinessRenewalCheckbox () {
    await this.click(BUSINESS_RENEWAL_AND_CERTIFICATION_CHECKBOX)
  }

  async clickOptaviaCertCourseAddtoCartBtn () {
    await this.click(OPTAVIA_CERTIFICATION_COURSE_ADDTOCART_BTN)
  }

  async clickViewAllBtn () {
    await this.click(VIEW_ALL_BUTTON)
  }

  async clickClassicFuelingsBtn () {
    await this.click(CLASSIC_FUELINGS_SHOP_BUTTON)
  }

  async clickOptaviaGuideBtn () {
    await this.waitForDisplayed(OPTAVIA_GUIDE_BTN, 3)
    await this.click(OPTAVIA_GUIDE_BTN)
  }

  async clickOptaviakit5and1USFlavors () {
    await this.click(OPTIMAL_KIT_FOR_5_AND_1_PLAN_US_FLAVORS_BTN)
  }

  async getProductPriceByName (productName) {
    const product =
      `//*[contains(text(),'${productName}')]//../div/*[@class="price"]`

    return this.getText(product)
  }

  async clickAddToCartByName (productName) {
    const product =
      `//*[contains(text(),'${productName}')]//..//..//..//../..//button`

    await this.click(product)
  }

  async isBusinessRenewalAndCertificationDisplayed () {
    return this.locatorFound(BUSINESS_RENEWAL_AND_CERTIFICATION_CHECKBOX)
  }

  async isHealthProfessionalMaterialsDisplayed () {
    return this.locatorFound(HEALTH_PROFESSIONAL_MATERIALS_CHECKBOX)
  }

  async isMarketingMaterialsDisplayed () {
    return this.locatorFound(MARKETING_MATERIALS_CHECKBOX)
  }

  async isSpanishMaterialsDisplayed () {
    return this.locatorFound(SPANISH_MATERIALS_CHECKBOX)
  }

  async clickOnCreamySmashedPotatoes () {
    await this.waitForDisplayed(ESSENTIAL_ROASTED_GARLIC_CREAMY_SMASHED_POTATOES, 5)
    await this.click(ESSENTIAL_ROASTED_GARLIC_CREAMY_SMASHED_POTATOES)
  }

  async clickOnOptimalCustomizeKitBtn () {
    await this.waitForDisplayed(OPTIMAL_KIT_CUSTOMIZE_KIT_BTN, 5)
    await this.click(OPTIMAL_KIT_CUSTOMIZE_KIT_BTN)
  }

  async clickOnFirstKitCustomizeKitBtn () {
    await this.waitForDisplayed(FIRST_KIT_CUSTOMIZE_BTN, 5)
    await this.click(FIRST_KIT_CUSTOMIZE_BTN)
  }

  async addFirstProductToCart () {
    await this.waitForDisplayed(FIRST_PRODUCT_ADD_TO_CART_BUTTON, 5)
    await this.click(FIRST_PRODUCT_ADD_TO_CART_BUTTON)
  }

  async clickOptaviaKit5and1 () {
    await this.click(ESSENTIAL_OPTIMAL_KIT_5_1_BUTTON)
  }

  async clickOptaviaKit5and1CustomizeKitButton () {
    await this.click(ESSENTIAL_OPTIMAL_KIT_5_1_CUSTOMIZE_KIT_BUTTON)
  }

  async isClassicFuelingsBtnDisplayed () {
    return this.locatorFound(CLASSIC_FUELINGS_SHOP_BUTTON)
  }
}
