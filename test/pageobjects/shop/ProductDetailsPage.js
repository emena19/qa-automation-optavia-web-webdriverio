// @flow
import BasePage from '../BasePage'
const PRODUCT_NAME = '//*[@class="product-shop"]/h1'
const PRODUCT_UNIT_DROP_DOWN_ARRAYLIST = '//select[@id="variant"]//option'
const PRODUCT_DROP_DOWN = '#variant'
const ADD_TO_CART_BTN = '#addToCartButton'
const ITEM_PRICE = '.classname = product-price'
const ITEM_SKU_NUMBER = '//div[@class="product-shop"]/p'
const ITEM_SERVINGS = '//span[@class="serving"]'
const CURRENTLY_UNAVAILABLE_BTN = '//button[@class="button button-default addToCartButton outOfStock "]'
const ITEM_QUANTITY = '#qtyInput'
const OUT_OF_STOCK_TEXT = '//form[@id="medifastAddToCartForm"]/p'
const CUSTOMIZE_KIT_BUTTON = '//a[@class="button button-alternate customize-kit"]'
const BODY_SECTION_ERROR = '//div[@id="container-box"]/small'
const NUTRITION_FACTS_TAB = '//div[@role="tab"]/span[text()="Nutrition Facts"]'
const NUTRITION_FACTS_TEXT = '#tab-2'
const NUTRITION_FACTS_CLICK_HERE_LINK = '//div[@id="tab-2"]/a'
const ITEM_NO_UOM_PRICE = '//*[@class="product-price"][2]'
const KIT_ADD_TO_TO_CART_BUTTON = '//*[@id="AddToCart-AddToCartAction"]/button'

export default class ProductDetailsPage extends BasePage {
  async verifyPageIsLoaded () {
    await this.waitForDisplayed(PRODUCT_NAME)
  }

  async verifyErrorIsDisplayed () {
    await this.waitForDisplayed(BODY_SECTION_ERROR)
  }

  async getPageTitle () {
    return this.getCurrentPageTitle()
  }

  async getPageUrl () {
    return this.getCurrentPageUrl()
  }

  async getProductNameText () {
    return this.getText(PRODUCT_NAME)
  }

  async getUnitsArrayList () {
    return this.getElementsTextArrayList(PRODUCT_UNIT_DROP_DOWN_ARRAYLIST)
  }

  async getProductDropDownText () {
    return this.getDropdownOptions(PRODUCT_DROP_DOWN)
  }

  async clickAddToCartBtn () {
    await this.moveToLocatorPosition(ADD_TO_CART_BTN)
    await this.click(ADD_TO_CART_BTN)
  }

  async clickCustomizeKitBtn () {
    await this.click(CUSTOMIZE_KIT_BUTTON)
  }

  async verifyAddToCartBtnIsDisplayed () {
    await this.waitForDisplayed(ADD_TO_CART_BTN)
  }

  async getItemPrice () {
    return this.getText(ITEM_PRICE)
  }

  async getItemSKUNumber () {
    return this.getText(ITEM_SKU_NUMBER)
  }

  async getItemServingsNo () {
    return this.getText(ITEM_SERVINGS)
  }

  async getCurrentlyUnavailableText () {
    return this.getText(CURRENTLY_UNAVAILABLE_BTN)
  }

  async getCurrentlyUnavailableAttributeValue () {
    return this.getAttributesValue(CURRENTLY_UNAVAILABLE_BTN, 'disabled')
  }

  async isItemQtyDisplayed () {
    return this.locatorFound(ITEM_QUANTITY)
  }

  async getProductOutOfStockText () {
    return this.getText(OUT_OF_STOCK_TEXT)
  }

  async getPageBodySectionError () {
    return this.getText(BODY_SECTION_ERROR)
  }

  async clickOnNutritionFactsTab () {
    await this.moveToLocatorPosition(NUTRITION_FACTS_TAB)
    await this.click(NUTRITION_FACTS_TAB)
  }

  async getNutritionFactsText () {
    return this.getText(NUTRITION_FACTS_TEXT)
  }

  async clickOnNutritionFactsLink () {
    await this.moveToLocatorPosition(NUTRITION_FACTS_CLICK_HERE_LINK)
    await this.click(NUTRITION_FACTS_CLICK_HERE_LINK)
  }

  async updateProductQty (qty: number) {
    await this.waitForDisplayed(ITEM_QUANTITY)
    await this.sendKeys(ITEM_QUANTITY, qty)
  }

  async selectProductFromDropDown (product: string) {
    await this.moveToLocatorPosition(PRODUCT_DROP_DOWN)
    await this.selectDropDownByText(PRODUCT_DROP_DOWN, product)
  }

  async getPriceFromMonetateItem () {
    return this.getText(ITEM_NO_UOM_PRICE)
  }

  async isMonetatePriceItemPresent () {
    return this.locatorFound(ITEM_NO_UOM_PRICE)
  }

  async clickKitAddToCartButton () {
    await this.click(KIT_ADD_TO_TO_CART_BUTTON)
  }
}
