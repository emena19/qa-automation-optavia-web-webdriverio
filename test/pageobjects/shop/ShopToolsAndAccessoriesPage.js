// @flow
import BasePage from '../BasePage'

const SHOP_ALL_BODY_TITLE = '//*[@id="content"]/div[2]/div[1]/div'
const SHOP_BODY_TITLE_SHOP_ALL_VIEW_ALL = '//*[@id="content"]/div[1]/div/div[@class="col-md-12 col-lg-9 main product-list-main"]'
const ADD_CART_OPTAVIA_GUIDE_BTN = '//*[@id="addToCartForm37883-optavia-guide"]/button'
const ADD_CART_HABITS_FOR_HEALTH = '//*[@id="addToCartFormHabitsHealthSystemKit_Product"]/button'

export default class ShopToolsAndAccessoriesPage extends BasePage {
  async verifyPageIsLoaded () {
    await this.waitForDisplayed(SHOP_ALL_BODY_TITLE)
  }

  async verifyShopToolsPageIsLoaded () {
    await this.waitForDisplayed(SHOP_BODY_TITLE_SHOP_ALL_VIEW_ALL)
  }

  async getPageTitle () {
    return this.getCurrentPageTitle()
  }

  async getPageUrl () {
    return this.getCurrentPageUrl()
  }

  async clickProductsInToolsAndAccessories (productName) {
    const product =
      `//div[contains(text(),'${productName}')]/../../../..//div/form[contains(@id,"addToCartForm")]/button`
    return this.click(product)
  }

  async clickAddToCartBtnForOptaviaGuide () {
    await this.click(ADD_CART_OPTAVIA_GUIDE_BTN)
  }

  async clickOnHabitsForHealth () {
    await this.waitForDisplayed(ADD_CART_HABITS_FOR_HEALTH, 2)
    await this.click(ADD_CART_HABITS_FOR_HEALTH)
  }
}
