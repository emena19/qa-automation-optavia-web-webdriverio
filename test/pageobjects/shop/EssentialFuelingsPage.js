// @flow
import { By } from 'selenium-webdriver'
import BasePage from '../BasePage'

const ESSENTIAL_FUELINGS_BODY_TITLE = '//*[@id="content"]/div[2]/div[1]/div'
const HEARTY_CHOICE_CHECKBOX = '//label[@for="subCategory_HeartyChoices"]' // '//label[@for="subCategory_HeartyChoices"]/parent::form/input[@id="subCategory_HeartyChoices"]')
const SOUPS_CHECKBOX = '//label[@for="subCategory_Soups"]'
const BARS_CHECKBOX = '//label[@for="subCategory_Bars"]'
const ROASTED_GARLIC_CREAMY_SMASHED_POTATOES_ADD_TO_CART_BUTTON = '//*[@id="addToCartForm81006-opt-garlic-potatoes"]/button'
const WILD_RICE_AND_CHICKEN_FLAVORED_SOUP_LINK = '//*[@id="content"]/div[3]/div[2]/div[2]/div[2]/div[1]/div/a'
const DRIZZLED_CHOCLCATE_FUDGE_CRISP_BAR = '//*[@id="addToCartForm81016-opt-choc-crisp-bar"]/button'
const ESSENTIAL_CHOCOLATE_MINT_COOKIE_CRISP_BAR_ADD_TO_CART_BTN = '//form[@id="addToCartFormopt-choc-mint-crisp-bar"]/button'
const RAISING_OAT_CINNAMON_CRISP_BAR_ADD_TO_CART_BTN = By.css('[id*="opt-raisin-cinn-crisp-bar"] button')
const HOMESTYLE_CHICKEN_FLAVORED_AND_VEGETABLES_NOODLE_SOUP = '//*[@id="content"]/div[3]/div[2]/div/div[2]/div[1]/div/a/div[1]/img'
const FIRST_PRODUCT__ADD_TO_CART_BUTTON = '(//*[@class="add_to_cart_form"]/button)[1]'
const FIRST_PRODUCT_IMAGE = '//*[@id="content"]//img'
const CRANBERRY_HONEY_NUT_BAR_IMAGE = '//*[@class="thumb"]/img[@title="Essential Cranberry Honey Nut Granola Bar"]'
const ZESTY_LEMON_CRISP_BAR_IMAGE = '//*[@class="thumb"]/img[@title="Essential Zesty Lemon Crisp Bar"]'
const CREAMY_DOUBLE_PEANUT_BUTTER_BAR_IMAGE = '//*[@class="thumb"]/img[@title="Essential Creamy Double Peanut Butter Crisp Bar"]'

export default class EssentialFuelingsPage extends BasePage {
  async verifyPageIsLoaded () {
    await this.waitForDisplayed(ESSENTIAL_FUELINGS_BODY_TITLE)
  }

  async getPageTitle () {
    return this.getCurrentPageTitle()
  }

  async getPageUrl () {
    return this.getCurrentPageUrl()
  }

  async getShopPageBodyTitle () {
    return this.getText(ESSENTIAL_FUELINGS_BODY_TITLE)
  }

  async checkHeartyChoiceCheckbox () {
    await this.click(HEARTY_CHOICE_CHECKBOX)
  }

  async checkSoupsCheckbox () {
    await this.click(SOUPS_CHECKBOX)
  }

  async clickRoastedGarlicCreamySmashedPotatoesAddToCartBtn () {
    await this.moveToLocatorPosition(ROASTED_GARLIC_CREAMY_SMASHED_POTATOES_ADD_TO_CART_BUTTON)
    await this.click(ROASTED_GARLIC_CREAMY_SMASHED_POTATOES_ADD_TO_CART_BUTTON)
  }

  async clickEssentialChocoMintBarAddToCartBtn () {
    await this.moveToLocatorPosition(ESSENTIAL_CHOCOLATE_MINT_COOKIE_CRISP_BAR_ADD_TO_CART_BTN)
    await this.click(ESSENTIAL_CHOCOLATE_MINT_COOKIE_CRISP_BAR_ADD_TO_CART_BTN)
  }

  async clickWildRiceAndChickenFlavoredSoupLink () {
    await this.click(WILD_RICE_AND_CHICKEN_FLAVORED_SOUP_LINK)
  }

  async clickChocolateFudgeCrispBarLink () {
    await this.moveToLocatorPosition(DRIZZLED_CHOCLCATE_FUDGE_CRISP_BAR)
    await this.click(DRIZZLED_CHOCLCATE_FUDGE_CRISP_BAR)
  }

  async clickProductsInPage (productName: string) {
    const product =
      `//div[@class="details"][contains(text(), '${productName}')]`
    await this.click(product)
  }

  async clickEssentialRaisinOatCinnamonCrispBar () {
    await this.waitForDisplayed(RAISING_OAT_CINNAMON_CRISP_BAR_ADD_TO_CART_BTN)
    await this.click(RAISING_OAT_CINNAMON_CRISP_BAR_ADD_TO_CART_BTN)
  }

  async isQtyStepperDisplayed (product: string) {
    let unavailableProductXpath = `//*[@class="details" and contains(text(),"${product}")]/ancestor::a/following-sibling::div//form/span`
    return this.locatorFound(unavailableProductXpath)
  }

  async clickHomeStyleChickenFlavoredNoodleSoupImage () {
    await this.waitForDisplayed(HOMESTYLE_CHICKEN_FLAVORED_AND_VEGETABLES_NOODLE_SOUP, 5)
    await this.click(HOMESTYLE_CHICKEN_FLAVORED_AND_VEGETABLES_NOODLE_SOUP)
  }

  async clickProductsInResultsPage (productName, indexNO) {
    const product =
      `(//div[contains(text(),'${productName}')]/../../../..//div/form[contains(@id,"addToCartForm")]/button)['${indexNO}']`
    await this.click(product)
  }

  async checkBarsCheckbox () {
    await this.click(BARS_CHECKBOX)
  }

  async clickProductsNameInPage (productName: string) {
    const product =
      `//*[@class="details" and contains(text(),"${productName}")]/ancestor::a`
    await this.moveToLocatorPosition(product)
    await this.click(product)
  }

  async addFirstProductToCart () {
    await this.waitForDisplayed(FIRST_PRODUCT__ADD_TO_CART_BUTTON)
    await this.click(FIRST_PRODUCT__ADD_TO_CART_BUTTON)
  }

  async clickOnFirstProductImage () {
    await this.click(FIRST_PRODUCT_IMAGE)
  }

  async clickCranberryHoneyNutGranolaImage () {
    await this.click(CRANBERRY_HONEY_NUT_BAR_IMAGE)
  }
  async clickZestyLemonCrispImage () {
    await this.click(ZESTY_LEMON_CRISP_BAR_IMAGE)
  }
  async clickCreamyDoublePeanutBarImage () {
    await this.click(CREAMY_DOUBLE_PEANUT_BUTTER_BAR_IMAGE)
  }
}
