// @flow
import BasePage from '../BasePage'

const SELECT_FUELINGS_BODY_TITLE = '//*[@id="content"]/div[2]/div[1]/div'
const HEARTY_CHOICE_CHECKBOX = '//label[@for="subCategory_HeartyChoices"]'
const BUTTERMILK_CHEDDAR_HERB_BISCUIT_ADD_TO_CART_BUTTON = '//*[@id="addToCartFormopt-cheddar-biscuit"]/button'
const ADD_CART_BUTTON = '//button[@type="submit" and contains(@class,"addToCartButton")]'
const PESTO_MAC_CHEESE_ADD_TO_CART_BUTTON = '//*[@id="addToCartFormopt-pesto-mac-cheese"]/button'
const SELECT_DARK_CHOC_COCONUT_CURRY_BAR = '//*[@id="addToCartFormopt-choc-curry-bar"]/button'
const SELECT_HONEY_SWEET_POTATOES = '//*[@id="addToCartFormopt-sweet-potatoes"]/button'
const MEAL_REPLACEMENT_CHECKBOX = '//label[@for="subCategory_MealReplacement"]'
const DARK_CHOCOLATE_CHERRY_SHAKE_ADD_TO_CART_BUTTON = '//*[@id="addToCartFormopt-choc-cherry-shake"]/button'
const CHOCOLATE_CHERRY_GANACHE_BAR_IMG = '//*[@class="thumb"]/img[@title="Select Chocolate Cherry Ganache Bar with Dried Cherries, Pomegranate, and Chia Seeds"]'

export default class SelectFuelingsPage extends BasePage {
  async verifyPageIsLoaded () {
    await this.waitForDisplayed(SELECT_FUELINGS_BODY_TITLE)
  }

  async getPageTitle () {
    return this.getCurrentPageTitle()
  }

  async getPageUrl () {
    return this.getCurrentPageUrl()
  }

  async getShopPageBodyTitle () {
    return this.getText(SELECT_FUELINGS_BODY_TITLE)
  }

  async checkHeartyChoiceCheckbox () {
    await this.click(HEARTY_CHOICE_CHECKBOX)
  }

  async clickButterMilkCheddarHerbBiscuitAddToCartBtn () {
    await this.click(BUTTERMILK_CHEDDAR_HERB_BISCUIT_ADD_TO_CART_BUTTON)
  }

  async clickSubCategoriesInEssentialsFuelings (itemName) {
    const subCategory =
      `//form[contains(@action,'${itemName}')]//label//input`

    return this.click(subCategory)
  }

  async checkAddToCartBtn () {
    await this.verifyPageIsLoaded(ADD_CART_BUTTON)
  }

  async clickProductsInEssentialsFuelings (productName) {
    const product =
      `//div[contains(text(),'${productName}')]`
    return this.click(product)
  }

  async clickAddToCartBtn () {
    await this.click(ADD_CART_BUTTON)
  }

  async clickPestoMacCheeseAddToCartButton () {
    await this.click(PESTO_MAC_CHEESE_ADD_TO_CART_BUTTON)
  }

  async clickOnSelectDarkChocCoconutCurryBar () {
    await this.click(SELECT_DARK_CHOC_COCONUT_CURRY_BAR)
  }

  async clickHoneySweetPotatoesAddToCartBtn () {
    await this.click(SELECT_HONEY_SWEET_POTATOES)
  }

  async clickOnMealReplacementCheckBox () {
    await this.waitForDisplayed(MEAL_REPLACEMENT_CHECKBOX, 10)
    await this.click(MEAL_REPLACEMENT_CHECKBOX)
  }

  async addToCartDarkChocolateCherryShake () {
    await this.waitForDisplayed(DARK_CHOCOLATE_CHERRY_SHAKE_ADD_TO_CART_BUTTON, 10)
    await this.click(DARK_CHOCOLATE_CHERRY_SHAKE_ADD_TO_CART_BUTTON)
  }

  async clickChocolateCherryGanacheImage () {
    await this.waitForDisplayed(CHOCOLATE_CHERRY_GANACHE_BAR_IMG)
    await this.click(CHOCOLATE_CHERRY_GANACHE_BAR_IMG)
  }
}
