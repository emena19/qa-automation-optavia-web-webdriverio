// @flow
import { By } from 'selenium-webdriver'
import BasePage from '../BasePage'

const SHOP_BODY_TITLE = '//*[@id="content"]/div[2]/div/div/div[1]/h1'
const SHOP_BODY_TITLE_SHOP_ALL_VIEW_ALL = '//*[@id="content"]/div[1]/div/div[@class="col-md-12 col-lg-9 main product-list-main"]'
const ESSENTIAL_CARAMEL_DELIGHT_CRISP_BAR_ADD_BUTTON = '//*[@id="addToCartFormopt-car-crisp-bar"]/button'
const ESSENTIAL_CARAMEL_DELIGHT_CRISP_BAR_ADD_QTY_BOX = '//*[@id="qtyopt-car-crisp-bar"]'
const ESSENTIAL_CAMPFIRE_SMORE_CRISP_BAR_ADD_BUTTON = '//*[@id="addToCartFormopt-smore-crisp-bar"]/button'
const ESSENTIAL_CAMPFIRE_SMORE_CRISP_BAR_ADD_QTY_BOX = '//*[@id="qtyopt-smore-crisp-bar"]'
const DARK_CHOCOLATE_DREAM = '//*[@id="addToCartFormdark-choc-optimal-bar"]/button'
const ESSENTIAL_OPTIMAL_KIT_FOR_5_AND_1_PLAN = '//*[contains(@formid,"EssentialOptimalHalalKit_Product")]'
const ESSENTIAL_ON_THE_GO_KIT_FOR_5_AND_1_PLAN = '//*[contains(@formid,"addToCartFormEssentialOnTheGoKit_Product")]'
const ESSENTIAL_CRANBERRY_HONEY_NUT_GRANOLA_BAR_ADD_BTN = '//*[contains(@id,"cran-nut-bar")]/button'
const KITS_CHECKBOX = '//*[@for="subCategory_Kits"]'
const OPTIMAL_HEALTH_CHECKBOX = '#subCategory_OptimalHealth'
const SHOP_ALL_OPTIMAL_WEIGHT_LINK = '//a[@title="Shop all Optimal Weight 5&1 Plan Fuelings"]'
const KOSHER_OUD_CHECKBOX = '#SpecialDiets_Kosher(OUD)'
const KOSHER_CHECKBOX = '#SpecialDiets_Kosher(OU)'
const HALAL_CHECKBOX = '#SpecialDiets_Halal'
const TOTAL_PRODUCT_GRID = '//div[@class="productGridItem "]'
const TOTAL_PRODUCT_LIST = '//div[@class="product-list-item "]'
const PRODUCT_PAGINATION_SELECTOR = '//*[@class="pagination-bar top"]//div[@class="pages"]/ol/li'
const CLEAR_ALL_LINK = '#clearFacetSelections'
const SHOP_PAGE_PAGINATION_RIGHT_ARROW = '//div[@class="pagination-bar top"]//i[@class="icon icon-arrow-alt-right"]//parent::a'
const SHOP_BODY_TITLE_HK = '//*[@id="sort_form1"]/ul/li/label'
const ESSENTIAL_OPTIMAL_KIT_FOR_5_AND_1_PLAN_US = '//*[@formid="addToCartFormEssentialOptimalKit_Product"]'
const PRODUCTS_DESCRIPTION_LIST = '//div[contains(@class,"productGridItem")]//div[@class="details"]'
const ESSENTIAL_CREAMY_CHOCOLATE_SHAKE = '//*[@id="addToCartForm81004-opt-choc-shake"]/button'
const ESSENTIAL_CHOCOLATE_MINT_CRISP_BAR = '//*[@id="addToCartFormopt-choc-mint-crisp-bar"]/button'
const OPTIMAL_HEALTH_KIT_3AND_3 = '//*[contains(@formid,"addToCartFormOptimal3and3Kit_Product")]'
const SHOP_PAGE_LEFT_FILTER_OPTIONS_LIST = '.facet-head accordion-header gor-active'
const SHOPPING_CART_SPINNER = '//*[@id="add-to-cart-spinner"]'
const ESSENTIAL_OPTIMAL_KIT_FOR_5_AND_1_PLAN_ASIAN = '//*[contains(@formid,"EssentialOptimalKitAsiaSpecials_Product")]'
const ESSENTIAL_CREAMY_DOUBLE_PEANUT_CRISP_BAR = '//*[@id="addToCartFormopt-peanut-crisp-bar"]/button'
const MEAL_REPLACEMENT_TITLE = '(//h1[contains(text(),"Meal Replacement")])[1]'
const KOSHER_OUD_NUMBER = '//input[@id="SpecialDiets_Kosher(OUD)"]//following-sibling::label/span'
const KOSHER_OU_NUMBER = '//input[@id="SpecialDiets_Kosher(OU)"]//following-sibling::label/span'
const SELECT_FUELINGS_TAB = '//a[@title="OPTAVIA Select FuelingsTM"]'

export default class ShopPage extends BasePage {
  async verifyPageIsLoaded () {
    await this.waitForDisplayed(SHOP_BODY_TITLE_SHOP_ALL_VIEW_ALL)
  }

  async verifyShopPageIsLoaded () {
    await this.waitForDisplayed(SHOP_BODY_TITLE)
  }

  async getPageTitle () {
    return this.getCurrentPageTitle()
  }

  async getPageUrl () {
    return this.getCurrentPageUrl()
  }

  async getShopPageBodyTitle () {
    return this.getText(SHOP_BODY_TITLE_SHOP_ALL_VIEW_ALL)
  }

  async getShopPageMainTitle () {
    return this.getText(SHOP_BODY_TITLE)
  }

  async sendQtyForEssentialCampfireSmoreCrispBar (qty: string) {
    await this.sendKeys(ESSENTIAL_CAMPFIRE_SMORE_CRISP_BAR_ADD_QTY_BOX, qty)
  }

  async clickAddToCartBtnForEssentialCampfireSmoreCrispBar () {
    await this.click(ESSENTIAL_CAMPFIRE_SMORE_CRISP_BAR_ADD_BUTTON)
  }

  async sendQtyForEssentialCaramelDelightCrispCaramelBar (qty: string) {
    await this.sendKeys(ESSENTIAL_CARAMEL_DELIGHT_CRISP_BAR_ADD_QTY_BOX, qty)
  }

  async clickAddToCartBtnForEssentialCaramelDelightCrispCaramelBar () {
    await this.click(ESSENTIAL_CARAMEL_DELIGHT_CRISP_BAR_ADD_BUTTON)
  }

  async clickProductsInShopPage (productName, indexNO) {
    const product =
      `(//div[contains(text(),'${productName}')]/../../../..//div/form[contains(@id,"addToCartForm")]/button)['${indexNO}']`
    return this.click(product)
  }

  async clickKitsCheckbox () {
    await this.click(KITS_CHECKBOX)
  }

  async clickOptimalKitFor5And1Plan () {
    await this.moveToLocatorPositionAndClick(ESSENTIAL_OPTIMAL_KIT_FOR_5_AND_1_PLAN)
  }

  async clickOnTheGoKitFor5And1Plan () {
    await this.click(ESSENTIAL_ON_THE_GO_KIT_FOR_5_AND_1_PLAN)
  }

  async clickCranberryHoneyGranolaBar () {
    await this.click(ESSENTIAL_CRANBERRY_HONEY_NUT_GRANOLA_BAR_ADD_BTN)
  }

  async clickAddToCartBtnForDarkChocolateDreamBar () {
    await this.click(DARK_CHOCOLATE_DREAM)
  }

  async clickOptimalHealthCheckbox () {
    await this.moveToLocatorPositionAndClick(OPTIMAL_HEALTH_CHECKBOX)
  }

  async clickShopAllOptimalWeightLink () {
    await this.click(SHOP_ALL_OPTIMAL_WEIGHT_LINK)
  }

  async clickKosherOUDCheckbox () {
    await this.moveToLocatorPositionAndClick(KOSHER_OUD_CHECKBOX)
  }

  async clickHalalCheckbox () {
    await this.moveToLocatorPositionAndClick(HALAL_CHECKBOX)
  }

  async getPaginationLength (numberOfProducts) {
    if (numberOfProducts >= 30 && (await this.locatorFound(PRODUCT_PAGINATION_SELECTOR))) {
      let pagination = await this.getElementsArray(PRODUCT_PAGINATION_SELECTOR)
      return pagination.length
    }
  }

  async getProductsDisplayedPerPage () {
    let productsPerPage = []
    let products = await this.getCountOfArrayList(TOTAL_PRODUCT_GRID)
    products = products + await this.getCountOfArrayList(TOTAL_PRODUCT_LIST)
    productsPerPage.push(products)
    let pagination = await this.getPaginationLength(products)
    if (pagination > 0) {
      for (let i = 0; i < pagination - 2; i++) {
        if (await this.isElementEnabled(SHOP_PAGE_PAGINATION_RIGHT_ARROW)) {
          await this.click(SHOP_PAGE_PAGINATION_RIGHT_ARROW)
          products = await this.getCountOfArrayList(TOTAL_PRODUCT_GRID)
          products = products + await this.getCountOfArrayList(TOTAL_PRODUCT_LIST)
          productsPerPage.push(products)
        }
      }
    }
    return productsPerPage.reduce((a, b) => a + b, 0)
  }

  async clickClearAllLink () {
    await this.waitForDisplayed(CLEAR_ALL_LINK, 3)
    await this.click(CLEAR_ALL_LINK)
  }

  async clickKosherCheckbox () {
    await this.moveToLocatorPositionAndClick(KOSHER_CHECKBOX)
  }

  async verifyShopPageIsLoadedHK () {
    await this.waitForDisplayed(SHOP_BODY_TITLE_HK, 3)
  }

  async getShopPageMainTitleHK () {
    return this.getText(SHOP_BODY_TITLE_HK)
  }

  async clickOnTheGoKitFor5And1PlanUS () {
    await this.click(ESSENTIAL_OPTIMAL_KIT_FOR_5_AND_1_PLAN_US)
  }

  async clickOnTheGoKitFor5And1PlanAsianFlav () {
    await this.click(ESSENTIAL_OPTIMAL_KIT_FOR_5_AND_1_PLAN_ASIAN)
  }

  async getProductArrayTextList () {
    await this.waitForDisplayed(PRODUCTS_DESCRIPTION_LIST)
    return this.getElementsTextArrayList(PRODUCTS_DESCRIPTION_LIST)
  }

  async getProductPriceByName (productName) {
    const product =
      `//*[contains(text(),'${productName}')]//..//..//..//*[@class="priceContainer"]`

    return this.getText(product)
  }

  async clickAddToCartByName (productName) {
    const product =
      `//*[contains(text(),'${productName}')]//..//..//..//..//button[contains(@class,'button button-default view-details')]`

    await this.click(product)
  }

  async clickAddToCartByNameOnSelectFuelingTab (productName) {
    const product =
      `//*[contains(text(),'${productName}')]/../../../..//button[@id="addToCartButton"]`

    await this.click(product)
  }

  async clickAddToCartBtnForEssentialCreamyChocolateShake () {
    await this.waitForDisplayed(ESSENTIAL_CREAMY_CHOCOLATE_SHAKE, 5)
    await this.click(ESSENTIAL_CREAMY_CHOCOLATE_SHAKE)
  }

  async clickEssentialChocolateMintBar () {
    await this.waitForDisplayed(ESSENTIAL_CHOCOLATE_MINT_CRISP_BAR, 5)
    await this.click(ESSENTIAL_CHOCOLATE_MINT_CRISP_BAR)
  }

  async clickOptimalHealthKit3And3 () {
    await this.waitForDisplayed(OPTIMAL_HEALTH_KIT_3AND_3, 5)
    await this.click(OPTIMAL_HEALTH_KIT_3AND_3)
  }

  async getShopLeftFilterOptionsTextList () {
    return this.getElementsTextArrayList(SHOP_PAGE_LEFT_FILTER_OPTIONS_LIST)
  }

  async verifyThatSpinnerDisappeared (retries: number = 1) {
    await this.waitForSpinnerToDisappear(SHOPPING_CART_SPINNER, retries)
  }

  async isSpinnerPresent () {
    return this.locatorFound(SHOPPING_CART_SPINNER)
  }

  async clickEssentialCreamyDoublePeanutBar () {
    await this.waitForDisplayed(ESSENTIAL_CREAMY_DOUBLE_PEANUT_CRISP_BAR, 5)
    await this.click(ESSENTIAL_CREAMY_DOUBLE_PEANUT_CRISP_BAR)
  }

  async isMealReplacementPageDisplayed () {
    return this.locatorFound(MEAL_REPLACEMENT_TITLE)
  }

  async getKosherOUDNumber () {
    await this.waitForDisplayed(KOSHER_OUD_NUMBER)
    let number = await this.getText(KOSHER_OUD_NUMBER)
    return parseInt(number.match(/(?<=\()[^(\]]+(?=\))/g), 10)
  }

  async getKosherOUNumber () {
    await this.waitForDisplayed(KOSHER_OU_NUMBER)
    let number = await this.getText(KOSHER_OU_NUMBER)
    return parseInt(number.match(/(?<=\()[^(\]]+(?=\))/g), 10)
  }

  async clickSelectFuelingsTab () {
    await this.click(SELECT_FUELINGS_TAB)
  }

  async clickOnSubCategoryBox (subCategories) {
    const checkBox = By.id(
      `subCategory_${subCategories}`
    )
    await this.moveToLocatorPositionAndClick(checkBox)
  }
}
