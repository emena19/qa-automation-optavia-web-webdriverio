// @flow
import BasePage from '../BasePage'

const SHOP_COACH_STORE_PAGE = '//*[@id="page"]'
// const SPANISH_MATERIAL_ADD_TO_CART_BTN = '//*[@id="content"]/div[3]/div[2]/div[1]/div/div[1]/div[2]/div/ul/li[3]/div')
// const SPANISH_CHECK_BOX = '//*[@id="subCategory_SpanishMaterials"]')
const CREATING_HEALTH_HP_GUIDE_ADD_TO_CART_BTN = '//*[@id="addToCartFormcreating-health-hp-guide"]/button'

export default class ShopCoachStorePage extends BasePage {
  async verifyPageIsLoaded () {
    await this.waitForDisplayed(SHOP_COACH_STORE_PAGE)
  }

  async getPageTitle () {
    return this.getCurrentPageTitle()
  }

  async getPageUrl () {
    return this.getCurrentPageUrl()
  }

  // async clickSpanishMaterialAddToCartBtn () {
  //   await this.click(SPANISH_MATERIAL_ADD_TO_CART_BTN)
  // }

  async clickCreatingHealthHpGuideAddToCartBtn () {
    await this.click(CREATING_HEALTH_HP_GUIDE_ADD_TO_CART_BTN)
  }

  //
  // async sendWellnessCreditClientID (id: string) {
  //   await this.sendKeys(WELLNESS_CREDIT_CLIENT_ID, id)
  // }
  // async sendWellnessCreditQty (qty: string) {
  //   await this.sendKeys(WELLNESS_CREDIT_QTY, qty)
  // }
  //
  // async clickWellnessCreditAddToCartButton () {
  //   await this.click(WELLNESS_CREDIT_ADD_TO_CART_BUTTON)
  // }
  // async isAddToCartBtnDisabled () {
  //   return this.hasAttributePresent(ADD_TO_CART_BTN, 'disabled')
  // }
}
