// @flow
import BasePage from '../BasePage'

const ORDER_CONFIRMATION_BODY_TITLE = '//*[@id="content"]/descendant::h1[contains(text(),"Thank you for your Order!")]'
const CONTINUE_SHOPPING_BTN = '//a[contains(text(),"Continue Shopping")]'
const ORDER_NUMBER = '//*[@class="orderConfirmationMsg"]/parent::div/div[1]'
const ORDER_TOTAL = '//*[@id="orderTotals"]'
const ORDER_DATE = '//*[@class="orderConfirmationMsg"]/parent::div/div[2]'
const ORDER_STATUS = '//*[@class="orderConfirmationMsg"]/parent::div/p[2]'
const ORDER_CONFIRMATION_DELIVERY_CHARGE = `//*[@id="orderTotals"]/tbody/descendant::td[contains(text(),"Delivery Charge:")]/following-sibling::td`
const ORDER_CONFIRMATION_APPLIED_REWARDS = `//*[@id="orderTotals"]/tbody/descendant::td[contains(text(),"Applied Rewards:")]/following-sibling::td`
const ORDER_CONFIRMATION_DELIVERY_METHOD = '//*[@id="content"]/div/div[2]/div[2]/div[4]/ul/li[1]'
const NO_REWARDS_EARNED_ON_ORDER_TEXT = '//*[@id="orderTotals"]/tbody/descendant::td[contains(text(),"Rewards:")]/following-sibling::td'
const ORDER_REVIEW_TAX = '//*[@id="orderTotals"]/tbody/descendant::td[contains(text(),"Tax:")]/following-sibling::td'
const ORDER_SUMMARY_WELLNESS_CREDIT_TEXT = '//*[@id="orderTotals"]/tbody/descendant::td[contains(text(),"Wellness Credit:")]'
const ORDER_CONFIRMATION_PAYMENT_DETAILS = '//*[@id="content"]//*/div[2]/ul[@class="reset-list"]'
const PRODUCT_NAME_ARRAY_LIST = '//*[@class="order-items details-block"]/table//div[@class="product-name"]'
const COACH_AVATAR_INFO_TEXT = '//*[@class="optavia-coach-details"]/strong'
const ORDER_CONFIRMATION_DELIVERY_ADDRESS = '//ul[@class="reset-list shipped-adress-details"]'

export default class OrderConfirmationPage extends BasePage {
  async verifyPageIsLoaded () {
    await this.waitForDisplayed(ORDER_CONFIRMATION_BODY_TITLE, 6)
  }

  async getPageTitle () {
    return this.getCurrentPageTitle()
  }

  async getPageUrl () {
    return this.getCurrentPageUrl()
  }

  async getOrderConfirmationBodyText () {
    return this.getText(ORDER_CONFIRMATION_BODY_TITLE)
  }

  async clickContinueShopping () {
    await this.click(CONTINUE_SHOPPING_BTN)
  }

  async getOrderNumber () {
    return this.getText(ORDER_NUMBER)
  }

  async getOrderTotals () {
    return this.getText(ORDER_TOTAL)
  }

  async getOrderDate () {
    return this.getText(ORDER_DATE)
  }

  async getOrderStatus () {
    return this.getText(ORDER_STATUS)
  }

  async getPremierDeliveryChargeText () {
    return this.getText(ORDER_CONFIRMATION_DELIVERY_CHARGE)
  }

  async getAppliedRewardsText () {
    return this.getText(ORDER_CONFIRMATION_APPLIED_REWARDS)
  }

  async getDeliveryAddressText () {
    return this.getText(ORDER_CONFIRMATION_DELIVERY_ADDRESS)
  }

  async getDeliveryMethod () {
    return this.getText(ORDER_CONFIRMATION_DELIVERY_METHOD)
  }

  async isRewardsTextLoaded () {
    return this.locatorFound(NO_REWARDS_EARNED_ON_ORDER_TEXT, 1)
  }

  async getTaxAmount () {
    return this.getText(ORDER_REVIEW_TAX)
  }

  async isWellnessCreditTextDisplayed () {
    return this.locatorFound(ORDER_SUMMARY_WELLNESS_CREDIT_TEXT)
  }

  async getOrderComfirmationPaymentDetails () {
    return this.getText(ORDER_CONFIRMATION_PAYMENT_DETAILS)
  }

  async getOrderConfirmationProductArrayList () {
    return this.getElementsTextArrayList(PRODUCT_NAME_ARRAY_LIST)
  }

  async isCoachInfoFromAvatarDisplayed () {
    return this.locatorFound(COACH_AVATAR_INFO_TEXT)
  }
}
