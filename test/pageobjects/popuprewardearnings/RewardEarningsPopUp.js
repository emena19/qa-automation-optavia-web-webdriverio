// @flow
import BasePage from '../BasePage'

const REWARD_EARNINGS_POP_UP_BODY_TITLE = '//*[@id="estimated-rewards"]/div/h3'
// Purchase

// You Earned
const REWARD_EARNINGS_POP_UP_CONSUMABLES_YOU_EARNED = '//*[@id="estimated-rewards"]/descendant::td[contains(text(),"You Earned")]/following-sibling::td[1]'
const REWARD_EARNINGS_POP_UP_NON_CONSUMABLES_YOU_EARNED = '//*[@id="estimated-rewards"]/descendant::td[contains(text(),"You Earned")]/following-sibling::td[2]'
// const REWARD_EARNINGS_POP_UP_TOTAL_YOU_EARNED = '//*[@id="estimated-rewards"]/descendant::td[contains(text(),"You Earned")]/following-sibling::td[contains(@data-header,"Total")]')
const REWARD_EARNINGS_POP_UP_TOTAL_YOU_EARNED = '//*[@id="estimated-rewards"]/div/table/tbody/tr[4]/td[4]'

const REWARD_EARNINGS_POP_UP_CLOSE_BTN = '//*[@id="estimated-rewards"]/div/button/i'

export default class RewardEarningsPopUp extends BasePage {
  async verifyPageIsLoaded () {
    await this.waitForDisplayed(REWARD_EARNINGS_POP_UP_BODY_TITLE)
  }

  async getPageTitle () {
    return this.getCurrentPageTitle()
  }

  async getPageUrl () {
    return this.getCurrentPageUrl()
  }

  async getCheckoutBodyTitle () {
    return this.getText(REWARD_EARNINGS_POP_UP_BODY_TITLE)
  }

  async getConsumablesRewardsYouEarned () {
    return this.getText(REWARD_EARNINGS_POP_UP_CONSUMABLES_YOU_EARNED)
  }

  async getNonConsumablesRewardsYouEarned () {
    return this.getText(REWARD_EARNINGS_POP_UP_NON_CONSUMABLES_YOU_EARNED)
  }

  async getTotalRewardsYouEarned () {
    return this.getText(REWARD_EARNINGS_POP_UP_TOTAL_YOU_EARNED)
  }

  async clickCloseRewardEarningsPopUp () {
    await this.click(REWARD_EARNINGS_POP_UP_CLOSE_BTN)
  }
}
