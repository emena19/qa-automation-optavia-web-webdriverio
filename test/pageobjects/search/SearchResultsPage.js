// @flow
import BasePage from '../BasePage'

const SEARCH_RESULTS_BODY_TITLE = '//*[@id="content"]/descendant::h1'
const SEARCH_RESULTS_PRODUCT_NAME = '//*[@class="productGridItem "]/descendant::div[@class="details"]'
const SEARCH_RESULTS_PRODUCT_PRICE = '//*[@class="productGridItem "]/descendant::span[@class="price"]'
const SEARCH_RESULTS_PRODUCT_ADD_TO_CART_BTN = '//*[@class="productGridItem "]/descendant::button'
const SEARCH_RESULTS_PRODUCT_IMAGE = '//*[@class="thumb"]/img'
const SEARCH_RESULTS_PRODUCT_NAME_LIST = 'div[class="details"]'
const SERACH_RESULTS_GLOBAL_MESSAGE = '//*[@class="title_holder"]/h2'

export default class SearchResultsPage extends BasePage {
  async verifyPageIsLoaded () {
    await this.waitForDisplayed(SEARCH_RESULTS_BODY_TITLE)
  }

  async getPageTitle () {
    return this.getCurrentPageTitle()
  }

  async getPageUrl () {
    return this.getCurrentPageUrl()
  }

  async getSearchResultTitle () {
    return this.getText(SEARCH_RESULTS_BODY_TITLE)
  }

  async getProductName () {
    return this.getTextByIndex(SEARCH_RESULTS_PRODUCT_NAME, 0)
  }

  async getProductPrice () {
    return this.getTextByIndex(SEARCH_RESULTS_PRODUCT_PRICE, 0)
  }

  async clickProductAddToCartButton () {
    return this.clickByIndex(SEARCH_RESULTS_PRODUCT_ADD_TO_CART_BTN, 0)
  }

  async clickProductImage () {
    return this.clickByIndex(SEARCH_RESULTS_PRODUCT_IMAGE, 0)
  }

  async getProductNameList () {
    return this.getElementsTextArrayList(SEARCH_RESULTS_PRODUCT_NAME_LIST)
  }

  async getProductIsUnavailableText (product: string) {
    let unavailableProductXpath = `//*[@class="details" and contains(text(),"${product}")]/ancestor::a/following-sibling::div//form/button`
    await this.waitForDisplayed(unavailableProductXpath, 10)
    await this.moveToLocatorPosition(unavailableProductXpath, 5)
    return this.getText(unavailableProductXpath)
  }

  async getSearchResultErrorTitle () {
    return this.getText(SERACH_RESULTS_GLOBAL_MESSAGE)
  }

  async clickFirstProductName () {
    return this.click(SEARCH_RESULTS_PRODUCT_NAME_LIST)
  }
}
