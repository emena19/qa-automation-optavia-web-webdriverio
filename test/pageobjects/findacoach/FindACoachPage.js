// @flow
import { By } from 'selenium-webdriver'
import BasePage from '../BasePage'

const GLOBAL_MESSAGE = '//*[@id="globalMessages"]/ul/li/ul/li'
const LOGIN_LINK = By.linkText('Log in')
const FIND_A_COACH_BODY_TITLE = '//*[@class="panel-container row"]/h1'
const FIND_A_COACH_MARKET_DROPDOWN_LIST = '//select[@id="market"]/option'
// Find by Name
const SEARCH_FOR_MY_COACH_TTL = '//*[@class="right-panel panel"]/h2'
const SEARCH_COACH_BY_NAME_FORM = `#findCoachByName`
const SEARCH_COACH_FIRSTNAME_TXTBX = '//*[@id="findcoach.firstName"]'
const SEARCH_COACH_LASTNAME_TXTBX = '//*[@id="findcoach.lastName"]'
const SEARCH_COACH_CITY_TXTBX = '//*[@id="findcoach.city"]'
const SEARCH_FOR_COACH_MARKET_DEFAULT = '//*[@id="marketDiv"]/descendant::span[contains (text(),"")]'
const SEARCH_COACH_MARKET_DROPDOWN = '#market'
const SEARCH_COACH_FIND_MY_COACH_BTN = '//*[@id="findCoachByName"]/div[2]/button'
const SEARCH_COACH_FIRSTNAME_ERRMSG = '#findcoach.firstName-error'
const SEARCH_COACH_LASTNAME_ERRMSG = '#findcoach.lastName-error'
const SEARCH_COACH_GO_BACK_LINK = '//div[@class="HCLeadForm--container"]//a'
// Find by Lead
const SEARCH_FOR_COACH_BY_LEADS_TTL = '//*[@class="left-panel panel"]/h2'
const SEARCH_COACH_LEADS_POOL = '//*[@class="searchCoachForm--wrapper coachLeadPoolForm alignCenter"]'
const SEARCH_COACH_CONNECT_WITH_A_COACH_BUTTON = '//button[contains(text(),"Connect with a Coach")]'
const SEARCH_COACH_NEARBY_COACH_LABEL = '//span[contains(text(),"Connect me to a nearby Coach")]'
const SEARCH_COACH_CAPTCHA_REQUIRED_MESSAGE = '//div[@class="g-recaptcha"]/following-sibling::label'

export default class FindACoachPage extends BasePage {
  async verifyPageIsLoaded () {
    await this.waitForDisplayed(FIND_A_COACH_BODY_TITLE, 200)
    await this.waitForDisplayed(SEARCH_COACH_BY_NAME_FORM)
    await this.waitForDisplayed(SEARCH_COACH_LEADS_POOL, 10)
  }

  async getPageTitle () {
    return this.getCurrentPageTitle()
  }

  async getPageUrl () {
    return this.getCurrentPageUrl()
  }

  async getFindACoachTitleText () {
    return this.getText(FIND_A_COACH_BODY_TITLE)
  }

  async getSearchForCoachPanelTitleText () {
    return this.getText(SEARCH_FOR_MY_COACH_TTL)
  }

  async getFirstNameTextboxText (attrib: string) {
    return this.getAttributesValue(SEARCH_COACH_FIRSTNAME_TXTBX, attrib)
  }

  async getLastNameTextboxText (attrib: string) {
    return this.getAttributesValue(SEARCH_COACH_LASTNAME_TXTBX, attrib)
  }

  async getFindACoachMarketDropdownList () {
    await this.waitForDisplayed(FIND_A_COACH_MARKET_DROPDOWN_LIST, 3)
    return this.getElementsTextArrayList(FIND_A_COACH_MARKET_DROPDOWN_LIST)
  }

  async enterFirstAndLastNameToSearchCoach (firstName: string, lastName: string) {
    await this.sendKeys(SEARCH_COACH_FIRSTNAME_TXTBX, firstName)
    await this.sendKeys(SEARCH_COACH_LASTNAME_TXTBX, lastName)
  }

  async getCityTextboxText (attrib: string) {
    return this.getAttributesValue(SEARCH_COACH_CITY_TXTBX, attrib)
  }

  async isCityTextboxDisplayed () {
    return this.locatorFound(SEARCH_COACH_CITY_TXTBX)
  }

  async enterCityToSearchCoach (city: string) {
    await this.sendKeys(SEARCH_COACH_CITY_TXTBX, city)
  }

  async enterMarketToSearchCoach (market: string) {
    await this.moveToLocatorPosition(SEARCH_COACH_MARKET_DROPDOWN)
    await this.selectDropDownByText(SEARCH_COACH_MARKET_DROPDOWN, market)
  }

  async getMarketDefaultText () {
    return this.getText(SEARCH_FOR_COACH_MARKET_DEFAULT)
  }

  async clickSearchCoachFindACoachButton () {
    return this.click(SEARCH_COACH_FIND_MY_COACH_BTN)
  }

  async getCoachFirstNameErrorMessage () {
    return this.getText(SEARCH_COACH_FIRSTNAME_ERRMSG)
  }

  async getCoachLastNameErrorMessage () {
    return this.getText(SEARCH_COACH_LASTNAME_ERRMSG)
  }

  async getGlobalMessage () {
    return this.getText(GLOBAL_MESSAGE)
  }

  // Find by Nearby Coach
  async getSearchForNearbyCoachPanelTitleText () {
    return this.getText(SEARCH_FOR_COACH_BY_LEADS_TTL)
  }

  async getNearbyCoachLabelText () {
    return this.getText(SEARCH_COACH_NEARBY_COACH_LABEL)
  }

  async verifyConnectWithACoachButtonIsDisplayed () {
    await this.waitForDisplayed(SEARCH_COACH_CONNECT_WITH_A_COACH_BUTTON)
  }

  async clickOnConnectWithACoachButton () {
    await this.waitForDisplayed(SEARCH_COACH_CONNECT_WITH_A_COACH_BUTTON)
    await this.click(SEARCH_COACH_CONNECT_WITH_A_COACH_BUTTON)
  }

  async getCoachCaptchaRequiredMessageText () {
    return this.getText(SEARCH_COACH_CAPTCHA_REQUIRED_MESSAGE)
  }

  async verifyGoBackLinkIsDisplayed () {
    await this.waitForDisplayed(SEARCH_COACH_GO_BACK_LINK)
  }

  async clickGoBackLink () {
    return this.click(SEARCH_COACH_GO_BACK_LINK)
  }

  async clickLogIn () {
    return this.click(LOGIN_LINK)
  }
}
