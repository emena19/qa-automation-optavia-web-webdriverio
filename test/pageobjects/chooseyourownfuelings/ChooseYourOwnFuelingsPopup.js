// @flow
import { By, Key } from 'selenium-webdriver'
import BasePage from '../BasePage'

const ADD_ITEMS_TOCART_POPUP_TITLE = '//*[@id="SelectMealsWrapper"]/div/div/h3/a/span[2]'
const ADD_ITEMS_TOCART_POPUP_HEADER = '//*[@id="selectMeal--category"]/div[1]/h2'
const CLOSE_ADD_ITEMS_TOCART_POPUP_ICON = '#cboxClose'
const SEARCH_PRODUCT_NAME = '#search'
const FIRST_SEARCHED_RESULT_SELECT_BTN = '//*[@id="simpleBar"]/div/div[3]/div/div[1]/descendant::button[3]'
const ADD_ITEMS_TO_CART_BTN = '//button[contains(text(),"Add Items to cart")]'
const FIRST_SEARCHED_RESULT_QTY_INPUT = '(//*[@id="simpleBar"]//*[@id="qty"])[1]'
const FIRST_SEARCHED_RESULT_SKU = '//*[@id="simpleBar"]/div/div[3]/div/div[1]/div/div[1]/div[2]/div/div[1]/p'
const CLOSE_CHOOSE_YOUR_OWN_FUELINGS_WINDOW = '//*[@id="cboxClose"]'
const SELECT_ITEMS_BTN_LIST = '//button[@class="button mealButton-alternate addItems"]'
const REMOVE_ALL_BTN = '//*[@id="quickorderaddtocart"]/div[1]/div/a'
const ESSENTIAL_BARS = '//*[@id="selectMeal--category"]/descendant::ul/li[2]/a'
const ESSENTIAL_BARS_RAISIN_OAT_CINNAMON_CRISP_BAR = '//*[@id="simpleBar"]/descendant::div[contains(text(),"Raisin Oat Cinnamon Crisp Bar")]'
const ESSENTIAL_BARS_DOUBLE_PEANUT_BUTTER_CRISP_BAR = '//*[@id="simpleBar"]/descendant::div[contains(text(),"Creamy Double Peanut Butter Crisp Bar")]'
const ESSENTIAL_BARS_RAISIN_OAT_CINNAMON_CRISP_BAR_PRODUCT_QTY = '//div[@data-prod="81014-opt-raisin-cinn-crisp-bar"]/descendant::input[3]'
const ESSENTIAL_BARS_DOUBLE_PEANUT_BUTTER_CRISP_BAR_PRODUCT_QTY = '//div[@data-prod="81017-opt-peanut-crisp-bar"]/descendant::input[3]'
const ESSENTIAL_ON_THE_GO_KIT_51_PLAN = '//*[@id="simpleBar"]/descendant::div[contains(text(),"On-The-Go")]'
const DELETE_ITEM_FROM_PRODUCT_SELECTIONS_LIST = 'div[data-meal="true"] a'
const ADD_ITEM_QUANTITY_FROM_PRODUCT_SELECTIONS_LIST = '//*[@id="cboxLoadedContent"]/descendant::button[contains(@class,"fa-plus")]'
const REMOVE_ITEM_QUANTITY_FROM_PRODUCT_SELECTIONS_LIST = 'button[class*="fa-minus"]'
const NON_SELECTED_ITEMS_DESCRIPTION_LIST = 'div[class*="mealProducts"] div[class="details"]'
const NON_SELECTED_ITEMS_QUANTITY_LIST = 'div[class*="mealProducts"] input[id="qty"]'
const SELECTED_ITEMS_DESCRIPTION_LIST = 'div[class="mealWrapper"] div[class="details"]'
const SELECTED_ITEMS_QUANTITY_LIST = 'div[class="mealWrapper"] input[id="qty"]'
const ADD_ITEMS_TOCART_POPUP_MENU_CATEGORIES_INDEX = '//*[@class="category--head"]'
const ADD_ITEMS_TOCART_ACTIVE_CATEGORY = 'category--head active'
const ESSENTIAL_KITS_SUB_CATEGORY = 'div[class="category--content"] a[href*="essential-kits"]'
const TOTAL_OF_MEALS = 'dl[class="meals"] dd'
const HEALTH_COACH_STORE_LEFT_MENU_CATEGORY = '//*[@id="selectMeal--category"]//*[contains(text(),"Health Coach Store")]'
const ACTIVE_SUBCATEGORY_ITEM_LIST = 'div[class="category--head active"] + div[class="category--content"] ul li'
const MARKETING_MATERIALS_SUBCATEGORY = '[id="selectMeal--category"] a[title="Marketing Materials"]'
const SPECIAL_DIETS_FILTER = '//div[@class="facet-head" and contains(text(),"Special Diets")]'
const SPECIAL_DIETS_HALAL_CHECKBOX = '#SpecialDiets_Halal'
const SHOPPING_CART_SPINNER = '//*[@id="add-to-cart-spinner"]'
const VIEW_DETAILS_BUTTON = '//*[@class="button mealButton-alternate"][contains(text(), "View Details")]'
const WELLNESS_CREDIT_LEFT_MENU_LINK = '//*[@id="selectMeal--category"]//*[contains(text(),"Wellness Credit")]'
const CLASSIC_FUELINGS_HEADER_CATEGORY = '//*[@class="category--head"][contains(text(), "Classic Fuelings")]'
const PRODUCT_NAME_AND_SKU_FROM_RESULTS_GRID = '//*[@class="details-wrapper"]'
const SELECT_FIRST_PRODUCT_BUTTON = '(//button[@class="button mealButton-alternate addItems"])[1]'

export default class ChooseYourOwnFuelingsPopup extends BasePage {
  async verifyPageIsLoaded () {
    await this.waitForDisplayed(ADD_ITEMS_TOCART_POPUP_HEADER)
  }

  async getPageUrl () {
    return this.getCurrentPageUrl()
  }

  async getAddItemsToCartPopupTitle () {
    // return this.getCurrentPageTitle()
    return this.getText(ADD_ITEMS_TOCART_POPUP_TITLE)
  }

  async increaseItemQuantity (qty: any) {
    await this.waitForDisplayed(FIRST_SEARCHED_RESULT_QTY_INPUT)
    await this.sendKeys(FIRST_SEARCHED_RESULT_QTY_INPUT, qty)
  }

  async increaseItemQuantityByIndex (index: any, qty: any) {
    await this.waitForDisplayed(NON_SELECTED_ITEMS_QUANTITY_LIST)
    await this.sendKeysByIndex(NON_SELECTED_ITEMS_QUANTITY_LIST, qty, index)
  }

  async clickOnClosePopupIcon () {
    await this.click(CLOSE_ADD_ITEMS_TOCART_POPUP_ICON)
  }

  async searchForProduct (keys: any) {
    await this.waitForDisplayed(SEARCH_PRODUCT_NAME, 3)
    await this.sendKeys(SEARCH_PRODUCT_NAME, keys + ' ' + Key.ENTER)
    // await this.moveToLocatorPositionAndClick(SEARCH_PRODUCT_NAME)
    // Commented for hybris Update
    //    const FIRST_SEARCHED_PRODUCT_LOADED = `//div[@class='simplebar-content']/div[1]/descendant::*[contains(text(),"${keys}")][1]`)
    //   await this.waitForDisplayed(FIRST_SEARCHED_PRODUCT_LOADED, 5)
    await browser.pause(5000)
  }

  async clickSearchfield () {
    await this.moveToLocatorPositionAndClick(SEARCH_PRODUCT_NAME)
  }

  async selectProduct () {
    await this.waitForDisplayed(FIRST_SEARCHED_RESULT_SELECT_BTN, 5)
    await this.click(FIRST_SEARCHED_RESULT_SELECT_BTN)
  }

  async selectProductByIndex (index: any) {
    await this.waitForDisplayed(SELECT_ITEMS_BTN_LIST)
    await this.clickByIndex(SELECT_ITEMS_BTN_LIST, index)
  }

  async selectAllActiveProducts () {
    let count = await this.getCountOfArrayList(NON_SELECTED_ITEMS_DESCRIPTION_LIST)
    for (let i = 0; i < count; i++) {
      await this.moveToLocatorPositionByIndex(SELECT_ITEMS_BTN_LIST, i)
      await this.clickByIndex(SELECT_ITEMS_BTN_LIST, i)
      await browser.pause(1000)
    }
  }

  async clickAddItemsToCart () {
    await this.waitForDisplayed(ADD_ITEMS_TO_CART_BTN)
    await this.click(ADD_ITEMS_TO_CART_BTN)
  }

  async clickClosebutton () {
    await this.click(CLOSE_CHOOSE_YOUR_OWN_FUELINGS_WINDOW)
  }

  async getSearchProductSKU () {
    return this.getText(FIRST_SEARCHED_RESULT_SKU)
  }

  async selectButton () {
    await this.waitForDisplayed(SELECT_ITEMS_BTN_LIST)
  }

  async removeAllButton () {
    await this.waitForDisplayed(REMOVE_ALL_BTN)
  }

  async addItemsToCartButton () {
    await this.waitForDisplayed(ADD_ITEMS_TO_CART_BTN)
  }

  async clickEssentialBars () {
    await this.click(ESSENTIAL_BARS)
  }

  async clickOnSpecialDietsFilter () {
    await this.waitForDisplayed(SPECIAL_DIETS_FILTER, 3)
    await this.click(SPECIAL_DIETS_FILTER)
  }

  async clickOnSpecialDietsHalal () {
    await this.click(SPECIAL_DIETS_HALAL_CHECKBOX)
  }

  async raisinOatCinnamonCrispBar () {
    await this.waitForDisplayed(ESSENTIAL_BARS_RAISIN_OAT_CINNAMON_CRISP_BAR)
  }

  async updateRaisinOatCinnamonCrispBarProductQTY (keys: any) {
    await this.waitForDisplayed(ESSENTIAL_BARS_RAISIN_OAT_CINNAMON_CRISP_BAR_PRODUCT_QTY)
    await this.sendKeys(ESSENTIAL_BARS_RAISIN_OAT_CINNAMON_CRISP_BAR_PRODUCT_QTY, keys)
    await this.clickByIndex(SELECT_ITEMS_BTN_LIST, 0)
  }

  async verifyDoublePeanutButterCrispBarIsDisplayed () {
    await this.waitForDisplayed(ESSENTIAL_BARS_DOUBLE_PEANUT_BUTTER_CRISP_BAR)
  }

  async updateDoublePeanutButterCrispBarProductQTY (keys: any) {
    await this.waitForDisplayed(ESSENTIAL_BARS_DOUBLE_PEANUT_BUTTER_CRISP_BAR_PRODUCT_QTY)
    await this.sendKeys(ESSENTIAL_BARS_DOUBLE_PEANUT_BUTTER_CRISP_BAR_PRODUCT_QTY, keys)
    await this.clickByIndex(SELECT_ITEMS_BTN_LIST, 0)
  }

  async selectOnTheGoKitProduct () {
    await this.waitForDisplayed(ESSENTIAL_ON_THE_GO_KIT_51_PLAN, 3)
    await this.waitForDisplayed(FIRST_SEARCHED_RESULT_SELECT_BTN)
    await this.click(FIRST_SEARCHED_RESULT_SELECT_BTN)
  }

  async removeItemFromProductSelection (key) {
    await this.waitForDisplayed(DELETE_ITEM_FROM_PRODUCT_SELECTIONS_LIST)
    await this.clickByIndex(DELETE_ITEM_FROM_PRODUCT_SELECTIONS_LIST, key)
  }

  async addItemQtyFromProductSelectionByIndex (index, qty) {
    await this.waitForDisplayed(ADD_ITEM_QUANTITY_FROM_PRODUCT_SELECTIONS_LIST)
    for (let i = 0; i < qty; i++) {
      await this.clickByIndex(ADD_ITEM_QUANTITY_FROM_PRODUCT_SELECTIONS_LIST, index)
    }
  }

  async removeItemQtyFromProductSelectionByIndex (index, qty) {
    await this.waitForDisplayed(REMOVE_ITEM_QUANTITY_FROM_PRODUCT_SELECTIONS_LIST)
    for (let i = 0; i < qty; i++) {
      await this.clickByIndex(REMOVE_ITEM_QUANTITY_FROM_PRODUCT_SELECTIONS_LIST, index)
    }
  }

  async getAddItemsToCartBtnAttributeValue () {
    return this.getAttributesValue(ADD_ITEMS_TO_CART_BTN, 'disabled')
  }

  async getSelectedItemsList () {
    let items = ''
    let count = await this.getCountOfArrayList(SELECTED_ITEMS_DESCRIPTION_LIST)
    for (let i = 0; i < count; i++) {
      items += '\n' + await this.getAttributesValueByIndex(SELECTED_ITEMS_QUANTITY_LIST, 'value', i) + '   ' + await this.getTextByIndex(SELECTED_ITEMS_DESCRIPTION_LIST, i)
    }
    return items
  }

  async getNonSelectedItemsList () {
    let items = ''
    let count = await this.getCountOfArrayList(NON_SELECTED_ITEMS_DESCRIPTION_LIST)
    for (let i = 0; i < count; i++) {
      items += '\n' + await this.getAttributesValueByIndex(NON_SELECTED_ITEMS_QUANTITY_LIST, 'value', i) + '   ' + await this.getTextByIndex(NON_SELECTED_ITEMS_DESCRIPTION_LIST, i)
    }
    return items
  }

  async clickOnLeftMenuCategoryByIndex (index: any) {
    await this.waitForDisplayed(ADD_ITEMS_TOCART_POPUP_MENU_CATEGORIES_INDEX)
    await this.clickByIndex(ADD_ITEMS_TOCART_POPUP_MENU_CATEGORIES_INDEX, index)
  }

  async clickOnMarketingMaterials () {
    await this.waitForDisplayed(MARKETING_MATERIALS_SUBCATEGORY)
    await this.click(MARKETING_MATERIALS_SUBCATEGORY)
  }

  async getTextFromLeftMenuCategoryByIndex (index: any) {
    await this.waitForDisplayed(ADD_ITEMS_TOCART_POPUP_MENU_CATEGORIES_INDEX)
    return this.getTextByIndex(ADD_ITEMS_TOCART_POPUP_MENU_CATEGORIES_INDEX, index)
  }

  async getTextFromLeftMenuActiveSubcategoryByIndex (index: any) {
    await this.waitForDisplayed(ACTIVE_SUBCATEGORY_ITEM_LIST)
    return this.getTextByIndex(ACTIVE_SUBCATEGORY_ITEM_LIST, index)
  }

  async getNonSelectedProductTextList () {
    await this.waitForDisplayed(NON_SELECTED_ITEMS_DESCRIPTION_LIST)
    let list = []
    let rawItems = await this.getElementsTextArrayList(NON_SELECTED_ITEMS_DESCRIPTION_LIST)
    let items = rawItems.toString().split('\n').toString().split(',')
    for (let i = 0; i < items.length; i += 2) {
      list.push(items[i])
    }
    return list
  }

  async getSubCategoryText (subcategory: string) {
    const SUBCATEGORY = '//*[@class="category--content"]//*[contains(text(),"' + subcategory + '")]'
    await this.waitForDisplayed(SUBCATEGORY)
    return this.getText(SUBCATEGORY)
  }

  async clickOnActiveCategoryFromLeftMenu () {
    await this.waitForDisplayed(ADD_ITEMS_TOCART_ACTIVE_CATEGORY, 3)
    await this.click(ADD_ITEMS_TOCART_ACTIVE_CATEGORY)
  }

  async getTextFromLeftMenuActiveCategory () {
    await this.waitForDisplayed(ADD_ITEMS_TOCART_ACTIVE_CATEGORY, 3)
    return this.getText(ADD_ITEMS_TOCART_ACTIVE_CATEGORY)
  }

  async clickDeleteLink (SKU: string) {
    const productName = `//form[@id="quickorderaddtocart"]//div/p[contains(text(),'${SKU}')]//..//../..//..//..//div[3]//a`
    await this.waitForDisplayed(productName)
    await this.click(productName)
  }

  async clickRemoveAllLink () {
    await this.click(REMOVE_ALL_BTN)
  }

  async isProductDisplayed (product: string) {
    const productName = `//form[@id="quickorderaddtocart"]//div[contains(text(),'${product}')]`
    return this.locatorFound(productName)
  }

  async isKitsSectionDefault () {
    return this.hasAttributePresent(ESSENTIAL_KITS_SUB_CATEGORY, 'class')
  }

  async getNonSelectedItemsQtyFieldList () {
    return this.getElementsArray(NON_SELECTED_ITEMS_QUANTITY_LIST)
  }

  async getNonSelectedItemsDescriptionList () {
    return this.getElementsArray(NON_SELECTED_ITEMS_DESCRIPTION_LIST)
  }

  async getNonSelectedItemsQtyFieldListTextByIndex (index) {
    return this.getAttributesValueByIndex(NON_SELECTED_ITEMS_QUANTITY_LIST, 'value', index)
  }

  async getSelectedItemsQtyFieldListTextByIndex (index) {
    return this.getAttributesValueByIndex(SELECTED_ITEMS_QUANTITY_LIST, 'value', index)
  }

  async getTotalOfMealsText () {
    return this.getText(TOTAL_OF_MEALS)
  }

  async isHealthCoachStoreLeftMenuDisplayed () {
    await this.waitForDisplayed(HEALTH_COACH_STORE_LEFT_MENU_CATEGORY)
    await this.moveToLocatorPosition(HEALTH_COACH_STORE_LEFT_MENU_CATEGORY)
    return this.locatorFound(HEALTH_COACH_STORE_LEFT_MENU_CATEGORY)
  }

  async getProductIsUnavailableText (product: string) {
    let unavailableProductXpath = By.css(`[data-prod*="${product}"] button`)
    await this.waitForDisplayed(unavailableProductXpath, 10)
    await this.moveToLocatorPosition(unavailableProductXpath)
    return this.getText(unavailableProductXpath)
  }

  async verifyThatSpinnerDisappeared (retries: number = 1) {
    await this.waitForSpinnerToDisappear(SHOPPING_CART_SPINNER, retries)
  }

  async isViewDetailsButtonLoaded () {
    await this.waitForDisplayed(VIEW_DETAILS_BUTTON, 3)
    return this.locatorFound(VIEW_DETAILS_BUTTON)
  }

  async clickOnViewDetailsButton () {
    await this.waitForDisplayed(VIEW_DETAILS_BUTTON, 3)
    await this.click(VIEW_DETAILS_BUTTON)
  }

  async clickOnHealthCoachFromLeftMenu () {
    await this.waitForDisplayed(HEALTH_COACH_STORE_LEFT_MENU_CATEGORY, 5)
    await this.click(HEALTH_COACH_STORE_LEFT_MENU_CATEGORY)
  }

  async clickOnWellnessCreditFromLeftMenu () {
    await this.waitForDisplayed(WELLNESS_CREDIT_LEFT_MENU_LINK, 5)
    await this.click(WELLNESS_CREDIT_LEFT_MENU_LINK)
  }

  async isClassicFuelingCategoryHeaderDisplayed () {
    return this.locatorFound(CLASSIC_FUELINGS_HEADER_CATEGORY)
  }

  async isSubcategoryDisplayed (item) {
    let subcategory = `//*[@class="category--head"]/..//a[text()="${item}"]`
    return this.locatorFound(subcategory)
  }

  async getProductResultsText () {
    return this.getElementsTextArrayList(PRODUCT_NAME_AND_SKU_FROM_RESULTS_GRID)
  }

  async addFirstProductToCart () {
    await this.waitForDisplayed(SELECT_FIRST_PRODUCT_BUTTON, 5)
    await this.click(SELECT_FIRST_PRODUCT_BUTTON)
  }
}
