// @flow
import BasePage from '../BasePage'

const SEARCH_COACH_RESULTS_BODY_TITLE = '//*[@class="HCSearchResults__wrapper clearfix"]/h1'
const COACH_CONNECT_ME_BTN = '//*[@class="panel-container HCSearchResults__innerWrap"]/div[1]/div[3]/button'
const COACH_NAME = '//*[@class="panel-container HCSearchResults__innerWrap"]/div[1]/div/div[2]/strong'
const CONNECT_ME_BTN = '//*[@id="findMyCoach"]/div[1]/div/button'
const ADVANCED_CERTIFICATE_ERROR_BUTTON = '#details-button'
const PROCEED_TO_LINK_CERTIFICATE_ERROR = '#proceed-link'

export default class SearchCoachResultsPage extends BasePage {
  async verifyPageIsLoaded () {
    await this.waitForDisplayed(SEARCH_COACH_RESULTS_BODY_TITLE, 100)
  }

  async getPageTitle () {
    return this.getCurrentPageTitle()
  }

  async getPageUrl () {
    return this.getCurrentPageUrl()
  }

  async getCoachSearchResultTitle () {
    return this.getText(SEARCH_COACH_RESULTS_BODY_TITLE)
  }

  async getCoachName () {
    return this.getText(COACH_NAME)
  }

  async clickConnectMeButton () {
    await this.click(COACH_CONNECT_ME_BTN)
  }

  async clickTempCoachConnectMeButton () {
    return this.click(CONNECT_ME_BTN)
  }

  async passChromeCertificateErrorIfPresent () {
    const advButton = await this.isCertificateErrorDisplayed()
    if (advButton === true) {
      await this.click(ADVANCED_CERTIFICATE_ERROR_BUTTON)
      await this.click(PROCEED_TO_LINK_CERTIFICATE_ERROR)
    }
  }

  async getJSONFromConnectMeButton () {
    await this.waitForDisplayed(COACH_CONNECT_ME_BTN)
    return this.getAttributesValue(COACH_CONNECT_ME_BTN, 'data-json')
  }

  async isCertificateErrorDisplayed () {
    return this.locatorFound(ADVANCED_CERTIFICATE_ERROR_BUTTON, 3)
  }
}
