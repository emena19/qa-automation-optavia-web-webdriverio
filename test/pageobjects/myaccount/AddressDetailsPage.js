// @flow
import BasePage from '../BasePage'
// Address Details
const ADDRESS_DETAILS_SECTION_TITLE = '//*[@class="col-md-9 pull-right main-content"]/descendant::h1[contains(text(),"Address Details")]'
const ADDRESS_DETAILS_ADDRESS_LINE1 = '//*[@id="address.line1"]'
const ADDRESS_DETAILS_ADDRESS_LINE2 = '//*[@id="address.line2"]'
const ADDRESS_DETAILS_CITY = '//*[@id="address.townCity"]'
const ADDRESS_DETAILS_STATE = '//*[@id="address.region"]'
const ADDRESS_DETAILS_ZIP = '//*[@id ="address.postcode"]'
const ADDRESS_DETAILS_COUNTRY_CODE = '#isdCode'
const ADDRESS_DETAILS_FIRST_NAME = '//*[@id="address.firstName"]'
const ADDRESS_DETAILS_LAST_NAME = '//*[@id="address.surname"]'
const ADDRESS_DETAILS_PHONE = '//*[@id="address.phone"]'
const ADDRESS_DETAILS_PHONE_ERROR = '//*[@id="address.phone-error"]'
const ADDRESS_DETAILS_DISTRICT_ERROR = '//*[@id="address.district-error"]'
const ADDRESS_DETAILS_DISTRICT = '//*[@id="address.district"]'
const ADDRESS_DETAILS_ZIP_ERROR = '//*[@id="address.postcode-error"]'
const ADDRESS_DETAILS_ZIP_ERROR_NEW_ADDRESS = '#zipcodeError'
const ADDRESS_DETAILS_DISABLED_COUNTRY = '//select[@id="address.country"]/option[@selected="selected"]'
// Save address
const ADDRESS_DETAILS_SAVE_BTN = '//*[@id="addressform_button_panel"]/button[1]'
const ADDRESS_DETAILS_PROCEED_TO_SAVE_BTN = '//*[@id="addressform_button_panel"]/button[2]'
const ADDRESS_DETAILS_CANCEL_BTN = '//*[@id="addressform_button_panel"]/a'
const ADDRESS_DETAILS_MAKE_DEFAULT_CHECKBOX = '//*[@class="custom-checkbox"]/input[1]'
// California Addresses
const ADDRESS_DETAILS_CALIFORNIA_PROPOSITION = '//*[@id="addressform_proposition"]'
const ADDRESS_DETAILS_CALIFORNIA_PROPOSITION_POPUP = '//*[@id="propositionPopup"]/p'
const ADDRESS_DETAILS_CALIFORNIA_PROP_CLICK_HERE = '//*[@id="addressform_proposition"]/p[2]/a'
const ADDRESS_DETAILS_CALIFORNIA_PROP_CLOSE = '//*[@id="cboxClose"]'
// Edit Address
const ADDRESS_DETAILS_EDIT_ADDRESS_BTN_BY_INDEX = '//*[@class="manage-addresses"]/descendant::a[contains(text(),"Edit")]'
// Address suggestion popup
const ADDRESS_DETAILS_SUGGESTION_POPUP_SUBMITASIS_BTN = '//button[contains(text(),"Submit As Is")]'
const ADDRESS_DETAILS_DISTRICT_LIST = '//select[@id="address.district"]/option[not(contains(@disabled,"disabled"))]'
const SHIPPING_ADDRESS_SELECTED_DISTRICT = '//select[@id="address.district"]/option[@selected="selected"]'
const ADDRESS_DETAILS_GLOBAL_MESSAGE = '//*[@id="globalMessages"]//ul//li'
const ADDRESS_DETAIL_TAX_BANNER = '//*[@class="address-form__tax d-none" and @style="display: block;"]//div[@class="address-form__tax-banner-content"]'
export default class AddressDetailsPage extends BasePage {
  async verifyPageIsLoaded () {
    await this.waitForDisplayed(ADDRESS_DETAILS_SECTION_TITLE)
  }

  async verifyCaliforniaPropositionIsLoaded () {
    await this.waitForDisplayed(ADDRESS_DETAILS_CALIFORNIA_PROPOSITION)
    await this.waitForDisplayed(ADDRESS_DETAILS_PROCEED_TO_SAVE_BTN)
  }

  async getPageTitle () {
    return this.getCurrentPageTitle()
  }

  async getPageUrl () {
    return this.getCurrentPageUrl()
  }

  async isMakeDefaultChecked () {
    return this.hasAttributePresent(ADDRESS_DETAILS_MAKE_DEFAULT_CHECKBOX, 'checked')
  }

  async clickMakeDefaultButton () {
    await this.click(ADDRESS_DETAILS_MAKE_DEFAULT_CHECKBOX)
  }

  async clickAddAddressSaveButton () {
    await this.click(ADDRESS_DETAILS_SAVE_BTN)
  }

  async clickAddAddressProceedToSaveButton () {
    await this.click(ADDRESS_DETAILS_PROCEED_TO_SAVE_BTN)
  }

  async clickCancelAddressDetails () {
    await this.click(ADDRESS_DETAILS_CANCEL_BTN)
  }

  async createShippingAddress (firstName: string, lastName: string, address1: string, city: string, state: string, zip: string, countryCode: any, phone: string, address2: string = '') {
    await this.sendKeys(ADDRESS_DETAILS_FIRST_NAME, firstName)
    await this.sendKeys(ADDRESS_DETAILS_LAST_NAME, lastName)
    await this.sendKeys(ADDRESS_DETAILS_ADDRESS_LINE1, address1)
    await this.sendKeys(ADDRESS_DETAILS_ADDRESS_LINE2, address2)
    await this.sendKeys(ADDRESS_DETAILS_CITY, city)
    await this.selectDropDownByText(ADDRESS_DETAILS_STATE, state)
    await this.sendKeys(ADDRESS_DETAILS_ZIP, zip)
    await this.selectDropDownByText(ADDRESS_DETAILS_COUNTRY_CODE, `${countryCode} United States`)
    await this.sendKeys(ADDRESS_DETAILS_PHONE, phone)
  }

  async createShippingAddressHK (addressline1: string, addressline2: string, district: any, countrycode: any, phone: any) {
    await this.sendKeys(ADDRESS_DETAILS_ADDRESS_LINE1, addressline1)
    await this.sendKeys(ADDRESS_DETAILS_ADDRESS_LINE2, addressline2)
    await this.selectDropDownByText(ADDRESS_DETAILS_DISTRICT, district, 4)
    await this.selectDropDownByText(ADDRESS_DETAILS_COUNTRY_CODE, `${countrycode} Hong Kong`)
    await this.sendKeys(ADDRESS_DETAILS_PHONE, phone)
  }

  async createShippingAddressSG (addressline1: string, addressline2: string, zip: any, countrycode: any, phone: any) {
    await this.sendKeys(ADDRESS_DETAILS_ADDRESS_LINE1, addressline1)
    await this.sendKeys(ADDRESS_DETAILS_ADDRESS_LINE2, addressline2)
    await this.sendKeys(ADDRESS_DETAILS_ZIP, zip)
    await this.sendKeys(ADDRESS_DETAILS_PHONE, phone)
    await this.selectDropDownByText(ADDRESS_DETAILS_COUNTRY_CODE, `${countrycode} Singapore`)
  }

  async enterZipCode (zip: any) {
    await this.sendKeys(ADDRESS_DETAILS_ZIP, zip)
  }

  async clickCaliforniaPropClickHere () {
    await this.moveToLocatorPosition(ADDRESS_DETAILS_CALIFORNIA_PROP_CLICK_HERE)
    await this.browser.pause(2000)
    await this.click(ADDRESS_DETAILS_CALIFORNIA_PROP_CLICK_HERE)
  }

  async getCaliforniaPropPopUpText () {
    await this.waitForDisplayed(ADDRESS_DETAILS_CALIFORNIA_PROPOSITION_POPUP, 7)
    return this.getText(ADDRESS_DETAILS_CALIFORNIA_PROPOSITION_POPUP)
  }

  async getPhoneErrorMessage () {
    return this.getText(ADDRESS_DETAILS_PHONE_ERROR)
  }

  async getSelectedDistrictText () {
    return this.getText(SHIPPING_ADDRESS_SELECTED_DISTRICT)
  }

  async getDisabledCountryText () {
    return this.getText(ADDRESS_DETAILS_DISABLED_COUNTRY)
  }

  async getDistrictErrorMessage () {
    return this.getText(ADDRESS_DETAILS_DISTRICT_ERROR)
  }

  async selectDistrictByName (district: string) {
    await this.selectDropDownByText(ADDRESS_DETAILS_DISTRICT, district, 4)
  }

  async clickCaliforniaPropPopUpClose () {
    await this.click(ADDRESS_DETAILS_CALIFORNIA_PROP_CLOSE)
  }

  async clickEditButtonByIndex (index) {
    await this.clickByIndex(ADDRESS_DETAILS_EDIT_ADDRESS_BTN_BY_INDEX, index)
  }

  async clickSubmitAsIsButton () {
    await this.click(ADDRESS_DETAILS_SUGGESTION_POPUP_SUBMITASIS_BTN)
  }

  async getAllHKDistricts () {
    await this.waitForDisplayed(ADDRESS_DETAILS_DISTRICT_LIST, 3)
    return this.getElementsTextArrayList(ADDRESS_DETAILS_DISTRICT_LIST)
  }

  async isZipCodeErrorMessageDisplayed () {
    let errorStatus = await this.locatorFound(ADDRESS_DETAILS_ZIP_ERROR)
    let message = await this.getText(ADDRESS_DETAILS_ZIP_ERROR)
    return errorStatus && message === 'Please provide a valid Postal Code.'
  }

  async getZipErrorMessage () {
    return this.getText(ADDRESS_DETAILS_ZIP_ERROR)
  }

  async getGlobalMessageText () {
    return this.getText(ADDRESS_DETAILS_GLOBAL_MESSAGE)
  }

  async getZipErrorForNewAddressText () {
    return this.getText(ADDRESS_DETAILS_ZIP_ERROR_NEW_ADDRESS)
  }

  async getTaxBannerText () {
    return this.getText(ADDRESS_DETAIL_TAX_BANNER)
  }

  async isTaxBannerDisplayedText () {
    return this.locatorFound(ADDRESS_DETAIL_TAX_BANNER)
  }
}
