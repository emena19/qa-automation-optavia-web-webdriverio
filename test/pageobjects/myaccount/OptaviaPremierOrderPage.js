// @flow
import BasePage from '@page-objects/BasePage'
import {market} from '@input-data/products/ProductTestData'
import CommonUtils from '@core-libs/CommonUtils'

const GLOBAL_MESSAGE_OPTAVIA_PREMIER = '//*[@id="globalMessages"]/ul/li/ul/li'
const OPTAVIA_PREMIER_START_DATE = '//*[@class="my-subscriptions"]/p'
const OPTAVIA_PREMIER_NEXT_DATE = '//*[@class="nextOrderDate"]'

// Billing & Shipping Information
const SUBSCRIPTIONS_BILLING_SHIPPING_INFO_TITLE = '//*[@class="subscription-info"]/h3'
const OPTAVIA_PREMIER_BILLING_SHIPPING_INFO_ARRAY = '//*[@class="col-md-3"]/address'
const OPTAVIA_PREMIER_SHIPPING_ADDRESS_EDIT_BUTTON = '//*[@class="subscription-info"]/div[1]/div[2]/a'
const OPTAVIA_PREMIER_PAYMENT_DETAILS_EDIT_BUTTON = '//*[@class="subscription-info"]/div[1]/div[3]/a'

// OPTAVIA Premier Edit Shipping Address POPUP window
const EDIT_SHIPPING_ADDRESS_POPUP = '//*[@id="shippingAddressForm"]/div/div'
const SELECT_DELIVERY_ADDRESS_DROPDOWN = '#deliveryAddressId'
const ADD_NEW_ADDRESS_BTN = '//*[@id="shippingAddressForm"]/descendant::a[contains(text(),"Add New Address")]'
const EDIT_SHIPPING_ADDRESS_SUBMIT_BTN = '//*[@id="shippingAddressForm"]/descendant::button[contains(text(),"Submit Changes")]'
const EDIT_SHIPPING_ADDRESS_DROPDOWN_ITEMS = '//*[@id="deliveryAddressId"]/option'
const EDIT_SHIPPING_ADDRESS_POPUP_CLOSE = '//*[@id="shippingAddressForm"]//a[@data-target="#shippingAddressForm"]'

// OPTAVIA Payment Details
const CREDIT_CARD_DETAILS = '//h5[contains(text(),"Payment Details")]/following-sibling::address'

// OPTAVIA Premier Edit Payment Details PopUp window
const EDIT_PAYMENT_DETAILS_POPUP = '//*[@id="paymentMethodForm"]/div/div'
const ADD_NEW_PAYMENT_DETAILS_BTN = '//*[@id="paymentMethodForm"]/descendant::a[contains(text(),"Add New Payment Details")]'
const EDIT_PAYMENT_DETAILS_SUBMIT_BTN = '//*[@id="paymentMethodForm"]/descendant::button[contains(text(),"Submit Changes")]'
const EDIT_PAYMENT_DETAILS_CARD_NUMBER_DROP_DOWN = '#paymentId'

const OPTAVIA_PREMIER_INFO_TITLE = '//*[@class="subscription-actions"]/h3'
const OPTAVIA_PREMIER_SCHEDULE = '//*[@class="subscription-actions"]/p'
const OPTAVIA_PREMIER_REINSTATE_BUTTON = '//*[@class="subscription-actions"]/div[1]/descendant::a[contains(text(), "Reinstate")]'
const OPTAVIA_PREMIER_EDIT_SCHEDULE_BUTTON = '//*[@class="subscription-actions"]/div[1]/descendant::a[.="Edit OPTAVIA Premier Schedule"]'
const OPTAVIA_PREMIER_PROCESS_NOW = '//*[@class="subscription-actions"]/div[1]/descendant::a[.="Process Now"]'

const CANCEL_OPTAVIA_PREMIER = '//*[@class="subscription-actions"]/div[1]/descendant::a[contains(text(), "Cancel")]'
const CANCEL_OPTAVIA_PREMIER_POP_UP = '//*[@id="cancel-subscription"]/div/div'
const CANCEL_PREMIER_POP_UP_CHANGE_MY_MIND_BUTTON = '//*[@id="cancel-subscription"]/descendant::a[contains(text(),"I changed my mind")]'
const CANCEL_OPTAVIA_PREMIER_POP_UP_CLOSE_LINK = '//*[@id="cancel-subscription"]/div/a'
const CANCEL_PREMIER_POP_UP_CANCEL_PREMIER_BUTTON = '//*[@id="cancel-subscription"]/descendant::a[contains(text(),"Cancel")]'
const AUTOSHIP_TEMPLATE_CANCEL_MESSAGE = '//*[@class="success-msg"]/ul/li'

const POP_UP_PROCESS_NOW = '//*[@id="runnow-subscription"]/div/div'
const POP_UP_PROCESS_NOW_CANCEL_BUTTON = '//*[@id="runnow-subscription"]/descendant::a[contains(text(),"Cancel")]'
const POP_UP_PROCESS_NOW_PROCESS_NOW_BUTTON = '//*[@id="runnow-subscription"]/descendant::a[contains(text(),"Process Now")]'

// Products Info in OPTAVIA Premier template
const SUBSCRIPTIONS_ITEMS_IN_ORDER_TITLE = '//*[@class="my-subscriptions"]/h3'
const PRODUCTS_IN_OPTVIA_PREMIER_TEMPLATE = '//*[@class="my-subscriptions"]/table/tbody/tr'
const OPTAVIA_PREMIER_PRODUCT_QTY = '//*[@class="my-subscriptions"]/table/tbody/tr/td[2]'
const OPTAVIA_PREMIER_PRODUCT_UNIT_PRICE = '//*[@class="my-subscriptions"]/table/tbody/tr/td[3]'
const OPTAVIA_PREMIER_PRODUCT_PRICE = '//*[@class="my-subscriptions"]/table/tbody/tr/td[4]'
const OPTAVIA_PREMIER_PRODUCTS_LIST_INFO_ARRAY = '//*[@class="my-subscriptions"]/table/descendant::div/p/a[contains(text(),"")]'
const OPTAVIA_PREMIER_PRODUCTS_LIST_IMG_ARRAY = '//*[@class="my-subscriptions"]/table/descendant::div/p/a[contains(text(),"")]//ancestor::td/a/img'

// Modifying the OPTAVIA Premier Template
const MODIFY_OPTAVIA_PREMIER_TEMPLATE_BUTTON = '//*[@class="my-subscriptions"]/descendant::a[contains(text(),"Modify Order")]'

// OPTAVIA Premier Order Summary section
const OPTAVIA_PREMIER_TEMPLATE_SUBTOTAL = '//*[@id="orderTotals"]/tbody/descendant::td[contains(text(),"Subtotal")]/following-sibling::td'
const OPTAVIA_PREMIER_APPLIED_REWARDS = '//*[@id="orderTotals"]/tbody/descendant::td[contains(text(),"Rewards:")]/following-sibling::td'
const OPTAVIA_PREMIER_DELIVERY_CHARGE = '//*[@id="orderTotals"]/tbody/descendant::td[contains(text(),"Delivery Charge:")]/following-sibling::td'
const OPTAVIA_PREMIER_TAX = '//*[@id="orderTotals"]/tbody/descendant::td[contains(text(),"Estimated tax:")]/following-sibling::td'
const OPTAVIA_PREMIER_ORDER_TOTAL = '//*[@id="orderTotals"]/tfoot/descendant::td[contains(text(),"Order Total")]/following-sibling::td'
const OPTAVIA_PREMIER_GST = '//*[@id="orderTotals"]/tfoot/descendant::td[contains(text(),"GST:")]/following-sibling::td'
// OPTAVIA Premier Estimated Rewards section
const OPTAVIA_PREMIER_ELIGIBLE_NEW_REWARDS = '//*[@class="table totals-table"]/descendant::td[contains(text(),"Eligible For New Rewards:")]/following-sibling::td'
const OPTAVIA_PREMIER_EARNINGS_THIS_ORDER = '//*[@class="table totals-table"]/descendant::td[contains(text(),"Earning This Order:")]/following-sibling::td'
const OPTAVIA_PREMIER_REWARDS_TEXT = '//*[@class="rewards-block details-block"]/div/p'
const ESTIMATED_REWARDS_SECTION = '//*[@class="rewards-block details-block"]'
const TAX_BANNER = '.address-form__tax-banner-content'
export default class OptaviaPremierOrderPage extends BasePage {
  async verifyPageIsLoaded () {
    await this.waitForDisplayed(SUBSCRIPTIONS_BILLING_SHIPPING_INFO_TITLE)
    await this.waitForDisplayed(OPTAVIA_PREMIER_INFO_TITLE)
  }

  async getPageTitle () {
    return this.getCurrentPageTitle()
  }

  async getPageUrl () {
    return this.getCurrentPageUrl()
  }

  async getGlobalMsg () {
    return this.getText(GLOBAL_MESSAGE_OPTAVIA_PREMIER)
  }

  async getOptaviaPremierBillingAndShippingText () {
    return this.getText(SUBSCRIPTIONS_BILLING_SHIPPING_INFO_TITLE)
  }

  async getBillingInfo () {
    return this.getTextByIndex(OPTAVIA_PREMIER_BILLING_SHIPPING_INFO_ARRAY, 0)
  }

  // SHIPPING ADDRESS
  async getShippingInfo () {
    return this.getTextByIndex(OPTAVIA_PREMIER_BILLING_SHIPPING_INFO_ARRAY, 1)
  }

  async clickEditShippingAddressButton () {
    await this.moveToLocatorPosition(OPTAVIA_PREMIER_SHIPPING_ADDRESS_EDIT_BUTTON)
    await this.click(OPTAVIA_PREMIER_SHIPPING_ADDRESS_EDIT_BUTTON)
  }

  // EDIT SHIPPING ADDRESS POPUP
  async verifyEditShippingAddressPopUpIsLoaded () {
    await this.waitForDisplayed(EDIT_SHIPPING_ADDRESS_POPUP)
  }

  async clickAddNewAddressPopUpBtn () {
    await this.moveToLocatorPosition(ADD_NEW_ADDRESS_BTN)
    await this.click(ADD_NEW_ADDRESS_BTN)
  }

  async clickEditShippingPopUpSubmitBtn () {
    await this.click(EDIT_SHIPPING_ADDRESS_SUBMIT_BTN)
  }

  async selectAddressFromDropdown (address: string) {
    await this.moveToLocatorPosition(SELECT_DELIVERY_ADDRESS_DROPDOWN)
    await this.selectDropDownByText(SELECT_DELIVERY_ADDRESS_DROPDOWN, address)
  }

  // PAYMENT DETAILS
  async getPaymentDetails () {
    return this.getTextByIndex(OPTAVIA_PREMIER_BILLING_SHIPPING_INFO_ARRAY, 2)
  }

  async clickPaymentDetailsEditButton () {
    await this.click(OPTAVIA_PREMIER_PAYMENT_DETAILS_EDIT_BUTTON)
  }

  // EDIT PAYMENT DETAILS POPUP
  async verifyEditPaymentDetailsPopUpIsLoaded () {
    await this.waitForDisplayed(EDIT_PAYMENT_DETAILS_POPUP)
  }

  async clickAddNewPaymentPopUpBtn () {
    await this.click(ADD_NEW_PAYMENT_DETAILS_BTN)
  }

  async clickEditPaymentPopUpSubmitBtn () {
    await this.click(EDIT_PAYMENT_DETAILS_SUBMIT_BTN)
  }

  // SHIPPING METHOD
  async getShippingMethod () {
    return this.getTextByIndex(OPTAVIA_PREMIER_BILLING_SHIPPING_INFO_ARRAY, 3)
  }

  async getOptaviaPremierInformationText () {
    return this.getText(OPTAVIA_PREMIER_INFO_TITLE)
  }

  async getOptaviaPremierItemsInOrderText () {
    return this.getText(SUBSCRIPTIONS_ITEMS_IN_ORDER_TITLE)
  }

  async getOptaviaScheduleInfo () {
    return this.getText(OPTAVIA_PREMIER_SCHEDULE)
  }

  async clickCancelOptaviaPremier () {
    await this.moveToLocatorPosition(CANCEL_OPTAVIA_PREMIER)
    await this.click(CANCEL_OPTAVIA_PREMIER)
  }

  async verifyCancelPremierPopUpIsLoaded () {
    await this.waitForDisplayed(CANCEL_OPTAVIA_PREMIER_POP_UP)
    await this.waitForDisplayed(CANCEL_PREMIER_POP_UP_CHANGE_MY_MIND_BUTTON)
    await this.waitForDisplayed(CANCEL_PREMIER_POP_UP_CANCEL_PREMIER_BUTTON)
  }

  async clickChangeMyMindButton () {
    await this.click(CANCEL_PREMIER_POP_UP_CHANGE_MY_MIND_BUTTON)
  }

  async clickCancelPremierOrderButton () {
    await this.click(CANCEL_PREMIER_POP_UP_CANCEL_PREMIER_BUTTON)
  }

  async getMessageAutoshipTemplateCancel () {
    await this.waitForDisplayed(AUTOSHIP_TEMPLATE_CANCEL_MESSAGE, 5)
    return this.getText(AUTOSHIP_TEMPLATE_CANCEL_MESSAGE)
  }

  async clickOptaviaPremierReinstateButton () {
    await this.click(OPTAVIA_PREMIER_REINSTATE_BUTTON)
  }

  async getReinstateBtnAttributeValue () {
    return this.getAttributesValue(OPTAVIA_PREMIER_REINSTATE_BUTTON, 'class')
  }

  async getReinstateOrderText () {
    return this.getText(OPTAVIA_PREMIER_REINSTATE_BUTTON)
  }

  async getEditScheduleBtnClassAttributeValue () {
    return this.getAttributesValue(OPTAVIA_PREMIER_EDIT_SCHEDULE_BUTTON, 'class')
  }

  async clickEditOptaviaPremierBtn () {
    await this.moveToLocatorPositionAndClick(OPTAVIA_PREMIER_EDIT_SCHEDULE_BUTTON)
  }

  async getProcessNowBtnClassAttributeValue () {
    return this.getAttributesValue(OPTAVIA_PREMIER_PROCESS_NOW, 'class')
  }

  async clickProcessNowBtn () {
    await this.click(OPTAVIA_PREMIER_PROCESS_NOW)
  }

  async clickCancelOptaviaPremierCloseLink () {
    await this.click(CANCEL_OPTAVIA_PREMIER_POP_UP_CLOSE_LINK)
  }

  async verifyProcessNowPopUpIsLoaded () {
    await this.waitForDisplayed(POP_UP_PROCESS_NOW)
    await this.waitForDisplayed(POP_UP_PROCESS_NOW_CANCEL_BUTTON)
    await this.waitForDisplayed(POP_UP_PROCESS_NOW_PROCESS_NOW_BUTTON)
  }

  async clickProcessNowPopUpCancelBtn () {
    await this.click(POP_UP_PROCESS_NOW_CANCEL_BUTTON)
  }

  async clickProcessNowPopUpProcessNowBtn () {
    await this.click(POP_UP_PROCESS_NOW_PROCESS_NOW_BUTTON)
    await browser.pause(10000)
  }

  async getPremierTemplateProductsListRowCount () {
    return this.getCountOfArrayList(PRODUCTS_IN_OPTVIA_PREMIER_TEMPLATE)
  }

  async getProductInfoTextFromList (num: any) {
    return this.getTextByIndex(OPTAVIA_PREMIER_PRODUCTS_LIST_INFO_ARRAY, num)
  }

  async getProductArrayList () {
    await this.waitForDisplayed(OPTAVIA_PREMIER_PRODUCTS_LIST_INFO_ARRAY, 10)
    return this.getElementsTextArrayList(OPTAVIA_PREMIER_PRODUCTS_LIST_INFO_ARRAY)
  }

  async getCountOfProductItemsOptaviaPremier () {
    return this.getCountOfArrayList(OPTAVIA_PREMIER_PRODUCTS_LIST_INFO_ARRAY)
  }

  async getPremierProductUnitPriceArrayList () {
    return this.getElementsTextArrayList(OPTAVIA_PREMIER_PRODUCT_UNIT_PRICE)
  }

  async getPremierProductQtyArrayList () {
    return this.getElementsTextArrayList(OPTAVIA_PREMIER_PRODUCT_QTY)
  }

  async getPremierProductPriceArrayList () {
    return this.getElementsTextArrayList(OPTAVIA_PREMIER_PRODUCT_PRICE)
  }

  async getProductImageDataSourceArrayList () {
    return this.getElementsAttributeValueArrayList(OPTAVIA_PREMIER_PRODUCTS_LIST_IMG_ARRAY, 'data-src')
  }

  async verifyModifyOrderBtnIsLoaded () {
    await this.waitForDisplayed(MODIFY_OPTAVIA_PREMIER_TEMPLATE_BUTTON)
  }

  async clickModifyOrderBtn () {
    await this.moveToLocatorPositionAndClick(MODIFY_OPTAVIA_PREMIER_TEMPLATE_BUTTON)
  }

  async getOptaviaPremierStartDateText () {
    return this.getText(OPTAVIA_PREMIER_START_DATE)
  }

  async getOptaviaPremierNextDateText () {
    return this.getText(OPTAVIA_PREMIER_NEXT_DATE)
  }

  async verifySubtotalIsLoaded () {
    await this.waitForDisplayed(OPTAVIA_PREMIER_TEMPLATE_SUBTOTAL)
  }

  async getPremierSubtotalText () {
    return this.getText(OPTAVIA_PREMIER_TEMPLATE_SUBTOTAL)
  }

  async verifyAppliedRewardsIsLoaded () {
    await this.waitForDisplayed(OPTAVIA_PREMIER_APPLIED_REWARDS)
  }

  async getPremierAppliedRewardsText () {
    return this.getText(OPTAVIA_PREMIER_APPLIED_REWARDS)
  }

  async isPremierAppliedRewardsPresent () {
    return this.locatorFound(OPTAVIA_PREMIER_APPLIED_REWARDS, 1)
  }

  async verifyDeliveryChargeIsLoaded () {
    await this.waitForDisplayed(OPTAVIA_PREMIER_DELIVERY_CHARGE)
  }

  async getPremierDeliveryChargeText () {
    return this.getText(OPTAVIA_PREMIER_DELIVERY_CHARGE)
  }

  async verifyTaxIsLoaded () {
    await this.waitForDisplayed(OPTAVIA_PREMIER_TAX)
  }

  async getPremierTaxText () {
    return this.getText(OPTAVIA_PREMIER_TAX)
  }

  async verifyOrderTotalIsLoaded () {
    await this.waitForDisplayed(OPTAVIA_PREMIER_ORDER_TOTAL)
  }

  async getPremierOrderTotalText () {
    return this.getText(OPTAVIA_PREMIER_ORDER_TOTAL)
  }

  async verifyEligibleForRewardsIsLoaded () {
    await this.waitForDisplayed(OPTAVIA_PREMIER_ELIGIBLE_NEW_REWARDS)
  }

  async getEligibleForRewardsText () {
    return this.getText(OPTAVIA_PREMIER_ELIGIBLE_NEW_REWARDS)
  }

  async verifyEarningThisOrderIsLoaded () {
    await this.waitForDisplayed(OPTAVIA_PREMIER_EARNINGS_THIS_ORDER)
  }

  async getEarningThisOrderText () {
    return this.getText(OPTAVIA_PREMIER_EARNINGS_THIS_ORDER)
  }

  async verifyRewardsTextIsLoaded () {
    await this.waitForDisplayed(OPTAVIA_PREMIER_REWARDS_TEXT)
  }

  async getEstimatedRewardsText () {
    return this.getText(OPTAVIA_PREMIER_REWARDS_TEXT)
  }

  async getCreditCardExpirirationDate () {
    return this.getText(CREDIT_CARD_DETAILS)
  }

  async openProcessNowInNewTab (url: string) {
    await this.openUrlInNewTab(url)
  }

  async getProcessNowButtonLink () {
    return this.getAttributesValue(POP_UP_PROCESS_NOW_PROCESS_NOW_BUTTON, 'href')
  }

  async switchTab (tabIndex: number) {
    await this.switchBrowserTab(tabIndex)
  }

  async selectExistingCardDetails (cardNumber: string) {
    return this.selectDropDownByText(EDIT_PAYMENT_DETAILS_CARD_NUMBER_DROP_DOWN, cardNumber)
  }

  async calculateEligibleForNewRewardsValue (subtotal: number, appliedRewards: number) {
    return subtotal - appliedRewards
  }

  async calculateEarningThisOrderValue (eligibleNewReward: number, subtotal: number, country: number = market.US) {
    let rewardsDiscount, earningThisOrder, mkt
    let us = {subtotalMin: 150, subtotalMed: 249.99, subtotalMax: 250}
    let hk = {subtotalMin: 1500, subtotalMed: 2499.99, subtotalMax: 2500}
    let sg = {subtotalMin: 270, subtotalMed: 449.99, subtotalMax: 500}

    if (country === 3) {
      mkt = hk
    } else if (country === 2) {
      mkt = sg
    } else {
      mkt = us
    }

    if (subtotal < mkt.subtotalMin) {
      rewardsDiscount = 1
    } else if (subtotal >= mkt.subtotalMin && subtotal <= mkt.subtotalMed) {
      rewardsDiscount = 0.05
    } else if (subtotal >= mkt.subtotalMax) {
      rewardsDiscount = 0.1
    }
    earningThisOrder = eligibleNewReward * rewardsDiscount
    return earningThisOrder
  }

  async isRewardsSectionDisplayed () {
    return this.locatorFound(ESTIMATED_REWARDS_SECTION)
  }

  async getPremierGSTText () {
    return this.getText(OPTAVIA_PREMIER_GST)
  }

  async getTextFromEditDelvAddressDropDown () {
    return this.getText(SELECT_DELIVERY_ADDRESS_DROPDOWN)
  }

  async getEditDelvAddressDropDownCount () {
    return this.getCountOfArrayList(EDIT_SHIPPING_ADDRESS_DROPDOWN_ITEMS)
  }

  async checkIfAddressExistsOnDelvAddressDropDown (address: string) {
    let items = await this.getEditDelvAddressDropDownCount()
    let addressFlag
    for (let i = 0; i < items; i++) {
      if (await this.getTextByIndex(EDIT_SHIPPING_ADDRESS_DROPDOWN_ITEMS, i) === address) {
        addressFlag = true
        break
      } else {
        addressFlag = false
      }
    }
    return addressFlag
  }

  async closeEditDelvAddressPopUp () {
    await this.moveToLocatorPosition(EDIT_SHIPPING_ADDRESS_POPUP_CLOSE)
    await this.click(EDIT_SHIPPING_ADDRESS_POPUP_CLOSE)
  }

  async convertAddressFromBillingAndShippingInfoToRegular (addressInBillingAndShipFormat) {
    let nonTaxFullAddress = addressInBillingAndShipFormat.split(',')
    let nonTaxState = nonTaxFullAddress[1].slice(-2)
    nonTaxState = await CommonUtils.convertState(nonTaxState, 'name')
    nonTaxFullAddress.push(nonTaxState)
    nonTaxFullAddress[1] = nonTaxFullAddress[1].slice(0, -2)
    nonTaxFullAddress = nonTaxFullAddress.map(string => string.trim())
    return nonTaxFullAddress
  }

  async getTaxBannerText () {
    return this.getText(TAX_BANNER)
  }
}
