// @flow
import BasePage from '@page-objects/BasePage'
import CommonUtils from '@core-libs/CommonUtils'

const DATE_PICKER_TITLE = '//*[@id="edit-subscription"]/div/div/h3'
const DATE_PICKER_CALENDAR = '//*[@id="edit-calendar"]'
const ALL_DATES_LIST = '//table[@class="ui-datepicker-calendar"]//td'
const AVAILABLE_DATE_LIST = '//div[@id="edit-calendar"]//table[@class="ui-datepicker-calendar"]//td[@class=" " or @class=" ui-datepicker-days-cell-over  ui-datepicker-current-day" or  @class=" ui-datepicker-week-end " or @class ="  ui-datepicker-current-day" or @class=" ui-datepicker-week-end ui-datepicker-days-cell-over  ui-datepicker-current-day"]'
const NEXT_MONTH_ARROW = '//*[@id="edit-calendar"]/div/div/a[2]'
const DATE_PICKER_SUBMIT_BUTTON = '//*[@id="subscriptionOrderForm2"]/div[1]/button[2]'
const DATE_PICKER_MONTH = '//*[@class="ui-datepicker-month"]'
const DATE_PICKER_YEAR = '//*[@class="ui-datepicker-year"]'
const DATE_PICKER_REWARDS_EXPIRY_MESSAGE = '//*[@id="edit-subscription"]/div/div/span[@class="start-date-message error hide show"]'
const DATE_PICKER_CANCEL_BUTTON = '//*[@id="subscriptionOrderForm2"]/div[1]/button[1]'
const DATE_PICKER_PAST_AUTOSHIP_TITLE = '//*[@id="warning-subscription"]/div/div/p'
const DATE_PICKER_PAST_AUTOSHIP_UPDATE_BUTTON = '//*[@id="subscriptionOrderForm3"]/div[1]/button[2]'

export default class DatePickerOptaviaPremier extends BasePage {
  async verifyPageIsLoaded () {
    await this.waitForDisplayed(DATE_PICKER_TITLE)
    await this.waitForDisplayed(DATE_PICKER_CALENDAR)
  }

  async verifyPastAutoship () {
    await this.waitForDisplayed(DATE_PICKER_PAST_AUTOSHIP_TITLE)
  }

  async isCalendarPickerDisplayed () {
    return this.locatorFound(DATE_PICKER_PAST_AUTOSHIP_UPDATE_BUTTON, 3)
  }

  async getNextOrderDate (day: any) {
    return this.getTextByIndex(AVAILABLE_DATE_LIST, day)
  }

  async getCountofDateList () {
    return this.getCountOfArrayList(AVAILABLE_DATE_LIST)
  }

  async clickAvailableDate (key: any) {
    await this.clickByIndex(AVAILABLE_DATE_LIST, key)
  }

  async selectDate () {
    await this.click(ALL_DATES_LIST)
  }

  async clickNextMonthArrowBtn () {
    await this.click(NEXT_MONTH_ARROW)
  }

  async clickDatePickerSubmitBtn () {
    await this.click(DATE_PICKER_SUBMIT_BUTTON)
  }

  async clickDatePickerCanceltBtn () {
    await this.click(DATE_PICKER_CANCEL_BUTTON)
  }

  async getCalendarMonthText () {
    return this.getText(DATE_PICKER_MONTH)
  }

  async getCalendarYearText () {
    return this.getText(DATE_PICKER_YEAR)
  }

  async getRewardsExpiryMessageText () {
    return this.getText(DATE_PICKER_REWARDS_EXPIRY_MESSAGE)
  }

  async isRewardsExpiryMessageTextDisplayed () {
    return this.locatorFound(DATE_PICKER_REWARDS_EXPIRY_MESSAGE)
  }

  /**
   *
   * @returns {Promise<string[]>}
   * @constructor
   */
  async updateNextOrderDate (nextOrderDate: Date): Promise<string[]> {
    let fullMonthDate = await CommonUtils.getDateFullMonth(nextOrderDate)
    let arrayResult = fullMonthDate.split('-')
    let monthYear = arrayResult[1]
    // Select Month
    for (let m = 0; m < 13; m++) {
      let selectMonth = await this.getCalendarMonthText()
      let selectYear = await this.getCalendarYearText()
      let actualMonthYear = `${selectMonth} ${selectYear}`
      if (monthYear === actualMonthYear) {
        // Select Day
        let nextMonthDay = await CommonUtils.getDayFromDate(nextOrderDate)
        let dLength = await this.getCountofDateList()
        for (let d = 0; d < dLength; d++) {
          let days = await this.getNextOrderDate(d)
          if (days === nextMonthDay) {
            await this.clickAvailableDate(d)
            // Verify Message
            let rewardsExpiryStatus = await this.isRewardsExpiryMessageTextDisplayed()
            if (rewardsExpiryStatus) {
              let rewardsExpiryMessage = await this.getRewardsExpiryMessageText()
              return [nextOrderDate, rewardsExpiryMessage]
            } else {
              return [nextOrderDate]
            }
          }
        }
      } else {
        await this.clickNextMonthArrowBtn()
      }
    }
  }

  async clickDatePickerPastAutoshipUpdateBtn () {
    await this.click(DATE_PICKER_PAST_AUTOSHIP_UPDATE_BUTTON)
  }

  async isUpdateBtnDisplayed () {
    return this.locatorFound(DATE_PICKER_PAST_AUTOSHIP_UPDATE_BUTTON)
  }
}
