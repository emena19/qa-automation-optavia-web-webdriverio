// @flow
import BasePage from '@page-objects/BasePage'
import AddressDetailsPage from '@page-objects/myaccount/AddressDetailsPage'

const ADDRESS_BOOK_GLOBAL_MESSAGE = '//*[@id="globalMessages"]/ul/li/ul/li'
const ADDRESS_BOOK_SECTION_TITLE = '//*[@class="manage-addresses"]/descendant::h1[contains(text(),"Address Book")]'
const ADDRESS_BOOK_DEFAULT_ADDRESS = '//*[@class="address"]/div[1]'
const ADDRESS_BOOK_DEFAULT_ADDRESS_EDIT_BUTTON = '//*[@class="address"]/div[2]/a'
const ADDRESS_BOOK_NON_DEFAULT_SHIP_ADDRESS_TO_DEFAULT_BTN = '//*[@class="address block"]/descendant::a[contains(text(),"Set as default")]'
const ADDRESS_BOOK_NON_DEFAULT_SHIP_ADDRESS_REMOVE_BTN = '//*[@class="button button-small button-alternate remove removeAddressButton color-green border-green"][contains(text(),"Remove")]'
const ADDRESS_BOOK_NON_DEFAULT_SHIP_ADDRESS = '//*[@class="address block"]/div[1]'
const ADDRESS_BOOK_ADD_ADDRESS = '//*[@id="content"]/div/div/section/div[2]/div/div/div[3]/a'
const ADDRESS_BOOK_ADD_NEW_ADDRESS = '//a[contains(text(), "Add Address")]'
const ADDRESS_BOOK_EDIT_ADDRESS_BTN_LIST = '//*[@class="manage-addresses"]/descendant::a[contains(text(),"Edit")]'
const ADDRESS_BOOK_NON_DEFAULT_SHIP_ADDRESS_REMOVE_BTN_LIST = '//*[@class="manage-addresses"]/descendant::a[contains(text(),"Remove")]'
const ADDRESS_BOOK_NON_DEFAULT_EDIT_ADDRESS_BTN = '//a[contains(text(),"Set as default")]/../a[contains(text(),"Edit")]'
// Remove Shipping Info Pop Up window
const ADDRESS_BOOK_REMOVE_SHIP_ADDRESS_POPUP = '//*[@class="additional-addresses"]/div[2]/div'
const ADDRESS_BOOK_REMOVE_SHIP_ADDRESS_POPUP_YES_BUTTON = '//*[@class="additional-addresses"]/div[2]/descendant::a[contains(text(),"Yes")]'
const ADDRESS_BOOK_REMOVE_SHIP_ADDRESS_POPUP_NO_BUTTON = '//*[@class="additional-addresses"]/div[2]/descendant::a[contains(text(),"No")]'
const ADDRESS_BOOK_SPINNER = '//*[@id="add-to-cart-spinner"]'

export default class AddressBookPage extends BasePage {
  async verifyPageIsLoaded () {
    await this.waitForDisplayed(ADDRESS_BOOK_SECTION_TITLE)
  }

  async getPageTitle () {
    return this.getCurrentPageTitle()
  }

  async getPageUrl () {
    return this.getCurrentPageUrl()
  }

  async getYourProfileSectionText () {
    return this.getText(ADDRESS_BOOK_SECTION_TITLE)
  }

  async getYourGlobalMessage () {
    return this.getText(ADDRESS_BOOK_GLOBAL_MESSAGE)
  }

  async getDefaultShippingAddress () {
    return this.getText(ADDRESS_BOOK_DEFAULT_ADDRESS)
  }

  async getNonDefaultShippingAddress () {
    return this.getText(ADDRESS_BOOK_NON_DEFAULT_SHIP_ADDRESS)
  }

  async clickDefaultAddressEditButton () {
    await this.click(ADDRESS_BOOK_DEFAULT_ADDRESS_EDIT_BUTTON)
  }

  async clickSetToDefaultAddress () {
    await this.click(ADDRESS_BOOK_NON_DEFAULT_SHIP_ADDRESS_TO_DEFAULT_BTN)
  }

  async clickEditButtonByIndex (keys) {
    await this.waitForDisplayed(ADDRESS_BOOK_EDIT_ADDRESS_BTN_LIST)
    await this.clickByIndex(ADDRESS_BOOK_EDIT_ADDRESS_BTN_LIST, keys)
  }

  async clickNonDefaultEditBtn () {
    await this.click(ADDRESS_BOOK_NON_DEFAULT_EDIT_ADDRESS_BTN)
  }

  async clickRemoveNonDefaultShipAddress () {
    await this.click(ADDRESS_BOOK_NON_DEFAULT_SHIP_ADDRESS_REMOVE_BTN)
  }

  async clickRemoveNonDefaultShipAddressByIndex (keys) {
    await this.click(ADDRESS_BOOK_NON_DEFAULT_SHIP_ADDRESS_REMOVE_BTN_LIST, keys)
  }

  async clickAddAddressButton () {
    await this.click(ADDRESS_BOOK_ADD_ADDRESS)
  }

  async clickAddNewAddressButton () {
    await this.click(ADDRESS_BOOK_ADD_NEW_ADDRESS)
  }

  async verifyNonDefaultShipAddressRemoveButtonIsDisplayed () {
    await this.waitForDisplayed(ADDRESS_BOOK_NON_DEFAULT_SHIP_ADDRESS_REMOVE_BTN)
  }

  // REMOVE ADDRESS POP UP WINDOW
  async verifyRemoveAddressPopUpIsLoaded () {
    await this.waitForDisplayed(ADDRESS_BOOK_REMOVE_SHIP_ADDRESS_POPUP)
  }

  async clickRemoveShipAddress () {
    await this.waitForDisplayed(ADDRESS_BOOK_REMOVE_SHIP_ADDRESS_POPUP_YES_BUTTON)
    await this.click(ADDRESS_BOOK_REMOVE_SHIP_ADDRESS_POPUP_YES_BUTTON)
  }

  async clickRemoveShipAddressNoBtn () {
    await this.waitForDisplayed(ADDRESS_BOOK_REMOVE_SHIP_ADDRESS_POPUP_NO_BUTTON)
    await this.click(ADDRESS_BOOK_REMOVE_SHIP_ADDRESS_POPUP_NO_BUTTON)
  }

  async isSpinnerPresent () {
    return this.locatorFound(ADDRESS_BOOK_SPINNER)
  }

  async isSetToDefaultButtonPresent () {
    return this.locatorFound(ADDRESS_BOOK_NON_DEFAULT_SHIP_ADDRESS_TO_DEFAULT_BTN)
  }

  async setDefaultAddressOrAddAddressIfNeeded (market: number, address: Object) {
    let setDefaultDisplayed = await this.isSetToDefaultButtonPresent()
    if (setDefaultDisplayed) {
      await this.clickSetToDefaultAddress()
    } else {
      await this.createShippingAddressAndSetAsDefault(market, address)
    }
  }

  async createShippingAddressAndSetAsDefault (market: number, address: Object) {
    await this.clickAddAddressButton()
    let addressDetailsPage = new AddressDetailsPage(this.driver)
    await addressDetailsPage.verifyPageIsLoaded()
    if (market === 1) {
      await addressDetailsPage.createShippingAddress(address.CUSTOMER_FIRST_NAME, address.CUSTOMER_LAST_NAME, address.SHIPPING_ADDRESS_LINE_1, address.SHIPPING_CITY, address.SHIPPING_FULL_STATE_NAME, address.SHIPPING_ZIP, address.SHIPPING_COUNTRY_CODE, address.SHIPPING_PHONE)
    } else if (market === 2) {
      await addressDetailsPage.createShippingAddressSG(address.SHIPPING_ADDRESS_LINE_1, address.SHIPPING_ADDRESS_LINE_2, address.SHIPPING_ZIP, address.SHIPPING_COUNTRY_CODE, address.SHIPPING_PHONE)
    } else {
      await addressDetailsPage.createShippingAddressHK(address.SHIPPING_ADDRESS_LINE_1, address.SHIPPING_ADDRESS_LINE_2, address.SHIPPING_DISTRICT, address.SHIPPING_COUNTRY_CODE, address.SHIPPING_PHONE)
    }
    await addressDetailsPage.clickAddAddressSaveButton()
    await this.verifyPageIsLoaded()
    await this.clickSetToDefaultAddress()
  }
}
