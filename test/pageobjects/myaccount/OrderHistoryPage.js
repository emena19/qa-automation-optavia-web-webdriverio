// @flow
import {By} from 'selenium-webdriver'
import BasePage from '../BasePage'

const ORDER_HISTORY_SECTION_TITLE = '//*[contains(@class,"accountBodyContentSlot")]/div/h1'
const ORDER_LIST = '//*[@class="order-list"]//div/a'
const ORDER_RECENT_WITH_P_ICON = '//*[@class="order-list"]/table//i'
const ORDER_HISTORY_TOTAL = '//*[@id="orderTotals"]/tfoot/descendant::td[contains(text(),"Total")]/following-sibling::td'
// const ORDER_HISTORY_PAGINATION_RIGHT_ARROW = '//*[@id="content"]/div/div/section/div[2]/div/div[3]/div/ol/li[5]/a/i')
const ORDER_HISTORY_PAGINATION_RIGHT_ARROW = '//div[@class="pagination-bar row bottom clearfix"]//i[@class="icon icon-arrow-alt-right"]//parent::a'
const ORDER_HISTORY_PAGINATION_LEFT_ARROW = '//div[@class="pagination-bar row bottom clearfix"]//i[@class="icon icon-arrow-alt-left"]//parent::a'
const ORDER_HISTORY_VIEW_LINK = '//*[@id="AccountOrderHistoryComponent-ViewOrderAction"]/a'
const ORDER_HISTORY_SORTING_FILTER = '//*[@id="sortOptions1"]'
const ORDER_HISTORY_DEFAULT_VALUE = '//*[@id="sortOptions1"]/option[3]'
const ORDERS_PER_PAGE = 5
// const ORDER_HISTORY_PAGINATION_PARTIAL_XPATH = '//*[@id="content"]/div/div/section/div[2]/div/div[3]/div/ol/li')
const ORDER_HISTORY_ORDER_NUMBER = '//div[@class="order-list"]//td[@data-header="Order Number"]'
const ORDER_HISTORY_SORT_BY_LABEL = '//form[@id="sort_form1"]//label[text()="Sort by"]'
const ORDER_HISTORY_SORT_OPTIONS_STYLE = '//form[@id="sort_form1"]//div'
const ORDER_HISTORY_PAGINATION_OPTIONS = '//div[@class="pagination-bar row bottom clearfix"]/div/ol'
const ORDER_HISTORY_PAGINATION_NOT_BOLD_NUMBER = '//div[@class="pagination-bar row bottom clearfix"]//ol[@class="pagination"]//li[not(@class="active") and not(@class="disabled")]'
// Product info
const PRODUCT_QTY = '//div[@class="order-quantity-total"]/div[@class="item-qty"]'
const PRODUCT_UNIT_PRICE = '//div[@class="order-unit-price"]'
const PRODUCT_PRICE = '//div[@class="order-quantity-total"]/div[@class="item-total"]'
const PRODUCTS_LIST_INFO_ARRAY = '//div[@class="product-info orderProduct--name"]/a/p'

export default class OrderHistoryPage extends BasePage {
  async verifyPageIsLoaded () {
    await this.waitForDisplayed(ORDER_HISTORY_SECTION_TITLE)
  }

  async getPageTitle () {
    return this.getCurrentPageTitle()
  }

  async getPageUrl () {
    return this.getCurrentPageUrl()
  }

  async getYourProfileSectionText () {
    return this.getText(ORDER_HISTORY_SECTION_TITLE)
  }

  async clickOnOrder () {
    await this.waitForDisplayed(ORDER_LIST)
    await this.click(ORDER_LIST)
  }

  async verifyPIconDisplays () {
    await this.waitForDisplayed(ORDER_RECENT_WITH_P_ICON)
  }

  async getOrderTotalText () {
    return this.getText(ORDER_HISTORY_TOTAL)
  }

  async getProductArrayList () {
    await this.waitForDisplayed(PRODUCTS_LIST_INFO_ARRAY, 7)
    return this.getElementsTextArrayList(PRODUCTS_LIST_INFO_ARRAY)
  }

  async getProductUnitPriceArrayList () {
    let unitPrice = await this.getElementsTextArrayList(PRODUCT_UNIT_PRICE)
    for (let i = 0; i < unitPrice.length; i++) {
      unitPrice[i] = unitPrice[i].substring(12)
    }
    return unitPrice
  }

  async getProductQtyArrayList () {
    let qty = await this.getElementsTextArrayList(PRODUCT_QTY)
    for (let i = 0; i < qty.length; i++) {
      qty[i] = qty[i].substring(5)
    }
    return qty
  }

  async getProductPriceArrayList () {
    return this.getElementsTextArrayList(PRODUCT_PRICE)
  }

  async clickOnPaginationRightArrow () {
    await this.waitForDisplayed(ORDER_HISTORY_PAGINATION_RIGHT_ARROW, 3)
    return this.click(ORDER_HISTORY_PAGINATION_RIGHT_ARROW)
  }

  async verifyPaginationRightArrowIsEnabled () {
    await this.waitForDisplayed(ORDER_HISTORY_PAGINATION_RIGHT_ARROW, 3)
    return this.driver.findElement(ORDER_HISTORY_PAGINATION_RIGHT_ARROW).isEnabled()
  }

  async clickOnPaginationLeftArrow () {
    await this.waitForDisplayed(ORDER_HISTORY_PAGINATION_LEFT_ARROW, 3)
    await this.click(ORDER_HISTORY_PAGINATION_LEFT_ARROW)
  }

  async verifyPaginationLightArrowIsEnabled () {
    await this.isElementEnabled(ORDER_HISTORY_PAGINATION_LEFT_ARROW)
  }

  async getPaginationLenght () {
    const ORDER_HISTORY_PAGINATION_PARTIAL_XPATH = '//*[@id="content"]/div/div/section/div[2]/div/div[3]/div/ol/li'
    let pagination = await this.getElementsAttributeValueArrayList(ORDER_HISTORY_PAGINATION_PARTIAL_XPATH, 'li')
    return pagination.length
  }

  async clickOnOrderFromHistoryTable (orderNumber) {
    const viewLink = By.className('link inverse-link')
    let ordersPerPage = 4
    let pagination = await this.getPaginationLenght()
    let paginationFullXpath = '//*[@id="content"]/div/div/section/div[2]/div/div[3]/div/ol/li' + '[' + pagination + ']/a'
    const paginationArrow = paginationFullXpath
    for (let i = 0; i < 5; i++) {
      let links = await this.getElementsAttributeValueArrayList(viewLink, 'href')
      if ((links[i].includes(orderNumber) === false) && (i === ordersPerPage)) {
        this.driver.sleep(3000)
        let arrow = await this.driver.findElement(paginationArrow).isEnabled()
        if (arrow) {
          await this.moveToLocatorPosition(paginationArrow)
          await this.click(paginationArrow)
          i = -1
        } else {
          // eslint-disable-next-line no-console
          console.log('END OF PAGINATION')
        }
      } else if (links[i].includes(orderNumber)) {
        // const foundOrder = By.partialLinkText(links[i])
        await this.clickByIndex(ORDER_HISTORY_VIEW_LINK, i)
        break
      }
    }
  }

  async verifySortingFilterIsDisplayed () {
    await this.waitForDisplayed(ORDER_HISTORY_DEFAULT_VALUE, 3)
  }

  async verifySortingFilterDefaultValue () {
    let attribute = await this.getAttributesValue(ORDER_HISTORY_DEFAULT_VALUE, 'selected')
    let text = await this.getText(ORDER_HISTORY_DEFAULT_VALUE)
    return [attribute, text]
  }

  async selectOptionFromDropdown (keys: string) {
    await this.selectDropDownByText(ORDER_HISTORY_SORTING_FILTER, keys)
  }

  async getColumnValuesFromOrderHistoryTable (columnNumber: number) {
    let pagination = await this.getPaginationLenght()
    let paginationFullXpath = '//*[@id="content"]/div/div/section/div[2]/div/div[3]/div/ol/li' + '[' + pagination + ']/a'
    const paginationArrow = paginationFullXpath
    let orders = []
    let orderNumber
    let i = 1
    while (i <= ORDERS_PER_PAGE) {
      let rows = await this.getOrderNumberCount(i)
      let orderNumberXpath = '//*[@id="content"]/div/div/section/div[2]/div/div[2]/table/tbody/tr[' + i + ']/td[' + columnNumber + ']'
      orderNumber = await this.getText(orderNumberXpath)
      orders.push(orderNumber)
      let arrow = await this.getCountOfArrayList(paginationArrow)
      if (arrow > 0 && i === ORDERS_PER_PAGE) {
        await this.moveToLocatorPosition(paginationArrow)
        await this.click(paginationArrow)
        i = 0
      } else if (rows < ORDERS_PER_PAGE && arrow === 0) {
        if (i === rows) {
          break
        }
      }
      i++
    }
    return orders
  }

  async getOrderNumberCount () {
    return this.getCountOfArrayList(ORDER_HISTORY_ORDER_NUMBER)
  }

  async getOrderNumberTextList () {
    return this.getElementsTextArrayList(ORDER_HISTORY_ORDER_NUMBER)
  }

  async verifySortByLabelIsDisplayed () {
    await this.switchToDefaultFrame()
    await this.waitForDisplayed(ORDER_HISTORY_SORT_BY_LABEL)
  }

  async verifySortOptionStyleIsDisplayed () {
    await this.waitForDisplayed(ORDER_HISTORY_SORT_OPTIONS_STYLE)
  }

  async verifyPaginationOptionIsDisplayed () {
    await this.waitForDisplayed(ORDER_HISTORY_PAGINATION_OPTIONS)
  }

  async clickOnBoldPaginationTextByIndex (index: any) {
    await this.waitForDisplayed(ORDER_HISTORY_PAGINATION_NOT_BOLD_NUMBER)
    await this.clickByIndex(ORDER_HISTORY_PAGINATION_NOT_BOLD_NUMBER, index)
  }

  async getASCSSortedDatesArray (datesArray: any) {
    return datesArray.sort(
      function (date1, date2) {
        return new Date(date1) - new Date(date2)
      })
  }

  async clickOnLastViewLink (index: any) {
    await this.waitForDisplayed(ORDER_HISTORY_VIEW_LINK)
    let count = await this.getCountOfArrayList(ORDER_HISTORY_ORDER_NUMBER)
    await this.clickByIndex(ORDER_HISTORY_VIEW_LINK, count - 1)
  }
}
