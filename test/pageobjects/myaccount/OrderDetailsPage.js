// @flow
import BasePage from '../BasePage'

const ORDER_DETAILS_TITLE = '//*[@id="content"]/descendant::h1[2]'
const ORDER_DETAILS_NUMBER = '//div[@class="order-code"]'
const ORDER_DETAILS_DATE = '//*[@class="order-detail"]'
const ORDER_DETAILS_STATUS = '//div[@class="order-status"]/div'
const ORDER_DETAILS_TYPE = '//div[@class="order-detail"]/div[2]'
const ORDER_DETAILS_DELIVERY_METHOD = '(//*[@class="reset-list shipping-method-details"]/li)[2]'
const ORDER_DETAILS_DELIVERY_CHARGE = `//*[@id="orderTotals"]/tbody/descendant::td[contains(text(),"Delivery Charge")]/following-sibling::td`
const ORDER_DETAILS_APPLIED_REWARDS = '//*[@id="orderTotals"]/tbody/descendant::td[contains(text(),"Rewards")]/following-sibling::td'
const ORDER_DETAILS_ORDER_TOTAL = '//*[@class="ordertotalAmt"]'
const ORDER_DETAILS_SUB_TOTAL = '//td[@class="subtotalAmt"]'
const ORDER_DETAILS_REORDER_BTN = '//*[@class="button-set"]/descendant::a[contains(text(),"Reorder")]'
const ORDER_DETAILS_RETURN_BTN = '//a[@class="btn-start__return button button-alternate-modified showLoaderOnClick"]'
const ORDER_DETAILS_COMPLETE_ORDER_TEXT = '//*[@class="orderList"]/table/descendant::p[@class="product-name"]'
const ORDER_DETAILS_COMPLETE_REORDER_TEXT = '//*[@class="order-details__body-section2"]//*[@class="product-name"]'
const ORDER_DETAILS_ITEMS_TO_DELIVER_NAME_LIST = 'table[class*="shopping-cart-items"] p'
const ORDER_DETAILS_GST_AMT = '//*[@class="order-totals details-block"]//td[@class="total taxAmt"]'
const ORDER_DETAILS_ELIGIBLE_FOR_REWARDS_TEXT = '//*[@class="table totals-table"]/tbody/tr/td[contains(text(),"Eligible For New Rewards:")]/following-sibling::td'
const ORDER_DETAILS_EARNING_THIS_ORDER_TEXT = '//*[@class="table totals-table"]/tbody/tr[2]/td[2]'
const ORDER_DETAILS_ESTIMATED_REWARDS_LINK = `//*[@class="table totals-table"]/tbody/tr/td[text()="Earned on this order"]/following-sibling::td/a`
const FREE_MEALS_INCLUDED_TEXT = '(//*[@id="content"]/descendant::ul[contains(@class,"styled-list")])[2]'
const FREE_ITEMS_NAME = '//div[@class="freemeal-description styled-list"]//p[@class="product-name"]'
const ORDER_STATUS_NAMES = '//div[@class="order-details__status-data-name"]'
const ORDER_STATUS_DATE = '//div[@class="order-details__status-data-date"]'
const ORDER_RETURN_DATE = '//*[@class="return-items-section__info-detail"]'
const ORDER_DETAILS_BACK_BUTTON = '//*[@class="order-details__header-back"]'

export default class OrderDetailsPage extends BasePage {
  async verifyPageIsLoaded () {
    await this.waitForDisplayed(ORDER_DETAILS_TITLE)
  }

  async getPageTitle () {
    return this.getCurrentPageTitle()
  }

  async getPageUrl () {
    return this.getCurrentPageUrl()
  }

  async getOrderDetailsSectionText () {
    return this.getText(ORDER_DETAILS_TITLE)
  }

  async getOrderNumber () {
    return this.getText(ORDER_DETAILS_NUMBER)
  }

  async getOrderDate () {
    let rawDate = (await this.getText(ORDER_DETAILS_DATE)).split('\n')[1].split(' ')
    return rawDate[2] + ' ' + rawDate[3] + ' ' + rawDate[4]
  }

  async getOrderReturnDate () {
    return this.getText(ORDER_RETURN_DATE)
  }

  async getOrderStatus () {
    return this.getText(ORDER_DETAILS_STATUS)
  }

  async getOrderType () {
    return this.getText(ORDER_DETAILS_TYPE)
  }

  async getOrderDetailsDeliveryChargeText () {
    return this.getText(ORDER_DETAILS_DELIVERY_CHARGE)
  }

  async getOrderDetailsSubTotalText () {
    return this.getText(ORDER_DETAILS_SUB_TOTAL)
  }

  async getOrderDetailsTotalText () {
    return this.getText(ORDER_DETAILS_ORDER_TOTAL)
  }

  async getOrderDetailsDeliveryMethodText () {
    return this.getText(ORDER_DETAILS_DELIVERY_METHOD)
  }

  async getPremierAppliedRewardsText () {
    return this.getText(ORDER_DETAILS_APPLIED_REWARDS)
  }

  async clickReorderButton () {
    return this.click(ORDER_DETAILS_REORDER_BTN)
  }

  async clickReturnButton () {
    await this.click(ORDER_DETAILS_RETURN_BTN)
  }

  async isReturnButtonDisplayed () {
    return this.locatorFound(ORDER_DETAILS_RETURN_BTN)
  }

  async isReturnButtonDisabled () {
    await this.waitForDisplayed(ORDER_DETAILS_RETURN_BTN, 10)
    return this.hasAttributePresent(ORDER_DETAILS_RETURN_BTN, 'disabled')
  }

  async isPremierAppliedRewardsPresent () {
    return this.locatorFound(ORDER_DETAILS_APPLIED_REWARDS, 1)
  }

  async getCompleteOrderText () {
    return this.getElementsTextArrayList(ORDER_DETAILS_COMPLETE_ORDER_TEXT)
  }

  async getCompleteReOrderText () {
    return this.getElementsTextArrayList(ORDER_DETAILS_COMPLETE_REORDER_TEXT)
  }

  async getItemsToDeliverNameList () {
    return this.getElementsTextArrayList(ORDER_DETAILS_ITEMS_TO_DELIVER_NAME_LIST)
  }

  async getGSTAmt () {
    return this.getText(ORDER_DETAILS_GST_AMT)
  }

  async verifyGSTAmtIsDisplayed () {
    await this.waitForDisplayed(ORDER_DETAILS_GST_AMT)
  }

  async getEligibleRewardsText () {
    return this.getText(ORDER_DETAILS_ELIGIBLE_FOR_REWARDS_TEXT)
  }

  async getEarningThisOrderText () {
    return this.getText(ORDER_DETAILS_EARNING_THIS_ORDER_TEXT)
  }

  async isEstimatedRewardsLinkDisplayed () {
    return this.locatorFound(ORDER_DETAILS_ESTIMATED_REWARDS_LINK)
  }

  async getFreeMealsText () {
    return this.getText(FREE_MEALS_INCLUDED_TEXT)
  }

  async getFreeItemsNames () {
    return this.getElementsTextArrayList(FREE_ITEMS_NAME)
  }

  async getOrderStatusNames () {
    return this.getText(ORDER_STATUS_NAMES)
  }

  async getOrderStatusDate () {
    return this.getText(ORDER_STATUS_DATE)
  }

  async isOrderStatusChecked (status: string) {
    let index
    switch (status) {
      case 'Ordered': index = 1
        break
      case 'Shipped': index = 2
        break
      case 'Arrives': index = 3
        break
    }
    let element = `//div[@class="order-details__status"]/img[${index}]`
    let text = await this.getAttributesValue(element, 'src')
    return text.includes('curentStep')
  }

  async clickBackButton () {
    await this.waitForDisplayed(ORDER_DETAILS_BACK_BUTTON)
    await this.click(ORDER_DETAILS_BACK_BUTTON)
  }
}
