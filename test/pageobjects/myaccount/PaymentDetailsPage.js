// @flow

import BasePage from '../BasePage'
import MyAccountLeftNavigation from './MyAccountLeftNavigation'
import PaymentAndBillingAddressTile from '../checkout/PaymentAndBillingAddressTile'

const PAYMENT_DETAILS_GLOBAL_MESSAGE = '//*[@id="globalMessages"]/ul/li/ul/li'
const PAYMENT_DETAILS_SECTION_TITLE = '//*[contains(@class,"accountBodyContentSlot")]/div/h1'
const NO_SAVED_PAYMENT_DETAILS_MESSAGE = '//p[contains(text(),"No Saved Payment Details")]'
const DEFAULT_CARD_TITLE = '//*[@class="default"][contains(text(),"Default")]'
const PAYMENT_INFO_DEFAULT_ADDRESS = '//*[@class="col-md-9 pull-right main-content"]/div[2]/div/div[1]'
const PAYMENT_INFO_DEFAULT_NUMBER = '//div[@class="payment-item row"][1]//dl/dd[1]'
const PAYMENT_INFO_NON_DEFAULT_ADDRESS = '//*[@class="col-md-9 pull-right main-content"]/div[2]/div/div[2]/div/div'
const PAYMENT_NON_DEFAULT_SET_AS_DEFAULT_BTN = '//*[@class="col-md-9 pull-right main-content"]/div[2]/div/div[2]/descendant::button[contains(text(),"Set as default")]'
const PAYMENT_NON_DEFAULT_REMOVE_BTN = '//*[@class="col-md-9 pull-right main-content"]/div[2]/div/div[2]/descendant::button[contains(text(),"Remove")]'
const PAYMENT_FIRST_NON_DEFAULT_SET_AS_DEFAULT_BTN = '(//button[contains(text(),"Set as default")])[1]'
// const PAYMENT_INFO_SAVE_DEFAULT_ADDRESS_BTN = '//*[@id="myWalletForm"]/descendant::button[contains(text(),"Save Payment Info")]')
const PAYMENT_INFO_SAVE_DEFAULT_ADDRESS_BTN = '//*[@id="myWalletForm"]//button[contains(text(),"Save Payment")]'
const PAYMENT_DETAILS_ADD_NEW_CARD_BTN = '//a[contains(text(),"Add a new card")]'
const PAYMENT_DETAILS_MY_PAYMENT_DETAILS = '//h1[contains(text(),"My Payment Details")]'
const PAYMENT_DETAILS_PAYMENT_DETAILS = '//h1[contains(text(),"Payment Details")]'
const PAYMENT_BILLING_ADDRESS_GLOBAL_MESSAGE = '#globalMessages'
const GLOBAL_MESSAGE_SUCCESS = '//*[@id="globalMessages"]/ul/li/ul/li'
const BILLING_ADDRESS_DROP_DOWN = '#savedBillingAddress'
const PAYMENT_DETAILS_CARD_SECTION = '//*[@class="payment-item row"]'
// Remove Payment Info Pop Up window
const PAYMENT_DETAILS_REMOVE_POPUP_BUTTON = '#confirmRemoval'
const PAYMENT_DETAILS_CANCEL_POPUP_BUTTON = '//button[contains(text(),"Cancel")]'
const PAYMENT_DETAILS_POPUP = '//*[@id="modal-alert"]/div'
const PAYMENT_DETAILS_POPUP_TITLE = '//*[@id="modal-alert"]/div/h3'
const PAYMENT_REMOVE_BTN = '//button[contains(@class, "button-remove")]'

const PAYMENT_DETAILS_NAME_ON_CARD = '#nameOnCard'
const PAYMENT_CARD_TYPE_DROP_DOWN = '#cardType'
const PAYMENT_DETAILS_CARD_NUMBER = '#cardNumber'
const PAYMENT_DETAILS_CARD_VERIFICATION = '#cardCVV'
const PAYMENT_DETAILS_MONTH_DROP_DOWN = '#expiryMonth'
const PAYMENT_DETAILS_YEAR_DROP_DOWN = '#expiryYear'
const BILLING_ADDRESS_FIRST_NAME = '#firstName'
const BILLING_ADDRESS_LAST_NAME = '#lastName'
const BILLING_ADDRESS_ADDRESS_LINE1 = '#line1'
const BILLING_ADDRESS_CITY = '#townCity'
const BILLING_ADDRESS_STATE = '#regionIso'
const BILLING_ADDRESS_ZIP = '#postcode'
const PAYMENT_BILLING_ADDRESS_EMPTY_MESSAGE_LIST = '.emptyMessage'
// payment SG

const PAYMENT_SAVE_INFO = '#lastInTheForm'
const PAYMENT_DETAILS_CANCEL_BTN = '//div[contains(@class,"form-actions")]//a[contains(text(),"Cancel")]'
const BILLING_ADDRESS_ZIP_ERROR_MESSAGE = '#postcode-error'
const PAYMENT_DETAILS_DEFAULT_REMOVE_BTN = '//h4[@class="default"]//parent::div/div[@class="buttons-set"]/form[contains(@id,"removePaymentDetails")]'
const PAYMENT_ASSOCIATED_MESSAGE = '//h4[contains(text(),"This payment method is associated with an active autoship")]'

export default class PaymentDetailsPage extends BasePage {
  async verifyPageIsLoaded () {
    await this.waitForDisplayed(PAYMENT_DETAILS_SECTION_TITLE)
  }

  async verifyRemovePaymentInfoPopUpIsLoaded () {
    await this.waitForDisplayed(PAYMENT_DETAILS_POPUP)
    await this.waitForDisplayed(PAYMENT_DETAILS_POPUP_TITLE)
  }

  async getPageTitle () {
    return this.getCurrentPageTitle()
  }

  async getPageUrl () {
    return this.getCurrentPageUrl()
  }

  async getYourProfileSectionText () {
    return this.getText(PAYMENT_DETAILS_SECTION_TITLE)
  }

  async getYourGlobalMessage () {
    return this.getText(PAYMENT_DETAILS_GLOBAL_MESSAGE)
  }

  async getDefaultPaymentLastFourDigits () {
    return this.getText(PAYMENT_INFO_DEFAULT_NUMBER)
  }

  async getNonDefaultAddress () {
    return this.getText(PAYMENT_INFO_NON_DEFAULT_ADDRESS)
  }

  async getDefaultAddress () {
    return this.getText(PAYMENT_INFO_DEFAULT_ADDRESS)
  }

  async clickSavePaymentInfoButton () {
    await this.moveToLocatorPosition(PAYMENT_INFO_SAVE_DEFAULT_ADDRESS_BTN)
    await this.click(PAYMENT_INFO_SAVE_DEFAULT_ADDRESS_BTN)
  }

  async clickSavePaymentInfoButtonSG () {
    await this.moveToLocatorPosition(PAYMENT_SAVE_INFO)
    await this.click(PAYMENT_SAVE_INFO)
  }

  async verifyCancelButtonIsDisplayed () {
    await this.waitForDisplayed(PAYMENT_DETAILS_CANCEL_BTN)
  }

  async clickCancelButton () {
    await this.moveToLocatorPosition(PAYMENT_DETAILS_CANCEL_BTN)
    await this.click(PAYMENT_DETAILS_CANCEL_BTN)
  }

  async createPaymentDetails (name: string, cardType: string, cardNumber: string, cardVerificationNumber: string, month: string, year: string) {
    await browser.pause(4000) // added sleep to fasttrack form submission
    await this.sendKeys(PAYMENT_DETAILS_NAME_ON_CARD, name)
    await this.selectDropDownByText(PAYMENT_CARD_TYPE_DROP_DOWN, cardType)
    await this.sendKeys(PAYMENT_DETAILS_CARD_NUMBER, cardNumber)
    await this.sendKeys(PAYMENT_DETAILS_CARD_VERIFICATION, cardVerificationNumber)
    await this.selectDropDownByText(PAYMENT_DETAILS_MONTH_DROP_DOWN, month)
    await this.selectDropDownByText(PAYMENT_DETAILS_YEAR_DROP_DOWN, year)
    await browser.pause(4000) // added sleep to fasttrack form submission
  }

  async createPaymentDetailsFastForm (name: string, cardType: string, cardNumber: string, cardVerificationNumber: string, month: string, year: string) {
    await this.sendKeys(PAYMENT_DETAILS_NAME_ON_CARD, name)
    await this.selectDropDownByText(PAYMENT_CARD_TYPE_DROP_DOWN, cardType)
    await this.sendKeys(PAYMENT_DETAILS_CARD_NUMBER, cardNumber)
    await this.sendKeys(PAYMENT_DETAILS_CARD_VERIFICATION, cardVerificationNumber)
    await this.selectDropDownByText(PAYMENT_DETAILS_MONTH_DROP_DOWN, month)
    await this.selectDropDownByText(PAYMENT_DETAILS_YEAR_DROP_DOWN, year)
  }

  async createBillingAddress (firstName: string, lastName: string, address1: string, city: string, state: string, zip: string) {
    await this.sendKeys(BILLING_ADDRESS_FIRST_NAME, firstName)
    await this.sendKeys(BILLING_ADDRESS_LAST_NAME, lastName)
    await this.sendKeys(BILLING_ADDRESS_ADDRESS_LINE1, address1)
    await this.sendKeys(BILLING_ADDRESS_CITY, city)
    await this.selectDropDownByText(BILLING_ADDRESS_STATE, state)
    await this.sendKeys(BILLING_ADDRESS_ZIP, zip)
  }

  async selectBillingAddressByValue (address: string) {
    await this.click(BILLING_ADDRESS_DROP_DOWN)
    const addressSelect =
      `//select[@id='savedBillingAddress']/option[@value='${address}']`
    return this.click(addressSelect)
  }

  async clickSetAsDefaultCreditCard () {
    await this.click(PAYMENT_NON_DEFAULT_SET_AS_DEFAULT_BTN)
  }

  async clickRemovePaymentInfoByIndex (keys: number) {
    await this.clickByIndex(PAYMENT_REMOVE_BTN, keys)
  }

  async clickRemovePaymentInfo () {
    await this.click(PAYMENT_NON_DEFAULT_REMOVE_BTN)
  }

  async clickPopUpOKToRemove () {
    await this.click(PAYMENT_DETAILS_REMOVE_POPUP_BUTTON)
  }

  async isPopUpOKButtonDisplayed () {
    return this.locatorFound(PAYMENT_DETAILS_REMOVE_POPUP_BUTTON)
  }

  async isPopUpCancelButtonDisplayed () {
    return this.locatorFound(PAYMENT_DETAILS_CANCEL_POPUP_BUTTON)
  }

  async verifyAddNewCardBTNIsLoaded () {
    await this.waitForDisplayed(PAYMENT_DETAILS_ADD_NEW_CARD_BTN)
  }

  async clickAddNewCardBTN () {
    await this.click(PAYMENT_DETAILS_ADD_NEW_CARD_BTN)
  }

  async verifyMyPaymentDetailsIsLoaded () {
    await this.waitForDisplayed(PAYMENT_DETAILS_MY_PAYMENT_DETAILS)
  }

  async verifyPaymentDetailsPageTitleIsLoaded () {
    await this.waitForDisplayed(PAYMENT_DETAILS_PAYMENT_DETAILS)
  }

  async getGlobalMessage () {
    await this.waitForDisplayed(PAYMENT_BILLING_ADDRESS_GLOBAL_MESSAGE)
    return this.getText(PAYMENT_BILLING_ADDRESS_GLOBAL_MESSAGE)
  }

  async getGlobalSuccessMsg () {
    await this.waitForDisplayed(GLOBAL_MESSAGE_SUCCESS)
    return this.getText(GLOBAL_MESSAGE_SUCCESS)
  }

  async isSetToDefaultDisplayed () {
    return this.locatorFound(PAYMENT_NON_DEFAULT_SET_AS_DEFAULT_BTN)
  }

  async isRemovePaymentDisplayedOnDefaultCard () {
    let removeButtons = await this.getCountOfArrayList(PAYMENT_NON_DEFAULT_REMOVE_BTN)
    let cardsAdded = await this.getCountOfArrayList(PAYMENT_DETAILS_CARD_SECTION)
    return !((removeButtons === 0 && cardsAdded === 1) || (removeButtons > 0 && cardsAdded === removeButtons + 1))
  }

  async getNumberOfAddedCards () {
    return this.getCountOfArrayList(PAYMENT_DETAILS_CARD_SECTION)
  }

  async getPaymentBillingEmptyMessage () {
    await this.waitForDisplayed(PAYMENT_BILLING_ADDRESS_EMPTY_MESSAGE_LIST)
    return this.getElementsTextArrayList(PAYMENT_BILLING_ADDRESS_EMPTY_MESSAGE_LIST)
  }

  async isAddNewCardBtnDisplayed () {
    return this.locatorFound(PAYMENT_DETAILS_ADD_NEW_CARD_BTN)
  }

  async isDefaultCardDisplayed () {
    return this.locatorFound(DEFAULT_CARD_TITLE)
  }

  async setDefaultCcOrAddCcIfNeeded (creditCard: {}, market: number, billingAddress: {} = null) {
    let ccIndex = 0
    let setDefaultDisplayed = await this.isSetToDefaultDisplayed()
    if (setDefaultDisplayed) {
      await this.clickSetAsDefaultCreditCard()
    } else {
      while (setDefaultDisplayed === false) {
        await this.addCreditCardIfNeeded(creditCard, market, billingAddress, ccIndex)
        setDefaultDisplayed = await this.isSetToDefaultDisplayed()
        ccIndex++
      }
      await this.clickSetAsDefaultCreditCard()
    }
  }

  async removeNonDefaultCCAddCCifNeeded (creditCard: {}, market: number, billingAddress: {} = null) {
    let ccIndex = 0
    let removeButtonIsDisplayed = await this.isRemoveCardButtonDisplayed()
    while (removeButtonIsDisplayed === false) {
      await this.addCreditCardIfNeeded(creditCard, market, billingAddress, ccIndex)
      removeButtonIsDisplayed = await this.isRemoveCardButtonDisplayed()
      ccIndex++
    }
    await this.clickRemovePaymentInfo()
    await this.verifyRemovePaymentInfoPopUpIsLoaded()
    await this.clickPopUpOKToRemove()
  }

  async addCreditCardIfNeeded (creditCard: {}, market: number, billingAddress: {} = null, ccIndex: number = 0) {
    await this.clickAddNewCardBTN()
    if (market === 1) {
      await this.createPaymentDetails(creditCard.nameOnCard[ccIndex], creditCard.cardType[ccIndex], creditCard.cardNumber[ccIndex], creditCard.cvv[ccIndex], creditCard.month[ccIndex], creditCard.year[ccIndex])
      await this.createBillingAddress(billingAddress.firstName, billingAddress.lastName, billingAddress.addressLine1, billingAddress.city, billingAddress.state, billingAddress.zip)
      await this.clickSavePaymentInfoButton()
    } else if (market !== 1) {
      let paymentAndBillingAddressTile = new PaymentAndBillingAddressTile(this.driver)
      await paymentAndBillingAddressTile.createPaymentDetailsHK(creditCard.nameOnCard[ccIndex], creditCard.cardNumber[ccIndex], creditCard.expdt[ccIndex], creditCard.cvv[ccIndex])
      await this.clickSavePaymentInfoButtonSG()
    }
    let myAccountLeftNavigation = new MyAccountLeftNavigation(this.driver)
    await myAccountLeftNavigation.verifyPageIsLoaded()
    await myAccountLeftNavigation.clickPaymentDetails()
    await this.verifyPageIsLoaded()
  }

  async isRemoveCardButtonDisplayed () {
    return this.locatorFound(PAYMENT_REMOVE_BTN)
  }

  async isZipCodeErrorMessageDisplayed () {
    let errorStatus = await this.locatorFound(BILLING_ADDRESS_ZIP_ERROR_MESSAGE)
    let message = await this.getText(BILLING_ADDRESS_ZIP_ERROR_MESSAGE)
    return errorStatus && message === 'Please provide a valid Postal Code.'
  }

  async isNoPaymentDetailsMsgDisplayed () {
    return this.locatorFound(NO_SAVED_PAYMENT_DETAILS_MESSAGE)
  }

  async removeNonDefaultCC () {
    await this.clickRemovePaymentInfo()
    await this.verifyRemovePaymentInfoPopUpIsLoaded()
    await this.clickPopUpOKToRemove()
  }

  async isAddNewCardDisabled () {
    return this.hasAttributePresent(PAYMENT_DETAILS_ADD_NEW_CARD_BTN, 'disabled')
  }

  async setFirstCardAsDefault () {
    await this.click(PAYMENT_FIRST_NON_DEFAULT_SET_AS_DEFAULT_BTN)
  }

  async isDefaultCardRemoveButtonDisplayed () {
    return this.locatorFound(PAYMENT_DETAILS_DEFAULT_REMOVE_BTN)
  }

  async isCardAssociatedWithAutoshipMessageDisplayed () {
    return this.locatorFound(PAYMENT_ASSOCIATED_MESSAGE)
  }
}
