// @flow
import BasePage from '../BasePage'

const ACCOUNT_DASHBOARD_LINK = '//a[contains(text(), "Account Dashboard")]'
const YOUR_PROFILE_LINK = '//a[contains(text(), "Your Profile")]'
const ADDRESS_BOOK_LINK = '//a[contains(text(), "Address Book")]'
const PAYMENT_DETAILS_LINK = '//a[contains(text(), "Payment Details")]'
const ORDER_HISTORY_LINK = '//a[contains(text(), "Order History")]'
const OPTAVIA_PREMIER_ORDER_LINK = '//a[contains(@title,"Premier Order")]'
const RETURN_STATUS = '//a[contains(@title,"Return Status")]'
const LEFT_NAVIGATION_LINK_TEXT_LIST = '//div[contains(@class,"block myaccount-sidebar")]//a'

export default class MyAccountLeftNavigation extends BasePage {
  async verifyPageIsLoaded () {
    await this.waitForDisplayed(ACCOUNT_DASHBOARD_LINK, 3)
  }

  // Left Nav Options
  async clickAccountDashboard () {
    await this.click(ACCOUNT_DASHBOARD_LINK)
  }

  async clickYourProfile () {
    await this.clickByIndex(YOUR_PROFILE_LINK, 1)
  }

  async clickAddressBook () {
    await this.clickByIndex(ADDRESS_BOOK_LINK, 1)
  }

  async clickPaymentDetails () {
    await this.clickByIndex(PAYMENT_DETAILS_LINK, 1)
  }

  async clickOrderHistory () {
    await this.clickByIndex(ORDER_HISTORY_LINK, 1)
  }

  async clickOptaviaPremierOrder () {
    await this.waitForDisplayed(OPTAVIA_PREMIER_ORDER_LINK)
    await this.click(OPTAVIA_PREMIER_ORDER_LINK)
  }

  async clickReturnStatus () {
    await this.waitForDisplayed(RETURN_STATUS)
    await this.click(RETURN_STATUS)
  }

  async isOptaviaPremierLinkPresent () {
    return this.locatorFound(OPTAVIA_PREMIER_ORDER_LINK, 1)
  }

  async getLeftNavigationLinkTextList () {
    return this.getElementsTextArrayList(LEFT_NAVIGATION_LINK_TEXT_LIST)
  }
}
