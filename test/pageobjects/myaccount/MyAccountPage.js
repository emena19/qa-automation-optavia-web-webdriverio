// @flow
import BasePage from '../BasePage'

const MY_ACCOUNT_BODY_TITLE = '//*[@id="content"]/div/h1[contains(text(),"My Account")]'
const WELCOME_MESSAGE = '//*[@class="welcomeMessage"]'
const ACCOUNT_NAME = '//*[@class="accountName"]'
const ACCOUNT_ID = '//*[@class="accountId"]'
const MY_ACCOUNT_GLOBAL_MESSAGE = '//*[@id="globalMessages"]/ul/li/ul/li'

// My Account Dashboard - My OPTAVIA Premier Order section
const EDIT_ORDER_LINK = '//*[@class="account-credit-heading"]/descendant::a[contains(text(),"Edit Order")]'
const CANCEL_ORDER_LINK = '//*[@class="premier-content-wrapper"]/descendant::a[contains(text(),"Cancel Order")]'
const REINSTATE_ORDER_LINK = '//*[@class="premier-content-wrapper"]/descendant::a [contains (text(), "Reinstate Order")]'
const CANCEL_OR_REINSTATE_LINK = '//*[@class="cancel-order-wrapper"]/descendant::a [contains (text(), "")]'
const NEXT_ORDER_DATE = '//*[@class="nextOrderDate"]'
const EDIT_SHIPPING_TO_LINK = '//a[contains(text(),"Edit")][contains(@href,"/my-account/address-book")]'
const EDIT_PAYMENT_METHOD_LINK = '//a[contains(text(),"Edit")][contains(@href,"/my-account/payment-details")]'
const ACCOUNT_CREDIT_HEADING_TEXT_MY = '//div[@class="credit-heading"]/text()[1]'
const ACCOUNT_CREDIT_HEADING_TEXT_OPTA = '//div[@class="credit-heading"]/strong[contains(text(),"OPTA")]'
const ACCOUNT_CREDIT_HEADING_TEXT_VIA = '//div[@class="credit-heading"]/text()[2]'
const ACCOUNT_CREDIT_HEADING_TEXT_TM = '//div[@class="credit-heading"]/sup[contains(text(),"TM")]'
const ACCOUNT_CREDIT_HEADING_TEXT_PREMIERORDER = '//div[@class="credit-heading"]/text()[3]'
const SUBSCRIPTION_PROCESS_DATE_UPDATE_BTN = '//a[contains(text(),"Update")]'
const ACCOUNT_DASHBOARD_DEFAULT_ADDRESS = '//*[text()="Shipping To"]//..//address'
const PAYMENT_METHOD_CARD_AND_EXP_DATE = '(//*[@class="premier-content-left"]/address)[2]'

// Edit Order pop up
const EDIT_SUBSCRIPTION_CANCEL_BTN = '//form[@id="subscriptionOrderForm3"]//div[contains(@class,"buttons-set")]/button[text()="Cancel"]'
const EDIT_SUBSCRIPTION_UPDATE_BTN = '//form[@id="subscriptionOrderForm3"]//div[contains(@class,"buttons-set")]/button[contains(text(),"Update")]'
const EDIT_WARNING_SUBSCRIPTION = '//div[@id="warning-subscription"]/div/div/p'
// My Account Dashboard - Account Credit
const ACCOUNT_CREDIT_WRAPPER = '//*[@class="account-credit-wrapper"]'
const ACCOUNT_CREDIT_HEADING = '//*[@class="account-credit-wrapper"]/div[1]'
const ACCOUNT_CREDIT_AMOUNT = '//*[@class="account-credit-amt hidden-sm"]'
const ACCOUNT_CREDIT_PREMIER_REWARDS = '//*[@class="acc-credit-container"]/div[2]'
const ACCOUNT_CREDIT_EXPIRE_DATE = '//*[@class="credit-expiry" and contains(text(),"Rewards expire on")]'

// My Account Dashboard - Order History
// const ORDER_HISTORY_ROW_ARRAY = '//*[@class="table-content"]/div')
// const ORDER_HISTORY_LIST = '//*[@class="order-list-table table table-stacked"]/div/div')
const ORDER_HISTORY_HEADER = '//div[@class="account-order-dash-wrapper"]/h1'
const ORDER_NUMBERS_LIST = '//*[@class="table-content"]/div[1]'
// const ORDER_NUMBERS_LIST = '//*[@class="order-list-table table table-stacked"]/div/div[1]')
const ORDER_HISTORY_ORDER_DATE_LIST = '//*[@class="table-content"]/div[2]'
const ORDER_HISTORY_TOTAL_LIST = '//*[@class="table-content"]/div[3]'
// const ORDER_HISTORY_ROW1_ARRAY = '//*[@class="table-content"]/div')
const LAST_ORDER_NUMBER = '//*[@class="order-list-table table table-stacked order-stack"]/div[2]/div/a'
const PREMIER_ORDER_ICON = '//*[@class="order-list-table table table-stacked order-stack"]/div[2]/div/i'
// Cancel Optavia Premier Pop Up
const POP_UP_REINSTATE = '//*[@id="reinstate-subscription"]/div/div'
const POP_UP_REINSTATE_ORDER_BUTTON = '//*[@id="reinstate-subscription"]/descendant::a[contains(text(),"Reinstate")]'
const POP_UP_REINSTATE_CANCEL_BUTTON = '//*[@id="reinstate-subscription"]/descendant::a[contains(text(), "Cancel")]'
const EDIT_ORDER_POP_UP_REINSTATE_TEXT = '//*[@class="inner"]/p[.="You must Reinstate your OPTAVIA Premier order before editing. Would you like to Reinstate your order now ?"]'
const EDIT_ORDER_POP_UP_REINSTATE_ORDER_BUTTON = '//*[@id="reinstate-cancel-subscription"]/descendant::a[contains(text(),"Reinstate")]'
const EDIT_ORDER_POP_UP_REINSTATE_CANCEL_BUTTON = '//*[@id="reinstate-cancel-subscription"]/descendant::a[contains(text(),"Cancel")]'
const POP_UP_CANCEL_OPTAVIA_PREMIER = '//*[@id="cancel-subscription"]/div/div'
const POP_UP_CANCEL_PREMIER_CHANGE_MY_MIND_BUTTON = '//*[@id="cancel-subscription"]/descendant::a[contains(text(),"I changed my mind")]'
const POP_UP_CANCEL_OPTAVIA_PREMIER_BUTTON = '//*[@id="cancel-subscription"]/descendant::a[contains(text(),"Cancel")]'
const POP_UP_CANCEL_OPTAVIA_PREMIER_CLOSE_LINK = '//*[@id="cancel-subscription"]/div/a'
const POP_UP_REINSTATE_ORDER_BUTTON_PAST_AUTOSHIP = '//button[@type="submit" and text() ="Reinstate"]'

// CSR Volumes
const PQV = '//*[@data-header="PQV"]'
const FQV = '//*[@data-header="FQV"]'
const GQV = '//*[@data-header="GQV"]'
const HAR = '//*[@data-header="Highest Achieve Rank"]'
const CR = '//*[@data-header="Current Rank"]'
const CSR_VOLUME_SECTION = '//*[@class="account-order-dash-wrapper csr-volumesWrap"]'

// Account Credit
const WELLNESS_CREDIT_BALANCE = '//p/strong[contains(text(),"Wellness")]/../../following-sibling::div[@class="account-credit-amt hidden-sm"]'

export default class MyAccountPage extends BasePage {
  async verifyPageIsLoaded () {
    await this.waitForDisplayed(WELCOME_MESSAGE, 10)
  }

  async getPageTitle () {
    return this.getCurrentPageTitle()
  }

  async getPageUrl () {
    return this.getCurrentPageUrl()
  }

  async getMyAccountBodyText () {
    return this.getText(MY_ACCOUNT_BODY_TITLE)
  }

  async historyHeaderIsLoaded () {
    await this.waitForDisplayed(ORDER_HISTORY_HEADER)
  }

  async getMyAccountGlobalMessage () {
    return this.getText(MY_ACCOUNT_GLOBAL_MESSAGE)
  }

  async getWelcomeMessage () {
    return this.getText(WELCOME_MESSAGE)
  }

  async getAccountName () {
    return this.getText(ACCOUNT_NAME)
  }

  async getAccountID () {
    return this.getText(ACCOUNT_ID)
  }

  // My Account Dashboard - My OPTAVIA Premier Order section
  async getDefaultShippingAddress () {
    return this.getText(ACCOUNT_DASHBOARD_DEFAULT_ADDRESS)
  }

  async getCancelOrderText () {
    return this.getText(CANCEL_ORDER_LINK)
  }

  async clickEditShippingToLink () {
    await this.click(EDIT_SHIPPING_TO_LINK, 5)
  }

  async clickEditPaymentMethodLink () {
    await this.click(EDIT_PAYMENT_METHOD_LINK)
  }

  async getMyTextInDashboardIsLoaded () {
    return this.getText(ACCOUNT_CREDIT_HEADING_TEXT_MY)
  }

  async getOptaTextInDashboardIsLoaded () {
    await this.getText(ACCOUNT_CREDIT_HEADING_TEXT_OPTA)
  }

  async getViaTextInDashboardIsLoaded () {
    await this.getText(ACCOUNT_CREDIT_HEADING_TEXT_VIA)
  }

  async getTmTextInDashboardIsLoaded () {
    await this.getText(ACCOUNT_CREDIT_HEADING_TEXT_TM)
  }

  async getPremierOrderTextInDashboardIsLoaded () {
    await this.getText(ACCOUNT_CREDIT_HEADING_TEXT_PREMIERORDER)
  }

  async clickSubscriptionProcessDateUpdateBtn () {
    await this.click(SUBSCRIPTION_PROCESS_DATE_UPDATE_BTN)
  }

  async clickCancelOrder () {
    await this.click(CANCEL_ORDER_LINK)
  }

  async clickEditOrder () {
    await this.click(EDIT_ORDER_LINK)
  }

  async getReinstateOrderText () {
    return this.getText(REINSTATE_ORDER_LINK)
  }

  async getReinstateOrderOrCancelLinkText () {
    return this.getText(CANCEL_OR_REINSTATE_LINK)
  }

  async clickReinstateLink () {
    await this.click(REINSTATE_ORDER_LINK)
  }

  async verifyReinstatePopUpIsLoaded () {
    await this.waitForDisplayed(POP_UP_REINSTATE)
    await this.waitForDisplayed(POP_UP_REINSTATE_ORDER_BUTTON)
    await this.waitForDisplayed(POP_UP_REINSTATE_CANCEL_BUTTON)
  }

  async verifyEditOrderReinstatePopUpIsLoaded () {
    await this.waitForDisplayed(EDIT_ORDER_POP_UP_REINSTATE_TEXT)
    await this.waitForDisplayed(EDIT_ORDER_POP_UP_REINSTATE_ORDER_BUTTON)
    await this.waitForDisplayed(EDIT_ORDER_POP_UP_REINSTATE_CANCEL_BUTTON)
  }

  async clickEditOrderPopUpReinstate () {
    await this.click(EDIT_ORDER_POP_UP_REINSTATE_ORDER_BUTTON)
  }

  async clickEditOrderPopUpCancelReinstate () {
    await this.click(EDIT_ORDER_POP_UP_REINSTATE_CANCEL_BUTTON)
  }

  async clickPopUpReinstateButton () {
    await this.waitForDisplayed(POP_UP_REINSTATE_ORDER_BUTTON)
    await this.click(POP_UP_REINSTATE_ORDER_BUTTON)
  }

  async clickPopUpCancelReinstate () {
    await this.click(POP_UP_REINSTATE_CANCEL_BUTTON)
  }

  async verifyCancelPremierPopUpIsLoaded () {
    await this.waitForDisplayed(POP_UP_CANCEL_OPTAVIA_PREMIER)
    await this.waitForDisplayed(POP_UP_CANCEL_PREMIER_CHANGE_MY_MIND_BUTTON)
    await this.waitForDisplayed(POP_UP_CANCEL_OPTAVIA_PREMIER_BUTTON)
  }

  async clickChangeMyMindButton () {
    await this.click(POP_UP_CANCEL_PREMIER_CHANGE_MY_MIND_BUTTON)
  }

  async clickCancelPremierOrderButton () {
    await this.click(POP_UP_CANCEL_OPTAVIA_PREMIER_BUTTON)
  }

  async clickCancelOptaviaPremierCloseLink () {
    await this.click(POP_UP_CANCEL_OPTAVIA_PREMIER_CLOSE_LINK)
  }

  async getOptaviaNextOrderDate () {
    return this.getText(NEXT_ORDER_DATE)
  }

  async getLastOrderNumber () {
    return this.getTextByIndex(LAST_ORDER_NUMBER, 0)
  }

  async getOrderNumberArray (arrayNum: number) {
    return this.getTextByIndex(ORDER_NUMBERS_LIST, arrayNum)
  }

  async getCountofArrayList () {
    return this.getCountOfArrayList(ORDER_NUMBERS_LIST)
  }

  async getOrderTotal (arrayNum: number) {
    return this.getTextByIndex(ORDER_HISTORY_TOTAL_LIST, arrayNum)
  }

  async getOrderDate (arrayNum: number) {
    return this.getTextByIndex(ORDER_HISTORY_ORDER_DATE_LIST, arrayNum)
  }

  async verifyCancelAndUpdateLinkIsDisplayedInEditPopUp () {
    await this.waitForDisplayed(EDIT_SUBSCRIPTION_CANCEL_BTN)
    await this.waitForDisplayed(EDIT_SUBSCRIPTION_UPDATE_BTN)
  }

  async clickEditOrderPopUpUpdatebtn () {
    await this.waitForDisplayed(EDIT_SUBSCRIPTION_UPDATE_BTN)
    await this.click(EDIT_SUBSCRIPTION_UPDATE_BTN)
  }

  async getWarningSubscriptionText () {
    return this.getText(EDIT_WARNING_SUBSCRIPTION)
  }

  async getPremierOrderTextInDashboardIsDisplayed () {
    await this.waitForDisplayed(ACCOUNT_CREDIT_HEADING_TEXT_PREMIERORDER)
  }

  // CSR Volumes
  async getPQVValues () {
    return this.getText(PQV)
  }

  async getFQVValues () {
    return this.getText(FQV)
  }

  async getGQVValues () {
    return this.getText(GQV)
  }

  async getHARValues () {
    return this.getText(HAR)
  }

  async getCRValues () {
    return this.getText(CR)
  }

  // Account Credit
  async verifyAccountCreditIsLoaded () {
    await this.waitForDisplayed(ACCOUNT_CREDIT_WRAPPER)
  }

  async getAccountCreditHeading () {
    return this.getText(ACCOUNT_CREDIT_HEADING)
  }

  async getAccountCreditAmount () {
    return this.getText(ACCOUNT_CREDIT_AMOUNT)
  }

  async getAccountCreditPremierRewards () {
    await this.waitForDisplayed(ACCOUNT_CREDIT_PREMIER_REWARDS, 10)
    return this.getText(ACCOUNT_CREDIT_PREMIER_REWARDS)
  }

  async getAccountCreditExpireDate () {
    return this.getText(ACCOUNT_CREDIT_EXPIRE_DATE)
  }

  async getWellnessCreditValue () {
    return this.getText(WELLNESS_CREDIT_BALANCE)
  }

  async verifyPremierOrderIconIsVisible () {
    await this.waitForDisplayed(PREMIER_ORDER_ICON)
  }

  async clickLastOrderNumberLink () {
    await this.moveToLocatorPositionAndClick(LAST_ORDER_NUMBER)
  }

  async clickOrderLinkByOrderNumber (orderNumber) {
    let ORDER_NUMBER_LINK = `//a[contains(text(),"${orderNumber}")]`
    await this.moveToLocatorPositionAndClick(ORDER_NUMBER_LINK)
  }

  async clickOnPremierOrder (orderNumber) {
    const ORDER_HISTORY_ORDER_NUMBER = '=orderNumber'
    await this.waitForDisplayed(ORDER_HISTORY_ORDER_NUMBER, 3)
    await this.click(ORDER_HISTORY_ORDER_NUMBER)
  }

  async verifyWellnessCreditTextIsDisplayed () {
    await this.waitForDisplayed(WELLNESS_CREDIT_BALANCE)
  }

  async isPremierIconDisplayed () {
    return this.locatorFound(PREMIER_ORDER_ICON)
  }

  async reinstateOrderElseCancel () {
    let actual = await this.getReinstateOrderOrCancelLinkText()
    if (actual === 'Cancel Order') {
      await this.clickCancelOrder()
      await this.clickCancelPremierOrderButton()
    }
  }

  async cancelOrderElseReinstate () {
    let actual = await this.getReinstateOrderOrCancelLinkText()
    if (actual === 'Reinstate Order') {
      await this.clickReinstateLink()
      await this.clickPopUpReinstateButton()
    }
  }

  async isCSRVolumesSectionDisplayed () {
    return this.locatorFound(CSR_VOLUME_SECTION)
  }

  async getPaymentMethodText () {
    return this.getText(PAYMENT_METHOD_CARD_AND_EXP_DATE)
  }

  async clickPastPopUpReinstateButton () {
    await this.waitForDisplayed(POP_UP_REINSTATE_ORDER_BUTTON_PAST_AUTOSHIP)
    await this.click(POP_UP_REINSTATE_ORDER_BUTTON_PAST_AUTOSHIP)
  }
}
