// @flow
import BasePage from '../BasePage'

const SHIPPING_METHOD_TILE_TTL = '//*[@id="checkoutContentPanel"]/descendant::h5[contains(text(), "2 - Select Delivery Method")]'
const SHIPPING_METHOD_TILE_BDY = '//*[@id="checkoutContentPanel"]'
// const SHIPPING_METHOD_RUSH_RADIO_BTN = '//*[@id="rush"]')
const SHIPPING_METHOD_STD_RADIO_BTN = '//*[@id="standard"]'
const SHIPPING_METHOD_TILE_NEXT_BTN = '//*[@id="chooseDeliveryMethod_continue_button"]'
const STANDARD_CHARGE = '//label[@for="standard"]'
// const RUSH_CHARGE = '//label[@for="rush"]')
const RUSH_CHARGE = '#rush'
const RUSH_CHARGE_LABEL = '//*[@id="selectDeliveryMethodForm"]/ul/li[2]/div/label'

export default class ShippingMethodTile extends BasePage {
  async verifyPageIsLoaded () {
    await this.waitForDisplayed(SHIPPING_METHOD_TILE_BDY)
    await this.waitForDisplayed(SHIPPING_METHOD_TILE_TTL)
  }

  async getPageTitle () {
    return this.getCurrentPageTitle()
  }

  async getPageUrl () {
    return this.getCurrentPageUrl()
  }

  async getShippingMethodTitleText () {
    return this.getText(SHIPPING_METHOD_TILE_TTL)
  }

  async isStdShippingSelected () {
    return this.hasAttributePresent(SHIPPING_METHOD_STD_RADIO_BTN, 'checked')
  }

  async clickNext () {
    await this.click(SHIPPING_METHOD_TILE_NEXT_BTN)
  }

  async getStandardDeliveryCharge () {
    return this.getText(STANDARD_CHARGE)
  }

  async getRushDeliveryCharge () {
    return this.getText(RUSH_CHARGE_LABEL)
  }

  async clickStandardShippingMethod () {
    return this.click(STANDARD_CHARGE)
  }

  async clickRushShippingMethod () {
    return this.click(RUSH_CHARGE)
  }

  async isRushShippingSelected () {
    return this.isSelected(RUSH_CHARGE)
  }

  async isRushShippingDisplayed () {
    return this.locatorFound(RUSH_CHARGE, 1)
  }
}
