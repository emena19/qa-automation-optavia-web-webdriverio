// @flow
import BasePage from '../BasePage'

const CHECKOUT_COMMON_SECTION_BODY_TITLE = '//h1[contains(text(), "Checkout")]'
const CHECKOUT_PROGRESS_TAB = '#checkoutProgress'
// Order Summary
const ORDER_SUMMARY_SUBTOTAL = '//*[@id="orderTotals"]/tbody/descendant::td[contains(text(),"Subtotal")]/following-sibling::td'
const ORDER_SUMMARY_DISCOUNTS = '//*[@id="orderTotals"]/tbody/descendant::td[contains(text(),"Discounts")]/following-sibling::td'
const ORDER_SUMMARY_DELIVERY_CHARGE = '//*[@id="orderTotals"]/tbody/descendant::td[contains(text(),"Delivery Charge:")]/following-sibling::td'
const ORDER_SUMMARY_SURCHARGES = '//*[@id="orderTotals"]/tbody/descendant::td[contains(text(),"Surcharges:")]/following-sibling::td'
const ORDER_SUMMARY_TAX = '//*[@id="orderTotals"]/tbody/descendant::td[contains(text(),"Estimated tax:")]/following-sibling::td'
const ORDER_SUMMARY_ORDER_TOTAL = '//*[@id="orderTotals"]/tfoot/descendant::td[contains(text(),"Order Total:")]/following-sibling::td'
const ORDER_TOTAL = '//*[@id="orderTotals"]'
const GST_AMT = '//*[@class="order-totals details-block"]//td[@class="total taxAmt"]'
// YOUR OPTAVIA PREMIER REWARDS SUMMARY
// Estimated Rewards
const ELIGIBLE_FOR_NEW_REWARDS = '//*[@class="table totals-table"]/tbody/descendant::td[contains(text(),"Eligible For New Rewards:")]/following-sibling::td'
const EARNING_REWARDS = '//*[@class="table totals-table"]/tbody/descendant::td[contains(text(),"Earning This Order:")]/following-sibling::td'
const REWARDS_MESSAGE = '//*[@id="checkoutOrderDetails"]/descendant::p'
// const ESTIMATED_REWARDS_LINKS = '//div[@id="checkoutOrderDetails"]//a[text()="Estimated Rewards"]')
const ESTIMATED_REWARDS_LINKS = '//*[@id="checkoutOrderDetails"]/descendant::a[contains(text(), "Estimated Rewards")]'

const CLICK_ON_REWARDS_EARNINGS_POPUP = '//div[@id="estimated-rewards"]/div/button/i'
const SHIPPING_ADDRESS_MENU = '//*[@id="checkoutProgress"]/div/div[1]/a'
// RECEIVED PROMOTIONS
// const CART_PROMOTIONS_APPLIED_LIST = '//*[@class="cart-promotions-applied"]')

// ITEMS TO BE DELIVERED
// const CART_PRODUCT_NAME_LIST = '//*[@class="product-name"]')

const ORDER_SUMMARY_WELLNESS_CREDIT_TEXT = '//*[@id="orderTotals"]/tbody/descendant::td[contains(text(),"Wellness Credit:")]'
const COACH_AVATAR_INFO_TEXT = '//*[@class="optavia-coach-details"]/strong'

export default class CheckoutCommonSection extends BasePage {
  async verifyPageIsLoaded () {
    await this.waitForDisplayed(CHECKOUT_COMMON_SECTION_BODY_TITLE, 5)
    await this.waitForDisplayed(CHECKOUT_PROGRESS_TAB, 5)
  }

  // Getting Order Summary Values
  async getOrderSummarySubtotal () {
    return this.getText(ORDER_SUMMARY_SUBTOTAL)
  }

  async getOrderSummaryDiscounts () {
    return this.getText(ORDER_SUMMARY_DISCOUNTS)
  }

  async getOrderSummaryDeliveryCharge () {
    return this.getText(ORDER_SUMMARY_DELIVERY_CHARGE)
  }

  async getOrderSummarySurcharge () {
    return this.getText(ORDER_SUMMARY_SURCHARGES)
  }

  async getOrderSummaryTax () {
    return this.getText(ORDER_SUMMARY_TAX)
  }

  async getOrderSummaryOrderTotal () {
    return this.getText(ORDER_SUMMARY_ORDER_TOTAL)
  }

  async getGSTAmt () {
    return this.getText(GST_AMT)
  }

  async getEligibleForNewRewards () {
    return this.getText(ELIGIBLE_FOR_NEW_REWARDS)
  }

  async getEarningRewards () {
    return this.getText(EARNING_REWARDS)
  }

  async getRewardsMessage () {
    return this.getText(REWARDS_MESSAGE)
  }

  async clickOnEstimatedRewards () {
    await this.waitForDisplayed(ESTIMATED_REWARDS_LINKS)
    await this.click(ESTIMATED_REWARDS_LINKS)
  }

  async isPremierEstimatedRewardsLinkPresentDisplayed () {
    return this.locatorFound(ESTIMATED_REWARDS_LINKS, 1)
  }

  async clickOnRewardsCloseBtn () {
    await this.click(CLICK_ON_REWARDS_EARNINGS_POPUP)
  }

  async getOrderSummaryTotals () {
    await this.moveToLocatorPosition(ORDER_TOTAL)
    return this.getText(ORDER_TOTAL)
  }

  async clickOnShippingAddressMenu () {
    await this.waitForDisplayed(SHIPPING_ADDRESS_MENU)
    await this.click(SHIPPING_ADDRESS_MENU)
  }

  async checkOrderSummaryTotal () {
    let tax = await this.getOrderSummaryTax()
    let taxNumber = parseFloat(tax.substr(1))
    let delivery = await this.getOrderSummaryDeliveryCharge()
    let deliveryNumber = parseFloat(delivery.substr(1))
    let subtotal = await this.getOrderSummarySubtotal()
    let subtotalNumber = parseFloat(subtotal.substr(1).replace(/,/g, ''))
    return parseFloat(deliveryNumber + taxNumber + subtotalNumber).toFixed(2)
  }

  async isWellnessCreditTextDisplayed () {
    return this.locatorFound(ORDER_SUMMARY_WELLNESS_CREDIT_TEXT)
  }

  async getCoachInfoFromAvatar () {
    return this.getText(COACH_AVATAR_INFO_TEXT)
  }

  async isDiscountTextDisplayed () {
    return this.locatorFound(ORDER_SUMMARY_DISCOUNTS, 1)
  }
}
