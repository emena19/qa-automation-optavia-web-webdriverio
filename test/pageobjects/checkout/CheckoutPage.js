// @flow
import BasePage from '../BasePage'

const CHECKOUT_BODY_TITLE = '//h1[contains(text(), "Checkout")]'
const CHECKOUT_PREMIER_ELIGIBLE_NEW_REWARDS = '//*[@class="table totals-table"]/descendant::td[contains(text(),"Eligible For New Rewards:")]/following-sibling::td'
const CHECKOUT_PREMIER_EARNINGS_THIS_ORDER = '//*[@class="table totals-table"]/descendant::td[contains(text(),"Earning This Order:")]/following-sibling::td'
const CHECKOUT_ESTIMATED_REWARDS_LINK = '//*[@id="checkoutOrderDetails"]/descendant::a[contains(text(),"Estimated Rewards")]'
const CHECKOUT_ACTION_LINKS_LIST = 'div[id=checkoutProgress] a'
const CHECKOUT_REWARDS_TOWARDS_NEXT_OPTAVIA_ORDER_TEXT = '//*[@id="checkoutOrderDetails"]/div[2]/div/p'
const CHECKOUT_GENDER = '//*[@id="i18nAddressForm"]/li[11]'
const CHECKOUT_DOB = '#customerForm.dob'

export default class CheckoutPage extends BasePage {
  async verifyPageIsLoaded () {
    await this.waitForDisplayed(CHECKOUT_BODY_TITLE)
  }

  async getPageTitle () {
    return this.getCurrentPageTitle()
  }

  async getPageUrl () {
    return this.getCurrentPageUrl()
  }

  async getCheckoutBodyTitle () {
    return this.getText(CHECKOUT_BODY_TITLE)
  }

  async getEarningThisOrderText () {
    return this.getText(CHECKOUT_PREMIER_EARNINGS_THIS_ORDER)
  }

  async getEligibleForRewardsText () {
    await this.moveToLocatorPosition(CHECKOUT_PREMIER_ELIGIBLE_NEW_REWARDS)
    return this.getText(CHECKOUT_PREMIER_ELIGIBLE_NEW_REWARDS)
  }

  async verifyEarningThisOrderIsLoaded () {
    await this.moveToLocatorPosition(CHECKOUT_PREMIER_ELIGIBLE_NEW_REWARDS)
    await this.waitForDisplayed(CHECKOUT_PREMIER_EARNINGS_THIS_ORDER)
  }

  async clickEstimatedRewards () {
    await this.click(CHECKOUT_ESTIMATED_REWARDS_LINK)
  }

  async clickCheckoutActionLinks (key) {
    await this.clickByIndex(CHECKOUT_ACTION_LINKS_LIST, key)
  }

  async isEstimatedRewardsLinkDisplayed () {
    return this.locatorFound(CHECKOUT_ESTIMATED_REWARDS_LINK)
  }

  async getRewardsTowardsNextOrderText () {
    return this.getText(CHECKOUT_REWARDS_TOWARDS_NEXT_OPTAVIA_ORDER_TEXT)
  }

  async isGenderDisplayed () {
    return this.locatorFound(CHECKOUT_GENDER)
  }

  async isDOBDisplayed () {
    return this.locatorFound(CHECKOUT_DOB)
  }

  async isGenderFieldDisplayed () {
    return this.isElementVisible(CHECKOUT_GENDER)
  }

  async isDOBFieldDisplayed () {
    return this.isElementVisible(CHECKOUT_DOB)
  }
}
