// @flow
import BasePage from '../BasePage'

const FINAL_REVIEW_BODY_TTL = '//*[@id="placeOrder"]/descendant::h5[contains(text(), "Final Review")]'
// Delivery Address
const FINAL_REVIEW_DELIVERY_ADDRESS_SECTION = '//*[@id="checkoutContentPanel"]/div[1]/ul'
const FINAL_REVIEW_DELIVERY_ADDRESS_LIST = '//*[@id="checkoutContentPanel"]/div[1]/ul/li'
// Delivery Options
const FINAL_REVIEW_DELIVERY_CHARGE = `//*[@id="orderTotals"]/tbody/descendant::td[contains(text(),"Delivery Charge:")]/following-sibling::td`
// const FINAL_REVIEW_DELIVERY_OPTIONS_SECTION = '//*[@id="checkoutContentPanel"]/div[2]')
const FINAL_REVIEW_DELIVERY_DELIVERY_LIST = '//*[@id="checkoutContentPanel"]/div[2]/div/div/div/ul/li'
// Payment Details
// const FINAL_REVIEW_PYAMENT_DETAILS_SECTION = '//*[@id="checkoutContentPanel"]/div[2]/div[1]')
// Billing Address
// const FINAL_REVIEW_BILLING_ADDRESS_SECTION = '//*[@id="checkoutContentPanel"]/div[3]/div[2]/ul')
const FINAL_REVIEW_PLACE_ORDER_BTN = '//button[contains(text(),"Submit Order")]'
const FINAL_REVIEW_TAX = '//*[@id="orderTotals"]/tbody/descendant::td[contains(text(),"Estimated tax:")]/following-sibling::td'
const FINAL_REVIEW_DELIVERY_CHARGES = '//*[@id="orderTotals"]/tbody/descendant::td[contains(text(),"Delivery Charge:")]/following-sibling::td'
const FINAL_REVIEW_DELIVERY_ADDRESS_EDIT_BUTTON = '//*[@id="checkoutContentPanel"]/div[1]/a'
const FINAL_REVIEW_PAYMENT_DETAILS_SECTION = '//*[@class="col-md-6"]/ul'
const FINAL_REVIEW_PAYMENT_EDIT_BTN_SG_HK = '//*[@id="checkoutContentPanel"]//div[2]/a'
const FINAL_REVIEW_VIEW_ITEMS_IN_KIT = 'div[class="product-info"] a'
const FINAL_REVIEW_ITEMS_KIT_LIST = 'div[class="modal-content"] ul li'
const FINAL_REVIEW_PAYMENT_EDIT_BTN_US = '//*[contains(@class,"summaryPayment")]//div[3]/a'
const FINAL_REVIEW_DELIVERY_OPTIONS_EDIT_BTN = '//div[@id="checkoutContentPanel"]/div[@class="summaryDeliveryMode summary clearfix"]/div/div/a'
const FINAL_REVIEW_PAYMENT_DETAILS_HEADER = '//*[@id="checkoutContentPanel"]/div[2]/div[1]/strong'
const FINAL_REVIEW_PAYMENT_AUTHORIZED_TEXT = '//*[@id="checkoutContentPanel"]//*[contains(text(), "Payment Authorized")]'

// FREE ITEMS
const ESSENTIALS_ON_THE_GO_KIT = '//*[@id="checkoutOrderDetails"]/descendant::div[contains(text(),"Essential On-The-Go Kit")]'
const FREE_BLENDER_BOTTLE = '//*[@id="checkoutOrderDetails"]/descendant::div[contains(text(),"BlenderBottle")]'
const FREE_GET_1_WEEK_FREE = '//*[@id="checkoutOrderDetails"]/descendant::div[contains(text(),"Get 1 Week FREE")]'
const FREE_OPTAVIA_GUIDE_TOP_TIPS_INSERT = '//*[@id="checkoutOrderDetails"]/descendant::div[contains(text(),"Tip")]'
const FREE_JOURNEY_KICKOFF_CARD_INSERT = '//*[@id="checkoutOrderDetails"]/descendant::div[contains(text(),"Journey Kickoff Card")]'
const FREE_OPTAVIA_GUIDE = '//*[@id="checkoutOrderDetails"]/descendant::div[contains(div,"OPTAVIA Guide")]'
const FINAL_REVIEW_CLOSE_POPUP_BUTTON = 'div[class="modal-content"] a[data-target="#kit-description"]'
const FINAL_REVIEW_GLOBAL_MESSAGE = '#globalMessages'
const FREE_CREAMY_CHOCOLATE_SHAKE = '//*[@id="checkoutOrderDetails"]/descendant::div[contains(., "Essential Creamy Chocolate Shake")]'
const FREE_TROPICAL_FRUIT_FRUIT_SMOOTHIE = '//*[@id="checkoutOrderDetails"]/descendant::div[contains(., "Tropical Fruit Smoothie")]'
const FREE_ZESTY_CHEDDAR_ITALIAN_HERB_FLAVORED_CRUNCHERS = '//*[@id="checkoutOrderDetails"]/descendant::div[contains(., "Zesty Cheddar")]'
const FREE_ZESTY_LEMON_CRISP_BAR = '//*[@id="checkoutOrderDetails"]/descendant::div[contains(., "Zesty Lemon Crisp Bar")]'
const FREE_CREAMY_DOUBLE_PEANUT_BUTTER_CRISP_BAR = '//*[@id="checkoutOrderDetails"]/descendant::div[contains(., "Creamy Double Peanut Butter")]'
const PRODUCT_NAME_ARRAY_LIST = '//*[@class="order-items details-block"]/table//div[@class="product-name"]'
const CREDIT_CARD_ERROR_MESSAGE = '//li[@class="error-msg"]//li'

const FREE_EGG_FLOWER_MUSHROOM_SOUP = '//*[@id="checkoutOrderDetails"]/descendant::div[contains(text(),"Essential Egg Flower & Mushroom Soup")]'
const FREE_GREEN_TEA_SHAKE = '//*[@id="checkoutOrderDetails"]/descendant::div[contains(text(),"Essential Green Tea Shake")]'
const FREE_SPICY_POPPERS = '//*[@id="checkoutOrderDetails"]/descendant::div[contains(text(),"Essential Spicy Poppers")]'
const FREE_CRANBERRY_HONEY_NUT = '//*[@id="checkoutOrderDetails"]/descendant::div[contains(text(),"Essential Cranberry Honey Nut Granola Bar")]'
const ITEMS_TO_DELIVER_LIST = '//*[@id="checkoutOrderDetails"]/div[5]/table'
const FREE_ITEMS_NAMES = '//tr//ul[@class="styled-list"]'

export default class FinalReviewTile extends BasePage {
  async verifyPageIsLoaded () {
    await this.waitForDisplayed(FINAL_REVIEW_BODY_TTL, 6)
  }

  async getPageTitle () {
    return this.getCurrentPageTitle()
  }

  async getPageUrl () {
    return this.getCurrentPageUrl()
  }

  async getFinalReviewBodyTitle () {
    await this.waitForDisplayed(FINAL_REVIEW_BODY_TTL)
    return this.getText(FINAL_REVIEW_BODY_TTL)
  }

  async getDeliveryAddress (keys: number) {
    await this.waitForDisplayed(FINAL_REVIEW_DELIVERY_ADDRESS_LIST, 10)
    return this.getTextByIndex(FINAL_REVIEW_DELIVERY_ADDRESS_LIST, keys)
  }

  async getDeliveryAddressSection () {
    await this.waitForDisplayed(FINAL_REVIEW_DELIVERY_ADDRESS_SECTION)
    return this.getText(FINAL_REVIEW_DELIVERY_ADDRESS_SECTION)
  }

  async getPaymentDetailSection () {
    await this.waitForDisplayed(FINAL_REVIEW_PAYMENT_DETAILS_SECTION)
    return this.getText(FINAL_REVIEW_PAYMENT_DETAILS_SECTION)
  }

  async clickPlaceOrderButton () {
    await this.moveToLocatorPosition(FINAL_REVIEW_PLACE_ORDER_BTN, 7)
    await this.click(FINAL_REVIEW_PLACE_ORDER_BTN, 5)
  }

  async getTaxAmount () {
    return this.getText(FINAL_REVIEW_TAX)
  }

  async getDeliveryCharge () {
    return this.getText(FINAL_REVIEW_DELIVERY_CHARGES)
  }

  async clickDeliveryAddressEditBtn () {
    await this.click(FINAL_REVIEW_DELIVERY_ADDRESS_EDIT_BUTTON)
  }

  async clickDeliveryOptionsEditBtn () {
    await this.click(FINAL_REVIEW_DELIVERY_OPTIONS_EDIT_BTN)
  }

  async verifyPlaceOrderButton () {
    await this.waitForDisplayed(FINAL_REVIEW_PLACE_ORDER_BTN)
  }

  async clickPaymentDetailsEditBtn () {
    await this.click(FINAL_REVIEW_PAYMENT_EDIT_BTN_SG_HK)
  }

  async clickPaymentDetailsEditBtnUS () {
    await this.click(FINAL_REVIEW_PAYMENT_EDIT_BTN_US)
  }

  async getPremierDeliveryChargeText () {
    return this.getText(FINAL_REVIEW_DELIVERY_CHARGE)
  }

  async getCreditCardErrorMessage () {
    return this.getText(CREDIT_CARD_ERROR_MESSAGE)
  }

  async clickViewItemsInKitLink () {
    await this.click(FINAL_REVIEW_VIEW_ITEMS_IN_KIT)
    this.driver.sleep(5000)
  }
  async clickViewItemsInKit () {
    await this.click(FINAL_REVIEW_VIEW_ITEMS_IN_KIT)
  }

  async getItemsKitList () {
    await this.waitForDisplayed(FINAL_REVIEW_ITEMS_KIT_LIST)
    return this.getElementsTextArrayList(FINAL_REVIEW_ITEMS_KIT_LIST)
  }

  async verifyFreeOptaviaGuideIsLoaded () {
    await this.waitForDisplayed(FREE_OPTAVIA_GUIDE)
  }

  async verifyFreeJourneyKickoffCardInsertIsLoaded () {
    await this.waitForDisplayed(FREE_JOURNEY_KICKOFF_CARD_INSERT)
  }

  async verifyFreeOptaviaGuideTopTipsInsertIsLoaded () {
    await this.waitForDisplayed(FREE_OPTAVIA_GUIDE_TOP_TIPS_INSERT)
  }

  async verifyFree1WeekMealIsLoaded () {
    await this.waitForDisplayed(FREE_GET_1_WEEK_FREE)
  }

  async verifyFreeBlenderBottleIsLoaded () {
    await this.waitForDisplayed(FREE_BLENDER_BOTTLE)
  }

  async verifyEssentialOnTheGoKitPlanIsLoaded () {
    await this.waitForDisplayed(ESSENTIALS_ON_THE_GO_KIT)
  }

  async closeViewItemsPopup () {
    await this.waitForDisplayed(FINAL_REVIEW_CLOSE_POPUP_BUTTON)
    await this.click(FINAL_REVIEW_CLOSE_POPUP_BUTTON)
  }

  async getGlobalMessage () {
    await this.waitForDisplayed(FINAL_REVIEW_GLOBAL_MESSAGE, 3)
    return this.getText(FINAL_REVIEW_GLOBAL_MESSAGE)
  }

  async getDeliveryOptionsText (keys: number) {
    return this.getTextByIndex(FINAL_REVIEW_DELIVERY_DELIVERY_LIST, keys)
  }

  async verifyFreeCreamyChocolateShakeIsLoaded () {
    await this.waitForDisplayed(FREE_CREAMY_CHOCOLATE_SHAKE)
  }

  async verifyFreeTropicalFruitSmoothieIsLoaded () {
    await this.waitForDisplayed(FREE_TROPICAL_FRUIT_FRUIT_SMOOTHIE)
  }

  async verifyFreeZestyCheddarCrunchersIsLoaded () {
    await this.waitForDisplayed(FREE_ZESTY_CHEDDAR_ITALIAN_HERB_FLAVORED_CRUNCHERS)
  }

  async verifyFreeZestyLemonCrispBarIsLoaded () {
    await this.waitForDisplayed(FREE_ZESTY_LEMON_CRISP_BAR)
  }

  async verifyFreeCreamyDoublePeanutButterCrispBarIsLoaded () {
    await this.waitForDisplayed(FREE_CREAMY_DOUBLE_PEANUT_BUTTER_CRISP_BAR)
  }

  async isEditLinkDisplayedSGandHK () {
    return this.locatorFound(FINAL_REVIEW_PAYMENT_EDIT_BTN_SG_HK)
  }

  async isPaymentDetailsHeaderDisplayed () {
    return this.locatorFound(FINAL_REVIEW_PAYMENT_DETAILS_HEADER)
  }

  async isPaymentAuthorizedTextDisplayed () {
    return this.locatorFound(FINAL_REVIEW_PAYMENT_AUTHORIZED_TEXT)
  }

  async getProductArrayList () {
    return this.getElementsTextArrayList(PRODUCT_NAME_ARRAY_LIST)
  }

  async isDeliveryChargeDisplayed () {
    return this.locatorFound(FINAL_REVIEW_DELIVERY_CHARGES)
  }

  async verifyFreeEggFlowerAndMushroomSoupIsLoaded () {
    await this.waitForDisplayed(FREE_EGG_FLOWER_MUSHROOM_SOUP)
  }

  async verifyFreeGreenTeaShakeIsLoaded () {
    await this.waitForDisplayed(FREE_GREEN_TEA_SHAKE)
  }

  async verifyFreeSpicyPoppersIsLoaded () {
    await this.waitForDisplayed(FREE_SPICY_POPPERS)
  }

  async verifyFreeCranberryHoneyNutIsLoaded () {
    await this.waitForDisplayed(FREE_CRANBERRY_HONEY_NUT)
  }

  async getItemsToDeliverText () {
    return this.getText(ITEMS_TO_DELIVER_LIST)
  }

  async isSubmitButtonDisplayed () {
    return this.locatorFound(FINAL_REVIEW_PLACE_ORDER_BTN)
  }

  async getFreeItemsNames () {
    return this.getText(FREE_ITEMS_NAMES)
  }
}
