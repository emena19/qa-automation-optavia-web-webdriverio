// @flow
import BasePage from '../BasePage'
import {market} from '@input-data/products/ProductTestData'

const SHIPPING_ADDRESS_TILE_TTL = '//*[@id="checkoutContentPanel"]/descendant::h5[contains(text(), "Address Details")]'
// const SHIPPING_ADDRESS_INPUT_LIST = '//*[@class="input-box"]/input')
const SHIPPING_ADDRESS_INPUT_LIST = '//input[@id="address.line1"]'
const SHIPPING_ADDRESS_DEFAULT_ADDRESS_CHECK_BOX = '//*[@class="add-address-left-input"]'
const SHIPPING_ADDRESS_NEXT_BTN = '//*[@id="addressform_button_panel"]/button[contains(text(), "Next")]'
const SHIPPING_ADDRESS_OK_TO_PROCEED_BTN = '//*[@id="addressform_button_panel"]/button[contains(text(), "Okay to Proceed")]'
const SHIPPING_ADDRESS_STATE_DROP_DOWN = '//*[@id="address.region"]'
const SHIPPING_ADDRESS_COUNTRY_CODE = '//*[@id="isdCode"]'
const SHIPPING_ADDRESS_SAVED_ADDRESS = '//*[@id="savedAddressList"]/div/div/address'
const SHIPPING_ADDRESS_CALIFORNIA_PROPOSITION = '//*[@id="addressform_proposition"]'
const SHIPPING_ADDRESS_CALIFORNIA_PROP_CLICK_HERE = '//*[@id="addressform_proposition"]/p[2]/a'
const SHIPPING_ADDRESS_CALIFORNIA_PROPOSITION_POPUP = '//*[@id="propositionPopup"]/p'
const SHIPPING_ADDRESS_CALIFORNIA_PROP_CLOSE = '//*[@id="cboxClose"]'
const SHIPPING_ADDRESS_LINE1 = '//input[@id="address.line1"]'
const SHIPPING_ADDRESS_LINE2 = '//input[@id="address.line2"]'
const SHIPPING_ADDRESS_POSTAL_CODE = '//input[@id="address.postcode"]'
const SHIPPING_ADDRESS_PHONE = '//input[@id="address.phone"]'
const SHIPPING_ADDRESS_DISTRICT = '//*[@id="address.district"]'
const SHIPPING_ADDRESS_SELECTED_DISTRICT = '//select[@id="address.district"]/option[@selected="selected"]'
const SHIPPING_ADDRESS_CITY = '//*[@id="address.townCity"]'
const SHIPPING_ADDRESS_ZIP = '//*[@id="address.postcode"]'
const OPTAVIA_BOTTOM_LOGO = '//*[@class="section1"]//a/img'
const SHIPPING_ADDRESS_COUNTRY = '//*[@id="address.country"]'
const SHIPPING_ADDRESS_COUNTRY_SELECTED = '//select[@id="address.country"]/option[@selected="selected"]'
const SHIPPING_ADDRESS_COUNTRY_TEXT_BOX = '//select[@id="isdCode"]//parent::div'
const SHIPPING_ADDRESS_FIRST_NAME = '//input[@id="address.firstName"]'
const SHIPPING_ADDRESS_LAST_NAME = '//input[@id="address.surname"]'
const SHIPPING_ADDRESS_VIEW_ADDRESS = 'viewAddressBook'
const SHIPPING_ADDRESS_ORDER_SUBTOTAL = '//*[@class="subtotalAmt"]'
const SHIPPING_ADDRESS_DELIVERY_CHARGE = `//*[@id="orderTotals"]/tbody/descendant::td[contains(text(),"Delivery Charge:")]/following-sibling::td`
const SHIPPING_ADDRESS_PHONE_ERROR_TEXT = '#address.phone-error'
const SHIPPING_ADDRESS_ZIP_ERROR = '#address.postcode-error'
const SHIPPING_ADDRESS_ZIP_ERROR_NEW_ADDRESS = '#postcode.errors'
const SHIPPING_ADDRESSLINE_1_ERROR = '#address.line1-error'
const SHIPPING_ADDRESS_CITY_ERROR = '#address.townCity-error'
const SHIPPING_STATE_PROVINCE_ERROR = '#address.region-error'
const SHIPPING_COUNTRY_CODE_ERROR = '#isdCode-error'
const SHIPPING_PHONE_ERROR = '#address.phone-error'

const SHIPPING_ADDRESS_GLOBAL_MESSAGE = '//*[@id="globalMessages"]//li//li'
const SHIPPING_DISTRICT_ERROR = '#address.district-error'

export default class ShippingAddressTile extends BasePage {
  async verifyPageIsLoaded () {
    await this.waitForDisplayed(SHIPPING_ADDRESS_TILE_TTL)
    // await this.waitForDisplayed(SHIPPING_ADDRESS_NEXT_BTN)
    await this.waitForDisplayed(SHIPPING_ADDRESS_INPUT_LIST)
  }

  async verifyProposition65IsLoaded () {
    await this.waitForDisplayed(SHIPPING_ADDRESS_CALIFORNIA_PROPOSITION)
    await this.waitForDisplayed(SHIPPING_ADDRESS_OK_TO_PROCEED_BTN)
  }

  async getPageTitle () {
    return this.getCurrentPageTitle()
  }

  async getPageUrl () {
    return this.getCurrentPageUrl()
  }

  async getShippingAddressTitleText () {
    return this.getText(SHIPPING_ADDRESS_TILE_TTL)
  }

  async sendFirstName (key: string) {
    await this.sendKeysByIndex(SHIPPING_ADDRESS_INPUT_LIST, key, 0)
  }

  async sendLastName (key: string) {
    await this.sendKeysByIndex(SHIPPING_ADDRESS_INPUT_LIST, key, 1)
  }

  async addShippingAddress (address: Object, line2: boolean = false) {
    await this.sendKeys(SHIPPING_ADDRESS_LINE1, address.SHIPPING_ADDRESS_LINE_1)
    line2 ? await this.sendKeys(SHIPPING_ADDRESS_LINE2, address.SHIPPING_ADDRESS_LINE_2)
      : await this.sendKeys(SHIPPING_ADDRESS_LINE2, '')
    await this.sendKeys(SHIPPING_ADDRESS_CITY, address.SHIPPING_CITY)
    if (address.SHIPPING_FULL_STATE_NAME !== '') {
      await this.selectDropDownByText(SHIPPING_ADDRESS_STATE_DROP_DOWN, address.SHIPPING_FULL_STATE_NAME)
    }
    await this.sendKeys(SHIPPING_ADDRESS_ZIP, address.SHIPPING_ZIP)
    if (address.SHIPPING_COUNTRY_CODE !== '') {
      await this.selectDropDownByText(SHIPPING_ADDRESS_COUNTRY_CODE, `${address.SHIPPING_COUNTRY_CODE} ${address.SHIPPING_COUNTRY}`)
    }
    await this.sendKeys(SHIPPING_ADDRESS_PHONE, address.SHIPPING_PHONE)
  }

  async addShippingFullName (firstName: string, lastName: string) {
    await this.sendKeysByIndex(SHIPPING_ADDRESS_LINE1, firstName, 0)
    await this.sendKeysByIndex(SHIPPING_ADDRESS_LINE1, lastName, 1)
  }

  async addShippingAddressHK (address: Object, line2: boolean = false) {
    await this.sendKeys(SHIPPING_ADDRESS_LINE1, address.SHIPPING_ADDRESS_LINE_1)
    line2 ? await this.sendKeys(SHIPPING_ADDRESS_LINE2, address.SHIPPING_ADDRESS_LINE_2)
      : await this.sendKeys(SHIPPING_ADDRESS_LINE2, '')
    if (address.SHIPPING_DISTRICT !== '') {
      await this.selectDropDownByText(SHIPPING_ADDRESS_DISTRICT, address.SHIPPING_DISTRICT, 4)
    }
    if (address.SHIPPING_COUNTRY_CODE !== '') {
      await this.selectDropDownByText(SHIPPING_ADDRESS_COUNTRY_CODE, `${address.SHIPPING_COUNTRY_CODE} ${address.SHIPPING_FULL_STATE_NAME}`)
    }
    await this.sendKeys(SHIPPING_ADDRESS_PHONE, address.SHIPPING_PHONE)
  }

  async addShippingAddressSG (address: Object, line2: boolean = false) {
    await this.sendKeys(SHIPPING_ADDRESS_LINE1, address.SHIPPING_ADDRESS_LINE_1)
    line2 ? await this.sendKeys(SHIPPING_ADDRESS_LINE2, address.SHIPPING_ADDRESS_LINE_2)
      : await this.sendKeys(SHIPPING_ADDRESS_LINE2, '')
    await this.sendKeys(SHIPPING_ADDRESS_POSTAL_CODE, address.SHIPPING_ZIP)
    await this.sendKeys(SHIPPING_ADDRESS_PHONE, address.SHIPPING_PHONE)
    if (address.SHIPPING_COUNTRY_CODE !== '') {
      await this.selectDropDownByText(SHIPPING_ADDRESS_COUNTRY_CODE, `${address.SHIPPING_COUNTRY_CODE} ${address.SHIPPING_COUNTRY}`)
    }
  }

  async clickToCheckDefaultAddressCheckBox () {
    await this.click(SHIPPING_ADDRESS_DEFAULT_ADDRESS_CHECK_BOX)
  }

  async clickNext () {
    await this.moveToLocatorPosition(SHIPPING_ADDRESS_NEXT_BTN)
    await this.click(SHIPPING_ADDRESS_NEXT_BTN)
  }

  async clickOkayToProceed () {
    await this.click(SHIPPING_ADDRESS_OK_TO_PROCEED_BTN)
  }

  async getSaveAddress () {
    return this.getText(SHIPPING_ADDRESS_SAVED_ADDRESS)
  }

  async getSelectedDistrictText () {
    return this.getText(SHIPPING_ADDRESS_SELECTED_DISTRICT)
  }

  async clickCaliforniaPropClickHere () {
    await this.moveToLocatorPosition(SHIPPING_ADDRESS_CALIFORNIA_PROP_CLICK_HERE)
    await this.driver.sleep(20000)
    await this.click(SHIPPING_ADDRESS_CALIFORNIA_PROP_CLICK_HERE)
  }

  async getCaliforniaPropPopUpText () {
    await this.waitForDisplayed(SHIPPING_ADDRESS_CALIFORNIA_PROPOSITION_POPUP, 10)
    return this.getText(SHIPPING_ADDRESS_CALIFORNIA_PROPOSITION_POPUP)
  }

  async clickCaliforniaPropPopUpClose () {
    await this.click(SHIPPING_ADDRESS_CALIFORNIA_PROP_CLOSE)
  }

  async clickOnBottomLogo () {
    await this.waitForDisplayed(OPTAVIA_BOTTOM_LOGO)
    await this.click(OPTAVIA_BOTTOM_LOGO)
  }

  async getAllHKDistricts () {
    return this.getText(SHIPPING_ADDRESS_DISTRICT)
  }

  async addAddressLine1 (addressLine1) {
    await this.sendKeys(SHIPPING_ADDRESS_LINE1, addressLine1)
  }

  async isShippingAddressFieldsDisplayed (market: any) {
    let addressCommonFields = [SHIPPING_ADDRESS_FIRST_NAME, SHIPPING_ADDRESS_LAST_NAME, SHIPPING_ADDRESS_LINE1,
      SHIPPING_ADDRESS_LINE2, SHIPPING_ADDRESS_PHONE, SHIPPING_ADDRESS_COUNTRY_TEXT_BOX]
    for (let i = 0; i < addressCommonFields.length; i++) {
      let field = await this.locatorFound(addressCommonFields[i])
      if (field === false) {
        return false
      }
    }
    if (market === 3) {
      let district = await this.locatorFound(SHIPPING_ADDRESS_DISTRICT)
      if (district === false) {
        return false
      }
    }
    if (market === 2) {
      let postalCode = await this.locatorFound(SHIPPING_ADDRESS_POSTAL_CODE)
      if (postalCode === false) {
        return false
      }
    }
    return true
  }

  async isCountryDisabled () {
    return this.hasAttributePresent(SHIPPING_ADDRESS_COUNTRY, 'disabled')
  }

  async getDisabledCountryText () {
    return this.getText(SHIPPING_ADDRESS_COUNTRY_SELECTED)
  }

  async clickViewAddress () {
    await this.click(SHIPPING_ADDRESS_VIEW_ADDRESS)
  }

  async verifySaveAddressPopUpIsDisplayed () {
    await this.waitForDisplayed(SHIPPING_ADDRESS_SAVED_ADDRESS)
  }

  async getFirstNameText () {
    return this.getAttributesValue(SHIPPING_ADDRESS_FIRST_NAME, 'value')
  }

  async getLastNameText () {
    return this.getAttributesValue(SHIPPING_ADDRESS_LAST_NAME, 'value')
  }

  async getAddressLine1Text () {
    return this.getAttributesValue(SHIPPING_ADDRESS_LINE1, 'value')
  }

  async getPostalCodeText () {
    return this.getAttributesValue(SHIPPING_ADDRESS_ZIP, 'value')
  }

  async getOrderSubtotal () {
    return this.getTextByIndex(SHIPPING_ADDRESS_ORDER_SUBTOTAL, 0)
  }

  async getPremierDeliveryChargeText () {
    return this.getText(SHIPPING_ADDRESS_DELIVERY_CHARGE)
  }

  async isShippingAddressFieldsRequired (market: any) {
    let addressCommonFields = [SHIPPING_ADDRESS_FIRST_NAME, SHIPPING_ADDRESS_LAST_NAME, SHIPPING_ADDRESS_LINE1,
      SHIPPING_ADDRESS_PHONE, SHIPPING_ADDRESS_COUNTRY_TEXT_BOX]
    for (let i = 0; i < addressCommonFields.length; i++) {
      let field = await this.getAttributesValue(addressCommonFields[i], 'aria-required')
      if (field === false) {
        return false
      }
    }
    if (market === 3) {
      let district = await this.getAttributesValue(SHIPPING_ADDRESS_DISTRICT, 'aria-required')
      if (district === false) {
        return false
      }
    }
    if (market === 2) {
      let postalCode = await this.getAttributesValue(SHIPPING_ADDRESS_POSTAL_CODE, 'aria-required')
      if (postalCode === false) {
        return false
      }
    }
    return true
  }

  async getCountryCodesFromDropdown () {
    return this.getText(SHIPPING_ADDRESS_COUNTRY_CODE)
  }

  async addShippingAddressWithoutPhoneSG (addressline1: string, addressline2: string, zip: any, countrycode: any) {
    await this.sendKeys(SHIPPING_ADDRESS_LINE1, addressline1)
    await this.sendKeys(SHIPPING_ADDRESS_LINE2, addressline2)
    await this.sendKeys(SHIPPING_ADDRESS_POSTAL_CODE, zip)
    await this.selectDropDownByText(SHIPPING_ADDRESS_COUNTRY_CODE, countrycode)
  }

  async sendKeysToPhoneField (phone: string) {
    await this.sendKeys(SHIPPING_ADDRESS_PHONE, phone)
  }

  async getPhoneErrorMessage () {
    return this.getText(SHIPPING_ADDRESS_PHONE_ERROR_TEXT)
  }

  async addShippingAddressWithoutPhoneHK (addressline1: string, addressline2: string, district: any, countrycode: any) {
    await this.sendKeys(SHIPPING_ADDRESS_LINE1, addressline1)
    await this.sendKeys(SHIPPING_ADDRESS_LINE2, addressline2)
    await this.selectDropDownByText(SHIPPING_ADDRESS_DISTRICT, district, 4)
    await this.selectDropDownByText(SHIPPING_ADDRESS_COUNTRY_CODE, countrycode)
  }

  async isZipCodeErrorMessageDisplayed () {
    let errorStatus = await this.locatorFound(SHIPPING_ADDRESS_ZIP_ERROR)
    let message = await this.getText(SHIPPING_ADDRESS_ZIP_ERROR)
    return errorStatus && message === 'Please provide a valid Postal Code.'
  }

  async getZipErrorMessage () {
    await this.waitForDisplayed(SHIPPING_ADDRESS_ZIP_ERROR, 7)
    return this.getText(SHIPPING_ADDRESS_ZIP_ERROR)
  }

  async getAddressFieldsErrorMessage (country: number = market.US) {
    let addressFieldsUS = [SHIPPING_ADDRESSLINE_1_ERROR, SHIPPING_ADDRESS_CITY_ERROR, SHIPPING_STATE_PROVINCE_ERROR, SHIPPING_ADDRESS_ZIP_ERROR,
      SHIPPING_COUNTRY_CODE_ERROR, SHIPPING_PHONE_ERROR]
    let addressFieldsHK = [SHIPPING_ADDRESSLINE_1_ERROR, SHIPPING_DISTRICT_ERROR, SHIPPING_COUNTRY_CODE_ERROR, SHIPPING_PHONE_ERROR]
    let addressFieldsSG = [SHIPPING_ADDRESSLINE_1_ERROR, SHIPPING_ADDRESS_ZIP_ERROR, SHIPPING_COUNTRY_CODE_ERROR, SHIPPING_PHONE_ERROR]
    let errorMessages = []
    let addressFields = country === market.HK ? addressFieldsHK : country === market.SG ? addressFieldsSG : addressFieldsUS
    for (let i = 0; i < addressFields.length; i++) {
      errorMessages.push(await this.getText(addressFields[i]))
    }
    return errorMessages
  }

  async getGlobalMessageText () {
    await this.waitForDisplayed(SHIPPING_ADDRESS_GLOBAL_MESSAGE, 3)
    return this.getText(SHIPPING_ADDRESS_GLOBAL_MESSAGE)
  }

  async getZipErrorForNewAddressText () {
    await this.waitForDisplayed(SHIPPING_ADDRESS_ZIP_ERROR_NEW_ADDRESS, 5)
    return this.getText(SHIPPING_ADDRESS_ZIP_ERROR_NEW_ADDRESS)
  }

  async isShippingAddressTitleDisplayed () {
    return this.locatorFound(SHIPPING_ADDRESS_TILE_TTL)
  }
}
