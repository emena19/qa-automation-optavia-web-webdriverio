// @flow
import BasePage from '../BasePage'

const PAYMENT_DETAILS_BODY_TTL = '//*[@id="checkoutContentPanel"]/descendant::h5[contains(text(), "Payment Details")]'
const PAYMENT_CARD_TYPE_DROP_DOWN = '#card_cardType'
const PAYMENT_DETAILS_MONTH_DROP_DOWN = '#ExpiryMonth'
const PAYMENT_DETAILS_YEAR_DROP_DOWN = '#ExpiryYear'
const BILLING_ADDRESS_BODY_TTL = '//*[@id="silentOrderPostForm"]/h5'
const BILLING_ADDRESS_CHECKBOX = '#useDeliveryAddress'
const PAYMENT_BILLING_ADDRESS_NEXT_BTN = '//*[@id="silentOrderPostForm"]/div[6]/button'
const PAYMENT_BILLING_ADDRESS_FIRST_NAME_TBOX = '//*[@id="address.firstName"]'
const PAYMENT_BILLING_ADDRESS_CARD_NAME = '#card_nameOnCard'
const PAYMENT_BILLING_ADDRESS_CARD_NUMBER = '#card_accountNumber'
const PAYMENT_BILLING_ADDRESS_CV_NUMBER = '#card_cvNumber'
const EXISTING_CARD_DETAILS = '//span[contains(text(),"1111")]//..//input[@type="radio"]'
const PAYMENT_NEXT_BTN = '#lastInTheForm'
const RECAPTCHA_CHECKBOX = '#recaptcha-anchor'
const PAYMENT_BILLING_VIEW_SAVED_PAYMENTS_BUTTON = '//*[@id="viewSavedPayments"]'
const PAYMENT_BILLING_SELECT_PAYMENT_POP_UP = '//*[@id="savedPaymentList"]/h2'
const PAYMENT_BILLING_POP_UP_USE_THIS_PAYMENT_BUTTON = '//*[@id="savedPaymentList"]/div[2]/div[1]/form[1]/button'

// SG
const PAYMENT_CC_SG = `//input[@aria-label='Credit or debit card number']`
const PAYMENT_EXPIRY_DATE_SG = `//input[@id='encryptedExpiryDate']`
const PAYMENT_CVV_SG = `//input[@id='encryptedSecurityCode' and contains(@style,"display")]`
const PAYMENT_BILLING_ADDRESS_GLOBAL_MESSAGE = '#globalMessages'
const SG_PAYMENT_DETAILS_BODY_TTL = `//*[@id="content"]//h5[contains(text(), "Payment Details")]`
const PAYMENT_NAME_SG = '//*[@id="card-div"]/div/div[2]/div[1]/label/span[2]/input'
const SG_PAYMENT_COUNT_OF_IFRAMES = '<iframe />'

// HK
const PAYMENT_DETAILS_INPUT_NAME_HK = '//input[contains(@class,"adyen-checkout__input")]'
const PAYMENT_DETAILS_INPUT_LIST_HK = '//*[contains(@class,"js-iframe")]'
const PAYMENT_DETAILS_SHIPPING_ADDRESS_TOP_LINK = '//*[@id="checkoutProgress"]/div/div[1]/a'

// US
const BILLING_ADDRESS_FIRST_NAME = '#address.firstName'
const BILLING_ADDRESS_LAST_NAME = '#address.surname'
const BILLING_ADDRESS_ADDRESS_LINE1 = '#address.line1'
const BILLING_ADDRESS_CITY = '#address.townCity'
const BILLING_ADDRESS_STATE = '#address.region'
const BILLING_ADDRESS_ZIP = '#address.postcode'
const BILLING_ADDRESS_ZIP_ERROR_MESSAGE = '#address.postcode-error'

export default class PaymentAndBillingAddressTile extends BasePage {
  async verifyPageIsLoaded () {
    await this.waitForDisplayed(PAYMENT_DETAILS_BODY_TTL, 6)
    await this.waitForDisplayed(BILLING_ADDRESS_BODY_TTL)
    await this.waitForDisplayed(PAYMENT_BILLING_ADDRESS_NEXT_BTN)
  }

  async verifyPageIsLoadedSG () {
    await this.waitForDisplayed(SG_PAYMENT_DETAILS_BODY_TTL)
  }

  async verifyPageIsLoadedHK () {
    await this.waitForDisplayed(SG_PAYMENT_DETAILS_BODY_TTL)
    await this.waitForDisplayed(PAYMENT_DETAILS_INPUT_NAME_HK)
  }

  async getPageTitle () {
    return this.getCurrentPageTitle()
  }

  async getPageUrl () {
    return this.getCurrentPageUrl()
  }

  async getPaymentDetailsBodyTitle () {
    return this.getText(PAYMENT_DETAILS_BODY_TTL)
  }

  async createPaymentDetailsFastForm (cardType: string, nameOnCard: string, cardNumber: any, cardVerificationNumber: any, month: string, year: string) {
    await this.selectDropDownByText(PAYMENT_CARD_TYPE_DROP_DOWN, cardType)
    await this.sendKeys(PAYMENT_BILLING_ADDRESS_CARD_NAME, nameOnCard)
    await this.sendKeys(PAYMENT_BILLING_ADDRESS_CARD_NUMBER, cardNumber)
    await this.sendKeys(PAYMENT_BILLING_ADDRESS_CV_NUMBER, cardVerificationNumber)
    await this.selectDropDownByText(PAYMENT_DETAILS_MONTH_DROP_DOWN, month)
    await this.selectDropDownByText(PAYMENT_DETAILS_YEAR_DROP_DOWN, year)
  }

  async createPaymentDetails (cardType: string, nameOnCard: string, cardNumber: any, cardVerificationNumber: any, month: string, year: string) {
    await browser.pause(4000) // added sleep to fasttrack form submission ticket CRT-861
    await this.selectDropDownByText(PAYMENT_CARD_TYPE_DROP_DOWN, cardType)
    await this.sendKeys(PAYMENT_BILLING_ADDRESS_CARD_NAME, nameOnCard)
    await this.sendKeys(PAYMENT_BILLING_ADDRESS_CARD_NUMBER, cardNumber)
    await this.sendKeys(PAYMENT_BILLING_ADDRESS_CV_NUMBER, cardVerificationNumber)
    await this.selectDropDownByText(PAYMENT_DETAILS_MONTH_DROP_DOWN, month)
    await this.selectDropDownByText(PAYMENT_DETAILS_YEAR_DROP_DOWN, year)
    await browser.pause(4000)
  }

  async clickRecaptchaCheckbox () {
    await this.click(RECAPTCHA_CHECKBOX)
  }

  async getBillingAddressBodyTitle () {
    return this.getText(BILLING_ADDRESS_BODY_TTL)
  }

  async clickToCheckUseDeliveryAddressCheckBox () {
    await this.click(BILLING_ADDRESS_CHECKBOX)
  }

  async isUseDeliveryAddressChecked () {
    return this.isSelected(BILLING_ADDRESS_CHECKBOX)
  }

  async clickNext () {
    await this.waitForDisplayed(PAYMENT_BILLING_ADDRESS_FIRST_NAME_TBOX)
    await this.click(PAYMENT_BILLING_ADDRESS_NEXT_BTN)
  }

  async getGlobalMessage () {
    await this.waitForDisplayed(PAYMENT_BILLING_ADDRESS_GLOBAL_MESSAGE)
    return this.getText(PAYMENT_BILLING_ADDRESS_GLOBAL_MESSAGE)
  }

  async clickNextHK () {
    await this.waitForDisplayed(PAYMENT_NEXT_BTN)
    await this.click(PAYMENT_NEXT_BTN)
  }

  // SG
  async createPaymentDetailsSG (nameOnCard: string, cardNumber: number, month: string, cardVerificationNumber: string) {
    const iFrames = await this.getCountOfIframes()
    let frameIndex
    if (iFrames === 3) {
      frameIndex = 0
    } else {
      frameIndex = iFrames - 3
    }
    await browser.pause(1000)
    await this.sendKeys(PAYMENT_NAME_SG, nameOnCard)
    await this.switchToFrameByIndex(frameIndex)
    await this.sendKeys(PAYMENT_CC_SG, cardNumber)
    await this.switchToDefaultFrame()
    await browser.pause(1000)
    await this.switchToFrameByIndex(frameIndex + 1)
    await browser.pause(1000)
    await this.sendKeys(PAYMENT_EXPIRY_DATE_SG, month)
    await browser.pause(1000)
    await this.switchToDefaultFrame()
    await browser.pause(1000)
    await this.switchToFrameByIndex(frameIndex + 2)
    await browser.pause(1000)
    await this.sendKeys(PAYMENT_CVV_SG, cardVerificationNumber)
    await this.switchToDefaultFrame()
  }

  async clickNextSG () {
    await this.click(PAYMENT_NEXT_BTN)
  }

  async clickOnExistingCardDetailsSG (cardVerificationNumber) {
    await this.waitForDisplayed(EXISTING_CARD_DETAILS)
    await this.click(EXISTING_CARD_DETAILS)
    await this.switchToFrameByIndex(0)
    await browser.pause(1000)
    await this.sendKeys(PAYMENT_CVV_SG, cardVerificationNumber)
    await this.switchToDefaultFrame()
  }

  async clickOnSpecificCardDetails (cardType, last4DigitCardNumber, cardVerificationNumber) {
    const ccNumber = `//span[contains(text(),${last4DigitCardNumber})]//..//input[@type="radio"]`
    await this.waitForDisplayed(ccNumber)
    await this.click(ccNumber)
    const cctype = `//div[contains(@data-pm,"${cardType}")]//div[2]/div/div/div/div/div[2]/div/div/div[2]/label/span/span/iframe`
    await this.switchToElementFrame(cctype)
    await browser.pause(1000)
    await this.sendKeys(PAYMENT_CVV_SG, cardVerificationNumber)
    await this.switchToDefaultFrame()
  }

  async createPaymentDetailsHK (nameOnCard: string, cardNumber: any, expiryDate: string, cardVerificationNumber: any) {
    const iFrames = await this.getCountOfIframes()

    let frameIndex
    if (iFrames === 3) {
      frameIndex = 0
    } else {
      frameIndex = iFrames - 3
    }
    await browser.pause(1000)
    await this.sendKeys(PAYMENT_DETAILS_INPUT_NAME_HK, nameOnCard)
    await this.switchToFrameByIndex(frameIndex)
    await browser.pause(1000)
    await this.sendKeysByIndex(PAYMENT_DETAILS_INPUT_LIST_HK, cardNumber, 0)
    await this.switchToDefaultFrame()
    await browser.pause(1000)
    await this.switchToFrameByIndex(frameIndex + 1)
    await this.sendKeysByIndex(PAYMENT_DETAILS_INPUT_LIST_HK, expiryDate, 1)
    await this.switchToDefaultFrame()
    await browser.pause(1000)
    await this.switchToFrameByIndex(frameIndex + 2)
    await this.sendKeysByIndex(PAYMENT_DETAILS_INPUT_LIST_HK, cardVerificationNumber, 2)
    await this.switchToDefaultFrame()
  }

  async clickViewSavedPaymentsButton () {
    await this.click(PAYMENT_BILLING_VIEW_SAVED_PAYMENTS_BUTTON)
  }

  async selectExistingPaymentFromPopUp () {
    await this.waitForDisplayed(PAYMENT_BILLING_SELECT_PAYMENT_POP_UP)
    await this.click(PAYMENT_BILLING_POP_UP_USE_THIS_PAYMENT_BUTTON)
  }

  async getCountOfIframes () {
    return this.getCountOfArrayList(SG_PAYMENT_COUNT_OF_IFRAMES)
  }

  async clickOnShippingAddressTopMenu () {
    await this.click(PAYMENT_DETAILS_SHIPPING_ADDRESS_TOP_LINK)
  }

  async arePaymentDetailsFieldsDisplayedHKAndSG () {
    let paymentFields = [PAYMENT_CC_SG, PAYMENT_EXPIRY_DATE_SG, PAYMENT_CVV_SG]
    const iFrames = await this.getCountOfIframes()

    let frameIndex
    if (iFrames === 3) {
      frameIndex = 0
    } else {
      frameIndex = iFrames - 3
    }
    let isNameDisplayed = await this.locatorFound(PAYMENT_NAME_SG)
    for (let i = 0; i < paymentFields.length; i++) {
      await this.switchToFrameByIndex(frameIndex)
      await browser.pause(1000)
      let isFieldDisplayed = await this.locatorFound(paymentFields[i])
      await this.switchToDefaultFrame()
      await browser.pause(1000)
      frameIndex++
      if (isFieldDisplayed === false || isNameDisplayed === false) {
        return false
      }
    }
    return true
  }

  async isZipCodeErrorMessageDisplayed () {
    let errorStatus = await this.locatorFound(BILLING_ADDRESS_ZIP_ERROR_MESSAGE)
    let message = await this.getText(BILLING_ADDRESS_ZIP_ERROR_MESSAGE)
    return errorStatus && message === 'Please provide a valid Postal Code.'
  }

  async createBillingAddress (address: Object) {
    await this.sendKeys(BILLING_ADDRESS_FIRST_NAME, address.CUSTOMER_FIRST_NAME)
    await this.sendKeys(BILLING_ADDRESS_LAST_NAME, address.CUSTOMER_LAST_NAME)
    await this.sendKeys(BILLING_ADDRESS_ADDRESS_LINE1, address.SHIPPING_ADDRESS_LINE_1)
    await this.sendKeys(BILLING_ADDRESS_CITY, address.SHIPPING_CITY)
    await this.selectDropDownByText(BILLING_ADDRESS_STATE, address.SHIPPING_STATE)
    await this.sendKeys(BILLING_ADDRESS_ZIP, address.SHIPPING_ZIP)
  }
}
