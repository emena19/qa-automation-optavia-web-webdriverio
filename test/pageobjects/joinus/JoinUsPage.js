
// @flow
import BasePage from '../BasePage'

const JOIN_US_BODY_TITLE = '#content > div > div.container-fluid.becomeCoachIntro'
const JOIN_US_TC_CHECK_BOX = '#tc-agreement-check'
// const RENEW_LICENSE = '//*[@class="cart clearfix"]/a[1]')
const RENEW_LICENSE = '//a[@class="login-account button button-default button-small"]'
const VISIT_COACH_STORE_BTN = '//a[contains(text(),"VISIT COACH STORE")]'
const ADD_TO_CART_BTN = '//*[contains(@id,"addToCartForm")]/button[contains(text(), "Add to Cart")]' // Removed with international
const COACH_AGREEMENT_LINK = '//label[@for="tc-agreement-check"]/a'
const REGISTER_BTN = '#userRegisterForCoachDiv'
const SIGN_UP_BTN = '#userLoginForCoachDiv'
const NATIONAL_ID_VALIDATION = '//*[@id="coachIdValidation.nationalIDHK"]'
const DOB_VALIDATION = '//*[@id="coachIdValidation.dob"]'
const DOB_MONTH_VALIDATION = '.ui-datepicker-month'
const DOB_YEAR_VALIDATION = '.ui-datepicker-year'
const DOB_DAY_VALIDATION = '.ui-state-default'
const VERIFY_VALIDATION_BTN = '#js-coachIdValidationForm-submit'
const ADD_ENGLISH_BUSINESS_KIT_BTN = '//*[@id="addToCartForm81808-optavia-coach-business-kit-kt"]/button[contains(text(), "Add English Business Kit")]'
const ADD_CHINESE_BUSINESS_KIT_BTN = '//*[@id="addToCartForm81809-optavia-coach-business-kit-kt"]/button[contains(text(), "Add Traditional Chinese Business Kit")]'
const NATIONAL_ID_ERROR_MESSAGE_TEXT = '#coachIdValidation.nationalIDHK-error'
const COACH_FIRST_NAME = '//*[@id="coachIdValidation.firstName"]'
const COACH_LAST_NAME = '//*[@id="coachIdValidation.lastName"]'

export default class JoinUsPage extends BasePage {
  async verifyPageIsLoaded () {
    await this.waitForDisplayed(JOIN_US_BODY_TITLE)
  }

  async getPageTitle () {
    return this.getCurrentPageTitle()
  }

  async getPageUrl () {
    return this.getCurrentPageUrl()
  }

  async clickCoachAgreementLink () {
    await this.click(COACH_AGREEMENT_LINK)
  }

  async clickCheckBox () {
    await this.moveToLocatorPosition(JOIN_US_TC_CHECK_BOX)
    await this.click(JOIN_US_TC_CHECK_BOX, 3)
  }

  async getRenewLicenseBtnAttributeValue () {
    return this.getAttributesValue(RENEW_LICENSE, 'class')
  }

  async waitRenewLicenseBtnIsVisible () {
    return this.waitForDisplayed(RENEW_LICENSE)
  }

  async clickRenewLicenseBtn () {
    await this.click(RENEW_LICENSE)
  }

  async getVisitCoachStoreBtnAttributeValue () {
    return this.getAttributesValue(VISIT_COACH_STORE_BTN, 'class')
  }

  async waitCoachStoreBtnIsVisible () {
    return this.waitForDisplayed(VISIT_COACH_STORE_BTN)
  }

  async clickVisitCoachStoreBtn () {
    await this.click(VISIT_COACH_STORE_BTN)
  }

  async checkboxLocatorNotFound () {
    return this.locatorNotFound(JOIN_US_TC_CHECK_BOX)
  }

  // Removed the functionality - No More Add to Cart button with International
  async clickAddToCartBtn () {
    await this.waitForDisplayed(ADD_TO_CART_BTN, 5)
    await this.click(ADD_TO_CART_BTN)
  }

  async isAddToCartBtnEnabled () {
    await this.waitForDisplayed(ADD_TO_CART_BTN, 10)
    await this.moveToLocatorPosition(ADD_TO_CART_BTN)
    return this.isElementEnabled(ADD_TO_CART_BTN)
  }

  async clickRegisterBtn () {
    await this.waitForDisplayed(REGISTER_BTN)
    await this.click(REGISTER_BTN)
  }

  async verifyRegisterBtnIsDisplayed () {
    await this.waitForDisplayed(REGISTER_BTN)
  }

  async verifySignUpBtnIsDisplayed () {
    await this.waitForDisplayed(SIGN_UP_BTN)
  }

  async beginEnrollment (nationalID: string, dob: string) {
    await this.sendKeys(NATIONAL_ID_VALIDATION, nationalID)
    await this.selectDatePicker(DOB_VALIDATION, DOB_DAY_VALIDATION, DOB_MONTH_VALIDATION, DOB_YEAR_VALIDATION, dob)
    await this.moveToLocatorPositionAndClick(JOIN_US_TC_CHECK_BOX)
    await this.moveToLocatorPositionAndClick(VERIFY_VALIDATION_BTN)
    await browser.pause(3000)
    await this.moveToLocatorPositionAndClick(ADD_ENGLISH_BUSINESS_KIT_BTN)
  }

  async beginEnrollmentSG (nationalID: string, dob: string) {
    await this.sendKeys(NATIONAL_ID_VALIDATION, nationalID)
    await this.selectDatePicker(DOB_VALIDATION, DOB_DAY_VALIDATION, DOB_MONTH_VALIDATION, DOB_YEAR_VALIDATION, dob)
    await this.click(JOIN_US_TC_CHECK_BOX)
    await this.moveToLocatorPosition(VERIFY_VALIDATION_BTN)
    await this.click(VERIFY_VALIDATION_BTN)
    await browser.pause(3000)
    await this.moveToLocatorPosition(ADD_TO_CART_BTN)
    await this.click(ADD_TO_CART_BTN)
  }

  async verifyBeginEnrollmentIsLoaded () {
    await this.waitForDisplayed(NATIONAL_ID_VALIDATION)
    await this.waitForDisplayed(DOB_VALIDATION)
  }

  async clickVerifyValidationBtn () {
    await this.waitForDisplayed(VERIFY_VALIDATION_BTN)
    await this.moveToLocatorPositionAndClick(VERIFY_VALIDATION_BTN)
  }

  async clickAddEnglishKitBtn () {
    await this.waitForDisplayed(ADD_ENGLISH_BUSINESS_KIT_BTN)
    await this.moveToLocatorPositionAndClick(ADD_ENGLISH_BUSINESS_KIT_BTN)
  }

  async clickSignInBtn () {
    await this.click(SIGN_UP_BTN)
  }

  async verifyKitBtnsAreDisabled () {
    let chinesseKit = await this.hasAttributePresent(ADD_CHINESE_BUSINESS_KIT_BTN, 'disabled')
    let englishKit = await this.hasAttributePresent(ADD_ENGLISH_BUSINESS_KIT_BTN, 'disabled')
    return [chinesseKit, englishKit]
  }

  async getNationalIDErrorText () {
    await this.waitForDisplayed(NATIONAL_ID_ERROR_MESSAGE_TEXT, 3)
    return this.getText(NATIONAL_ID_ERROR_MESSAGE_TEXT)
  }

  async beginEnrollmentWithoutBuyingKit (nationalID: string, dob: string) {
    await this.sendKeys(NATIONAL_ID_VALIDATION, nationalID)
    await this.selectDatePicker(DOB_VALIDATION, DOB_DAY_VALIDATION, DOB_MONTH_VALIDATION, DOB_YEAR_VALIDATION, dob)
    await this.moveToLocatorPositionAndClick(JOIN_US_TC_CHECK_BOX)
    await this.moveToLocatorPositionAndClick(VERIFY_VALIDATION_BTN)
    await browser.pause(3000)
  }

  async getCoachFirstNameText () {
    await this.waitForDisplayed(COACH_FIRST_NAME, 3)
    return this.getAttributesValue(COACH_FIRST_NAME, 'value')
  }

  async getCoachLastNameText () {
    await this.waitForDisplayed(COACH_LAST_NAME, 3)
    return this.getAttributesValue(COACH_LAST_NAME, 'value')
  }

  async getCoachNationalIDText () {
    await this.waitForDisplayed(NATIONAL_ID_VALIDATION, 3)
    return this.getText(NATIONAL_ID_VALIDATION)
  }

  async getCoachDOBText () {
    await this.waitForDisplayed(DOB_VALIDATION, 3)
    return this.getText(DOB_VALIDATION)
  }
}
