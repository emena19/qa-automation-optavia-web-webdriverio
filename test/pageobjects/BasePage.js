// @flow
import {Key, until} from 'selenium-webdriver'
import CommonUtils from '/src/lib/CommonUtils'
import { assert } from 'chai'

/**
 * Function to wait until locator is located
 * @param driver
 * @param locator
 * @param retries
 * @returns {Promise<void>}
 */
async function waitForLocated (driver: WebDriverClass, locator: WebDriverLocator, retries: number = 7): Promise<void> {
  try {
    await driver.wait(until.elementLocated(locator), 3000)
  } catch (err) {
    if (retries === 0) {
      throw new Error(`Still not able to locate element ${locator.toString()} after maximum retries, Error message: ${err.message.toString()}`)
    }
    await browser.pause(250)
    return waitForLocated(driver, locator, retries - 1)
  }
}

/**
 * Function to wait until web element is visible
 * @param driver
 * @param locator
 * @param retries
 * @returns {Promise<void>}
 */
async function waitForVisible (driver: WebDriverClass, locator: WebDriverLocator, retries: number = 7): Promise<void> {
  try {
    const element = await driver.findElement(locator)
    await driver.wait(until.elementIsVisible(element), 3000)
  } catch (err) {
    if (retries === 0) {
      throw new Error(`Element ${locator.toString()} still not visible after maximum retries, Error message: ${err.message.toString()}`)
    }
    await driver.sleep(250)
    return waitForVisible(driver, locator, retries - 1)
  }
}

/**
 * Function to wait until web element is visible
 * @param driver
 * @param locator
 * @param retries
 * @returns {Promise<void>}
 */
async function waitForNotVisible (driver: WebDriverClass, locator: WebDriverLocator, retries: number = 3): Promise<void> {
  try {
    const element = await driver.findElement(locator)
    await driver.wait(until.elementIsNotVisible(element), 3000)
  } catch (err) {
    if (retries === 0) {
      throw new Error(`Element ${locator.toString()} is visible after maximum retries, Error message: ${err.message.toString()}`)
    }
    await driver.sleep(250)
    return waitForNotVisible(driver, locator, retries - 1)
  }
}

export default class BasePage {
    driver: WebDriverClass
    browser: WebdriverIO.Browser

    constructor (webdriver: WebDriverClass) {
      this.driver = webdriver
    }

    /**
     * Function to wait until locator is located and web element is visible
     * @param locator
     * @param retries
     * @returns {Promise<WebDriverElementPromise>}
     */
    async waitForDisplayed (locator: WebDriverLocator, retries: number = 1): Promise<WebDriverElement> {
      try {
        await $(locator).waitForDisplayed()
        return true
      } catch (err) {
        if (retries === 0) {
          throw new Error(`Element ${locator.toString()} is not displayed after maximum retries, Error message: ${err.message.toString()}`)
        }
      }
      await browser.pause(250)
      return this.waitForDisplayed(locator, retries - 1)
    }

    /**
     * Function to wait until locator is located and web element is not visible
     * @param locator
     * @param retries
     * @returns {Promise<WebDriverElementPromise>}
     */
    async waitForNotDisplayed (locator: WebDriverLocator, errmsg, retries: number = 1): Promise<void> {
      try {
        await waitForLocated(this.driver, locator)
        await waitForNotVisible(this.driver, locator)
      } catch (err) {
        const er = new Error(`${errmsg}`)
        assert.ifError(er)
      }
    }

    /**
     * Function for locator is enabled
     * @param locator
     * @param retries
     * @returns {Promise<WebDriverElementPromise>}
     */
    async isElementEnabled (locator: WebDriverLocator, retries: number = 1): Promise<boolean> {
      try {
        const element = $(locator)
        await element.isEnabled()
        return true
      } catch (err) {
        if (retries === 0) {
          return false
        }
        return this.isElementEnabled(locator, retries - 1)
      }
    }

    /**
     * Function for locator is found
     * @param locator
     * @param retries
     * @returns {Promise<void>}
     */
    async locatorFound (locator: WebDriverLocator, retries: number = 1): Promise<boolean> {
      try {
        const element = await $(locator)
        return (await element).isDisplayed()
      } catch (err) {
        if (retries === 0) {
          return false
        }
        await driver.pause(7000)
        return this.locatorFound(locator, retries - 1)
      }
    }

    /**
     * Function for locator is not found
     * @param locator
     * @param retries
     * @returns {Promise<void>}
     */
    async locatorNotFound (locator: WebDriverLocator, retries: number = 1): Promise<boolean> {
      try {
        await waitForLocated(this.driver, locator)
        return false
      } catch (err) {
        if (retries === 0) {
          return true
        }
        await this.driver.sleep(250)
        return this.locatorNotFound(locator, retries - 1)
      }
    }

    /**
     * Function to get current page title as string
     * @param retries
     * @returns {Promise<*>}
     */
    async getCurrentPageTitle (retries: number = 1): Promise<string> {
      try {
        return browser.getTitle()
      } catch (err) {
        if (retries === 0) {
          throw new Error(`Unable to get page title after maximum retries, Error message: ${err.message}`)
        }
        await browser.sleep(250)
        return this.getCurrentPageTitle(retries - 1)
      }
    }

    /**
     * Function to get current page url as string
     * @param retries
     * @returns {Promise<*>}
     */
    async getCurrentPageUrl (retries: number = 1): Promise<string> {
      try {
        return this.driver.getCurrentUrl()
      } catch (err) {
        if (retries === 0) {
          throw new Error(`Unable to get page title after maximum retries, Error message: ${err.message}`)
        }
        await this.driver.sleep(250)
        return this.getCurrentPageUrl(retries - 1)
      }
    }

    /**
     * Function to send values to text box using defined locator
     * @param locator
     * @param keys
     * @param retries
     * @returns {Promise<void>}
     */
    async sendKeys (locator: WebDriverLocator, keys: any, retries: number = 1): Promise<void> {
      try {
        const element = await $(locator)
        await (await element).click()
        await (await element).clearValue()
        await (await element).setValue(keys)
      } catch (err) {
        if (retries === 0) {
          throw new Error(`Unable to send keys to ${locator.toString()} after maximum retries, Error message: ${err.message}`)
        }
        await browser.pause(250)
        return this.sendKeys(locator, keys, retries - 1)
      }
    }

    /**
     * Function to send values to text box using defined index of elements based on locator
     * @param locator
     * @param keys
     * @param index
     * @param retries
     * @returns {Promise<void>}
     */

    async sendKeysByIndex (locator: WebDriverLocator, keys: string, index: number, retries: number = 1): Promise<void> {
      try {
        const elementList = await $$(locator)
        await (await elementList[index].click())
        await (await elementList[index].clearValue())
        await (await elementList[index].setValue(keys))
      } catch (err) {
        if (retries === 0) {
          throw new Error(`Unable to send keys to ${locator.toString()} after maximum retries, Error message: ${err.message.toString()}`)
        }
        await browser.pause(250)
        return this.sendKeysByIndex(locator, keys, retries - 1)
      }
    }

    /**
     * Function to get text using defined locator
     * @param locator
     * @param retries
     * @returns {Promise<*>}
     */
    async getText (locator: WebDriverLocator, retries: number = 1): Promise<string> {
      try {
        const element = await $(locator)
        return (await element).getText()
      } catch (err) {
        if (retries === 0) {
          throw new Error(`Unable to get ${locator.toString()} text after maximum retries, Error message: ${err.message}`)
        }
        await browser.pause(250)
        return this.getText(locator, retries - 1)
      }
    }

    /**
     * Function get text using index number from arrays of locator
     * @param locator
     * @param key
     * @param retries
     * @returns {Promise<*>}
     */
    async getTextByIndex (locator: WebDriverLocator, key: number, retries: number = 1): Promise<string> {
      try {
        const elements = await $$(locator)
        return (await elements[key]).getText()
      } catch (err) {
        if (retries === 0) {
          throw new Error(`Unable to get ${locator.toString()} text after maximum retries, Error message: ${err.message}`)
        }
        await browser.pause(250)
        return this.getTextByIndex(locator, key, retries - 1)
      }
    }

    /**
     * Function to get web element attributes value using locator
     * @param locator
     * @param attrib - attributes to extract value of
     * @param retries
     * @returns {Promise<*>}
     */
    async getAttributesValue (locator: WebDriverLocator, attrib: string, retries: number = 1): Promise<string> {
      try {
        const element = await $(locator)
        return (await element).getAttribute(attrib)
      } catch (err) {
        if (retries === 0) {
          throw new Error(`Unable to get ${locator.toString()} attributes value after maximum retries, Error message: ${err.message}`)
        }
        await browser.pause(250)
        return this.getAttributesValue(locator, attrib, retries - 1)
      }
    }

    /**
     * Function to get web element attributes value using locator
     * @param locator
     * @param attrib - attributes to extract value of
     * @param index
     * @param retries
     * @returns {Promise<*>}
     */
    async getAttributesValueByIndex (locator: WebDriverLocator, attrib: string, index: number, retries: number = 1): Promise<string> {
      try {
        const elements = await this.driver.findElements(locator)
        return elements[index].getAttribute(attrib)
      } catch (err) {
        if (retries === 0) {
          throw new Error(`Unable to get ${locator.toString()} attributes value after maximum retries, Error message: ${err.message}`)
        }
        await this.driver.sleep(250)
        return this.getAttributesValueByIndex(locator, attrib, index, retries - 1)
      }
    }

    /**
     * Function to get web element attributes value using locator
     * @param locator
     * @param attrib - attributes to extract value of
     * @param retries
     * @returns {Promise<*>}
     */
    async hasAttributePresent (locator: WebDriverLocator, attrib: string, retries: number = 1): Promise<boolean> {
      try {
        let result = false
        const element = await $(locator)
        const text = (await element).getAttribute(attrib)
        if (text !== null) {
          result = true
        } else if (text === '') {
          result = true
        } else if (text === ' ') {
          result = true
        }
        return result
      } catch (err) {
        if (retries === 0) {
          throw new Error(`Unable to get ${locator.toString()} attributes value after maximum retries, Error message: ${err.message}`)
        }
        await browser.pause(250)
        return this.hasAttributePresent(locator, attrib, retries - 1)
      }
    }

    /**
     * Function to click on web element using defined locator
     * @param locator
     * @param retries
     * @returns {Promise<*>}
     */
    async click (locator: WebDriverLocator, retries: number = 3): Promise<void> {
      await this.waitForPageToBeLoaded()
      try {
        await $(locator).click()
      } catch (err) {
        if (retries === 0) {
          throw new Error(`Still not able to click ${locator.toString()} after maximum retries ${retries}, Error message: ${err.message.toString()}`)
        }
        await browser.pause(250)
        return this.click(locator, retries - 1)
      }
    }

    /**
     * Function to double click on web element using defined locator
     * @param locator
     * @param retries
     * @returns {Promise<*>}
     */
    async doubleClick (locator: WebDriverLocator, retries: number = 1): Promise<void> {
      try {
        const element = await this.driver.findElement(locator)
        const actions = await this.driver.actions({bridge: true})
        await actions.doubleClick(element).perform()
      } catch (err) {
        if (retries === 0) {
          throw new Error(`Still not able to double click ${locator.toString()} after maximum retries, Error message: ${err.message.toString()}`)
        }
        await this.driver.sleep(250)
        return this.doubleClick(locator, retries - 1)
      }
    }

    /**
     * Function to click on web element using index number from array of locators
     * @param locator
     * @param key
     * @param retries
     * @returns {Promise<*>}
     */
    async clickByIndex (locator: WebDriverLocator, key: number, retries: number = 1): Promise<void> {
      try {
        const elements = await $$(locator)
        await elements[key].click()
        return true
      } catch (err) {
        if (retries === 0) {
          throw new Error(`Still not able to click ${locator.toString()} after maximum retries, Error message: ${err.message.toString()}`)
        }
        await browser.pause(250)
        return this.clickByIndex(locator, key, retries - 1)
      }
    }

    /**
     * Function to click on web element using visible text from arrays of locators
     * @param locator
     * @param keys
     * @param retries
     * @returns {Promise<void>}
     */
    async clickByText (locator: WebDriverLocator, keys: string, retries: number = 1): Promise<void> {
      try {
        await this.waitForDisplayed(locator)
        const elementList = await $$(locator)
        for (let element of elementList) {
          let option = await (await element).getText()
          if (option === keys) {
            await element.click()
          }
        }
      } catch (err) {
        if (retries === 0) {
          throw new Error(`Still not able to click ${locator.toString()} after maximum retries, Error message: ${err.message.toString()}`)
        }
        await browser.pause(250)
        return this.clickByText(locator, keys, retries - 1)
      }
    }

    /**
     * Function to move mouse to defined web elements using locator
     * @param locator
     * @param retries
     * @returns {Promise<void>}
     */
    async moveToLocatorPosition (locator: WebDriverLocator, retries: number = 3): Promise<void> {
      await this.waitForPageToBeLoaded()
      try {
        let element = $(locator)
        await element.scrollIntoView()
        await element.moveTo()
        await browser.pause(500)
      } catch (err) {
        if (retries === 0) {
          throw new Error(`Still not able to move to ${locator.toString()} after maximum retries, Error message: ${err.message.toString()}`)
        }
        await browser.pause(250)
        return this.moveToLocatorPosition(locator, retries - 1)
      }
    }

    /**
     * Function to move mouse to defined web elements using locator
     * @param locator
     * @param key
     * @param retries
     * @returns {Promise<void>}
     */
    async moveToLocatorPositionByIndex (locator: WebDriverLocator, key: number, retries: number = 3): Promise<void> {
      try {
        const elements = await $$(locator)
        await (await elements[key]).moveTo()
      } catch (err) {
        if (retries === 0) {
          throw new Error(`Still not able to move to ${locator.toString()} after maximum retries, Error message: ${err.message.toString()}`)
        }
        await browser.pause(250)
        return this.moveToLocatorPositionByIndex(locator, key, retries - 1)
      }
    }

    /**
     * Function to move mouse to defined web elements using locator and clicking
     * @param locator
     * @param retries
     * @returns {Promise<void>}
     */
    async moveToLocatorPositionAndClick (locator: WebDriverLocator, retries: number = 3): Promise<void> {
      try {
        let element = await $(locator)
        await (await element).waitForDisplayed()
        await (await element).scrollIntoView()
        await (await element).moveTo()
        await browser.pause(500)
        await (await element).click()
      } catch (err) {
        if (retries === 0) {
          throw new Error(`Still not able to move to ${locator.toString()} after maximum retries, Error message: ${err.message.toString()}`)
        }
        await browser.pause(250)
        return this.moveToLocatorPositionAndClick(locator, retries - 1)
      }
    }

    /**
     * Function to move mouse to defined web elements using locator and clicking
     * @param locator
     * @param retries
     * @returns {Promise<void>}
     */
    async moveToLocatorPositionAndDoubleClick (locator: WebDriverLocator, retries: number = 3): Promise<void> {
      try {
        let element = await $(locator)
        await (await element).moveTo()
        await (await element).doubleClick()
      } catch (err) {
        if (retries === 0) {
          throw new Error(`Still not able to move to ${locator.toString()} after maximum retries, Error message: ${err.message.toString()}`)
        }
        await browser.pause(250)
        return this.moveToLocatorPositionAndClick(locator, retries - 1)
      }
    }

    /**
     * Function to move mouse to defined web elements using locator and clicking
     * @param locator
     * @param key
     * @param retries
     * @returns {Promise<void>}
     */
    async moveToLocatorPositionAndClickByIndex (locator: WebDriverLocator, key: number, retries: number = 3): Promise<void> {
      try {
        const actions = await this.driver.actions({bridge: true})
        let elements = await this.driver.findElements(locator)
        await this.scrollElementIntoViewByIndex(locator, key)
        await actions.move({duration: 3000, origin: elements[key], x: 0, y: 100}).click().perform()
      } catch (err) {
        if (retries === 0) {
          throw new Error(`Still not able to move to ${locator.toString()} after maximum retries, Error message: ${err.message.toString()}`)
        }
        await this.driver.sleep(250)
        return this.moveToLocatorPositionAndClickByIndex(locator, key, retries - 1)
      }
    }

    /**
     * Function to select visible text from dropdown list using locator
     * @param locator
     * @param key
     * @param retries
     * @returns {Promise<void>}
     */
    async selectDropDownByText (locator: WebDriverLocator, key: string, retries: number = 1): Promise<void> {
      try {
        const selectList = await $(locator)
        await (await selectList).selectByVisibleText(key)
      } catch (err) {
        if (retries === 0) {
          throw new Error(`Still not able to click ${locator.toString()} after maximum retries, Error message: ${err.message.toString()}`)
        }
        await browser.pause(250)
        return this.selectDropDownByText(locator, key, retries - 1)
      }
    }

    /**
     * Function to send search values to text box using defined locator and pressing ENTER Key
     * @param locator
     * @param keys
     * @param retries
     * @returns {Promise<void>}
     */
    async searchKeys (locator: WebDriverLocator, keys: string, retries: number = 1): Promise<void> {
      try {
        const element = await this.driver.findElement(locator)
        await element.click()
        await element.clear()
        await element.sendKeys(keys)
      } catch (err) {
        if (retries === 0) {
          throw new Error(`Unable to send keys to ${locator.toString()} after maximum retries, Error message: ${err.message}`)
        }
        await this.driver.sleep(250)
        return this.searchKeys(locator, keys, retries - 1)
      }
    }

    /**
     * Function to return array of elements based on locator
     * @param locator
     * @param retries
     * @returns {Promise<WebDriverElementPromise>}
     */
    async getElementsArray (locator: WebDriverLocator, retries: number = 3): Promise<WebDriverElement[]> {
      try {
        await waitForLocated(this.driver, locator, retries)
        await waitForVisible(this.driver, locator, retries)
        return this.driver.findElements(locator)
      } catch (err) {
        if (retries === 0) {
          throw new Error(`Unable to get ${locator.toString()} text after maximum retries, Error message: ${err.message}`)
        }
      }
      await this.driver.sleep(250)
      return this.getElementsArray(locator, retries - 1)
    }

    /**
     * Function to get length of the array
     * @param locator
     * @param retries
     * @returns {Promise<*>}
     */
    async getCountOfArrayList (locator: WebDriverLocator, retries: number = 1): Promise<number> {
      try {
        const elements = await $$(locator)
        return elements.length
      } catch (err) {
        if (retries === 0) {
          throw new Error(`Unable to get ${locator.toString()} text after maximum retries, Error message: ${err.message}`)
        }
        await browser.pause(250)
        return this.getCountOfArrayList(locator, retries - 1)
      }
    }

    async getElementsTextArrayList (locator: WebDriverLocator, retries: number = 1): Promise<any> {
      await this.waitForPageToBeLoaded()
      await this.waitForDisplayed(locator)
      try {
        const elements = await $$(locator)
        const pLength = elements.length
        let elementsArrayList = []
        for (let p = 0; p < pLength; p++) {
          let text = await (await elements[p]).getText()
          elementsArrayList[p] = text.toString().trim()
        }
        return elementsArrayList
      } catch (err) {
        if (retries === 0) {
          throw new Error(`Unable to get ${locator.toString()} text after maximum retries, Error message: ${err.message}`)
        }
        await browser.sleep(250)
        return this.getElementsTextArrayList(locator, retries - 1)
      }
    }

    async getElementsAttributeValueArrayList (locator: WebDriverLocator, attrib: string, retries: number = 1): Promise<any> {
      try {
        const elements = await $$(locator)
        const pLength = elements.length
        let elementsArrayList = []
        for (let p = 0; p < pLength; p++) {
          elementsArrayList[p] = await elements[p].getAttribute(attrib)
        }
        return elementsArrayList
      } catch (err) {
        if (retries === 0) {
          throw new Error(`Unable to get ${locator.toString()} text after maximum retries, Error message: ${err.message}`)
        }
        await browser.pause(250)
        return this.getElementsAttributeValueArrayList(locator, attrib, retries - 1)
      }
    }

    async getExistingEmailId () {
      try {
        let fs = require('fs')
        let path = process.cwd()
        let buffer = fs.readFileSync(path + '/write.txt')
        return buffer.toString()
      } catch (err) {
        throw new Error(`${err.message}`)
      }
    }

    async StoreEmailID (email) {
      let fs = require('fs')
      fs.writeFileSync('write.txt', email)
    }

    async switchToFrameByIndex (data: string) {
      try {
        const num = parseInt(data)
        browser.switchToFrame(num)
      } catch (err) {
        throw new Error(`Unable to get text after maximum retries, Error message: ${err.message}`)
      }
      return this.switchToFrameByIndex
    }

    async switchToDefaultFrame () {
      try {
        browser.switchToParentFrame()
      } catch (err) {
        throw new Error(`Unable to get text after maximum retries, Error message: ${err.message}`)
      }
      return this.switchToDefaultFrame
    }

    /**
     * Function to locate an element which is inside the iframe
     * @param locator
     * @returns {Promise<*>}
     */
    async switchToElementFrame (locator: WebDriverLocator): Promise<any> {
      try {
        const element = await $(locator)
        browser.switchToFrame(element)
      } catch (err) {
        throw new Error(`Unable to locate an element which is inside the iframe, Error message: ${err.message}`)
      }
      return this.switchToElementFrame
    }

    async selectDatePicker (dobFieldActivator: WebDriverLocator, dayLocator: WebDriverLocator, monthLocator: WebDriverLocator, yearLocator: WebDriverLocator, dob: any, retries: number = 1): Promise<void> {
      try {
        await $(dobFieldActivator).click()
        let day = await CommonUtils.getDayFromDate(dob)
        let month = await CommonUtils.getMonthFromDate(dob)
        let year = await CommonUtils.getYearFromDate(dob)
        await this.click(yearLocator)
        await this.selectDropDownByText(yearLocator, year)
        await this.click(monthLocator)
        await this.selectDropDownByText(monthLocator, month)

        await this.clickByText(dayLocator, day)
      } catch (err) {
        throw new Error(`Unable to get text after maximum retries, Error message: ${err.message}`)
      }
    }

    async scrollElementIntoViewByIndex (locator: WebDriverLocator, key: number, retries: number = 3): Promise<void> {
      try {
        let elements = await this.driver.findElements(locator)
        await this.driver.executeScript('arguments[0].scrollIntoView()', elements[key])
      } catch (err) {
        if (retries === 0) {
          throw new Error(`Still not able to move to ${locator.toString()} after maximum retries, Error message: ${err.message.toString()}`)
        }
        await this.driver.sleep(250)
        return this.scrollElementIntoViewByIndex(locator, key, retries - 1)
      }
    }

    async scrollElementIntoView (locator: WebDriverLocator, retries: number = 3): Promise<void> {
      try {
        await $(locator).scrollIntoView({ block: 'center', inline: 'nearest' })
      } catch (err) {
        if (retries === 0) {
          throw new Error(`Still not able to move to ${locator.toString()} after maximum retries, Error message: ${err.message.toString()}`)
        }
        await browser.pause(250)
        return this.scrollElementIntoView(locator, retries - 1)
      }
    }

    async waitForSpinnerToDisappear (locator: WebDriverLocator, retries: number = 1): Promise<void> {
      try {
        let spinner
        for (let i = 0; i < 10; i++) {
          spinner = await this.locatorFound(locator)
          if (spinner) {
            await browser.pause(3000)
          } else {
            break
          }
        }
      } catch (err) {
        if (retries === 0) {
          throw new Error(`Spinner still visible ${locator.toString()} after maximum retries, Error message: ${err.message.toString()}`)
        }
        await browser.pause(250)
        return this.waitForSpinnerToDisappear(locator, retries - 1)
      }
    }

    async openUrlInNewTab (url: any): Promise<any> {
      try {
        let script = `window.open('${url}', '_blank')`
        await this.driver.executeScript(script)
        await this.driver.sleep(250)
      } catch (err) {
        throw new Error(`Unable to Open URL in new tab, Error message: ${err.message}`)
      }
    }

    async switchBrowserTab (tabIndex: number) {
      try {
        let tabs = await this.driver.getAllWindowHandles()
        this.driver.switchTo().window(tabs[tabIndex])
        await this.driver.sleep(250)
      } catch (err) {
        throw new Error(`Unable to switch tab, Error message: ${err.message}`)
      }
    }

    async isSelected (locator: WebDriverLocator, retries: number = 1): Promise<boolean> {
      try {
        const element = await $(locator)
        return (await element).isSelected()
      } catch (err) {
        if (retries === 0) {
          return false
        }
        await this.driver.sleep(250)
        return this.isSelected(locator, retries - 1)
      }
    }

    async isElementVisible (locator: WebDriverLocator, retries: number = 1): Promise<any> {
      try {
        const element = await this.driver.findElement(locator)
        return element.isDisplayed()
      } catch (err) {
        if (retries === 0) {
          throw new Error(`Unable to get ${locator.toString()} text after maximum retries, Error message: ${err.message}`)
        }
        await this.driver.sleep(250)
        return this.isElementVisible(locator, retries - 1)
      }
    }

    async getCssAttributeValue (locator: WebDriverLocator, attrib: string, retries: number = 1): Promise<string> {
      try {
        const element = await this.driver.findElement(locator)
        return element.getCssValue(attrib)
      } catch (err) {
        if (retries === 0) {
          throw new Error(`Unable to get ${locator.toString()} css attributes value after maximum retries, Error message: ${err.message}`)
        }
        await this.driver.sleep(250)
        return this.getCssAttributeValue(locator, attrib, retries - 1)
      }
    }

    async navigateToURL (url: any): Promise<any> {
      try {
        await this.driver.get(url)
      } catch (err) {
        throw new Error(`Unable to Open URL, Error message: ${err.message}`)
      }
    }

    async browserRefresh () {
      await this.driver.navigate().refresh()
    }

    async closeTab () {
      await this.driver.close()
    }

    async navigateBack () {
      await this.driver.navigate().back()
    }

    async sendTabKeyToCurrentElement () {
      try {
        const element = await this.driver.switchTo().activeElement()
        await element.sendKeys(Key.TAB)
      } catch (err) {
        throw new Error(`Unable to send tab key after maximum retries, Error message: ${err.message}`)
      }
    }

    async waitForPageToBeLoaded (timeoutMs = 30000) {
      const isJSLoaded = async function () {
        const isjQueryReady = await browser.execute('return typeof jQuery !== \'undefined\'')
        if (isjQueryReady) {
          const isDocLoad = await browser.execute('return document.readyState') === 'complete'
          let isjQueryLoaded
          try {
            isjQueryLoaded = await browser.execute('return jQuery.active') === 0
          } catch (err) {
            throw new Error(`Error Message: ${err.message}`)
          }
          return isDocLoad && isjQueryLoaded
        } else {
          return false
        }
      }
      await browser.waitUntil(isJSLoaded, {
        timeout: timeoutMs,
        timeoutMsg: `page is not loaded after ${timeoutMs / 1000} sec`
      })
    }

    /**
     * Function to get a text list using defined locator
     * @param locator
     * @param retries
     * @returns {Promise<*>}
     */
    async getDropdownOptions (locator: WebDriverLocator, retries: number = 1): Promise<string> {
      try {
        let elements = $$(`${locator} option`)
        let options = []
        for (let i = 0; i < await elements.length; i++) {
          let element = await elements[i]
          options.push(await element.getText())
        }
        return options
      } catch (err) {
        if (retries === 0) {
          throw new Error(`Unable to get ${locator.toString()} text after maximum retries, Error message: ${err.message}`)
        }
        await browser.pause(250)
        return this.getDropdownOptions(locator, retries - 1)
      }
    }
}
