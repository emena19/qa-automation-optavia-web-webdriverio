// @flow
import BasePage from '../BasePage'

const BECOME_AN_OPTAVIA_COACH_POP_UP_BODY_TITLE = '//*[@class="user-heading"]'

const LOGIN_SECTION_TITLE = '//*[@class="userLogin"]/h1'
const LOGIN_USER_NAME = '#okta-signin-username'
const LOGIN_PASSWORD = '#okta-signin-password'
const LOGIN_USER_NAME_ERROR = '//*[@class="o-form-fieldset-container"]/descendant::p[1]'
const LOGIN_PASSWORD_ERROR = '//*[@class="o-form-fieldset-container"]/descendant::p[2]'
const LOGIN_COMMON_ERROR = '//*[@class="okta-form-infobox-error infobox infobox-error"]/p'
const LOGIN_FORGOT_YOUR_PASSWORD = '//*[@class="password-forgotten"]'
const LOGIN_SIGN_IN_BUTTON = '#okta-signin-submit'
const LOGIN_GENDER = '#gender'
const LOGIN_DOB = '//*[@id="register.dob"]'

const CREATE_AN_ACCOUNT_SECTION_TITLE = '//*[@class="userRegister"]/h1'
const FIRST_NAME = '//*[@id="register.firstName"]'
const LAST_NAME = '//*[@id="register.lastName"]'
const EMAIL_ADDRESS = '//*[@id="register.email"]'
const PASSWORD = '#password'
const CONFIRM_PASSWORD = '//*[@id="register.checkPwd"]'
const COUNTRY_CODE = '#isdCode'
const COUNTRY_NAME = '//*[@id="register.country"]'
const PHONE = '//*[@id="register.mobileNumber"]'
const CHECK_REGISTER_ACKNOWLEDGE = '//*[@id="register.acknowledge"]'
const REGISTER_BTN = '//*[@id="medifastRegisterForm"]/descendant::button'
const FIRST_NAME_ERROR = '//*[@id="register.firstName-error"]'
const LAST_NAME_ERROR = '//*[@id="register.lastName-error"]'
const EMAIL_ERROR = '//*[@id="register.email-error"]'
const PASSWORD_ERROR = '//*[@id="password-error"]'
const CONFIRM_PASSWORD_ERROR = '//*[@id="register.checkPwd-error"]'
const COUNTRY_CODE_ERROR_MESSAGE = '//*[@id="isdCode-error"]'
const PHONE_ERROR_MESSAGE = '//*[@id="register.mobileNumber-error"]'
const EXISTING_EMAIL_ERROR = '//*[@id="email.errors"]'
const GLOBAL_MESSAGE = '//*[@id="globalMessages"]/ul/li/ul/li'
const USER_OR_PASSWORD_ERROR = '//*[@id="form19"]/div[1]/div[1]/div/div/p'

// Reset Password Popup
const RESET_PASSWORD_TITLE = '//*[@class="headline"]'
const RESET_PASSWORD_EMAIL = '//*[@id="forgottenPwd.email"]'
const RESET_PASSWORD_EMAIL_ERROR = '//*[@id="forgottenPwd.email-error"]'
const RESET_SEND_EMAIL_BUTTON = '//*[@id="forgottenPwdForm"]/button'
const RESET_PASSWORD_GLOBAL_MESSAGE = '//*[@id="validEmail"]/ul/li'

// Welcome to medifast site
const WELCOME_TO_MEDIFAST_TITLE = '//*[@id="content"]/h1'
const WELCOME_TO_MEDIFAST_ANSWER_TEXT = '//*[@id="loginForm.securityAnswer"]'
const WELCOME_TO_MEDIFAST_CREATE_ACCOUNT_BUTTON = '//*[@id="next-button"]'

export default class LoginAndRegisterPage extends BasePage {
  async verifyPageIsLoaded () {
    await this.waitForDisplayed(LOGIN_SECTION_TITLE)
    await this.waitForDisplayed(CREATE_AN_ACCOUNT_SECTION_TITLE)
  }

  async verifyBecomeAnOptaviaCoachPopUpIsLoaded () {
    await this.waitForDisplayed(BECOME_AN_OPTAVIA_COACH_POP_UP_BODY_TITLE)
  }

  async verifyCreateAnAccountPopUpIsLoaded () {
    await this.waitForDisplayed(CREATE_AN_ACCOUNT_SECTION_TITLE)
  }

  async verifyLoginPopUpIsLoaded () {
    await this.waitForDisplayed(LOGIN_SECTION_TITLE)
  }

  async getPageTitle () {
    return this.getCurrentPageTitle()
  }

  async getPageUrl () {
    return this.getCurrentPageUrl()
  }

  async getCreateAnAccountTitleText () {
    return this.getText(CREATE_AN_ACCOUNT_SECTION_TITLE)
  }

  async createAnAccount (firstname: string, lastname: string, email: string, password: string) {
    await this.sendKeys(FIRST_NAME, firstname)
    await this.sendKeys(LAST_NAME, lastname)
    await this.sendKeys(EMAIL_ADDRESS, email)
    await this.waitForDisplayed(PASSWORD, 10)
    await this.sendKeys(PASSWORD, password)
    await browser.pause(2000)
    await this.waitForDisplayed(CONFIRM_PASSWORD, 10)
    await this.sendKeys(CONFIRM_PASSWORD, password)
    await this.moveToLocatorPositionAndClick(REGISTER_BTN)
  }

  async createAnAccountOtherCountry (firstname: string, lastname: string, email: string, password: string, countryCode: any, phone: number) {
    let country
    await this.sendKeys(FIRST_NAME, firstname)
    await this.sendKeys(LAST_NAME, lastname)
    await this.sendKeys(EMAIL_ADDRESS, email)
    await this.sendKeys(PASSWORD, password)
    await this.waitForDisplayed(CONFIRM_PASSWORD, 4)
    await browser.pause(2000)
    await this.sendKeys(CONFIRM_PASSWORD, password)
    await this.moveToLocatorPosition(COUNTRY_CODE)
    if (countryCode.toString() === '+65') {
      country = 'Singapore'
    } else {
      country = 'Hong Kong'
    }
    await this.selectDropDownByText(COUNTRY_CODE, `${countryCode} ${country}`)
    await this.sendKeys(PHONE, phone)
    await browser.pause(1000)
    await this.moveToLocatorPosition(CHECK_REGISTER_ACKNOWLEDGE)
    await this.click(CHECK_REGISTER_ACKNOWLEDGE)
    await this.click(REGISTER_BTN)
  }

  async loginAsExistingCustomer (username: string, password: string, error: boolean = false) {
    await this.sendKeys(LOGIN_USER_NAME, username)
    await this.sendKeys(LOGIN_PASSWORD, password)
    // Add this line to verify the error in Login scenarios
    if (error) {
      await browser.pause(7500)
    }
    await this.click(LOGIN_SIGN_IN_BUTTON)
    await browser.pause(5000)
  }

  async clickRegister () {
    await this.click(REGISTER_BTN)
  }

  async getFirstNamePlaceholderValue () {
    return this.getAttributesValue(FIRST_NAME, 'placeholder')
  }

  async getLastNamePlaceHolderValue () {
    return this.getAttributesValue(LAST_NAME, 'placeholder')
  }

  async getEmailPlaceHolderValue () {
    return this.getAttributesValue(EMAIL_ADDRESS, 'placeholder')
  }

  async getPasswordPlaceHolderValue () {
    return this.getAttributesValue(PASSWORD, 'placeholder')
  }

  async getConfirmPlaceHolderValue () {
    return this.getAttributesValue(CONFIRM_PASSWORD, 'placeholder')
  }

  async getCountryCodePlaceHolderValue () {
    return this.getAttributesValue(COUNTRY_CODE, 'value')
  }

  async getCountryPlaceHolderValue () {
    return this.getAttributesValue(COUNTRY_NAME, 'value')
  }

  async getPhonePlaceHolderValue () {
    return this.getAttributesValue(PHONE, 'placeholder')
  }

  async getFirstNameErrorMessage () {
    return this.getText(FIRST_NAME_ERROR)
  }

  async getEmailErrorMessage () {
    return this.getText(EMAIL_ERROR)
  }

  async getPasswordErrorMessage () {
    return this.getText(PASSWORD_ERROR)
  }

  async getConfirmPasswordErrorMessage () {
    return this.getText(CONFIRM_PASSWORD_ERROR)
  }

  async getLastNameErrorMessage () {
    return this.getText(LAST_NAME_ERROR)
  }

  async getCountryCodeErrorMessage () {
    return this.getText(COUNTRY_CODE_ERROR_MESSAGE)
  }

  async getPhoneErrorMessage () {
    return this.getText(PHONE_ERROR_MESSAGE)
  }

  async sendKeysPassword (keys: string) {
    return this.sendKeys(PASSWORD, keys)
  }

  async sendKeysConfirmPassword (keys: string) {
    return this.sendKeys(CONFIRM_PASSWORD, keys)
  }

  async sendKeysEmailAddress (keys: string) {
    return this.sendKeys(EMAIL_ADDRESS, keys)
  }

  async sendKeysFirstName (keys: string) {
    return this.sendKeys(FIRST_NAME, keys)
  }

  async sendKeysLastName (keys: string) {
    return this.sendKeys(LAST_NAME, keys)
  }

  async getExistingEmailErrorMessage () {
    await this.waitForDisplayed(EXISTING_EMAIL_ERROR)
    return this.getText(EXISTING_EMAIL_ERROR)
  }

  async getExistingEmailGlobalErrorMessage () {
    return this.getText(GLOBAL_MESSAGE)
  }

  // Login
  async getLoginUsernamePlaceholderValue () {
    return this.getAttributesValue(LOGIN_USER_NAME, 'placeholder')
  }

  async getLoginPasswordPlaceholderValue () {
    return this.getAttributesValue(LOGIN_PASSWORD, 'placeholder')
  }

  async getLoginUsernameFieldErrorMessage () {
    return this.getText(LOGIN_USER_NAME_ERROR)
  }

  async getLoginPasswordFieldErrorMessage () {
    return this.getText(LOGIN_PASSWORD_ERROR)
  }

  async getLoginCommonErrorMessage () {
    await this.waitForDisplayed(LOGIN_COMMON_ERROR)
    return this.getText(LOGIN_COMMON_ERROR)
  }

  async clickSignIn () {
    return this.click(LOGIN_SIGN_IN_BUTTON)
  }

  async isSignInButtonDisabled () {
    await this.waitForDisplayed(LOGIN_SIGN_IN_BUTTON, 10)
    return this.hasAttributePresent(LOGIN_SIGN_IN_BUTTON, 'disabled')
  }

  async sendKeysUsername (keys: string) {
    await this.sendKeys(LOGIN_USER_NAME, keys)
  }

  async sendKeysLoginPassword (keys: string) {
    await this.sendKeys(LOGIN_PASSWORD, keys)
  }

  // Reset Password
  async clickForgotYourPassword () {
    return this.click(LOGIN_FORGOT_YOUR_PASSWORD)
  }

  // Reset Password Popup
  async verifyResetPasswordPopupIsLoaded () {
    await this.waitForDisplayed(RESET_PASSWORD_TITLE)
  }

  async verifyResetPasswordGlobalMessageIsLoaded () {
    await this.waitForDisplayed(RESET_PASSWORD_GLOBAL_MESSAGE)
  }

  async getResetPasswordPopupTitle () {
    return this.getText(RESET_PASSWORD_TITLE)
  }

  async getResetPasswordEmailPlaceholderValue () {
    return this.getAttributesValue(RESET_PASSWORD_EMAIL, 'placeholder')
  }

  async getResetPasswordEmailErrorMessage () {
    return this.getText(RESET_PASSWORD_EMAIL_ERROR)
  }

  async sendKeysResetPasswordEmailAddress (keys: string) {
    await this.sendKeys(RESET_PASSWORD_EMAIL, keys)
  }

  async clickResetPasswordSendEmailButton () {
    await this.click(RESET_SEND_EMAIL_BUTTON)
  }

  async getResetPasswordGlobalMessage () {
    return this.getText(RESET_PASSWORD_GLOBAL_MESSAGE)
  }

  async isUserNotAddedToOkta () {
    let errorStatus = await this.locatorFound(USER_OR_PASSWORD_ERROR)
    let message = await this.getText(USER_OR_PASSWORD_ERROR)
    return !(errorStatus && message === 'Your username or password was incorrect.')
  }

  async verifyWelcomeToMedifastIsLoaded () {
    await this.waitForDisplayed(WELCOME_TO_MEDIFAST_TITLE, 5)
  }

  async answerPasswordQuestions (keys: any) {
    await this.sendKeys(WELCOME_TO_MEDIFAST_ANSWER_TEXT, keys)
  }

  async clickOnCreateAccountButton () {
    await this.click(WELCOME_TO_MEDIFAST_CREATE_ACCOUNT_BUTTON)
  }

  async isWelcomeToMedifastDisplayed () {
    return this.locatorFound(WELCOME_TO_MEDIFAST_TITLE)
  }

  async clickRegisterAcknowledgeCheck () {
    await this.click(CHECK_REGISTER_ACKNOWLEDGE)
  }

  async isGenderFieldDisplayed () {
    return this.isElementVisible(LOGIN_GENDER)
  }

  async isDOBFieldDisplayed () {
    return this.isElementVisible(LOGIN_DOB)
  }

  async isLoginErrorDisplayed () {
    return this.locatorFound(LOGIN_COMMON_ERROR)
  }
}
