// @flow
import BasePage from '@page-objects/BasePage'

const SUGGESTED_DELIVERY_ADDRESSES_POPUP_TTL = '//*[@id="popup_suggested_delivery_addresses"]/div[1]/h4'
const SUGGESTED_DELIVERY_ADDRESSES_SECTION = '//*[@id="popup_suggested_delivery_addresses"]/div[1]/div[1]/ul'
const USE_THIS_ADDRESS_BTN = '//*[@id="popup_suggested_delivery_addresses"]/div[1]/descendant::button[contains(text(),"Use this address")]'
const SUBMIT_AS_IS_BTN = '//*[@id="popup_suggested_delivery_addresses"]/div[2]/descendant::button[contains(text(),"Submit As Is")]'

export default class PopupSuggestedDeliveryAddressesForm extends BasePage {
  async verifyPageIsLoaded () {
    await this.waitForDisplayed(SUGGESTED_DELIVERY_ADDRESSES_POPUP_TTL)
    await this.waitForDisplayed(USE_THIS_ADDRESS_BTN)
  }

  async getSuggestedAddressTitle () {
    return this.getText(SUGGESTED_DELIVERY_ADDRESSES_POPUP_TTL)
  }

  async isPopUpPresent () {
    return this.locatorFound(SUGGESTED_DELIVERY_ADDRESSES_POPUP_TTL)
  }

  async getSuggestedDeliveryAddressSection () {
    return this.getText(SUGGESTED_DELIVERY_ADDRESSES_SECTION)
  }

  async clickUseThisAddress () {
    await this.click(USE_THIS_ADDRESS_BTN)
  }

  async clickSubmitAsIsButton () {
    await this.click(SUBMIT_AS_IS_BTN)
  }

  async isSuggestedDeliveryAddressPopupDisplayed () {
    return this.locatorFound(SUGGESTED_DELIVERY_ADDRESSES_POPUP_TTL, 1)
  }

  async clickOnOptionIfSuggestedPopUpIsDisplayed (option: string) {
    let popUpIsDisplayed = await this.isSuggestedDeliveryAddressPopupDisplayed()
    if (popUpIsDisplayed) {
      await this.browser.pause(3000)
      option === 'submit' ? await this.clickSubmitAsIsButton() : await this.clickUseThisAddress()
    }
  }
}
