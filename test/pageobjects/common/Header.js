// @flow
import BasePage from '../BasePage'

const HEADER_LOGO = '//*[@class="logoLeft"]/descendant::img'
const ABOUT_US_LINK = '//*[@class="quickLinks"]/descendant::a[@title="about us"]'
const GET_IN_TOUCH_LINK = '//*[@class="quickLinks"]/descendant::a[@title="get in touch"]'
const FIND_A_COACH = '//*[@class="quickLinks"]/descendant::a[@title="find a coach"]'
const BECOME_A_COACH = '//*[@class="quickLinks"]/descendant::a[@title="become a coach"]'
const OPTAVIA_CONNECT_LINK = '//*[@class="quickLinks"]/descendant::a[@title="OPTAVIA Connect"]'
const MY_ACCOUNT_LINK = '//span[contains(text(), "My Account")]'
const LOG_IN_LINK = '//span[contains(text(), "Log In")]'
const LOG_OUT_LINK = '//a[contains(text(), "Log Out")]'
const CART_ICON = '//*[@id="mini-cart-component"]/descendant::a[contains(@href,"/cart")]/span[3]'
const MARKET_SELECTOR_LINK = '#market-selectorLink'
const SG_MARKET_FLAG = '//*[@src="/_ui/desktop/theme-optavia/images/singapore-flag.png"]'
const HK_MARKET_FLAG = '//*[@src="/_ui/desktop/theme-optavia/images/HK-Flag.png"]'
const US_MARKET_FLAG = '//*[@src="/_ui/desktop/theme-optavia/images/US-Flag.png"]'
const COACH_INFORMATION = '//*[@id="headerHealthSection"]/a/strong'
const LOG_IN_HEADER = '//a[contains(text(), "Log in")]'
const SHOPPING_CART_ICON = '//*[@id="mini-cart-component"]//a'
const SHOPPING_CART_ICON_VALUE = '//*[@id="mini-cart-component"]//*[contains(@class,"count item_count")]'
const COACH_AVATAR_INFO_TEXT = '//*[@id="headerHealthSection"]/ul/li/div'
const COACH_AVATAR_VIEW_PROFILE_LINK = '//*[@id="headerHealthSection"]/ul/li/div/div/div/a'
const COACH_INFO_TOP_RIGHT_ASM = '//*[@id="header"]/div[2]/div/strong'
const COACH_AVATAR_IMAGE = '//img[@class="avatar avatar-sm img-responsive"]'
const COACH_AVATAR_NAME_AND_ID_TEXT = '//li[@id="headerHealthSection"]/a/strong'
const CHOOSE_YOUR_LOCATION_POPUP_TITLE = '//div[@id="cboxLoadedContent"]//h6[contains(text(), "CHOOSE YOUR LOCATION")]'
const COUNTRY_NAME_US = '//div[@id="cboxLoadedContent"]//span[contains(text(), "United States")]'
const COUNTRY_NAME_SG = '//div[@id="cboxLoadedContent"]//span[contains(text(), "Singapore")]'
const COUNTRY_NAME_HK = '//div[@id="cboxLoadedContent"]//span[contains(text(), "Hong Kong")]'
const ACCESSIBILITY_ICON = '//div[@class="accessibility-icon"]/a'
const ACCESSIBILITY_ICON_TOOT_TIP = '//div[@class="accessibility-icon"]'
const CREATE_ACCOUNT_LINK = '//*[@class="label"][text()="Create Account"]'

export default class Header extends BasePage {
  async verifyPageIsLoaded () {
    await this.waitForDisplayed(HEADER_LOGO, 10)
  }

  async verifyMyAccountLinkIsLoaded () {
    await this.waitForDisplayed(MY_ACCOUNT_LINK)
  }

  async verifyOptaviaConnectLinkIsLoaded () {
    await this.waitForDisplayed(OPTAVIA_CONNECT_LINK)
  }

  async verifyFlagIsLoaded () {
    await this.waitForDisplayed(MARKET_SELECTOR_LINK)
  }

  async verifyUSFlagIsLoaded () {
    await this.waitForDisplayed(US_MARKET_FLAG)
  }

  async verifySGFlagIsLoaded () {
    await this.waitForDisplayed(SG_MARKET_FLAG)
  }

  async verifyHKFlagIsLoaded () {
    await this.waitForDisplayed(HK_MARKET_FLAG)
  }

  async verifyAboutUsIsDisplayed () {
    await this.waitForDisplayed(ABOUT_US_LINK)
  }

  async clickAboutUs () {
    await this.click(ABOUT_US_LINK)
  }

  async verifyGetInTouchIsDisplayed () {
    await this.waitForDisplayed(GET_IN_TOUCH_LINK)
  }

  async clickGetInTouch () {
    await this.click(GET_IN_TOUCH_LINK)
  }

  async verifyFindACoachIsDisplayed () {
    await this.waitForDisplayed(FIND_A_COACH)
  }

  async clickFindACoach () {
    await this.click(FIND_A_COACH)
  }

  async clickBecomeACoach () {
    await this.click(BECOME_A_COACH)
  }

  async clickOptaviaConnect () {
    await this.click(OPTAVIA_CONNECT_LINK)
  }

  async isOptaviaConnectLinkFound () {
    await this.waitForDisplayed(OPTAVIA_CONNECT_LINK, 5)
    return this.locatorFound(OPTAVIA_CONNECT_LINK)
  }

  async getCoachInformation () {
    return this.getText(COACH_INFORMATION)
  }

  async isCoachInformationDisplayed () {
    return this.locatorFound(COACH_INFORMATION)
  }

  async clickMyAccount () {
    await this.waitForDisplayed((MY_ACCOUNT_LINK))
    await this.click(MY_ACCOUNT_LINK)
  }

  async verifyLogInIsLoaded () {
    await this.waitForDisplayed(LOG_IN_LINK)
  }

  async clickLogIn () {
    await this.click(LOG_IN_LINK)
  }

  async verifyLogOutIsLoaded () {
    await this.waitForDisplayed(LOG_OUT_LINK)
  }

  async clickLogOut () {
    await this.waitForDisplayed(LOG_OUT_LINK, 5)
    await this.click(LOG_OUT_LINK)
  }

  async verifyCartIconIsDisplayed () {
    await this.waitForDisplayed(CART_ICON)
  }

  async clickCartIcon () {
    await this.click(CART_ICON)
  }

  async clickHeaderLogo () {
    await this.click(HEADER_LOGO)
  }

  async clickHeaderLogIn () {
    await this.click(LOG_IN_HEADER)
  }

  async clickHeaderShoppingCartIcon () {
    await this.click(SHOPPING_CART_ICON)
  }

  async verifyLoginLinkIsLoaded () {
    await this.waitForDisplayed(LOG_IN_HEADER)
  }

  async getShoppingCartCount () {
    return this.getText(SHOPPING_CART_ICON_VALUE)
  }

  async verifyBecomeACoachIsLoaded () {
    await this.waitForDisplayed(BECOME_A_COACH)
  }

  async hoverOverCoach () {
    await this.moveToLocatorPosition(COACH_INFORMATION)
  }

  async getCoachInfoFromAvatar () {
    await this.hoverOverCoach()
    return this.getText(COACH_AVATAR_INFO_TEXT)
  }

  async getCoachImageSrc () {
    return this.getAttributesValue(COACH_AVATAR_IMAGE, 'src')
  }

  async getCoachNameAndId () {
    return this.getText(COACH_AVATAR_NAME_AND_ID_TEXT)
  }

  async clickOnViewProfileLink () {
    await this.hoverOverCoach()
    await this.click(COACH_AVATAR_VIEW_PROFILE_LINK)
  }

  async clickMarketSelector () {
    await this.click(MARKET_SELECTOR_LINK)
  }

  async clickHKMarket () {
    await this.clickByIndex(HK_MARKET_FLAG, 1)
  }

  async clickSGMarket () {
    await this.clickByIndex(SG_MARKET_FLAG, 1)
  }

  async clickUSMarket () {
    await this.clickByIndex(US_MARKET_FLAG, 1)
  }

  async isHeaderLogoDisplayed () {
    return this.locatorFound(HEADER_LOGO)
  }

  async isBecomeACoachDisplayed () {
    return this.locatorFound(BECOME_A_COACH)
  }

  async getCoachInformationASM () {
    return this.getText(COACH_INFO_TOP_RIGHT_ASM)
  }

  async isMarketSwitchPopupIsLoaded () {
    return this.locatorFound(CHOOSE_YOUR_LOCATION_POPUP_TITLE)
  }

  async clickOnCountryName (market: number) {
    switch (market) {
      case 1: await this.click(COUNTRY_NAME_US)
        break
      case 2: await this.click(COUNTRY_NAME_SG)
        break
      case 3: await this.click(COUNTRY_NAME_HK)
        break
    }
  }

  async isLogInIsLoaded () {
    return this.locatorFound(LOG_IN_LINK)
  }

  async hoverOverAccessibilityIcon () {
    await this.moveToLocatorPosition(ACCESSIBILITY_ICON)
  }

  async getAccIconInfo () {
    await this.hoverOverAccessibilityIcon()
    return this.getAttributesValue(ACCESSIBILITY_ICON_TOOT_TIP, 'data-tooltip')
  }

  async isAccessibilityIconDisplayed () {
    return this.locatorFound(ACCESSIBILITY_ICON)
  }

  async clickAccessibilityIcon () {
    await this.click(ACCESSIBILITY_ICON)
  }

  async clickOnCreateAccountLink () {
    await this.waitForDisplayed(CREATE_ACCOUNT_LINK, 3)
    await this.click(CREATE_ACCOUNT_LINK)
  }
}
