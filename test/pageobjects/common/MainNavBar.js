// @flow
import BasePage from '../BasePage'
import EnvConfig from 'src/testdata/inputdata/EnvConfig.js'
import MiscTestData, {page} from 'src/testdata/inputdata/various/MiscTestData.js'

const WE_ARE_OPTAVIA_LINK = '//a[@title="WE ARE OPTAVIA"]'
const OPTAVIA_COMMUNITY_LINK = '//a[@title="OPTAVIA COMMUNITY"]'
const PRODUCTS_AND_PROGRAMS_LINK = 'a[title*="PRODUCTS"]'
const SHOP_LINK = '//a[@title="SHOP"]'
const JOIN_US_LINK = '//a[@title="JOIN US"]'
const SEARCH_ICON = '//*[@class="search-box"]/a'
const SEARCH_TEXT_BOX = '#input_SearchBox'
const SEARCH_SUBMIT_BUTTON = '//*[@class="search-box"]/button'
const PRODUCTS_AND_PROGRAMS_SUBCATEGORIES_LIST = '[id="navigation-wrap"] ul ul li a[href*="weight-loss-products-programs"]'
const COACH_AND_COMMUNITY_LINK = '//a[@title="COACH & COMMUNITY"]'

// WE_ARE_OPTAVIA sub menu
const IN_THE_NEWS = '//*[@id="navigation-wrap"]/descendant::strong[contains(text(),"In the news")]'

// SHOP sub menu
const SHOP_HEALTH_COACH_STORE = '//a[@title="SHOP"]/parent::li/descendant::strong[contains(text(),"Coach Store")]'
const SHOP_OPTIMAL_WEIGHT = '//a[@title="SHOP"]/parent::li/descendant::strong[contains(text(),"Optimal Weight")]'
// const SHOP_OPTIMAL_HEALTH = '//a[@title="SHOP"]/parent::li/descendant::strong[contains(text(),"Optimal Health")]')
const SHOP_SNACKS = '//a[@title="SHOP"]/parent::li/descendant::strong[contains(text(),"Snacks")]'
const SHOP_PURPOSEFUL_HYDRATION = '//a[@title="SHOP"]/parent::li/descendant::strong[contains(text(),"Purposeful")]/parent::p'
const SHOP_CHOOSE_YOUR_OWN_FUELINGS = '//a[@title="SHOP"]/parent::li/descendant::strong[contains(text(),"Choose Your Own")]'
const SHOP_ALL = '//*[@title="Shop All"]'
const SHOP_COACH_STORE = '//a[@title="SHOP"]/parent::li/descendant::strong[contains(text(),"Coach Store")]'

export default class MainNavBar extends BasePage {
  async verifyPageIsLoaded () {
    await this.waitForDisplayed(SHOP_LINK, 10)
  }

  async verifyWeAreOptaviaIsDisplayed () {
    await this.waitForDisplayed(WE_ARE_OPTAVIA_LINK)
  }

  async clickWeAreOptavia () {
    await this.click(WE_ARE_OPTAVIA_LINK)
  }

  async clickInTheNews () {
    await this.moveToLocatorPosition(WE_ARE_OPTAVIA_LINK)
    await this.moveToLocatorPositionAndClick(IN_THE_NEWS)
  }

  async verifyOptaviaCommunityIsDisplayed () {
    await this.waitForDisplayed(OPTAVIA_COMMUNITY_LINK)
  }

  async clickOptaviaCommunity () {
    await this.click(OPTAVIA_COMMUNITY_LINK)
  }

  async verifyProductsAndProgramsIsDisplayed () {
    await this.waitForDisplayed(PRODUCTS_AND_PROGRAMS_LINK)
  }

  async clickProductsAndPrograms () {
    await this.click(PRODUCTS_AND_PROGRAMS_LINK)
  }

  async verifyShopLinkIsDisplayed () {
    await this.waitForDisplayed(SHOP_LINK)
  }

  async clickShop () {
    await this.click(SHOP_LINK)
  }

  async verifyJoinUsIsDisplayed () {
    await this.waitForDisplayed(JOIN_US_LINK)
  }

  async clickJoinUs () {
    await this.click(JOIN_US_LINK)
  }

  async hoverOverShop () {
    await this.moveToLocatorPosition(SHOP_LINK)
  }

  async hoverOverProductsAndPrograms () {
    await this.moveToLocatorPosition(PRODUCTS_AND_PROGRAMS_LINK)
  }

  async clickChooseYourOwnFuelings () {
    await this.moveToLocatorPosition(SHOP_LINK, 5)
    await this.moveToLocatorPositionAndClick(SHOP_CHOOSE_YOUR_OWN_FUELINGS)
  }

  async clickSnacks () {
    await this.moveToLocatorPositionAndClick(SHOP_SNACKS)
  }

  async clickShopAll () {
    await this.moveToLocatorPosition(SHOP_LINK)
    await this.moveToLocatorPositionAndClick(SHOP_ALL)
  }

  async clickHealthCoachStore () {
    await this.moveToLocatorPosition(SHOP_LINK)
    await this.moveToLocatorPositionAndClick(SHOP_HEALTH_COACH_STORE)
  }

  async verifySearchIconIsDisplayed () {
    await this.waitForDisplayed(SEARCH_ICON)
  }

  async clickSearchIcon () {
    await this.click(SEARCH_ICON)
  }

  async clickOptimalWeight () {
    await this.click(SHOP_OPTIMAL_WEIGHT)
  }

  async getSearchIconAttributeValue () {
    return this.getAttributesValue(SEARCH_ICON, 'class')
  }

  async sendKeysSearchTextBox (key: string) {
    await this.sendKeys(SEARCH_TEXT_BOX, key)
  }

  async clickSearchSubmitButton () {
    await this.click(SEARCH_SUBMIT_BUTTON)
  }

  async searchProduct (keys: string) {
    await this.moveToLocatorPosition(SEARCH_ICON)
    await this.click(SEARCH_ICON)
    await this.sendKeys(SEARCH_TEXT_BOX, keys)
    await this.click(SEARCH_ICON)
  }

  async clickPurposefulHydration () {
    await this.moveToLocatorPosition(SHOP_LINK)
    await this.moveToLocatorPositionAndClick(SHOP_PURPOSEFUL_HYDRATION)
  }

  async clickOnBars () {
    await this.moveToLocatorPositionAndClick(SHOP_SNACKS)
  }

  async getProductsAndProgramsItemTextList () {
    await this.moveToLocatorPosition(PRODUCTS_AND_PROGRAMS_LINK)
    let rawElementTextList = await this.getElementsTextArrayList(PRODUCTS_AND_PROGRAMS_SUBCATEGORIES_LIST)
    let elementTextList = rawElementTextList.toString().replace(/\n+/g, ' ')
    return elementTextList.split(',')
  }

  async clickOnProductsAndProgramsSubcategoriesByIndex (index: any) {
    await this.moveToLocatorPosition(PRODUCTS_AND_PROGRAMS_LINK)
    await this.clickByIndex(PRODUCTS_AND_PROGRAMS_SUBCATEGORIES_LIST, index)
  }

  async verifyCoachStoreSubItemIsDisplayed () {
    return this.locatorFound(SHOP_COACH_STORE)
  }

  async verifyCoachAndCommunityIsDisplayed () {
    await this.waitForDisplayed(COACH_AND_COMMUNITY_LINK, 5)
  }

  async isPurposefulHydrationDisplayed () {
    return this.locatorFound(SHOP_PURPOSEFUL_HYDRATION)
  }

  async clickCoachAndCommunityLink () {
    await this.click(COACH_AND_COMMUNITY_LINK)
  }

  async navigateToMAShop () {
    await this.navigateToURL(`${await EnvConfig.BASE_URL()}${await MiscTestData.PAGE_URL(page.NavBar.MOMENTUM_ACTIVE)}`)
  }

  async navigateToMyProfile () {
    await this.navigateToURL(`${await EnvConfig.BASE_URL()}${await MiscTestData.PAGE_URL(page.MyAccount.YOUR_PROFILE)}`)
  }
}
