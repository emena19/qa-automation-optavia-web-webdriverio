// @flow
import BasePage from '../BasePage'

const ASM_HEADER_LOGO = '//*[@class="ASM_logo h4"]'
const ASM_HEADER_USERNAME = '//input[@name="username"]'
const ASM_HEADER_PASSWORD = '//input[@name="password"]'
const ASM_HEADER_SIGNIN_BTN = '//button[contains(text(),"Sign In")]'
const ASM_HEADER_TIMER = '//*[@id="sessionTimer"]'
const ASM_HEADER_SIGNED_IN_AS = '//*[@class="ASM_loggedin_text"]/span[2]'
const ASM_HEADER_SIGNOUT_BTN = '//button[contains(text(),"Sign Out")]'
const LOG_IN_LINK = '//span[contains(text(), "Log In")]'
const RESET_LINK = '//button[@id="resetButton"]'
const ASM_HEADER_CUSTOMER_NAME_EMAIL = '//input[@name="customerName"]'
const ASM_HEADER_CUSTOMER_NAME_EMAIL_AUTO_COMPLETE_LIST_FIRST = '//*[@id="asmAutoComplete"]/ul/li[1]'
const ASM_HEADER_CART_ORDER_ID = '//input[@name="cartId"]'
const ASM_HEADER_START_SESSION_BTN = '//button[contains(text(),"Start")]'
const ASM_HEADER_END_SESSION_BTN = '//button[contains(text(),"End")]'
const ASM_MEDIFAST_HEADER = '//*[@id="_asm"]/div/div[3]/div/div[1]'
const MARKET_SWITCH_MESSAGE = '//div[@class="col-lg-3 channelAsmMsg"]/p'
const MARKET_SWITCH_CHANNEL_BUTTON_YES = '//div[@class="col-lg-3 channelAsmMsg"][1]/a'
const MARKET_SWITCH_CHANNEL_BUTTON_NO = '//div[@class="col-lg-3 channelAsmMsg"][2]/a'
const ERROR_MESSAGE = '//*[@class="note-msg  ASM_alert_cart"]/li'
const CUSTOMER_ID_ERROR_MESSAGE_TEXT = '//li[@class="ui-menu-item"]/a/span'
const TIMER_COUNT = '#timerCount'
const ASM_SPINNER = '//*[@id="add-to-cart-spinner"]'
export default class ASMHeader extends BasePage {
  async verifyPageIsLoaded () {
    await this.waitForDisplayed(ASM_HEADER_LOGO, 10)
  }

  async getPageTitle () {
    return this.getCurrentPageTitle()
  }

  async getPageUrl () {
    return this.getCurrentPageUrl()
  }

  async loginASMAsCSR (username: string, password: string) {
    await this.sendKeys(ASM_HEADER_USERNAME, username)
    await this.sendKeys(ASM_HEADER_PASSWORD, password)
    await this.click(ASM_HEADER_SIGNIN_BTN)
  }

  async verifyASMLoginIsSuccessful () {
    await this.waitForDisplayed(ASM_HEADER_SIGNOUT_BTN)
    await this.waitForDisplayed(RESET_LINK)
  }

  async verifyLoginLinkIsDisplayed () {
    await this.waitForDisplayed(LOG_IN_LINK)
  }

  async clickLogin () {
    await this.click(LOG_IN_LINK)
  }

  async getASMTimerText () {
    await this.getTextByIndex(ASM_HEADER_TIMER, 1)
  }

  async getASMSignedInAsText () {
    await this.getTextByIndex(ASM_HEADER_SIGNED_IN_AS, 1)
  }

  async clickASMSignOut () {
    await this.waitForDisplayed(ASM_HEADER_SIGNOUT_BTN, 3)
    await this.click(ASM_HEADER_SIGNOUT_BTN)
  }

  async startASMSessionForCustomer (customerName: string) {
    await this.waitForDisplayed(ASM_HEADER_CUSTOMER_NAME_EMAIL, 3)
    await this.sendKeys(ASM_HEADER_CUSTOMER_NAME_EMAIL, customerName)
    await this.waitForSpinnerToDisappear(ASM_SPINNER)
    await this.waitForDisplayed(ASM_HEADER_CUSTOMER_NAME_EMAIL_AUTO_COMPLETE_LIST_FIRST, 5)
    await this.click(ASM_HEADER_CUSTOMER_NAME_EMAIL_AUTO_COMPLETE_LIST_FIRST)
    await this.waitForDisplayed(ASM_HEADER_START_SESSION_BTN, 20)
    await this.click(ASM_HEADER_START_SESSION_BTN)
  }

  async startASMSessionForCartOrderID (cartOrderID: string) {
    await this.waitForDisplayed(ASM_HEADER_CART_ORDER_ID, 3)
    await this.sendKeys(ASM_HEADER_CART_ORDER_ID, cartOrderID)
    await this.waitForSpinnerToDisappear(ASM_SPINNER)
    await this.waitForDisplayed(ASM_HEADER_START_SESSION_BTN, 20)
    await this.click(ASM_HEADER_START_SESSION_BTN)
  }

  async clickASMEndSession () {
    await this.click(ASM_HEADER_END_SESSION_BTN)
  }

  async verifyMedifastASMIsloaded () {
    await this.waitForDisplayed(ASM_MEDIFAST_HEADER)
  }

  async isMarketSwitchMessageDisplayed () {
    return this.locatorFound(MARKET_SWITCH_MESSAGE)
  }

  async isMarketSwitchYesButtonDisplayed () {
    return this.locatorFound(MARKET_SWITCH_CHANNEL_BUTTON_YES)
  }

  async isMarketSwitchNoButtonDisplayed () {
    return this.locatorFound(MARKET_SWITCH_CHANNEL_BUTTON_NO)
  }

  async clickYesToSwitchMarket () {
    await this.click(MARKET_SWITCH_CHANNEL_BUTTON_YES)
  }

  async clickNoToSwitchMarket () {
    await this.click(MARKET_SWITCH_CHANNEL_BUTTON_NO)
  }

  async isErrorMessageDisplayed () {
    return this.locatorFound(ERROR_MESSAGE)
  }

  async getErrorMessageText () {
    return this.getText(ERROR_MESSAGE)
  }

  async getCustomerErrorMessageText () {
    return this.getText(CUSTOMER_ID_ERROR_MESSAGE_TEXT)
  }

  async startASMSessionForInvalidEmail (customerName: string) {
    await this.waitForDisplayed(ASM_HEADER_CUSTOMER_NAME_EMAIL, 3)
    await this.sendKeys(ASM_HEADER_CUSTOMER_NAME_EMAIL, customerName)
    await this.waitForSpinnerToDisappear(ASM_SPINNER)
    await this.waitForDisplayed(ASM_HEADER_CUSTOMER_NAME_EMAIL_AUTO_COMPLETE_LIST_FIRST, 5)
    await this.waitForDisplayed(ASM_HEADER_START_SESSION_BTN, 20)
  }

  async isResetLinkDisplayed () {
    await this.waitForDisplayed(RESET_LINK, 3)
    await this.locatorFound(RESET_LINK)
  }

  async clickOnResetLink () {
    await this.waitForDisplayed(RESET_LINK, 3)
    await this.click(RESET_LINK)
  }

  async getTimerCountText () {
    await this.waitForDisplayed(TIMER_COUNT, 3)
    return this.getText(TIMER_COUNT)
  }
}
