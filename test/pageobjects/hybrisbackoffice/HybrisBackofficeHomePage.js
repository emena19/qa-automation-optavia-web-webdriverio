// @flow
import BasePage from '../BasePage'
const SIGN_OUT_BTN = '//a[@title="Logout"]/span/img'
const CUSTOMER_SUPPORT_BTN = '//li[@title="Customer Support"]/a/img'
const HOME_MENU = 'classname = y-tree-icon-backoffice_dashboard z-treecell'
const ASM_DROP_DOWN_LNK = '//span[contains(text(),\'Launch ASM\')]/parent::div/descendant::a[@class=\'z-combobox-button\']/i'
const ASM_DROP_DOWN_LIST = '//ul[@class=\'z-combobox-content\']/li'
const ASM_LAUNCH_BTN = '//button[contains(@class,\'asm-icon\')]'
const SESSION_CONTEXT_COLLAPSE = '(//*[@class="z-south-splitter-button"]/i[2])[3]'
const CUSTOMER_SUPPORT_ORDER_LINK = '//*[@class="yw-treeCellInner z-div"][.="Orders"]'
const CUSTOMER_SUPPORT_USER_GRP_LINK = '//*[@class="yw-treeCellInner z-div"][.="User Groups"]'
const CUSTOMER_SUPPORT_EMPLOYEE_LINK = '//*[@class="yw-treeCellInner z-div"][.="Employees"]'
const CUSTOMER_SUPPORT_ALL_ORDERS_LINK = `//*[@class='yw-treeCellInner z-div'][.="All Orders"]`
const ASM_SUPPORT_ORDER_LINK = '//tr[@title="Order"]'
const ASM_USER_LINK = '//tr[@title="User"]'
const COLLAPSIBLE_LEFT_MENU_ITEMS = 'div[class*="noBG yw-layoutregion z-west z-west-noborder"] tr[class*="yw-navigationNode-level1"]'
const USER_CUSTOMER_LEFT_MENU = '//*[contains(@class,"yw-navigationNode-level2")]//*[contains(text(),"Customers")]'
const HOME_TOP_BAR = 'div[class*="yw-systemBarContainer"]'
const EDIT_LINK = '(//*[@class="yw-dashboard-lock z-button"][text()="Edit"])[2]'
const NO_TYPE_SELECTED_TEXT = '//span[text()="No type selected"]'
const ROLE_TYPE_TEXT = '(//*[@class="z-caption"])[1]'
const ROLE_TYPE_CUST_SUPPORT = '//*[@class="z-treecell-text"][contains(text(),"Customer Support")]'
const SEARCH_FILTER_TREE_INPUT = '(//*[@placeholder="Filter Tree entries"])[last()]'
const TYPE_LEFT_MENU_LINK = '//*[@class="z-label"][text()="Types"]'
const CUSTOMER_CSR_LINK = '//*[contains(@class,"z-label")][contains(text(),"Customers")]/..'
const CRONJOBS_LEFT_MENU_LINK = '//*[@class="z-label"][text()="CronJobs"]'
const RETURNS_CSR_LINK = '//*[contains(@class,"z-label")][contains(text(),"Returns")]/..'
const HYBRIS_LAUNCH_ASM_SELECT = '//*[@class="z-combobox-input"]'
const HYBRIS_LAUNCH_ASM_SELECT_TEXT = '//*[@class="z-comboitem-text"]'
const HYBRIS_LAUNCH_ASM_BUTTON = '//*[@class="z-button-image"]'
const HYBRIS_END_SESSION_BUTTON = '//*[@class="z-button"][text()="End Session"]'

export default class HybrisBackofficeHomePage extends BasePage {
  async verifyPageIsLoaded () {
    await this.waitForDisplayed(HOME_TOP_BAR, 10)
    let role = await this.getRoleTypeText()
    let isAdminRole = role.includes('Administration Cockpit')
    if (isAdminRole === false) {
      await this.verifyCSIsLoaded()
    } else {
      await this.waitForDisplayed(EDIT_LINK, 10)
    }
  }

  async verifyCSIsLoaded () {
    await this.waitForDisplayed(NO_TYPE_SELECTED_TEXT, 10)
  }

  async getPageTitle () {
    return this.getCurrentPageTitle()
  }

  async getPageUrl () {
    return this.getCurrentPageUrl()
  }

  async clickASMCustomerSupport () {
    await this.click(CUSTOMER_SUPPORT_BTN)
    await this.waitForDisplayed(ASM_DROP_DOWN_LNK)
  }

  async launchASMOptavia () {
    await this.click(ASM_DROP_DOWN_LNK)
    await this.clickByIndex(ASM_DROP_DOWN_LIST, 1)
    await this.click(ASM_LAUNCH_BTN)
  }

  async clickSignOut () {
    await this.waitForDisplayed(SIGN_OUT_BTN)
    await this.click(SIGN_OUT_BTN)
  }

  async clickOrder () {
    await this.waitForDisplayed(CUSTOMER_SUPPORT_ORDER_LINK)
    await this.click(CUSTOMER_SUPPORT_ORDER_LINK)
  }

  async clickOrderAll () {
    await this.click(CUSTOMER_SUPPORT_ALL_ORDERS_LINK)
  }

  async clickSessionContextCollapse () {
    await this.click(SESSION_CONTEXT_COLLAPSE)
  }

  async clickASMOrder () {
    await this.click(ASM_SUPPORT_ORDER_LINK)
  }

  async clickASMUser () {
    await this.click(ASM_USER_LINK)
  }
  /*
Left Menu Index

0 - Home
1 - Inbox
2 - Personal Data Reports
3 - System
4 - Catalog
5 - Multimedia
6 - User
7 - Order
8 - Price Settings
9 - Internationalization
10 - Marketing
11 - Cockpit
12 - Rule engine
14 - Ticket System
15 - Base Commerce
16 - Deeplink Urls
 */

  async expandLeftMenuItem (item) {
    await this.clickByIndex(COLLAPSIBLE_LEFT_MENU_ITEMS, item)
  }

  async clickOnCustomerSubItem () {
    await this.waitForDisplayed(USER_CUSTOMER_LEFT_MENU, 5)
    await this.click(USER_CUSTOMER_LEFT_MENU)
  }

  async clickHome () {
    await this.click(HOME_MENU)
  }

  async clickOnUserItem () {
    await this.clickByIndex(COLLAPSIBLE_LEFT_MENU_ITEMS, 6)
  }

  async getRoleTypeText () {
    await this.waitForDisplayed(ROLE_TYPE_TEXT, 10)
    return this.getAttributesValue(ROLE_TYPE_TEXT, 'title')
  }

  async changeRoleFromAdminToCS () {
    await this.click(ROLE_TYPE_TEXT)
    await this.waitForDisplayed(ROLE_TYPE_CUST_SUPPORT, 10)
    await this.click(ROLE_TYPE_CUST_SUPPORT)
  }

  async searchOnTreeFilter (item: string) {
    await this.waitForDisplayed(SEARCH_FILTER_TREE_INPUT, 7)
    await this.sendKeys(SEARCH_FILTER_TREE_INPUT, item)
    await this.browser.pause(3000)
  }

  async clickOnTypeFromLeftMenu () {
    await this.waitForDisplayed(TYPE_LEFT_MENU_LINK, 10)
    await this.click(TYPE_LEFT_MENU_LINK)
  }

  async clickOnCSRCustomerLink () {
    await this.waitForDisplayed(CUSTOMER_CSR_LINK, 10)
    await this.moveToLocatorPosition(CUSTOMER_CSR_LINK)
    await this.click(CUSTOMER_CSR_LINK)
  }

  async clickOnCronJobsFromLeftMenu () {
    await this.waitForDisplayed(CRONJOBS_LEFT_MENU_LINK, 10)
    await this.click(CRONJOBS_LEFT_MENU_LINK)
  }

  async isCSRReturnLinkDisplayed () {
    return this.locatorFound(RETURNS_CSR_LINK)
  }

  async clickLaunchASMSelect () {
    await this.waitForDisplayed(HYBRIS_LAUNCH_ASM_SELECT, 10)
    await this.click(HYBRIS_LAUNCH_ASM_SELECT)
  }

  async getLaunchASMOptionsText () {
    return this.getElementsTextArrayList(HYBRIS_LAUNCH_ASM_SELECT_TEXT)
  }

  async clickOnLaunchASMOptionByIndex (option: string) {
    await this.clickByIndex(HYBRIS_LAUNCH_ASM_SELECT_TEXT, option)
    await this.clickOnLaunchASMButton()
  }

  async clickOnLaunchASMButton () {
    await this.click(HYBRIS_LAUNCH_ASM_BUTTON)
  }

  async clickUserGrp () {
    await this.waitForDisplayed(CUSTOMER_SUPPORT_USER_GRP_LINK)
    await this.click(CUSTOMER_SUPPORT_USER_GRP_LINK)
  }

  async clickEmployee () {
    await this.waitForDisplayed(CUSTOMER_SUPPORT_EMPLOYEE_LINK)
    await this.click(CUSTOMER_SUPPORT_EMPLOYEE_LINK)
  }

  async clickOnEndSessionButton () {
    await this.waitForDisplayed(HYBRIS_END_SESSION_BUTTON, 10)
    await this.click(HYBRIS_END_SESSION_BUTTON)
  }
}
