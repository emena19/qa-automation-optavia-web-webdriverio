// @flow
import BasePage from '../BasePage'
import CommonUtils from '@core-libs/CommonUtils'

const FRAUD_REPORT_TAB = '//span[contains(text(),\'Fraud Report\')][@class=\'yw-editorarea-tablabel z-label\']'
const ADMIN_REPORT_TAB = '//span[text()="Administration"]'
const FRAUD_ACTIONS_APPROVED_BUTTON = '//*[@class=\'fraudbutton acceptfrauditem z-button\']'
const REFRESH_BUTTON = `(//button[contains(@class,'z-button')][contains(text(), "Refresh")])[last()]`
const ADMIN_SAVE = `//div[@class="y-toolbar-rightslot z-div"]//button[contains(text(), "Save")]`
const ORDER_COMMISSIONABLE_VOLUME = '//span[@title="commissionableVolume"]//..//.././div/input'
const ORDER_QUALIFYING_VOLUME = '//span[@title="qualifyingVolume"]//..//.././div/input'
const COUNT_INVALID_AUTHORIZATION = '//span[text()="Count of Invalid Authorizations"]//..//.././div/input'
const LAST_INVALID_AUTHR_TIME = '//span[text()="Last Invalid Auth Time"]//..//.././div/input'
const ORDERS_REPORT_TAB = '//span[text()="Orders"]'
const DELIVERY_MODE_CSM_TEXT = '//*[@class="z-listcell-content"]//*[contains(text(),"Rush[rush]")]'
const SAVED_ITEMS_MESSAGE = '//span[contains(text(),"Saved items:")]'
const ORDER_SHIPPING_TAX = '//span[text()="Shipping Tax"]//..//.././div/input'
const ADMIN_TAB_FIRST_CHANGES_DEFAULT_PAYMENT = '(//*[@class="ye-default-reference-editor-selected-item-container z-div"]//*[contains(text(),"defaultPaymentInfo")])[1]'
const PAYMENT_REPORT_TAB = '//span[text()="Payments"]'
const PAYMENT_METHODS_LAST_TEXT = '(//*[@class="ye-default-reference-editor-selected-item-label z-label"])[last()]'
const PAYMENT_METHOD_LAST_ITEM = '(//*[@class="ye-default-reference-editor-selected-item-container ye-remove-enabled z-div"])[last()]'
const ADMINISTRATION_POPUP_CHANGED_ATTRIBUTE = '(//span[text()="Changed attributes"]//parent::div)//following-sibling::div/div/div/div'
const CUSTOMER_REWARDS_TEXT_BOX = '//*[@class="ye-default-reference-editor-selected-item-label z-label"][contains(text(),"CustomerRewards")]'
const ADV_SEARCH_DELETE_FILTER_ICON = '//*[@class="yw-advancedsearch-delete-btn ye-delete-btn z-button"]'
const ORDER_ADV_SEARCH_CATEGORIES_DROP_DOWN = '//*[@class="yw-additional-attributes-selector z-combobox z-combobox-readonly"]//*[@class="z-combobox-input"]'
const HYBRIS_PROCESSING_SPINNER = '//*[@class="z-loading-indicator"]'
const ORDER_ADVANCED_SEARCH_BUTTON = '//*[@class="yw-toggle-advanced-search z-button"]'
const ORDER_SEARCH_TOP_BUTTON = '//button[contains(@class,"yw-textsearch-searchbutton y-btn-primary z-button")]'
const ORDER_ADV_SEARCH_CATEGORIES_SEARCH_DROPDOWN = '//*[@class="yw-advancedsearch-editor z-div"]/span/input[@class="z-combobox-input"]'
const FIRST_ORDER_TEXT_BOX = '(//span[@class="yw-listview-cell-label z-label"])[1]'
const ORDER_ADV_SEARCH_ADD_BUTTON = '//*[@class="z-row-content"]//button[contains(@class,"z-button")]'
const ADV_SEARCH_DESC_ORDER_ARROW = '//*[@class="yw-advancedsearch-desc z-radio z-radio-default"]'
const ORDER_STATUS_GRID_TEXT = '(//*[@class="yw-listview-cell-label z-label"])[5]'
const ORDER_SEARCH_TOP_TEXTBOX = '//*[@class="yw-search-mode-container z-div"]//input'
const ORDER_DATE_COLUMN_NAME = '//*[@class="z-listheader-content"][text()="Order Date"]'
const LINE_DETAILS_PRODUCT_CODE_FIRST_ITEM = '(//span[text()="Entries"]//parent::div)//following-sibling::table/tbody[1]/tr[1]/td[2]'
const LINE_DETAILS_PRODUCT_NAME_FIRST_ITEM = '(//span[text()="Entries"]//parent::div)//following-sibling::table/tbody[1]/tr[1]/td[3]'
const ORDER_STATUS_DROPDOWN_BUTTON = '(//span[text()="Order Status"]//parent::div)//following-sibling::div/span/a/i'
const ORDER_STATUS_DROPDOWN_TEXT = '.z-comboitem-text'
const ORDER_STATUS_TEXT = '(//span[text()="Order Status"]//parent::div)//following-sibling::div//input'
const ADMIN_PROFILE_TAB = '//span[text()="Profile"]'
const PROFILE_TAB_GROUPS_TEXT = '//*[@class="yw-editorarea-tabbox-tabpanels-tabpanel-groupbox-attrcell-label z-label"][contains(text(),"Groups")]'
const PROFILE_TAB_ADD_GROUP = '(//*[@class="ye-default-reference-editor-bandbox ye-default-reference-editor-bandbox-list-collapsed z-bandbox"]/input)[last()]'
const AUDIT_TRAIL_LAST_CHANGE = '(//*[@class="ye-default-reference-editor-selected-item-container z-div"])[last()]'
const ORDER_ENTRY_TEXT = '(//span[text()="Entries"]/parent::div//following-sibling::div//tr//div[@class="ye-default-reference-editor-selected-item-container ye-remove-enabled z-div"])[1]'
const POSITION_PRICE_REPORT_TAB = '//span[text()="Positions and Prices"]'
const CONSIGNMENTS_ACTION_DOT = '.ye-actiondots z-listcell'
const EDIT_DETAILS_POP_UP = '//span[text()="Edit Details"]'
const DISCOUNT_INCLUDED_TEXT = '((//span[text()="Discounts Included"]//parent::div)//following-sibling::div//td/div)[1]'
const ORDER_ENTRY_GROUPS = '(//span[text()="Entry groups"]/parent::div//following-sibling::div//tr//div[@class="z-listcell-content"])'
export default class HybrisBackofficeOrderDetailsPage extends BasePage {
  async verifyPageIsLoaded (order: any) {
    const ORDER_DETAILS_BODY_TITLE = `//*[@class="z-label"][contains(text(),"${order}")]`
    await this.waitForDisplayed(ORDER_DETAILS_BODY_TITLE, 40)
  }

  async verifyCustomerIsLoaded (customer: string) {
    const CUSTOMER_DETAILS_BODY_TITLE = `//*[@class="z-label"][contains(text(),"${customer.toLowerCase()}")]`
    await this.waitForDisplayed((CUSTOMER_DETAILS_BODY_TITLE))
  }

  async getPageTitle () {
    return this.getCurrentPageTitle()
  }

  async getPageUrl () {
    return this.getCurrentPageUrl()
  }

  async clickFraudReportTab () {
    await this.click(FRAUD_REPORT_TAB)
  }

  async clickAdminReportTab () {
    await this.moveToLocatorPosition(ADMIN_REPORT_TAB, 40)
    await this.click(ADMIN_REPORT_TAB)
  }

  async getOrderCommissionalbleVolume () {
    await this.waitForDisplayed(ORDER_COMMISSIONABLE_VOLUME, 30)
    return this.getAttributesValue(ORDER_COMMISSIONABLE_VOLUME, 'value')
  }

  async getQualifyingVolume () {
    await this.waitForDisplayed(ORDER_QUALIFYING_VOLUME, 30)
    return this.getAttributesValue(ORDER_QUALIFYING_VOLUME, 'value')
  }

  async getCountOFInvalidAuthorization () {
    await this.waitForDisplayed(COUNT_INVALID_AUTHORIZATION, 30)
    return this.getAttributesValue(COUNT_INVALID_AUTHORIZATION, 'value')
  }

  async getLastAuthrInValidTime () {
    await this.waitForDisplayed(LAST_INVALID_AUTHR_TIME, 30)
    return this.getAttributesValue(LAST_INVALID_AUTHR_TIME, 'value')
  }

  async clickFraudActionButton () {
    await this.driver.sleep(5000)
    await this.click(REFRESH_BUTTON)
    await this.driver.sleep(6000)
    await this.click(FRAUD_ACTIONS_APPROVED_BUTTON)
  }

  async setCountOfInvalidAuthorization (value: any) {
    await this.waitForDisplayed(COUNT_INVALID_AUTHORIZATION)
    await this.sendKeys(COUNT_INVALID_AUTHORIZATION, value, 2)
    await this.driver.sleep(3000)
  }

  async setLastInvalidAuthTime (value: any) {
    await this.waitForDisplayed(LAST_INVALID_AUTHR_TIME)
    await this.sendKeys(LAST_INVALID_AUTHR_TIME, value, 2)
    await this.driver.sleep(3000)
  }

  async clickSaveBtn () {
    await this.waitForDisplayed(ADMIN_SAVE, 20)
    await this.moveToLocatorPositionAndClick(ADMIN_SAVE)
    await this.waitForDisplayed(SAVED_ITEMS_MESSAGE, 5)
  }

  async clickOnOrdersReportTab () {
    await this.moveToLocatorPosition(ORDERS_REPORT_TAB)
    await this.click(ORDERS_REPORT_TAB)
  }

  async doubleClickOrder (order: any) {
    const ORDER_ITEM = `//*[@class="ye-default-reference-editor-selected-item-label z-label"][contains(text(),"${order}")]`
    await this.waitForDisplayed(ORDER_ITEM, 5)
    await this.moveToLocatorPosition(ORDER_ITEM)
    await this.doubleClick(ORDER_ITEM)
  }

  async getDeliveryModeText () {
    await this.waitForDisplayed(DELIVERY_MODE_CSM_TEXT)
    return this.getText(DELIVERY_MODE_CSM_TEXT)
  }

  async getShippingTaxValue () {
    await this.waitForDisplayed(ORDER_SHIPPING_TAX, 30)
    return this.getAttributesValue(ORDER_SHIPPING_TAX, 'value')
  }

  async getDefaultPaymentTextByIndex (index: number) {
    return this.getTextByIndex(ADMIN_TAB_FIRST_CHANGES_DEFAULT_PAYMENT, index)
  }

  async doubleClickOnAdminChangedAttribute () {
    await this.driver.sleep(3000)
    await this.waitForDisplayed(ADMINISTRATION_POPUP_CHANGED_ATTRIBUTE, 10)
    await this.moveToLocatorPosition(ADMINISTRATION_POPUP_CHANGED_ATTRIBUTE)
    await this.doubleClick(ADMINISTRATION_POPUP_CHANGED_ATTRIBUTE)
  }

  async clickOnPaymentReportTab () {
    await this.moveToLocatorPosition(PAYMENT_REPORT_TAB)
    await this.click(PAYMENT_REPORT_TAB)
  }

  async getLastPaymentMethodText () {
    await this.driver.sleep(3000)
    return this.getAttributesValue(PAYMENT_METHODS_LAST_TEXT, 'textContent')
  }

  async doubleClickOnLastPaymentMethod () {
    await this.driver.sleep(3000)
    await this.moveToLocatorPosition(PAYMENT_METHOD_LAST_ITEM)
    await this.waitForDisplayed(PAYMENT_METHOD_LAST_ITEM, 10)
    await this.doubleClick(PAYMENT_METHOD_LAST_ITEM)
  }

  async doubleClickOnCustomerReward () {
    await this.waitForDisplayed(CUSTOMER_REWARDS_TEXT_BOX, 10)
    await this.moveToLocatorPosition(CUSTOMER_REWARDS_TEXT_BOX)
    await this.doubleClick(CUSTOMER_REWARDS_TEXT_BOX)
    this.driver.sleep(4000)
  }

  async getFilterCount () {
    await this.waitForDisplayed(ADV_SEARCH_DELETE_FILTER_ICON, 10)
    return this.getCountOfArrayList(ADV_SEARCH_DELETE_FILTER_ICON)
  }

  async deleteAdvanceSearchFilter () {
    await this.waitForDisplayed(ADV_SEARCH_DELETE_FILTER_ICON, 5)
    await this.driver.sleep(5000)
    await this.click(ADV_SEARCH_DELETE_FILTER_ICON)
  }

  async deleteAllAdvanceSearchFilters () {
    let filters = await this.getFilterCount()
    for (let i = 0; i < filters; i++) {
      await this.deleteAdvanceSearchFilter()
    }
  }

  async performOrderAdvancedSearchByCategory (category: any, categoryText: string) {
    let categoryItemXpath = '//*[@class="z-comboitem"]//*[contains(text(),"' + category + '")]'
    await this.waitForDisplayed(ORDER_ADV_SEARCH_CATEGORIES_DROP_DOWN)
    await this.driver.sleep(1000)
    await this.click(ORDER_ADV_SEARCH_CATEGORIES_DROP_DOWN)
    await this.driver.sleep(3000)
    await this.click(categoryItemXpath)
    await this.selectCategoriesTextBox(categoryText)
    await this.clickOnAddButtonFromAdvancedSearch()
  }

  async clickOnAdvancedSearchButton () {
    await this.waitForDisplayed(ORDER_ADVANCED_SEARCH_BUTTON, 5)
    await this.click(ORDER_ADVANCED_SEARCH_BUTTON)
  }

  async clickOnSearchTopButton () {
    await this.waitForDisplayed(ORDER_SEARCH_TOP_BUTTON, 3)
    await this.click(ORDER_SEARCH_TOP_BUTTON)
    await this.waitForSpinnerToDisappear(HYBRIS_PROCESSING_SPINNER)
  }

  async selectCategoriesTextBox (keys: string) {
    let wordsOnCategory = await CommonUtils.countWordsOnString(keys)
    let nonBreakSpace = '\u00a0'
    if (wordsOnCategory > 1) {
      keys = keys.replace(/ /g, nonBreakSpace)
    }

    let categoryTypeXpath = '//*[@class="z-combobox-popup ye-com_hybris_cockpitng_editor_defaultenum z-combobox-open z-combobox-shadow"]' +
        '/ul/li/span[2][@class="z-comboitem-text"][text()="' + keys + '"]'
    await this.waitForDisplayed(ORDER_ADV_SEARCH_CATEGORIES_SEARCH_DROPDOWN, 10)
    await this.click(ORDER_ADV_SEARCH_CATEGORIES_SEARCH_DROPDOWN)
    await this.driver.sleep(2000)
    await this.click(categoryTypeXpath, 7)
  }

  async clickOnAddButtonFromAdvancedSearch () {
    await this.waitForDisplayed(ORDER_ADV_SEARCH_ADD_BUTTON, 7)
    await this.click(ORDER_ADV_SEARCH_ADD_BUTTON)
  }

  async getFirstRowFromRegistry () {
    return this.getText(FIRST_ORDER_TEXT_BOX)
  }

  async clickOnAdvSearchDescendingOrderArrow () {
    await this.waitForDisplayed(ADV_SEARCH_DESC_ORDER_ARROW, 10)
    await this.click(ADV_SEARCH_DESC_ORDER_ARROW)
  }

  async getOrderStatusText () {
    await this.waitForDisplayed(ORDER_STATUS_GRID_TEXT, 3)
    return this.getText(ORDER_STATUS_GRID_TEXT)
  }

  async searchOrder (order) {
    await this.waitForDisplayed(ORDER_SEARCH_TOP_TEXTBOX, 20)
    await this.sendKeys(ORDER_SEARCH_TOP_TEXTBOX, order)
    await this.clickOnSearchTopButton()
    await this.verifySearchedCustomerIsLoaded(order)
  }

  async clickOnOrderDateColumn () {
    await this.waitForDisplayed(ORDER_DATE_COLUMN_NAME, 8)
    await this.click(ORDER_DATE_COLUMN_NAME)
    await this.driver.sleep(6000)
  }

  async getLineDetailsFirstProductCode () {
    return this.getText(LINE_DETAILS_PRODUCT_CODE_FIRST_ITEM)
  }

  async getLineDetailsFirstProductName () {
    return this.getText(LINE_DETAILS_PRODUCT_NAME_FIRST_ITEM)
  }

  async isRewardTextBoxDisplayed () {
    return this.locatorFound(CUSTOMER_REWARDS_TEXT_BOX)
  }

  async enterStatusToChange (status: string) {
    await this.moveToLocatorPositionAndClick(ORDER_STATUS_DROPDOWN_BUTTON)
    await this.clickByText(ORDER_STATUS_DROPDOWN_TEXT, status)
  }

  async getCSMOrderStatusText () {
    await this.waitForDisplayed(ORDER_STATUS_TEXT, 30)
    return this.getAttributesValue(ORDER_STATUS_TEXT, 'value')
  }

  async clickProfileReportTab () {
    await this.moveToLocatorPosition(ADMIN_PROFILE_TAB, 40)
    await this.click(ADMIN_PROFILE_TAB)
  }

  async isGroupSectionDisplayed () {
    await this.waitForDisplayed(PROFILE_TAB_GROUPS_TEXT, 10)
    return this.locatorFound(PROFILE_TAB_GROUPS_TEXT)
  }

  async clickAddGroupFromProfileTab () {
    await this.waitForDisplayed(PROFILE_TAB_ADD_GROUP, 10)
    await this.click(PROFILE_TAB_ADD_GROUP)
  }

  async addGroupOnProfileTab (group: string) {
    let groupOption = `//*[@class="z-label"][contains(text(),"${group}")]`
    await this.waitForDisplayed(groupOption)
    await this.moveToLocatorPositionAndClick(groupOption)
  }

  async clickOnLastChageFromAuditTrail () {
    await this.waitForDisplayed(AUDIT_TRAIL_LAST_CHANGE, 10)
    await this.doubleClick(AUDIT_TRAIL_LAST_CHANGE)
  }

  async deleteGroupOnProfileTab (group: string) {
    let groupDeleteOption = `//*[@class="ye-default-reference-editor-selected-item-label z-label"][contains(text(),"${group}")]/../div`
    let groupOption = `//*[@class="ye-default-reference-editor-selected-item-label z-label"][contains(text(),"${group}")]`
    await this.waitForDisplayed(groupOption)
    await this.moveToLocatorPosition(groupOption)
    await this.moveToLocatorPositionAndClick(groupDeleteOption)
  }

  async clickPositionPriceTab () {
    await this.moveToLocatorPosition(POSITION_PRICE_REPORT_TAB, 40)
    await this.click(POSITION_PRICE_REPORT_TAB)
  }

  async doubleClickOnOrderEntry () {
    await this.waitForSpinnerToDisappear(HYBRIS_PROCESSING_SPINNER)
    await this.waitForDisplayed(ORDER_ENTRY_TEXT, 10)
    await this.moveToLocatorPosition(ORDER_ENTRY_TEXT)
    await this.doubleClick(ORDER_ENTRY_TEXT)
  }

  async clickConsignmentsActionDot () {
    await this.waitForDisplayed(CONSIGNMENTS_ACTION_DOT, 40)
    await this.click(CONSIGNMENTS_ACTION_DOT)
  }

  async clickOnEditDetails () {
    await this.waitForDisplayed(EDIT_DETAILS_POP_UP, 40)
    await this.click(EDIT_DETAILS_POP_UP)
  }

  async getDiscountIncludedText () {
    await this.waitForDisplayed(DISCOUNT_INCLUDED_TEXT, 5)
    return this.getText(DISCOUNT_INCLUDED_TEXT)
  }

  async getEntryGroupsCount () {
    await this.waitForDisplayed(ORDER_ENTRY_GROUPS, 3)
    let kit = await this.getElementsTextArrayList(ORDER_ENTRY_GROUPS, 3)
    return kit.length
  }

  async clickLastRefreshButton () {
    await this.waitForDisplayed(REFRESH_BUTTON, 3)
    await this.click(REFRESH_BUTTON)
  }
}
