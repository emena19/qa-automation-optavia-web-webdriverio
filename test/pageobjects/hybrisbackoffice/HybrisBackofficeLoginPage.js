// @flow
import BasePage from '../BasePage'

const LOGIN_FORM = '//*[@id="loginForm"]'
const USERNAME = '//input[@name="j_username"]'
const PASSWORD = '//input[@name="j_password"]'
const LOGIN_BTN = '//button[contains(text(),"Login")]'

export default class HybrisBackofficeLoginPage extends BasePage {
  async verifyPageIsLoaded () {
    await this.waitForDisplayed(LOGIN_FORM)
    await this.waitForDisplayed(USERNAME)
    await this.waitForDisplayed(PASSWORD)
    await this.waitForDisplayed(LOGIN_BTN)
  }

  async getPageTitle () {
    return this.getCurrentPageTitle()
  }

  async getPageUrl () {
    return this.getCurrentPageUrl()
  }

  async loginAsCSR (username: string, password: string) {
    await this.sendKeys(USERNAME, username)
    await this.sendKeys(PASSWORD, password)
    await this.waitForDisplayed(LOGIN_BTN, 5)
    await this.click(LOGIN_BTN)
  }
}
