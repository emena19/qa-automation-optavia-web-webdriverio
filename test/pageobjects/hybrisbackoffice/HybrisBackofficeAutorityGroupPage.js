// @flow
import BasePage from '../BasePage'

const AUTORITY_GROUPS = '//span[contains(text(),"Authority Groups")]'
const CSAGENT_ROLE_RADIO_BTN = '//div[contains(text(),"Customer Support Agent Role")]/span'
const CSADMIN_ROLE_RADIO_BTN = '//div[contains(text(),"Customer Support Administrator Role")]/span'
const PROCEED_BTN = '//button[contains(text(),"PROCEED")]'

export default class HybrisBackofficeAutorityGroupPage extends BasePage {
  async verifyPageIsLoaded () {
    await this.waitForDisplayed(AUTORITY_GROUPS, 20)
    await this.waitForDisplayed(CSAGENT_ROLE_RADIO_BTN)
    // await this.waitForDisplayed(CSADMIN_ROLE_RADIO_BTN)
    await this.waitForDisplayed(PROCEED_BTN, 20)
  }

  async getPageTitle () {
    return this.getCurrentPageTitle()
  }

  async getPageUrl () {
    return this.getCurrentPageUrl()
  }

  async proceedAsCSAgentRole () {
    await this.waitForDisplayed(CSAGENT_ROLE_RADIO_BTN, 5)
    await this.click(CSAGENT_ROLE_RADIO_BTN)
    await this.click(PROCEED_BTN)
  }

  async proceedAsCSAdminRole () {
    await this.click(CSADMIN_ROLE_RADIO_BTN)
    await this.click(PROCEED_BTN)
  }

  async proceedAsCsmRole () {
    await this.click(PROCEED_BTN)
  }

  async isAuthorityGroupLoaded () {
    return this.locatorFound(AUTORITY_GROUPS)
  }
}
