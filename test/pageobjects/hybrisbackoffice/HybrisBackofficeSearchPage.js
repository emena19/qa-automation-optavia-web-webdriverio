// @flow
import BasePage from '../BasePage'

const SEARCH_ORDER_NUMBER_TEXTBOX = '//*[@class="yw-search-mode-container z-div"]//input'
const ATTRIBUTE_ORDER_OR_CUST_ID_TEXTBOX = '(//div[@class="z-grid-body"]//input)[2]'
const SEARCH_BUTTON = '//button[contains(@class,"yw-textsearch-searchbutton y-btn-primary z-button")]'
const SEARCH_GRID_ITEMS = '//tr[contains(@class,"z-listitem")]'
const SEARCH_GRID_FIRST_ITEM = '//tr[contains(@class,"z-listitem")][1]'
const HYBRIS_PROCESSING_SPINNER = '//*[@class="z-loading-indicator"]'
const SEARCH_CANCEL_BUTTON = '//*[@class="z-label"][text()="Cancel"]/parent::*'
const LOGOUT_BUTTON = '(//*[@class="z-toolbarbutton"])[last()]'
const SEARCH_BY_TYPE_ICON = '//*[@class="cng-action-icon z-image"][@title="Search by type"]'
const RESULTS_WINDOW = '//*[@class="z-label"][text()="No items selected"]'
const COLLAPSE_RESULTS_WINDOW = '(//*[@class="yw-expandCollapse z-button"])[2]'
const ADVANCED_SEARCH_ADDITIONAL_FIELDS_LIST = '//tr[contains(@class,"yw-active-attributes-grid yw-advancedsearch-last-row-for-condition z-row")]/td/span'
const HEADER_FIELDS_LIST = '//th[contains(@class,"yw-listview-colheader z-listheader")]/div'
const SEARCH_ITEM_NO_ENTRY_TEXT = '(//div[contains(@class,"z-listbox-body")]//*[text()="No entries"])[1]'
const ADVANCED_SEARCH_ID = '//*[@class="z-label"][text()="ID"]/../../td[3]//input'
const SEARCH_RUN_CRONJOB_BUTTON = '//*[@title="Run CronJob"]'
const SEARCH_POPUP_YES_BUTTON = '//*[@class="z-messagebox-button z-button"][text()="Yes"]'
const CRONJOB_CURRENT_STATUS_TEXT = '(//span[text()="Current status"]//parent::div)//following-sibling::div//input'
const REFRESH_BUTTON = '(//*[@class="z-button"][text() ="Refresh"])[2]'
const RETURNS_COLUMN_HEADER = `//div[@class='z-listheader-content' and contains(text(),"Return Reasons")]`

export default class HybrisBackofficeSearchPage extends BasePage {
  async verifyPageIsLoaded () {
    await this.waitForDisplayed(SEARCH_ORDER_NUMBER_TEXTBOX, 7)
    await this.waitForDisplayed(SEARCH_BUTTON)
    await this.waitForDisplayed(SEARCH_GRID_ITEMS)
  }

  async getPageTitle () {
    return this.getCurrentPageTitle()
  }

  async getPageUrl () {
    return this.getCurrentPageUrl()
  }

  // Refrain to use or update this function, it will be removed later
  async searchOrderNumber (order: any) {
    await this.waitForDisplayed(SEARCH_ORDER_NUMBER_TEXTBOX)
    await browser.pause(33000) // TODO: This needs to be refactored
    await this.sendKeys(SEARCH_ORDER_NUMBER_TEXTBOX, order, 2)
  }

  // Refrain to use or update this function, it will be removed later
  async clickSearchBtn () {
    await this.click(SEARCH_BUTTON)
    await browser.pause(10000) // TODO: This needs to be refactored
  }

  // Refrain to use or update this function, it will be removed later
  async clickSearchResult (order: any) {
    let searchedResult = `//*[@class="yw-listview-cell z-listcell"]/descendant::span[2][contains(text(),"${order}")]`
    await this.waitForDisplayed(searchedResult)
    await this.click(searchedResult)
    await browser.pause(10000) // TODO: This needs to be refactored
  }

  async search (orderNumber: string) {
    await this.waitForDisplayed(SEARCH_ORDER_NUMBER_TEXTBOX, 10)
    await this.sendKeys(SEARCH_ORDER_NUMBER_TEXTBOX, orderNumber)
    await this.click(SEARCH_BUTTON)
    await browser.pause(5000)
  }

  async searchOrder (orderNumber: string) {
    await this.search(orderNumber)
    await this.waitForSpinnerToDisappear(HYBRIS_PROCESSING_SPINNER)
    await this.verifySearchedOrderIsDisplayed(orderNumber)
  }

  async searchOnCSR (orderOrIdNumber: string) {
    await this.waitForDisplayed(ATTRIBUTE_ORDER_OR_CUST_ID_TEXTBOX, 10)
    await this.sendKeys(ATTRIBUTE_ORDER_OR_CUST_ID_TEXTBOX, orderOrIdNumber)
    await this.click(SEARCH_BUTTON)
    await browser.pause(5000)
  }

  async searchOnCsrByOrderOrCustomerId (orderOrIdNumber: string) {
    await this.searchOnCSR(orderOrIdNumber)
    await this.verifySearchedOrderIsDisplayed(orderOrIdNumber)
  }

  async verifySearchedOrderIsDisplayed (orderNumber: string) {
    let searchedOrder = `//tr[contains(@class,"z-listitem")][1]//*[contains(text(),"${orderNumber}")]`
    await this.waitForDisplayed(searchedOrder, 100)
  }

  async clickSearchedOrder () {
    await this.waitForDisplayed(SEARCH_GRID_FIRST_ITEM, 10)
    await this.click(SEARCH_GRID_FIRST_ITEM, 5)
    await this.waitForSpinnerToDisappear(HYBRIS_PROCESSING_SPINNER)
  }

  async cleanSearch () {
    await this.sendKeys(SEARCH_ORDER_NUMBER_TEXTBOX, '')
    await this.click(SEARCH_BUTTON)
    await this.waitForDisplayed(SEARCH_GRID_ITEMS, 30)
  }

  async getCancelButtonCssAttribute () {
    await this.waitForDisplayed(SEARCH_CANCEL_BUTTON, 5)
    return this.getCssAttributeValue(SEARCH_CANCEL_BUTTON, 'display')
  }

  async logOutFromHybris () {
    await this.waitForDisplayed(LOGOUT_BUTTON, 5)
    await this.click(LOGOUT_BUTTON)
  }

  async isSearchBoxPresent () {
    return this.locatorFound(SEARCH_ORDER_NUMBER_TEXTBOX, 3)
  }

  async clickOnSpecificSearchResult (searchCriteria: string) {
    let searchItem = `//tr[contains(@class,"z-listitem")]//*[contains(text(),"${searchCriteria}")]`
    await this.waitForDisplayed(searchItem, 100)
    await this.click(searchItem)
  }

  async clickOnSearchByTypeIcon () {
    await this.waitForDisplayed(SEARCH_BY_TYPE_ICON, 7)
    await this.click(SEARCH_BY_TYPE_ICON)
  }

  async collapseResultWindow () {
    if (await this.locatorFound(RESULTS_WINDOW)) {
      await this.moveToLocatorPosition(COLLAPSE_RESULTS_WINDOW)
      await this.click(COLLAPSE_RESULTS_WINDOW)
    }
  }

  async getAdditionalAttributeFieldsTextList () {
    await this.waitForDisplayed(ADVANCED_SEARCH_ADDITIONAL_FIELDS_LIST, 5)
    return this.getElementsTextArrayList(ADVANCED_SEARCH_ADDITIONAL_FIELDS_LIST)
  }

  async getHeaderFieldsTextList () {
    await this.waitForDisplayed(HEADER_FIELDS_LIST, 5)
    return this.getElementsTextArrayList(HEADER_FIELDS_LIST)
  }

  async verifyNoEntryTextDisplayed () {
    await this.waitForDisplayed(SEARCH_ITEM_NO_ENTRY_TEXT, 20)
  }

  async searchByEmailAdvSearch (email: string) {
    await this.waitForDisplayed(ADVANCED_SEARCH_ID, 10)
    await this.sendKeys(ADVANCED_SEARCH_ID, email)
  }

  async clickOnRunCronJobButton () {
    await this.waitForDisplayed(SEARCH_RUN_CRONJOB_BUTTON, 10)
    await this.click(SEARCH_RUN_CRONJOB_BUTTON)
  }

  async clickYesFromPopUpWindow () {
    await this.waitForDisplayed(SEARCH_POPUP_YES_BUTTON, 10)
    await this.click(SEARCH_POPUP_YES_BUTTON)
  }

  async clickRefreshButton () {
    await this.waitForDisplayed(REFRESH_BUTTON, 10)
    await this.click(REFRESH_BUTTON)
  }

  async getCronJobStatus () {
    await this.waitForDisplayed(CRONJOB_CURRENT_STATUS_TEXT, 10)
    return this.getAttributesValue(CRONJOB_CURRENT_STATUS_TEXT, 'value')
  }

  async runCronJobAndWaitUntilIsCompleted () {
    await this.clickOnRunCronJobButton()
    await this.clickYesFromPopUpWindow()
    await this.clickRefreshButton()
    let i = 0
    let status = null
    while (status !== 'FINISHED') {
      await browser.pause(20000)
      await this.clickRefreshButton()
      status = await this.getCronJobStatus()
      i++
      if (i === 11) {
        await browser.pause(5000)
        break
      }
    }
  }

  async searchOrderIDOnCSR (orderID: string) {
    await this.searchOnCSR(orderID, 'Order Nr.')
    await this.verifySearchedOrderIsLoaded(orderID)
  }

  async verifySearchedOrderIsLoaded (order) {
    let searchedOrder = `//tr[contains(@class,"z-listitem")][1]//*[contains(text(),"${order}")]`
    await this.waitForDisplayed(searchedOrder, 20)
  }

  async isReturnsColumnDisplayed () {
    return this.locatorFound(RETURNS_COLUMN_HEADER)
  }
}
