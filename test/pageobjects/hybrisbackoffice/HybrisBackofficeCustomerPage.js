// @flow
import BasePage from '../BasePage'

const CUSTOMER_SEARCH_TOP_TEXTBOX = '//*[@class="yw-search-mode-container z-div"]//input'
const CUSTOMER_SEARCH_TOP_BUTTON = '//button[contains(@class,"yw-textsearch-searchbutton y-btn-primary z-button")]'
const CUSTOMER_FIRST_GRID_RESULT = '//tr[contains(@class,"z-listitem")][1]'
const CUSTOMER_GRID_RESULT = '//tr[contains(@class,"z-listitem")]'
const CUSTOMER_TOP_MENU_ITEMS = '//*[contains(@class,"yw-editorarea-tabbox-tabs-tab")]'
const CUSTOMER_TOP_MENU = '//*[@class="yw-editorarea-tabbox-tabs z-tabs"]'
const CUSTOMER_CART_TEXT = '//span[contains(text(),"Carts")]/parent::div/following-sibling::div//span[@class="ye-default-reference-editor-selected-item-label z-label"]'
const CUSTOMER_ORDERS_LIST = '//span[contains(text(),"Orders")]/parent::div/following-sibling::div//span[@class="ye-default-reference-editor-selected-item-label z-label"]'
const ADDRESS_ADMIN_REPORT_TAB = '(//span[text()="Administration"])[2]'
const COUNTY_TEXT = '//span[text()="County"]//..//.././div/input'
const DISTRICT_TEXT = '//span[text()="District"]//..//.././div/input'
const STREET_NAME_TEXT = '//span[text()="Street Name"]//..//.././div/input'
const POSTAL_CODE_TEXT = '//span[text()="Postal Code"]//..//.././div/input'
const TOWN_TEXT = '//span[text()="Town"]//..//.././div/input'
const PHONE_TEXT = '//span[text()="Phone1"]//..//.././div/input'
const CUSTOMER_ADVANCED_SEARCH_BUTTON = '//*[@class="yw-toggle-advanced-search z-button"]'
const CUSTOMER_ADV_SEARCH_CATEGORIES_DROP_DOWN = '//*[@class="yw-additional-attributes-selector z-combobox z-combobox-readonly"]//*[@class="z-combobox-input"]'
const CUST_ADV_SEARCH_COMPARATOR_DROP_DOWN_LIST = '//*[@class="yw-advancedsearch-operator z-combobox z-combobox-readonly"]'
const CUSTUMER_ADV_SEARCH_CATEGORIES_TEXT_BOX = '//*[@class="ye-default-reference-editor-bandbox ye-default-reference-editor-bandbox-list-collapsed z-bandbox"]//*[@class="z-bandbox-input"]'
const CUSTUMER_ADV_SEARC_CATEGORIES_SEARCH_ICON = '//*[@class="ye-default-reference-editor-bandbox ye-default-reference-editor-bandbox-list-collapsed z-bandbox"]//*[@class="z-bandbox-icon z-icon-search"]'
const CUSTOMER_ADV_SEARCH_ADD_BUTTON = '//*[@class="z-row-content"]//button[contains(@class,"z-button")]'
const CUSTOMER_PAGINATION_RESULTS_TEXT = '(//input[@type="text"][@value="1"][@class="z-paging-input"])[3]'
const CUSTOMER_EMAIL_TEXT_BOX = '(//span[2][@class="yw-listview-cell-label z-label"])[1]'
const ADVANCED_SEARCH_COMPARATOR_DOES_NOT_CONTAIN = '/html/body/div[4]/ul/li[2]/span[2]'
const CUSTOMER_ORDERS_SUB_SECTION = '//div[@title="hmc.section.orders"]'
const HYBRIS_PROCESSING_SPINNER = '//*[@class="z-loading-indicator"]'
const ADDRESSES_REPORT_TAB = '//*[@class="z-caption-content"]//*[contains(text(),"Addresses")]'
const DEFAULT_SHIPMENT_ADDRESS = '(//*[@class="ye-default-reference-editor-selected-item-label z-label"])[2]'
const DEFAULT_SHIPMENT_ADDRESS_POP_UP = '(//*[@class="yw-editorarea-tabbox-tabpanels-tabpanel-groupbox-caption yw-editorarea-tabbox-tabpanels-tabpanel-groupbox-caption-essential z-caption"])[3]'
const DEFAULT_SHIPMENT_ADDRESS_POSTAL_CODE = '(//*[@class="ye-input-text ye-com_hybris_cockpitng_editor_defaulttext z-textbox"])[14]'
const SHIPMENT_ADDRESS_LIST = '(//*[@class="ye-default-reference-editor-selected-item-label z-label"])'
const GENERAL_REPORT_TAB = '//*[@class="z-caption-content"]//*[contains(text(),"General")]'
const LAST_CHANGES_LIST = '(//span[text()="Last changes"]//parent::div)//following-sibling::div/div[2]//span'
const LAST_CHANGES_LAST_ITEM = '((//span[text()="Last changes"]//parent::div)//following-sibling::div/div[2]//span)[last() - 1]'
const SHIPPING_METHOD_TEXT = '(//span[text()="Shipping Method"]//parent::div)//following-sibling::div//input'
const PASSWORD_TAB = '//span[text()="Password"]'
const DISABLE_LOGIN_RADIO_BUTTON_TRUE = '(//*[@class="z-radio-content"][contains(text(),"True")])[last()]'
const DISABLE_LOGIN_RADIO_BUTTON_FALSE = '(//*[@class="z-radio-content"][contains(text(),"False")])[last()]'
const TABS_MENU_SAVE_BUTTON = '(//*[@class="y-btn-primary z-button"][contains(text(),"Save")])[2]'
const CUSTOMER_CART_TEXT_DISABLED = '//span[contains(text(),"Cart")]/parent::div//span[@class="ye-default-reference-editor-selected-item-label ye-editor-disabled z-label"]'
const CUSTOMER_CART = '//span[contains(text(),"Carts")]/parent::div/following-sibling::div//span[@class="ye-default-reference-editor-selected-item-label z-label"]'
const CUSTOMER_POP_UP_POSITIONS_AND_PRICES = '//*[@class="yw-editorarea-tablabel z-label"][text()="Positions and Prices"]'
const CUSTOMER_POP_UP_ADMINISTRATION_TAB = '(//*[@class="yw-editorarea-tablabel z-label"][text()="Administration"])[last()]'
const CUST_POP_UP_CART_ENTRY_KIT_ID = '(//span[text()="Kit No"]//parent::div)//following-sibling::div//input'
const CUST_POP_UP_CLOSE_ICON = '(//*[@class="z-window-icon z-window-close"])[last()]'
const CONFIGURED_PRODUCTS_HASH_CODE_INPUT = '//*[@class="ye-com_hybris_cockpitng_editor_defaultinteger z-longbox"]'
const KIT_MODIFIED_DATE_TIME = '(//span[text()="Time modified"]//parent::div)//following-sibling::div//input'
const CLOSE_ADV_SEARCH_BUTTON = '//*[@class="yw-toggle-advanced-search yw-toggle-open z-button"]'
const ITEMS_DETAILS_EXPAND_ICON = '(//*[@class="yw-expandCollapse z-button"])[2]'
const SEARCH_ITEM_NO_ENTRY_TEXT = '(//div[contains(@class,"z-listbox-body")]//*[text()="No entries"])[1]'
const GENERAL_ID_TEXT = '(//span[text()="ID"]//parent::div)//following-sibling::div//input'
const GENERAL_NAME_TEXT = '(//span[text()="Name"]//parent::div)//following-sibling::div//input'
const GENERAL_GROUP_LIST = '//*[@class="ye-default-reference-editor-selected-item-container z-div"]'
const READONLY_TEXTBOX_COUNT_LIST = '.ye-input-text ye-com_hybris_cockpitng_editor_defaulttext z-textbox z-textbox-readonly'
const READONLY_DROPDOWN_COUNT_LIST = '.ye-default-reference-editor-selected-item-label ye-editor-disabled z-label'
const READONLY_DATEBOX_COUNT_LIST = '.z-datebox-input'
const GENERAL_ACTIVE_AUTOSHIP_GROUP_ITEM = '//*[@title="Active Autoship [Active_Autoship]"]'
const REFRESH_BUTTON = `(//button[contains(@class,'z-button')][contains(text(), "Refresh")])[2]`
const KIT_ENTRY_GROUPS = '(//span[text()="GeneratedKitIds"]/parent::div//following-sibling::div//tr//div[@class="z-listcell-content"])'
export default class HybrisBackofficeCustomerPage extends BasePage {
  async verifyPageIsLoaded () {
    await this.waitForDisplayed(CUSTOMER_SEARCH_TOP_TEXTBOX, 20)
    await this.waitForDisplayed(CUSTOMER_SEARCH_TOP_BUTTON, 20)
    await this.waitForDisplayed(CUSTOMER_GRID_RESULT, 20)
  }

  async verifySearchedCustomerIsLoaded (customer) {
    let searchedCustomer = `//tr[contains(@class,"z-listitem")][1]//*[contains(text(),"${customer.toLowerCase()}")]`
    await this.waitForDisplayed(searchedCustomer, 20)
  }

  async verifyCustomerTopMenuIsLoaded () {
    await this.waitForDisplayed(CUSTOMER_TOP_MENU, 20)
  }

  async verifyNoEntryTextDisplayed () {
    await this.waitForDisplayed(SEARCH_ITEM_NO_ENTRY_TEXT, 20)
  }

  async search (textToSearch) {
    await this.waitForDisplayed(CUSTOMER_SEARCH_TOP_TEXTBOX, 20)
    // await this.waitForSpinnerToDisappear(HYBRIS_PROCESSING_SPINNER)
    await this.sendKeys(CUSTOMER_SEARCH_TOP_TEXTBOX, textToSearch)
    await this.click(CUSTOMER_SEARCH_TOP_BUTTON)
    await browser.pause(5000)
  }

  async searchCustomer (customer) {
    await this.search(customer)
    await this.verifySearchedCustomerIsLoaded(customer)
  }

  async clickOnFirstRegistry () {
    await this.waitForDisplayed(CUSTOMER_FIRST_GRID_RESULT, 20)
    await this.click(CUSTOMER_FIRST_GRID_RESULT)
    await this.verifyCustomerTopMenuIsLoaded()
  }

  async clickOnOrdersTopMenu () {
    await this.waitForDisplayed(CUSTOMER_TOP_MENU_ITEMS, 20)
    await this.clickByIndex(CUSTOMER_TOP_MENU_ITEMS, 5)
    await this.waitForDisplayed(CUSTOMER_ORDERS_SUB_SECTION, 20)
  }

  async getCartText () {
    await this.waitForDisplayed(CUSTOMER_CART_TEXT, 20)
    return this.getText(CUSTOMER_CART_TEXT)
  }

  async getCustomerOrdersTextList () {
    let ordersList = await this.getElementsTextArrayList(CUSTOMER_ORDERS_LIST)
    let ordersArray = ordersList.toString().split(' - ').toString().split(',')
    let resultArray = []
    for (let i = 0; i <= ordersArray.length - 1; i += 4) {
      resultArray.push(ordersArray[i])
    }
    return resultArray
  }

  async getCartCount () {
    await this.waitForDisplayed(CUSTOMER_CART_TEXT, 5)
    return this.getCountOfArrayList(CUSTOMER_CART_TEXT)
  }

  async clickOnTopMenuAddress () {
    await browser.pause(2000)
    await this.waitForDisplayed(CUSTOMER_TOP_MENU_ITEMS, 3)
    await this.clickByIndex(CUSTOMER_TOP_MENU_ITEMS, 2)
  }

  async getAddressCount () {
    await this.waitForDisplayed(SHIPMENT_ADDRESS_LIST, 5)
    return this.getCountOfArrayList(SHIPMENT_ADDRESS_LIST)
  }

  async doubleClickOnAddressHistory (index) {
    await browser.pause(4000)
    const ADDRESS =
      `(//div[@class="ye-default-reference-editor-selected-item-container ye-remove-enabled z-div"])[${index}]`
    await this.waitForDisplayed(ADDRESS, 3)
    await this.scrollElementIntoView(ADDRESS)
    await this.moveToLocatorPositionAndDoubleClick(ADDRESS)
  }

  async clickOnAdminTab () {
    await browser.pause(2000)
    await this.waitForDisplayed(ADDRESS_ADMIN_REPORT_TAB, 3)
    await this.click(ADDRESS_ADMIN_REPORT_TAB)
  }

  async getCountyValue () {
    await browser.pause(7000)
    await this.moveToLocatorPosition(COUNTY_TEXT)
    return this.getAttributesValue(COUNTY_TEXT, 'value')
  }

  async getDistrictValue () {
    await this.waitForDisplayed(DISTRICT_TEXT)
    await this.moveToLocatorPosition(DISTRICT_TEXT)
    return this.getAttributesValue(DISTRICT_TEXT, 'value')
  }

  async getStreetNameValue () {
    await this.waitForDisplayed(STREET_NAME_TEXT)
    await this.moveToLocatorPosition(STREET_NAME_TEXT)
    return this.getAttributesValue(STREET_NAME_TEXT, 'value')
  }

  async getPostalCodeValue () {
    await this.waitForDisplayed(POSTAL_CODE_TEXT)
    await this.moveToLocatorPosition(POSTAL_CODE_TEXT)
    return this.getAttributesValue(POSTAL_CODE_TEXT, 'value')
  }

  async getTownValue () {
    await this.waitForDisplayed(TOWN_TEXT)
    await this.moveToLocatorPosition(TOWN_TEXT)
    return this.getAttributesValue(TOWN_TEXT, 'value')
  }

  async getPhoneValue () {
    await this.waitForDisplayed(PHONE_TEXT)
    await this.moveToLocatorPosition(PHONE_TEXT)
    return this.getAttributesValue(PHONE_TEXT, 'value')
  }

  async clickOnSearchTopButton () {
    await this.waitForDisplayed(CUSTOMER_SEARCH_TOP_BUTTON, 3)
    await this.waitForSpinnerToDisappear(HYBRIS_PROCESSING_SPINNER)
    await this.click(CUSTOMER_SEARCH_TOP_BUTTON)
  }

  async clickOnAdvancedSearchButton () {
    await this.waitForSpinnerToDisappear(HYBRIS_PROCESSING_SPINNER)
    await this.waitForDisplayed(CUSTOMER_ADVANCED_SEARCH_BUTTON, 5)
    await this.click(CUSTOMER_ADVANCED_SEARCH_BUTTON)
  }

  async sendKeysToCategoriesTextBox (keys: string, searchOptions: any) {
    let xpath = '//*[@class="z-listcell-content"]//*[contains(text(),"' + keys + '")]'
    await this.clickByIndex(CUSTUMER_ADV_SEARC_CATEGORIES_SEARCH_ICON, searchOptions)
    await browser.pause(2000)
    await this.sendKeysByIndex(CUSTUMER_ADV_SEARCH_CATEGORIES_TEXT_BOX, keys.substr(0, 7), searchOptions)
    await browser.pause(2000)
    await this.click(xpath, 7)
  }

  async clickOnAddButtonFromAdvancedSearch () {
    await this.waitForDisplayed(CUSTOMER_ADV_SEARCH_ADD_BUTTON, 7)
    await this.click(CUSTOMER_ADV_SEARCH_ADD_BUTTON)
  }

  async performAdvancedSearchByCategory (category: any, categoryText: string, searchOptions: number, comparator: any = 'contains') {
    let xpath = '//*[@class="z-comboitem"]//*[contains(text(),"' + category + '")]'
    await this.waitForDisplayed(CUSTOMER_ADV_SEARCH_CATEGORIES_DROP_DOWN)
    await browser.pause(1000)
    await this.click(CUSTOMER_ADV_SEARCH_CATEGORIES_DROP_DOWN)
    await browser.pause(3000)
    await this.click(xpath)
    await this.sendKeysToCategoriesTextBox(categoryText, searchOptions)
    if (comparator !== 'contains') {
      let index = 3 + searchOptions
      await this.waitForDisplayed(CUST_ADV_SEARCH_COMPARATOR_DROP_DOWN_LIST)
      await this.clickByIndex(CUST_ADV_SEARCH_COMPARATOR_DROP_DOWN_LIST, index)
      await this.click(ADVANCED_SEARCH_COMPARATOR_DOES_NOT_CONTAIN)
    }
    await this.clickOnAddButtonFromAdvancedSearch()
  }

  async modifyPaginationResults (results: any) {
    await this.waitForDisplayed(CUSTOMER_PAGINATION_RESULTS_TEXT, 5)
    await this.sendKeys(CUSTOMER_PAGINATION_RESULTS_TEXT, results + await browser.keys('Enter'))
    await this.waitForSpinnerToDisappear(HYBRIS_PROCESSING_SPINNER)
  }

  async getFirstEmailFromRegistry () {
    await this.waitForSpinnerToDisappear(HYBRIS_PROCESSING_SPINNER)
    return this.getText(CUSTOMER_EMAIL_TEXT_BOX)
  }

  async cleanSearch () {
    await this.sendKeys(CUSTOMER_SEARCH_TOP_TEXTBOX, '')
    await this.click(CUSTOMER_SEARCH_TOP_BUTTON)
    await this.waitForDisplayed(CUSTOMER_GRID_RESULT, 30)
  }

  async clickOnAddressesReportTab () {
    await this.moveToLocatorPosition(ADDRESSES_REPORT_TAB)
    await this.click(ADDRESSES_REPORT_TAB)
  }

  async defaultShipmentAddressPopUpIsLoaded () {
    await this.waitForDisplayed(DEFAULT_SHIPMENT_ADDRESS_POP_UP, 3)
  }

  async doubleClickDefaultShipmentAddress () {
    await this.waitForDisplayed(DEFAULT_SHIPMENT_ADDRESS)
    await this.scrollElementIntoView(DEFAULT_SHIPMENT_ADDRESS)
    await this.moveToLocatorPositionAndDoubleClick(DEFAULT_SHIPMENT_ADDRESS)
  }

  async getDefaultShipmentAddressPostalCodeText () {
    await this.scrollElementIntoView(DEFAULT_SHIPMENT_ADDRESS_POSTAL_CODE)
    return this.getAttributesValue(DEFAULT_SHIPMENT_ADDRESS_POSTAL_CODE, 'value')
  }

  async clickOnGeneralReportTab () {
    await this.moveToLocatorPosition(GENERAL_REPORT_TAB)
    await this.click(GENERAL_REPORT_TAB)
  }

  async getGroupsList (index) {
    const groupValue =
      `(//span[@class="ye-default-reference-editor-selected-item-label z-label"])[${index}]`
    await this.waitForDisplayed(groupValue, 3)
    await this.scrollElementIntoView(groupValue)
    return this.getText(groupValue)
  }

  async getLastChangesCount () {
    await this.waitForDisplayed(LAST_CHANGES_LIST, 5)
    return this.getCountOfArrayList(LAST_CHANGES_LIST)
  }

  async doubleClickOnLastChangeHistory () {
    await this.waitForDisplayed(LAST_CHANGES_LAST_ITEM, 3)
    await this.scrollElementIntoView(LAST_CHANGES_LAST_ITEM)
    await this.moveToLocatorPositionAndDoubleClick(LAST_CHANGES_LAST_ITEM)
    browser.pause(7000)
  }

  async doubleClickOnLastChangeWithNoScroll () {
    browser.pause(4000)
    await this.waitForDisplayed(LAST_CHANGES_LAST_ITEM, 3)
    await this.moveToLocatorPositionAndDoubleClick(LAST_CHANGES_LAST_ITEM)
  }

  async getShippingMethodText () {
    await this.waitForDisplayed(SHIPPING_METHOD_TEXT, 5)
    return this.getText(SHIPPING_METHOD_TEXT)
  }

  async clickPasswordTab () {
    await this.waitForDisplayed(PASSWORD_TAB, 3)
    await this.click(PASSWORD_TAB)
  }

  async clickDisableLoginRadioButton (disable: boolean) {
    if (disable) {
      await this.waitForDisplayed(DISABLE_LOGIN_RADIO_BUTTON_TRUE, 3)
      await this.moveToLocatorPosition(DISABLE_LOGIN_RADIO_BUTTON_TRUE)
      await this.click(DISABLE_LOGIN_RADIO_BUTTON_TRUE)
    } else {
      await this.waitForDisplayed(DISABLE_LOGIN_RADIO_BUTTON_FALSE, 3)
      await this.moveToLocatorPosition(DISABLE_LOGIN_RADIO_BUTTON_FALSE)
      await this.click(DISABLE_LOGIN_RADIO_BUTTON_FALSE)
    }
    await browser.pause(3000)
  }

  async clickOnSaveButtonFromTabsMenu () {
    await this.waitForDisplayed(TABS_MENU_SAVE_BUTTON, 10)
    await this.moveToLocatorPosition(TABS_MENU_SAVE_BUTTON, 5)
    await this.click(TABS_MENU_SAVE_BUTTON)
    await browser.pause(10000)
  }

  async clickOnTopMenu (index: number) {
    await this.waitForDisplayed(CUSTOMER_TOP_MENU_ITEMS, 20)
    await this.clickByIndex(CUSTOMER_TOP_MENU_ITEMS, index)
  }

  async isCartDisabled () {
    return this.locatorFound(CUSTOMER_CART_TEXT_DISABLED)
  }

  async doubleClickOnCart (index: number) {
    browser.pause(4000)
    const cart =
      `(//span[contains(text(),"Cart")]/parent::div//span[@class="ye-default-reference-editor-selected-item-label ye-editor-disabled z-label"])[${index}]`
    await this.waitForDisplayed(cart, 3)
    // await this.scrollElementIntoView(cart)
    await this.doubleClick(cart)
  }

  async doubleClickOnCustomerCart () {
    await this.waitForDisplayed(CUSTOMER_CART, 5)
    await this.doubleClick(CUSTOMER_CART)
  }

  async clickOnPositionsAndPrices () {
    await this.waitForDisplayed(CUSTOMER_POP_UP_POSITIONS_AND_PRICES, 5)
    await this.click(CUSTOMER_POP_UP_POSITIONS_AND_PRICES)
  }

  async openKitFromPopUp (product: string) {
    let kitXpath = `//*[@class="ye-default-reference-editor-selected-item-label z-label"][contains(text(),"${product}")]`
    await browser.pause(3000)
    await this.waitForDisplayed(kitXpath, 10)
    await this.moveToLocatorPosition(kitXpath, 5)
    await this.doubleClick(kitXpath, 5)
    await browser.pause(3000)
  }

  async clickOnCustPopUpAdministrationTab () {
    await this.waitForDisplayed(CUSTOMER_POP_UP_ADMINISTRATION_TAB, 25)
    await this.click(CUSTOMER_POP_UP_ADMINISTRATION_TAB)
    await browser.pause(2000)
  }

  async getCartEntryKitId () {
    await this.waitForDisplayed(CUST_POP_UP_CART_ENTRY_KIT_ID, 7)
    await this.moveToLocatorPosition(CUST_POP_UP_CART_ENTRY_KIT_ID)
    return this.getAttributesValue(CUST_POP_UP_CART_ENTRY_KIT_ID, 'value')
  }

  async closeCustPopUp () {
    await this.waitForDisplayed(CUST_POP_UP_CLOSE_ICON, 7)
    await this.moveToLocatorPosition(CUST_POP_UP_CLOSE_ICON)
    await this.click(CUST_POP_UP_CLOSE_ICON)
  }

  async sendKeysToConfiguredProductsHashCode (keys: string) {
    await this.waitForDisplayed(CONFIGURED_PRODUCTS_HASH_CODE_INPUT, 7)
    await this.sendKeys(CONFIGURED_PRODUCTS_HASH_CODE_INPUT, keys)
  }

  async searchKitType (kitType) {
    await this.waitForDisplayed(CUSTOMER_SEARCH_TOP_TEXTBOX, 20)
    await browser.pause(3000)
    await this.sendKeys(CUSTOMER_SEARCH_TOP_TEXTBOX, kitType)
    await this.click(CUSTOMER_SEARCH_TOP_BUTTON)
  }

  async getDateTimeFromKit () {
    await this.waitForDisplayed(KIT_MODIFIED_DATE_TIME, 7)
    return this.getAttributesValue(KIT_MODIFIED_DATE_TIME, 'value')
  }

  async clickOnCloseAdvanceSearchButton () {
    await this.waitForDisplayed(CLOSE_ADV_SEARCH_BUTTON, 7)
    await this.click(CLOSE_ADV_SEARCH_BUTTON)
  }

  async clickOnExpandItemDetailsIcon () {
    await browser.pause(2000)
    await this.waitForDisplayed(ITEMS_DETAILS_EXPAND_ICON, 7)
    await this.click(ITEMS_DETAILS_EXPAND_ICON)
  }

  async openCustomerTemplate (templateId: string) {
    let templateXpath = `//*[@class="ye-default-reference-editor-selected-item-container ye-remove-enabled z-div"]//*[contains(text(),'${templateId}')]`
    await this.waitForDisplayed(templateXpath, 10)
    await this.moveToLocatorPosition(templateXpath)
    await this.doubleClick(templateXpath)
  }

  async doubleClickOnDefaultPayment (index: number) {
    browser.pause(4000)
    const defaultPaymentMethod =
      `(//span[contains(text(),"Default Payment Method")]/parent::div//span[@class="ye-default-reference-editor-selected-item-label ye-editor-disabled z-label"])[${index}]`
    await this.waitForDisplayed(defaultPaymentMethod, 3)
    await this.scrollElementIntoView(defaultPaymentMethod)
    await this.moveToLocatorPositionAndDoubleClick(defaultPaymentMethod)
  }

  async searchOnCSR (searchText: string, attribute: String) {
    const ATTRIBUTE_SEARCH_TEXTBOX =
      `//span[text()="${attribute}"]/parent::td/following-sibling::td/div[@class="yw-advancedsearch-editor z-div"]//input`
    await this.waitForDisplayed(ATTRIBUTE_SEARCH_TEXTBOX, 10)
    await this.sendKeys(ATTRIBUTE_SEARCH_TEXTBOX, searchText)
    await this.click(CUSTOMER_SEARCH_TOP_BUTTON)
    await browser.pause(5000)
  }

  async searchCustomerOnCSR (customerEmail: string) {
    await this.searchOnCSR(customerEmail, 'ID')
    await this.verifySearchedCustomerIsLoaded(customerEmail)
  }

  async searchCartNumberOnCSR (cartNumber: string) {
    await this.searchOnCSR(cartNumber, 'Cart Customer Number')
    await this.verifySearchedCustomerIsLoaded(cartNumber)
  }

  async searchOrderIDOnCSR (orderID: string) {
    await this.searchOnCSR(orderID, 'Order Nr.')
    await this.verifySearchedOrderIsLoaded(orderID)
  }

  async verifySearchedOrderIsLoaded (order) {
    let searchedOrder = `//tr[contains(@class,"z-listitem")][1]//*[contains(text(),"${order}")]`
    await this.waitForDisplayed(searchedOrder, 20)
  }

  async getGeneralID (index) {
    await this.waitForDisplayed(GENERAL_ID_TEXT, 7)
    return this.getAttributesValueByIndex(GENERAL_ID_TEXT, 'value', index)
  }

  async getGeneralName (index) {
    await this.waitForDisplayed(GENERAL_NAME_TEXT, 7)
    return this.getAttributesValueByIndex(GENERAL_NAME_TEXT, 'value', index)
  }

  async doubleClickOnGroups () {
    await this.waitForDisplayed(GENERAL_GROUP_LIST, 7)
    await this.scrollElementIntoView(GENERAL_GROUP_LIST)
    await this.moveToLocatorPositionAndDoubleClick(GENERAL_GROUP_LIST)
  }

  async getCountOfTextBox () {
    return this.getCountOfArrayList(READONLY_TEXTBOX_COUNT_LIST)
  }

  async getCountOfDropDown () {
    return this.getCountOfArrayList(READONLY_DROPDOWN_COUNT_LIST)
  }

  async getCountOfDateBox () {
    await this.waitForDisplayed(READONLY_DATEBOX_COUNT_LIST, 3)
    return this.getCountOfArrayList(READONLY_DATEBOX_COUNT_LIST)
  }

  async getDateBoxAttribute (index) {
    await this.waitForDisplayed(GENERAL_NAME_TEXT, 7)
    return this.getAttributesValueByIndex(READONLY_DATEBOX_COUNT_LIST, 'disabled', index)
  }

  async isActiveAutoShipGroupDisplayed () {
    return this.locatorFound(GENERAL_ACTIVE_AUTOSHIP_GROUP_ITEM)
  }

  async getCustomerOrdersCount () {
    return this.getCountOfArrayList(CUSTOMER_ORDERS_LIST)
  }

  async isCartDisplayed () {
    return this.locatorFound(CUSTOMER_CART_TEXT)
  }

  async clickRefreshButton () {
    await this.waitForDisplayed(REFRESH_BUTTON, 3)
    await this.click(REFRESH_BUTTON)
    await browser.pause(15000)
  }

  async getKitIDCount () {
    await this.waitForDisplayed(KIT_ENTRY_GROUPS, 3)
    let kit = await this.getElementsTextArrayList(KIT_ENTRY_GROUPS, 3)
    return kit.length
  }
}
