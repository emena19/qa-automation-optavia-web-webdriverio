// @flow
import BasePage from '../BasePage'

const COACH_STORE_LINK = '//*[@id="content"]/div/div/div[2]'
//  '//*[@id="optavia_categbanner_Thu Aug 16 07:17:44 EDT 2018"]/div/div/h1')
const WELLNESS_CREDIT_CHECKBOX = '//*[@class="facet_block-label"][contains (text(), \'Wellness Credit\')]'
const WELLNESS_CREDIT_LINK = '//*[@class="category--content"]/descendant::a[contains(text(), "Wellness Credit")]'
const MARKETING_MATERIALS_LINK = '//*[@class="category--content"]/descendant::a[contains(text(), "Marketing Materials")]'
const RENEWAL_KIT_ADD_TO_CART_BUTTON = '//*[@id="addToCartFormbusiness-renewal"]/button'
const HEALT_COACH_POP_UP_CHECKOUT_BUTTON = '//*[@id="addto-cart-layer"]/div/div[3]/a[contains(text(), "Checkout")]'
const HEALTH_COACH_STORE_ARRAY_LIST = '//div[@class="category--head category--link active"]/following-sibling::div//ul//a'
const COACH_STORE_BODY_TITLE = '//*[@id="content"]/div/div/div[2]/h3'

export default class CoachStorePage extends BasePage {
  async verifyPageIsLoaded () {
    await this.waitForDisplayed(COACH_STORE_LINK)
  }

  async getPageTitle () {
    return this.getCurrentPageTitle()
  }

  async getPageUrl () {
    return this.getCurrentPageUrl()
  }

  async clickWellnessCreditLink () {
    await this.click(WELLNESS_CREDIT_LINK)
  }

  async clickMarketingMaterialsLink () {
    await this.click(MARKETING_MATERIALS_LINK)
  }

  async clickWellnessCreditCheckbox () {
    await this.click(WELLNESS_CREDIT_CHECKBOX)
  }

  async clickRenewalKitAddToCartbtn () {
    await this.click(RENEWAL_KIT_ADD_TO_CART_BUTTON)
  }

  async getHealthCoachTextValueArrayList () {
    return this.getElementsTextArrayList(HEALTH_COACH_STORE_ARRAY_LIST)
  }

  async clickOnCheckOutPopUPButton () {
    await this.waitForDisplayed(HEALT_COACH_POP_UP_CHECKOUT_BUTTON, 3)
    await this.click(HEALT_COACH_POP_UP_CHECKOUT_BUTTON)
  }

  async clickProductsInPage (productName, indexNO) {
    const product =
      `(//div[contains(text(),'${productName}')]/../../../..//div/form[contains(@id,"addToCartForm")]/button)['${indexNO}']`
    await this.click(product)
  }

  async getCoachStoreBodyTitle () {
    return this.getText(COACH_STORE_BODY_TITLE)
  }
}
