// @flow
import BasePage from '../BasePage'

const WELLNESS_CREDIT_PAGE = '//*[@id="content"]/div/div/div[2]/h3/span'
const WELLNESS_CREDIT_PAGE_TITLE = '//*[@class="product-shop"]/h1[contains(text(), "Wellness Credit")]'
const WELLNESS_CREDIT_VIEW_DETAILS_BUTTON = '//*[@id="addToCartFormwellness-credit"]/a'
const WELLNESS_CREDIT_CLIENT_ID = '//*[@id="for-client"]'
const WELLNESS_CREDIT_QTY = '//*[@id="qtyInput"]'
const WELLNESS_CREDIT_ADD_TO_CART_BUTTON = '//*[@id="addToCartButton"]'
const WELLNESS_CREDIT_AUTOSHIP_ERROR_MESSAGE = '//*[@class="otp-autoship-msg"]'
const WELLNESS_CREDIT_DISABLED_BUTTON = '//*[@id="actions-container-for-AddToCart"]/div[2]/button'

const ADD_TO_CART_BTN = '//*[@id="addToCartButton"]'

export default class WellnessCreditPage extends BasePage {
  async verifyPageIsLoaded () {
    await this.waitForDisplayed(WELLNESS_CREDIT_PAGE)
  }

  async getPageTitle () {
    return this.getCurrentPageTitle()
  }

  async getPageUrl () {
    return this.getCurrentPageUrl()
  }

  async clickWellnessCreditViewDetailsButton () {
    await this.click(WELLNESS_CREDIT_VIEW_DETAILS_BUTTON)
  }

  async sendWellnessCreditClientID (id: string) {
    await this.sendKeys(WELLNESS_CREDIT_CLIENT_ID, id)
  }

  async sendWellnessCreditQty (qty: string) {
    await this.sendKeys(WELLNESS_CREDIT_QTY, qty)
  }

  async clickWellnessCreditAddToCartButton () {
    await this.click(WELLNESS_CREDIT_ADD_TO_CART_BUTTON)
  }

  async isAddToCartBtnDisabled () {
    return this.hasAttributePresent(ADD_TO_CART_BTN, 'disabled')
  }

  async verifyPageTitleIsLoaded () {
    await this.waitForDisplayed(WELLNESS_CREDIT_PAGE_TITLE, 5)
  }

  async isClientIDTextBoxLoaded () {
    return this.locatorFound(WELLNESS_CREDIT_CLIENT_ID)
  }

  async getAutoshipWCErrorMessage () {
    await this.waitForDisplayed(WELLNESS_CREDIT_AUTOSHIP_ERROR_MESSAGE, 5)
    return this.getText(WELLNESS_CREDIT_AUTOSHIP_ERROR_MESSAGE)
  }

  async isWCAddToCartButtonDisabled () {
    await this.waitForDisplayed(WELLNESS_CREDIT_DISABLED_BUTTON, 5)
    return this.getAttributesValue(WELLNESS_CREDIT_DISABLED_BUTTON, 'disabled')
  }
}
