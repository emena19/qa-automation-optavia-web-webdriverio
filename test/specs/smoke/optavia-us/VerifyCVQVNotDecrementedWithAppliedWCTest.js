// @flow
import {assert} from 'chai'
import driverutils from '@core-libs/DriverUtils'
import CustomerTestData from '@input-data/customers/CustomerTestData'
import ProductTestData, {attb, market, product} from '@input-data/products/ProductTestData'
import HomePage from '@page-objects/home/HomePage'
import Header from '@page-objects/common/Header'
import MainNavBar from '@page-objects/common/MainNavBar'
import LoginPage from '@page-objects/loginPage/LoginPage'
import MyAccountPage from '@page-objects/myaccount/MyAccountPage'
import ShopAllPage from '@page-objects/shop/ShopAllPage'
import AddedToYourShoppingCartPopup from '@page-objects/shop/AddedToYourShoppingCartPopup'
import ShoppingCartPage from '@page-objects/shoppingcart/ShoppingCartPage'
import FinalReviewTile from '@page-objects/checkout/FinalReviewTile'
import OrderConfirmationPage from '@page-objects/orderconfirmation/OrderConfirmationPage'
import {AGENT_USER, GLOBAL_PASSWORD} from '@core-libs/ConstantUtil'
import ShopPage from '@page-objects/shop/ShopPage'
import CheckoutCommonSection from '@page-objects/checkout/CheckoutCommonSection'
import allureReporter from '@wdio/allure-reporter'
import HybrisBackofficeLoginPage from '@page-objects/hybrisbackoffice/HybrisBackofficeLoginPage'
import HybrisBackofficeAutorityGroupPage from '@page-objects/hybrisbackoffice/HybrisBackofficeAutorityGroupPage'
import HybrisBackofficeHomePage from '@page-objects/hybrisbackoffice/HybrisBackofficeHomePage'
import HybrisBackofficeSearchPage from '@page-objects/hybrisbackoffice/HybrisBackofficeSearchPage'
import HybrisBackofficeOrderDetailsPage from '@page-objects/hybrisbackoffice/HybrisBackofficeOrderDetailsPage'
import OrderDetailsPage from '@page-objects/myaccount/OrderDetailsPage'

let screencapture = false // capture screenshot when test step 'passed' also by default only capture screenshot of failed steps
let homePage, header, mainNavBar, loginPage, shopAllPage, shopPage, addedToYourShoppingCartPopup, shoppingCartPage, finalReviewTile,
  checkoutCommonSection, orderConfirmationPage, orderDetailsPage

// Product
let productUnitPrice, productQty, productSubtotal, wellnessCreditBalance
// Customer Login Info
let email, password, myAccountPage, orderNumber, qualifyingVolume

// Hybris Backoffice
let agentEmail, agentPassword, hybrisBackofficeLoginPage, hybrisBackofficeAutorityGroupPage, hybrisBackofficeHomePage, hybrisBackofficeSearchPage, hybrisBackofficeOrderDetailsPage

describe('C5857 US - OPTAVIA - Create an order with Wellness Credits and verify wellness credits will not decrement against Commission Volume and Qualifying Volume', function () {
  let testTitle = this.title

  before(async function () {
    await driverutils.goToOptaviaWebSiteHomePage()
  })

  beforeEach(async function () {
    await driverutils.addAllureReport(allureReporter, __dirname, 'US Smoke', testTitle)
  })

  it('Preparing Test Data', async function () {
    let custID = '831966892'
    let TestData = await CustomerTestData.getCustomerInfo(custID)
    email = TestData.CUSTOMER_EMAIL
    password = TestData.CUSTOMER_PASSWORD
    productUnitPrice = await ProductTestData.PRODUCT_INFO(attb.UNIT_PRICE, product.bars.OPT_CRAN_NUT_BAR, market.US)
    productQty = 21
    productSubtotal = (productUnitPrice * productQty).toFixed(2).toString()
    qualifyingVolume = '441.00'
    wellnessCreditBalance = '$0.00'
  })

  it('Loads the Home Page', async function () {
    homePage = new HomePage()
    await homePage.verifyPageIsLoaded()
    header = new Header()
    await header.verifyPageIsLoaded()
    mainNavBar = new MainNavBar()
    await mainNavBar.verifyPageIsLoaded()
  })

  it('Navigate to Login page', async function () {
    await header.clickLogIn()
    loginPage = new LoginPage()
    await loginPage.verifyPageIsLoaded()
  })

  it('Login as existing Customer', async function () {
    await allureReporter.addDescription(`Login as existing Customer email: ${email} and password: ${password}`)
    await loginPage.loginAsExistingCustomer(email, password)
  })

  it('Loads My Account Page', async function () {
    myAccountPage = new MyAccountPage()
    await myAccountPage.verifyPageIsLoaded()
  })

  it('Verify Wellness credit Displayed in Account Page', async function () {
    screencapture = true
    await myAccountPage.verifyWellnessCreditTextIsDisplayed()
  })

  it('Loads the Shop All Page', async function () {
    await mainNavBar.clickShopAll()
    shopAllPage = new ShopAllPage()
    await shopAllPage.verifyPageIsLoaded()
  })

  it('Click on View All button', async function () {
    await shopAllPage.clickViewAllBtn()
  })

  it('Loads Shop Page', async function () {
    shopPage = new ShopPage()
    await shopPage.verifyPageIsLoaded()
  })

  it('Click on Add To Cart Button for Essential Creamy Double Peanut Bar', async function () {
    await shopPage.clickEssentialCreamyDoublePeanutBar()
  })

  it('Loads the Added To Your Shopping Cart Popup', async function () {
    addedToYourShoppingCartPopup = new AddedToYourShoppingCartPopup()
    await addedToYourShoppingCartPopup.verifyPageIsLoaded()
  })

  it('Update Product quantity to 21', async function () {
    await addedToYourShoppingCartPopup.enterQuantity(productQty)
    await addedToYourShoppingCartPopup.clickUpdateLink()
  })

  it('Loads the Shopping Cart Page', async function () {
    shoppingCartPage = new ShoppingCartPage()
    await shoppingCartPage.verifyPageIsLoaded()
  })

  it('Verify the Wellness Credit displayed in Order Summary Section', async function () {
    screencapture = true
    assert.isTrue(await shoppingCartPage.isWellnessCreditTextDisplayed(), 'Wellness Credit Is Not displayed in Order Summary Section')
  })

  it('Click on Proceed to Checkout button', async function () {
    await shoppingCartPage.clickProceedToCheckoutBtn()
  })

  it('Loads the Final Review page', async function () {
    finalReviewTile = new FinalReviewTile()
    await finalReviewTile.verifyPageIsLoaded()
  })

  it('Loads the Checkout Common Section page', async function () {
    checkoutCommonSection = new CheckoutCommonSection()
    await checkoutCommonSection.verifyPageIsLoaded()
  })

  it('Verify the Wellness Credit displayed in Order Summary Section', async function () {
    screencapture = true
    assert.isTrue(await checkoutCommonSection.isWellnessCreditTextDisplayed(), 'Wellness Credit Is Not displayed in Order Summary Section')
  })

  it('Click on Submit Order Button', async function () {
    await finalReviewTile.clickPlaceOrderButton()
  })

  it('Loads the Order Confirmation page', async function () {
    screencapture = true
    orderConfirmationPage = new OrderConfirmationPage()
    await orderConfirmationPage.verifyPageIsLoaded()
    let actual = await orderConfirmationPage.getOrderNumber()
    let resultArray = actual.split(' ')
    orderNumber = resultArray[4]
    await allureReporter.addDescription(`Order Number : ${orderNumber}`)
  })

  it('Verify the Wellness Credit displayed in Order Summary Section', async function () {
    screencapture = true
    assert.isTrue(await checkoutCommonSection.isWellnessCreditTextDisplayed(), 'Wellness Credit Is Not displayed in Order Summary Section')
  })

  it('Click on Continue Shopping button on Order Confirmation page', async function () {
    await orderConfirmationPage.clickContinueShopping()
  })

  it('Loads the Home page', async function () {
    await header.verifyMyAccountLinkIsLoaded()
    await mainNavBar.verifyPageIsLoaded()
  })

  it('Navigate to My Account menu', async function () {
    await header.clickMyAccount()
  })

  it('Logout Coach', async function () {
    await header.verifyLogOutIsLoaded()
    await header.clickLogOut()
    await header.verifyPageIsLoaded()
  })

  afterEach(async function () {
    await driverutils.saveScreenshots(this.currentTest.state, screencapture)
    screencapture = false
  })
})

describe('C5857 Login to Hybris Backoffice as Customer Support Admin Role to Verify CV and QV of the order', function () {
  let testTitle = this.title

  before(async function () {
    await driverutils.goToHybrisBackofficeSitePage()
  })

  beforeEach(async function () {
    await driverutils.addAllureReport(allureReporter, __dirname, 'US Smoke', testTitle)
  })

  it('Preparing Test Data', async function () {
    agentEmail = AGENT_USER
    agentPassword = GLOBAL_PASSWORD
  })

  it('Hybris Backoffice page is loaded ', async function () {
    hybrisBackofficeLoginPage = new HybrisBackofficeLoginPage()
    await hybrisBackofficeLoginPage.verifyPageIsLoaded()
  })

  it('Login into Hybris', async function () {
    await hybrisBackofficeLoginPage.loginAsCSR(agentEmail, agentPassword)
  })

  it('Backoffice Authority group page displays ', async function () {
    hybrisBackofficeAutorityGroupPage = new HybrisBackofficeAutorityGroupPage()
    await hybrisBackofficeAutorityGroupPage.verifyPageIsLoaded()
  })

  it('Click on Customer Support Admin Role radio button', async function () {
    await hybrisBackofficeAutorityGroupPage.proceedAsCSAdminRole()
    hybrisBackofficeHomePage = new HybrisBackofficeHomePage()
    await hybrisBackofficeHomePage.verifyPageIsLoaded()
  })

  it('Click on Orders link from Customer Admin section', async function () {
    await hybrisBackofficeHomePage.clickASMOrder()
  })

  it('Click on Order link from Customer Admin section', async function () {
    await hybrisBackofficeHomePage.clickOrder()
  })

  it('Search Textbox is loaded', async function () {
    hybrisBackofficeSearchPage = new HybrisBackofficeSearchPage()
    await hybrisBackofficeSearchPage.verifyPageIsLoaded()
  })

  it('Search the order number', async function () {
    await hybrisBackofficeSearchPage.searchOrder(orderNumber)
  })

  it('Click on Search result', async function () {
    await hybrisBackofficeSearchPage.clickSearchedOrder()
  })

  it('Verify Order Details body title displays', async function () {
    hybrisBackofficeOrderDetailsPage = new HybrisBackofficeOrderDetailsPage()
    await hybrisBackofficeOrderDetailsPage.verifyPageIsLoaded(orderNumber)
  })

  it('Click on Administration Report', async function () {
    await hybrisBackofficeOrderDetailsPage.clickAdminReportTab()
  })

  it('Verify the Order commissionable Volume is same as Order Sub total', async function () {
    let expected = productSubtotal
    let cvValue = await hybrisBackofficeOrderDetailsPage.getOrderCommissionalbleVolume()
    cvValue = parseFloat(cvValue)
    let actual = cvValue.toFixed(2)
    assert.strictEqual(actual, expected, `Order commissionable volume : ${expected} is not matched with Order Sub Total`)
  })

  it('Verify the Order Qualifying Volume', async function () {
    let expected = qualifyingVolume
    let qvValue = await hybrisBackofficeOrderDetailsPage.getQualifyingVolume()
    qvValue = parseFloat(qvValue)
    let actual = qvValue.toFixed(2)
    assert.strictEqual(actual, expected, `Order Qualifying volume : ${expected}is not matched`)
  })

  it('Click on Sign out button', async function () {
    await hybrisBackofficeHomePage.clickSignOut()
    await browser.pause(3000)
  })

  afterEach(async function () {
    await driverutils.saveScreenshots(this.currentTest.state, screencapture)
    screencapture = false
  })
})

describe('C5857 Login into application and Reorder the latest order', function () {
  let testTitle = this.title

  before(async function () {
    await driverutils.goToOptaviaWebSiteHomePage()
  })

  beforeEach(async function () {
    await driverutils.addAllureReport(allureReporter, __dirname, 'US Smoke', testTitle)
  })

  it('Loads the Home Page', async function () {
    homePage = new HomePage(driver)
    await homePage.verifyPageIsLoaded()
    header = new Header(driver)
    await header.verifyPageIsLoaded()
    mainNavBar = new MainNavBar(driver)
    await mainNavBar.verifyPageIsLoaded()
  })

  it('Navigate to Login page', async function () {
    await header.clickLogIn()
    loginPage = new LoginPage(driver)
    await loginPage.verifyPageIsLoaded()
  })

  it('Login as existing Customer', async function () {
    await allureReporter.addDescription(`Login as existing Customer email: ${email} and password: ${password}`)
    await loginPage.loginAsExistingCustomer(email, password)
  })

  it('Loads My Account Page', async function () {
    myAccountPage = new MyAccountPage(driver)
    await myAccountPage.verifyPageIsLoaded()
  })

  it('Verify Wellness credit Displayed Zero/no balance in Account Page', async function () {
    let expected = wellnessCreditBalance
    let actual = await myAccountPage.getWellnessCreditValue()
    assert.strictEqual(actual, expected, `Wellness credit Actual: ${actual} does not match expected: ${expected}`)
  })

  it('Verify the Latest order Number', async function () {
    screencapture = true
    await allureReporter.addDescription(`Expected last order is ${orderNumber}`)
    let actual = await myAccountPage.getLastOrderNumber()
    let expected = orderNumber
    assert.strictEqual(actual, expected, `Latest order Number Actual: ${actual} does not match expected: ${expected}`)
  })

  it('Click on Last Order Number link', async function () {
    await myAccountPage.clickLastOrderNumberLink()
  })

  it('Loads the Order Details page', async function () {
    orderDetailsPage = new OrderDetailsPage(driver)
    await orderDetailsPage.verifyPageIsLoaded()
  })

  it('Click on reorder button', async function () {
    await orderDetailsPage.clickReorderButton()
  })

  it('Loads Shopping cart page', async function () {
    shoppingCartPage = new ShoppingCartPage(driver)
    await shoppingCartPage.verifyPageIsLoaded()
  })

  it('Verify the Wellness Credit not displayed in Order Summary Section', async function () {
    screencapture = true
    assert.isFalse(await shoppingCartPage.isWellnessCreditTextDisplayed(), 'Wellness Credit Is displayed in Order Summary Section')
  })

  it('Click on Proceed to Checkout button', async function () {
    await shoppingCartPage.clickProceedToCheckoutBtn()
  })

  it('Loads the Final Review page', async function () {
    finalReviewTile = new FinalReviewTile(driver)
    await finalReviewTile.verifyPageIsLoaded()
  })

  it('Loads the Checkout Common Section page', async function () {
    checkoutCommonSection = new CheckoutCommonSection(driver)
    await checkoutCommonSection.verifyPageIsLoaded()
  })

  it('Verify the Wellness Credit not displayed in Order Summary Section', async function () {
    screencapture = true
    assert.isFalse(await checkoutCommonSection.isWellnessCreditTextDisplayed(), 'Wellness Credit Is displayed in Order Summary Section')
  })

  it('Click on Submit Order Button', async function () {
    await finalReviewTile.clickPlaceOrderButton()
  })

  it('Loads the Order Confirmation page', async function () {
    screencapture = true
    orderConfirmationPage = new OrderConfirmationPage(driver)
    await orderConfirmationPage.verifyPageIsLoaded()
    let actual = await orderConfirmationPage.getOrderNumber()
    let resultArray = actual.split(' ')
    orderNumber = resultArray[4]
    await allureReporter.addDescription(`Order Number : ${orderNumber}`)
  })

  it('Verify the Wellness Credit not displayed in Order Summary Section', async function () {
    screencapture = true
    assert.isFalse(await checkoutCommonSection.isWellnessCreditTextDisplayed(), 'Wellness Credit Is displayed in Order Summary Section')
  })

  it('Click on Continue Shopping button on Order Confirmation page', async function () {
    await orderConfirmationPage.clickContinueShopping()
  })

  it('Loads the Home page', async function () {
    await header.verifyMyAccountLinkIsLoaded()
    await mainNavBar.verifyPageIsLoaded()
  })

  it('Navigate to My Account menu', async function () {
    await header.clickMyAccount()
  })

  it('Logout Coach', async function () {
    await header.verifyLogOutIsLoaded()
    await header.clickLogOut()
    await header.verifyPageIsLoaded()
  })

  afterEach(async function () {
    await driverutils.saveScreenshots(this.currentTest.state, screencapture)
    screencapture = false
  })
})

describe('C5857 Login to Hybris Backoffice as Customer Support Admin Role to Verify CV and QV of the order', function () {
  let testTitle = this.title

  before(async function () {
    await driverutils.goToHybrisBackofficeSitePage()
  })

  beforeEach(async function () {
    await driverutils.addAllureReport(allureReporter, __dirname, 'US Smoke', testTitle)
  })

  it('Preparing Test Data', async function () {
    agentEmail = AGENT_USER
    agentPassword = GLOBAL_PASSWORD
  })

  it('Hybris Backoffice page is loaded ', async function () {
    hybrisBackofficeLoginPage = new HybrisBackofficeLoginPage()
    await hybrisBackofficeLoginPage.verifyPageIsLoaded()
  })

  it('Login into BackOffice', async function () {
    await hybrisBackofficeLoginPage.loginAsCSR(agentEmail, agentPassword)
  })

  it('Backoffice Authority group page displays ', async function () {
    hybrisBackofficeAutorityGroupPage = new HybrisBackofficeAutorityGroupPage()
    await hybrisBackofficeAutorityGroupPage.verifyPageIsLoaded()
  })

  it('Click on Customer Support Admin Role radio button', async function () {
    await hybrisBackofficeAutorityGroupPage.proceedAsCSAdminRole()
    hybrisBackofficeHomePage = new HybrisBackofficeHomePage()
    await hybrisBackofficeHomePage.verifyPageIsLoaded()
  })

  it('Click on Orders link from Customer Admin section', async function () {
    await hybrisBackofficeHomePage.clickASMOrder()
  })

  it('Click on Order link from Customer Admin section', async function () {
    await hybrisBackofficeHomePage.clickOrder()
  })

  it('Search Textbox is loaded', async function () {
    hybrisBackofficeSearchPage = new HybrisBackofficeSearchPage()
    await hybrisBackofficeSearchPage.verifyPageIsLoaded()
    await browser.pause(60000)
  })

  it('Search the order number', async function () {
    await hybrisBackofficeSearchPage.searchOrder(orderNumber)
  })

  it('Click on Search result', async function () {
    await hybrisBackofficeSearchPage.clickSearchedOrder()
  })

  it('Verify Order Details body title displays', async function () {
    hybrisBackofficeOrderDetailsPage = new HybrisBackofficeOrderDetailsPage()
    await hybrisBackofficeOrderDetailsPage.verifyPageIsLoaded(orderNumber)
  })

  it('Click on Administration Report', async function () {
    await hybrisBackofficeOrderDetailsPage.clickAdminReportTab()
  })

  it('Verify the Order commissionable Volume is same as Order Sub total', async function () {
    let expected = productSubtotal
    let cvValue = await hybrisBackofficeOrderDetailsPage.getOrderCommissionalbleVolume()
    cvValue = parseFloat(cvValue)
    let actual = cvValue.toFixed(2)
    assert.strictEqual(actual, expected, `Order commissionable volume : ${expected} is not matched with Order Sub Total`)
  })

  it('Verify the Order Qualifying Volume', async function () {
    let expected = qualifyingVolume
    let qvValue = await hybrisBackofficeOrderDetailsPage.getQualifyingVolume()
    qvValue = parseFloat(qvValue)
    let actual = qvValue.toFixed(2)
    assert.strictEqual(actual, expected, `Order Qualifying volume : ${expected} is not matched`)
  })

  it('Click on Sign out button', async function () {
    await hybrisBackofficeHomePage.clickSignOut()
  })

  afterEach(async function () {
    await driverutils.saveScreenshots(this.currentTest.state, screencapture)
    screencapture = false
  })
})
