// @flow
import { assert } from 'chai'
import driverutils from '@core-libs/DriverUtils'
import CustomerTestData from '@input-data/customers/CustomerTestData'
import CoachTestData from '@input-data/coaches/CoachTestData'
import HomePage from '@page-objects/home/HomePage'
import MainNavBar from '@page-objects/common/MainNavBar'
import LoginPage from '@page-objects/loginPage/LoginPage'
import MyAccountPage from '@page-objects/myaccount/MyAccountPage'
import allureReporter from '@wdio/allure-reporter'
import Header from '@page-objects/common/Header'
import PopupMarketSwitch from '@page-objects/popupmarketswitch/PopupMarketSwitch'

describe('US - OPTAVIA - Market Switch test', function () {
  declare var allure: any;

  let screencapture = false // capture screenshot when test step 'passed' also by default only capture screenshot of failed steps
  let homePage, header, mainNavBar, loginPage
  let myAccountPage, popupMarketSwitch

  // Customer Registration
  let email, password
  // Customer Information
  let firstName, lastName, clientID, coachFirstName, coachLastName, coachID, coachName

  let testTitle = this.title

  before(async function () {
    await driverutils.goToOptaviaWebSiteHomePage()
  })

  beforeEach(async function () {
    await driverutils.addAllureReport(allureReporter, __dirname, 'US Smoke', testTitle)
  })

  it('Preparing Test Data', async function () {
    let custID = '6520000402035'
    let TestData = await CustomerTestData.getCustomerInfo(custID)
    // Customer Information
    clientID = `Client ID #${TestData.CUSTOMER_ID}`
    firstName = TestData.CUSTOMER_FIRST_NAME
    lastName = TestData.CUSTOMER_LAST_NAME
    email = TestData.CUSTOMER_EMAIL
    password = TestData.CUSTOMER_PASSWORD
    // Coach Information
    coachID = await CoachTestData.COACH_ID(5, 2)
    coachFirstName = await CoachTestData.COACH_FIRST_NAME(5, 2)
    coachLastName = await CoachTestData.COACH_LAST_NAME(5, 2)
    coachName = `${coachFirstName} ${coachLastName}`
  })

  it('Loads the Home Page', async function () {
    screencapture = true
    homePage = new HomePage()
    await homePage.verifyPageIsLoaded()
    header = new Header()
    await header.verifyPageIsLoaded()
    mainNavBar = new MainNavBar()
    await mainNavBar.verifyPageIsLoaded()
    await header.verifyFlagIsLoaded()
  })

  it('Navigate to Login and Register Page', async function () {
    screencapture = true
    await header.clickLogIn()
    loginPage = new LoginPage()
    await loginPage.verifyPageIsLoaded()
  })

  it('Login as existing Singapore Coach', async function () {
    screencapture = true
    await allureReporter.addDescription(`Login as existing Singapore coach email: ${email} and password: ${password}`)
    await loginPage.loginAsExistingCustomer(email, password)
  })

  it('Verify market switch popup displays Page', async function () {
    screencapture = true
    popupMarketSwitch = new PopupMarketSwitch()
    await popupMarketSwitch.verifyPageIsLoaded()
  })

  it('Verify market switch popup body title displays', async function () {
    let expected = 'YOUR OPTAVIA SHOPPING EXPERIENCE!'
    let actual = await popupMarketSwitch.getPopupBodyTitle()
    assert.strictEqual(actual, expected, `${expected} Is not Displayed`)
  })

  it('Click on Return to my homepage button', async function () {
    await popupMarketSwitch.clickReturnToMyHomepageButton()
  })

  it('Verify My Account page is loaded', async function () {
    screencapture = true
    myAccountPage = new MyAccountPage()
    await myAccountPage.verifyPageIsLoaded()
  })

  it('Verify Singapore flag is displayed', async function () {
    await header.verifySGFlagIsLoaded()
  })

  it('Verify Coach Information', async function () {
    let expected = `${coachName}, ${coachID}`
    let actual = await header.getCoachInformation()
    assert.strictEqual(actual, expected, `${expected} Coach Info Is not Displayed`)
  })

  it('Verify Welcome message', async function () {
    let expected = 'Welcome Back,'
    let actual = await myAccountPage.getWelcomeMessage()
    assert.strictEqual(actual, expected, `${expected} message Is not Displayed`)
  })

  it('Verify Client Name', async function () {
    let expected = `${firstName} ${lastName}`
    let actual = await myAccountPage.getAccountName()
    assert.strictEqual(actual, expected, `${expected} Client Name Is not Displayed`)
  })

  it('Verify Client ID', async function () {
    let expected = clientID
    let actual = await myAccountPage.getAccountID()
    assert.strictEqual(actual, expected, `${expected} Client ID Is not Displayed`)
  })

  afterEach(async function () {
    await driverutils.saveScreenshots(allureReporter, this.currentTest.state, screencapture)
    screencapture = false
  })
})
