// @flow
import { assert } from 'chai'
import driverutils from '@core-libs/DriverUtils'
import allureReporter from '@wdio/allure-reporter'
import CustomerTestData from '@input-data/customers/CustomerTestData'
import CoachTestData from '@input-data/coaches/CoachTestData'
import ProductTestData, {attb, market, product} from '@input-data/products/ProductTestData'
import HomePage from '@page-objects/home/HomePage'
import Header from '@page-objects/common/Header'
import MainNavBar from '@page-objects/common/MainNavBar'
import ChooseYourOwnFuelingsPopup from '@page-objects/chooseyourownfuelings/ChooseYourOwnFuelingsPopup'
import ShoppingCartPage from '@page-objects/shoppingcart/ShoppingCartPage'
import LoginPage from '@page-objects/loginPage/LoginPage'
import MyAccountPage from '@page-objects/myaccount/MyAccountPage'
import JoinUsPage from '@page-objects/joinus/JoinUsPage'
import ShopCoachStorePage from '@page-objects/shop/ShopCoachStorePage'
import ProductDetailsPage from '@page-objects/shop/ProductDetailsPage'

describe('US OPTAVIA - Adding Product and Logging as existing coach', function () {
  declare var allure: any;

  let screencapture = false // capture screenshot when test step 'passed' also by default only capture screenshot of failed steps
  let homePage, header, mainNavBar, loginPage
  let addItemsToCartPopup, shoppingCartPage, myAccountPage, joinUsPage, shopCoachStorePage, productDetailsPage

  let searchProduct, searchProductSKU
  // Customer Registration
  let email, password
  // Customer Information
  let firstName, lastName, clientID, coachFirstName, coachLastName, coachName, coachID, renewalProduct

  let testTitle = this.title

  before(async function () {
    await driverutils.goToOptaviaWebSiteHomePage()
  })

  beforeEach(async function () {
    await driverutils.addAllureReport(allureReporter, __dirname, 'US Smoke', testTitle)
  })

  it('Preparing Test Data', async function () {
    let custID = '879678720'
    let TestData = await CustomerTestData.getCustomerInfo(custID)

    let coachRow = 3
    email = TestData.CUSTOMER_EMAIL
    password = TestData.CUSTOMER_PASSWORD
    firstName = TestData.CUSTOMER_FIRST_NAME
    lastName = TestData.CUSTOMER_LAST_NAME
    clientID = `Client ID #${TestData.CUSTOMER_ID}`
    coachID = await CoachTestData.COACH_ID(coachRow)
    coachFirstName = await CoachTestData.COACH_FIRST_NAME(coachRow)
    coachLastName = await CoachTestData.COACH_LAST_NAME(coachRow)
    coachName = `${coachFirstName} ${coachLastName}`
    searchProductSKU = await ProductTestData.PRODUCT_INFO(attb.SKU, product.bars.OPT_CHOC_MINT_CRISP_BAR, market.US)
    searchProduct = await ProductTestData.PRODUCT_INFO(attb.NAME, product.bars.OPT_CHOC_MINT_CRISP_BAR, market.US)
    renewalProduct = await ProductTestData.PRODUCT_INFO(attb.NAME, product.renewal_certification.OPT_BUSINESS_RENEWAL, market.US)
  })

  it('Loads the Home Page', async function () {
    screencapture = true
    homePage = new HomePage()
    await homePage.verifyPageIsLoaded()
    header = new Header()
    await header.verifyPageIsLoaded()
    mainNavBar = new MainNavBar()
    await mainNavBar.verifyPageIsLoaded()
  })

  it('Navigate to Choose Your Own Fuelings', async function () {
    screencapture = true
    await mainNavBar.clickChooseYourOwnFuelings()
    addItemsToCartPopup = new ChooseYourOwnFuelingsPopup()
  })

  it('Search and select searched product', async function () {
    screencapture = true
    await allureReporter.addDescription(`Searched product is ${searchProduct}`)
    await addItemsToCartPopup.searchForProduct(searchProductSKU)
    await addItemsToCartPopup.selectProduct()
  })

  it('Add searched product to Shopping Cart', async function () {
    await addItemsToCartPopup.clickAddItemsToCart()
    shoppingCartPage = new ShoppingCartPage()
    await shoppingCartPage.verifyPageIsLoaded()
  })

  it('Verify added product is in the cart', async function () {
    screencapture = true
    await allureReporter.addDescription(`Added product is ${searchProduct}`)
    let expected = `${searchProduct} (Box)`
    let actual = await shoppingCartPage.getProductNameText()
    assert.strictEqual(actual, expected, `${expected} Product Is not Displayed`)
  })

  it('Remove added product from the cart', async function () {
    screencapture = true
    await shoppingCartPage.clickRemoveProductIcon()
    assert.isTrue(await shoppingCartPage.isLoadedProductRemovedMessage(), 'Product is not removed')
  })

  it('Navigate to Login and Register Page', async function () {
    screencapture = true
    await header.clickLogIn()
    loginPage = new LoginPage()
    await loginPage.verifyPageIsLoaded()
  })

  it('Login as existing Coach', async function () {
    screencapture = true
    await allureReporter.addDescription(`Login as existing Customer email: ${email} and password: ${password}`)
    await loginPage.loginAsExistingCustomer(email, password)
  })

  it('Verify My Account page is loaded', async function () {
    screencapture = true
    myAccountPage = new MyAccountPage()
    await myAccountPage.verifyPageIsLoaded()
  })

  it('Verify Coach Information', async function () {
    header = new Header()
    await header.verifyPageIsLoaded()
    let expected = `${coachName}, ${coachID}`
    let actual = await header.getCoachInformation()
    assert.strictEqual(actual, expected, `${expected} Coach Info Is not Displayed`)
  })

  it('Verify Welcome message', async function () {
    let expected = 'Welcome Back,'
    let actual = await myAccountPage.getWelcomeMessage()
    assert.strictEqual(actual, expected, `${expected} message Is not Displayed`)
  })

  it('Verify Client Name', async function () {
    let expected = `${firstName} ${lastName}`
    let actual = await myAccountPage.getAccountName()
    assert.strictEqual(actual, expected, `${expected} Client Name Is not Displayed`)
  })

  it('Verify Client ID', async function () {
    let expected = clientID
    let actual = await myAccountPage.getAccountID()
    assert.strictEqual(actual, expected, `${expected} Client ID Is not Displayed`)
  })

  it('Navigate to Join Us page', async function () {
    await mainNavBar.clickJoinUs()
    joinUsPage = new JoinUsPage()
    await joinUsPage.verifyPageIsLoaded()
  })

  it('Verify Renew License button is enabled', async function () {
    screencapture = true
    let actual = await joinUsPage.getRenewLicenseBtnAttributeValue()
    assert.notInclude(actual, 'disabled', 'Renew License button is disabled')
  })

  it('Verify Visit Coach Store button is enabled', async function () {
    screencapture = true
    let actual = await joinUsPage.getVisitCoachStoreBtnAttributeValue()
    assert.notInclude(actual, 'disabled', 'Visit Coach Store button is disabled')
  })

  it('Click on Visit Coach Store button', async function () {
    await joinUsPage.clickVisitCoachStoreBtn()
  })

  it('Loads the Coach Store Page', async function () {
    shopCoachStorePage = new ShopCoachStorePage()
    await shopCoachStorePage.verifyPageIsLoaded()
  })

  it('Navigate to Join Us page', async function () {
    await mainNavBar.clickJoinUs()
    await joinUsPage.verifyPageIsLoaded()
  })

  it('Click on Renew License button', async function () {
    await joinUsPage.clickRenewLicenseBtn()
  })

  it('Navigate to 12-Month Business Renewal page', async function () {
    await browser.pause(10000)
    productDetailsPage = new ProductDetailsPage()
    await productDetailsPage.verifyPageIsLoaded()
    let expected = renewalProduct
    let actual = await productDetailsPage.getProductNameText()
    assert.strictEqual(actual, expected, `${expected} Is not Displayed`)
  })

  it('Logout Coach', async function () {
    screencapture = true
    await header.verifyLogOutIsLoaded()
    await header.clickLogOut()
    await homePage.verifyPageIsLoaded()
  })

  afterEach(async function () {
    await driverutils.saveScreenshots(allureReporter, this.currentTest.state, screencapture)
    screencapture = false
  })
})
