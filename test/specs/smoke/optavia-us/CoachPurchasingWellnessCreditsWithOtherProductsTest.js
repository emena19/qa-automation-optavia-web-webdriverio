// @flow
import {assert} from 'chai'
import driverutils from '@core-libs/DriverUtils'
import CustomerTestData from '@input-data/customers/CustomerTestData'
import ProductTestData, {attb, product} from '@input-data/products/ProductTestData'
import HomePage from '@page-objects/home/HomePage'
import Header from '@page-objects/common/Header'
import MainNavBar from '@page-objects/common/MainNavBar'
import LoginPage from '@page-objects/loginPage/LoginPage'
import MyAccountPage from '@page-objects/myaccount/MyAccountPage'
import CoachStorePage from '@page-objects/coachstore/CoachStorePage'
import WellnessCreditPage from '@page-objects/coachstore/WellnessCreditPage'
import ShoppingCartPage from '@page-objects/shoppingcart/ShoppingCartPage'
import ChooseYourOwnFuelingsPopup from '@page-objects/chooseyourownfuelings/ChooseYourOwnFuelingsPopup'
import FinalReviewTile from '@page-objects/checkout/FinalReviewTile'
import OrderConfirmationPage from '@page-objects/orderconfirmation/OrderConfirmationPage'
import AddedToYourShoppingCartPopup from '@page-objects/shop/AddedToYourShoppingCartPopup'
import allureReporter from '@wdio/allure-reporter'
import WCTestData from '@input-data/various/MiscTestData'
// Uncomment for Fraud check
/* import {GLOBAL_PASSWORD} from '../../../src/lib/ConstantUtil'
import HybrisBackofficeLoginPage from '../../../src/pageobjects/hybrisbackoffice/HybrisBackofficeLoginPage'
import HybrisBackofficeHomePage from '../../../src/pageobjects/hybrisbackoffice/HybrisBackofficeHomePage'
import HybrisBackofficeAutorityGroupPage
  from '../../../src/pageobjects/hybrisbackoffice/HybrisBackofficeAutorityGroupPage'
import HybrisBackofficeSearchPage from '../../../src/pageobjects/hybrisbackoffice/HybrisBackofficeSearchPage'
import HybrisBackofficeOrderDetailsPage from '../../../src/pageobjects/hybrisbackoffice/HybrisBackofficeOrderDetailsPage' */

declare var allure: any;

let screencapture = false // capture screenshot when test step 'passed' also by default only capture screenshot of failed steps
let homePage, header, mainNavBar, loginPage

// Customer Login Info
let email, password, myAccountPage
// Coach Store
let coachStorePage, wellnessCreditPage, clientid, addedToYourShoppingCartPopup, wellnessCreditBalance
// Choose Your Own Fuelings Page

let consumableProduct, nonConsumableProduct, addItemsToCartPopup
// Shopping Cart

let shoppingCartPage

// Order Confirmation Page
let orderConfirmationPage, finalReviewPage, orderNumber

/*
// Hybris Backoffice
let agentEmail, agentPassword, hybrisBackofficeLoginPage, hybrisBackofficeAutorityGroupPage, hybrisBackofficeHomePage, hybrisBackofficeSearchPage, hybrisBackofficeOrderDetailsPage
*/

describe('C2264 - US OPTAVIA - Coach Store - Wellness Credits - Coach Purchasing Wellness Credits With Other Products Test', function () {
  let testTitle = this.title

  before(async function () {
    await driverutils.goToOptaviaWebSiteHomePage()
  })

  beforeEach(async function () {
    await driverutils.addAllureReport(allureReporter, __dirname, 'US Smoke', testTitle)
  })

  it('Preparing Test Data', async function () {
    let custID = '779341462'
    let TestData = await CustomerTestData.getCustomerInfo(custID)
    email = TestData.CUSTOMER_EMAIL
    password = TestData.CUSTOMER_PASSWORD
    let wcRow = 6
    clientid = await WCTestData.CLIENT_ID(wcRow)
    wellnessCreditBalance = await WCTestData.WELLNESS_CREDIT_QTY(wcRow)
    consumableProduct = await ProductTestData.PRODUCT_INFO(attb.SKU, product.shakes_smoothies.OPT_CHOC_SHAKE)
    nonConsumableProduct = await ProductTestData.PRODUCT_INFO(attb.SKU, product.tools_accessories.OPT_BLENDER_BOTTLE)
  })

  it('Loads the Home Page', async function () {
    homePage = new HomePage()
    await homePage.verifyPageIsLoaded()
    header = new Header()
    await header.verifyPageIsLoaded()
    mainNavBar = new MainNavBar()
    await mainNavBar.verifyPageIsLoaded()
  })

  it('Navigate to Login page', async function () {
    await header.clickLogIn()
    loginPage = new LoginPage()
    await loginPage.verifyPageIsLoaded()
  })

  it('Login as existing Coach', async function () {
    await allureReporter.addDescription(`Login as existing Coach email: ${email} and password: ${password}`)
    await loginPage.loginAsExistingCustomer(email, password)
  })

  it('Loads the My Account Page', async function () {
    myAccountPage = new MyAccountPage()
    await myAccountPage.verifyPageIsLoaded()
  })

  it('Select Choose Health Coach Store from Shop', async function () {
    await mainNavBar.clickHealthCoachStore()
  })

  it('Loads the Coach Store Page', async function () {
    coachStorePage = new CoachStorePage()
    await coachStorePage.verifyPageIsLoaded()
  })

  it('Click on Wellness Credit Link', async function () {
    await coachStorePage.clickWellnessCreditLink()
  })

  it('Loads the Wellness Credit Page', async function () {
    wellnessCreditPage = new WellnessCreditPage()
    await wellnessCreditPage.verifyPageIsLoaded()
  })

  it('Click on View Details Button', async function () {
    await wellnessCreditPage.clickWellnessCreditViewDetailsButton()
  })

  it('Verify Add to Cart button is disabled', async function () {
    await wellnessCreditPage.clickWellnessCreditAddToCartButton()
    let actual = await wellnessCreditPage.isAddToCartBtnEnabled()
    assert.isFalse(actual, 'Add to cart button is not disabled')
  })

  it('Enter Client ID', async function () {
    await wellnessCreditPage.sendWellnessCreditClientID(clientid)
  })

  it('Enter Wellnellness Credit Quantity', async function () {
    await wellnessCreditPage.sendWellnessCreditQty(wellnessCreditBalance)
  })

  it('Click on Wellness Credit Add to cart Button', async function () {
    await wellnessCreditPage.clickWellnessCreditAddToCartButton()
  })
  it('Loads the Shopping Cart PopUp ', async function () {
    addedToYourShoppingCartPopup = new AddedToYourShoppingCartPopup()
    await addedToYourShoppingCartPopup.verifyPageIsLoaded()
  })
  it('Click on Checkout button on pop up window', async function () {
    await addedToYourShoppingCartPopup.clickCheckoutButton()
  })

  it('Loads the Shopping Cart Page', async function () {
    shoppingCartPage = new ShoppingCartPage()
    await shoppingCartPage.verifyPageIsLoaded()
  })

  it('Navigate to Choose Your Own Fuelings from Shop', async function () {
    await mainNavBar.clickChooseYourOwnFuelings()
    addItemsToCartPopup = new ChooseYourOwnFuelingsPopup()
    await addItemsToCartPopup.verifyPageIsLoaded()
  })

  it('Search and add Consumable product', async function () {
    await allureReporter.addDescription(`Search consumable product SKU = ${consumableProduct}`)
    await addItemsToCartPopup.searchForProduct(consumableProduct)
    await addItemsToCartPopup.selectProduct()
  })

  it('Search and add NonConsumable product', async function () {
    await allureReporter.addDescription(`Search nonconsumable product SKU = ${nonConsumableProduct}`)
    await addItemsToCartPopup.searchForProduct(nonConsumableProduct)
    await addItemsToCartPopup.selectProduct()
    await addItemsToCartPopup.clickAddItemsToCart()
  })

  it('Loads the Shopping Cart Page', async function () {
    shoppingCartPage = new ShoppingCartPage()
    await shoppingCartPage.verifyPageIsLoaded()
  })

  it('CLick on CheckOut Button', async function () {
    await shoppingCartPage.clickProceedToCheckoutBtn()
  })
  it('Loads the Final Review Page', async function () {
    finalReviewPage = new FinalReviewTile()
    await finalReviewPage.verifyPageIsLoaded()
  })

  it('Click on Submit Order', async function () {
    await finalReviewPage.clickPlaceOrderButton()
  })

  it('Loads the Order Confirmation Page', async function () {
    orderConfirmationPage = new OrderConfirmationPage()
    await orderConfirmationPage.verifyPageIsLoaded()
  })

  it('Verify Order Total in Confirmation Page', async function () {
    screencapture = true
    orderNumber = await orderConfirmationPage.getOrderNumber()
    let resultArray = orderNumber.split(' ')
    orderNumber = await resultArray[4]
  })

  it('Click on Continue shopping Button', async function () {
    await orderConfirmationPage.clickContinueShopping()
  })
  it('Loads the Home Page', async function () {
    homePage = new HomePage()
    await homePage.verifyPageIsLoaded()
  })

  it('Click on Logout', async function () {
    await header.clickLogOut()
  })

  afterEach(async function () {
    await driverutils.saveScreenshots(this.currentTest.state, screencapture)
    screencapture = false
  })
})

/* describe('Login to Hybris Backoffice as CSR to approve the order', function () {
  let testTitle = this.title

before(async function () {
    driverBuilder = new DriverBuilder()
    driver = driverBuilder.driver
    await driverutils.goToHybrisBackofficeSitePage(driver)
  })

  beforeEach(async function () {
  await driverutils.addAllureReport(__dirname, 'US Smoke', testTitle)
  })

  it('Preparing Test Data', async function () {
    agentEmail = AGENT_USER
    agentPassword = GLOBAL_PASSWORD
  })

  it('Hybris Backoffice page is loaded ', async function () {
    hybrisBackofficeLoginPage = new HybrisBackofficeLoginPage(driver)
    await hybrisBackofficeLoginPage.verifyPageIsLoaded()
  })

  it('Login into Hybris', async function () {
    await hybrisBackofficeLoginPage.loginAsCSR(agentEmail, agentPassword)
  })

  it('Backoffice Authority group page displays ', async function () {
    hybrisBackofficeAutorityGroupPage = new HybrisBackofficeAutorityGroupPage(driver)
    await hybrisBackofficeAutorityGroupPage.verifyPageIsLoaded()
  })

  it('Click on Customer Support Agent Role radio button', async function () {
    await hybrisBackofficeAutorityGroupPage.proceedAsCSAgentRole()
  })

  it('Verify BackOffice Home page is loaded', async function () {
    hybrisBackofficeHomePage = new HybrisBackofficeHomePage(driver)
    await hybrisBackofficeHomePage.verifyCSIsLoaded()
  })

  it('Click on Session Context collapse arrow', async function () {
    await hybrisBackofficeHomePage.clickSessionContextCollapse()
  })

  it('Click on Orders link from Customer Support section', async function () {
    await hybrisBackofficeHomePage.clickOrder()
  })

  it('Search Textbox is loaded', async function () {
    hybrisBackofficeSearchPage = new HybrisBackofficeSearchPage(driver)
    hybrisBackofficeSearchPage.verifyPageIsLoaded()
  })

  it('Search the wellness credit order number', async function () {
    await hybrisBackofficeSearchPage.searchOrder(orderNumber)
  })

  it('Click on Search result', async function () {
    await hybrisBackofficeSearchPage.clickSearchedOrder()
  })

  it('Verify Order Details body title displays', async function () {
    hybrisBackofficeOrderDetailsPage = new HybrisBackofficeOrderDetailsPage(driver)
    await hybrisBackofficeOrderDetailsPage.verifyPageIsLoaded(orderNumber)
  })

  it('Click on Fraud Report', async function () {
    await hybrisBackofficeOrderDetailsPage.clickFraudReportTab()
  })

  it('Click on Fraud Approved button', async function () {
    await hybrisBackofficeOrderDetailsPage.clickFraudActionButton()
    await driver.sleep(6000)
  })

  it('Click on Sign out button', async function () {
    await driver.sleep(6000)
    await hybrisBackofficeHomePage.clickSignOut()
  })

afterEach(async function () {
  await driverutils.saveScreenshots(this.currentTest.state, screencapture, driver)
  screencapture = false
})

  after(async function () {
    await driverBuilder.quit()
  })
}) */
