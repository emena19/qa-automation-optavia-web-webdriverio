// @flow
import { assert } from 'chai'
import driverutils from '@core-libs/DriverUtils'
import allureReporter from '@wdio/allure-reporter'
import Header from '@page-objects/common/Header'
import HomePage from '@page-objects/home/HomePage'
import CustomerTestData from '@input-data/customers/CustomerTestData'
import LoginPage from '@page-objects/loginPage/LoginPage'
import CreateAccountPage from '@page-objects/createAccountPage/CreateAccountPage'
import {GLOBAL_PASSWORD} from '@core-libs/ConstantUtil'
import FindACoachPage from '@page-objects/findacoach/FindACoachPage'

describe('SG Account Registration for Singapore Market Test', function () {
  // declare var allure: any;

  let screencapture = false // capture screenshot when test step 'passed' also by default only capture screenshot of failed steps
  let homePage, header, loginPage, findACoachPage, createAccountPage

  // Customer Registration
  let customerInfo, TestData

  let testTitle = this.title

  before(async function () {
    await driverutils.goToOptaviaWebSiteHomePageSG()
  })

  beforeEach(async function () {
    await driverutils.addAllureReport(allureReporter, __dirname, 'SG Smoke', testTitle)
  })

  it('Preparing Test Data', async function () {
    customerInfo = await CustomerTestData.getNewCustomer('SG')
    TestData = await CustomerTestData.getCustomerInfo('6520000402035')
  })

  it('Loads the Home Page', async function () {
    screencapture = true
    homePage = new HomePage()
    await homePage.verifyPageIsLoaded()
    header = new Header()
    await header.verifyPageIsLoaded()
  })

  it('Navigate to Login and Register Page', async function () {
    screencapture = true
    await header.clickLogIn()
    loginPage = new LoginPage()
    await loginPage.verifyPageIsLoaded()
    await loginPage.clickCreateAccountLink()
  })

  it('Loads the Create Account page', async function () {
    createAccountPage = new CreateAccountPage()
    await createAccountPage.verifyPageIsLoaded()
  })

  it('Create an Account', async function () {
    screencapture = true
    await allureReporter.addDescription(`Create an Account Full name: ${customerInfo.CUSTOMER_FIRST_NAME} ${customerInfo.CUSTOMER_LAST_NAME}, email: ${customerInfo.CUSTOMER_EMAIL}, password: ${GLOBAL_PASSWORD}`)
    await createAccountPage.createAnAccount(`${customerInfo.CUSTOMER_FIRST_NAME} ${customerInfo.CUSTOMER_LAST_NAME}`, customerInfo.CUSTOMER_EMAIL, GLOBAL_PASSWORD, TestData.SHIPPING_PHONE)
  })

  it('Loads the Find a Coach page', async function () {
    findACoachPage = new FindACoachPage()
    await findACoachPage.verifyPageIsLoaded()
    let expected = 'Thank you for registering.'
    let actual = await findACoachPage.getGlobalMessage()
    assert.strictEqual(actual, expected, `${expected} Message is not displayed`)
  })

  afterEach(async function () {
    await driverutils.saveScreenshots(this.currentTest.state, screencapture)
    screencapture = false
  })
})
