// @flow
import { assert } from 'chai'
import driverutils from '@core-libs/DriverUtils'
import CustomerTestData from '@input-data/customers/CustomerTestData'
import ProductTestData, {attb, market, product} from '@input-data/products/ProductTestData'
import CoachTestData from '@input-data/coaches/CoachTestData'
import TestCreditCards from '@input-data/creditcards/CreditCardTestData'
import HomePage from '@page-objects/home/HomePage'
import Header from '@page-objects/common/Header'
import MainNavBar from '@page-objects/common/MainNavBar'
import ShopAllPage from '@page-objects/shop/ShopAllPage'
import ChooseYourOwnFuelingsPopup from '@page-objects/chooseyourownfuelings/ChooseYourOwnFuelingsPopup'
import ShoppingCartPage from '@page-objects/shoppingcart/ShoppingCartPage'
import FindACoachPage from '@page-objects/findacoach/FindACoachPage'
import CreateAccountPage from '@page-objects/createAccountPage/CreateAccountPage'
import {GLOBAL_PASSWORD} from '@core-libs/ConstantUtil'
import PopupSuggestedDeliveryAddressesForm
  from '@page-objects/popupsuggesteddeliveryaddressesform/PopupSuggestedDeliveryAddressesForm'
import PaymentAndBillingAddressTile from '@page-objects/checkout/PaymentAndBillingAddressTile'
import FinalReviewTile from '@page-objects/checkout/FinalReviewTile'
import OrderConfirmationPage from '@page-objects/orderconfirmation/OrderConfirmationPage'
import allureReporter from '@wdio/allure-reporter'
import CheckoutPage from '@page-objects/checkout/CheckoutPage'
import ShippingAddressTile from '@page-objects/checkout/ShippingAddressTile'
import SearchCoachResultsPage from '@page-objects/searchcoachresults/SearchCoachResultsPage'

describe('HK OPTAVIA-Add an OnDemand Order', function () {
  // declare var allure: any;

  let screencapture = false // capture screenshot when test step 'passed' also by default only capture screenshot of failed steps
  let homePage, header, createAccountPage, findACoachPage, searchCoachResultsPage, mainNavBar, shopAllPage, popupSuggestedDeliveryAddressesForm
  let shoppingCartPage, checkoutPage, shippingAddressTile, paymentAndBillingAddressTile, finalReviewTile, orderConfirmationPage, searchProduct

  // Customer Registration
  let TestData, customerInfo

  // Coach (Enroller) Info
  let coachFirstName, coachLastName, coachName, coachID

  // Product search
  let addItemsToCartPopup

  // Payment Details
  let nameOnCard, cardNumber, cvv, expdt, month, year

  let testTitle = this.title

  before(async function () {
    await driverutils.goToOptaviaWebSiteHomePageHK()
  })

  beforeEach(async function () {
    await driverutils.addAllureReport(allureReporter, __dirname, 'HK Smoke', testTitle)
  })

  it('Preparing Test Data', async function () {
    TestData = await CustomerTestData.getCustomerInfo('90000000018')
    customerInfo = await CustomerTestData.getNewCustomer()
    let coachRow = 2
    searchProduct = await ProductTestData.PRODUCT_INFO(attb.NAME, product.bars.OPT_CHOC_CRISP_BAR, market.HK)
    // Coach Information
    coachFirstName = await CoachTestData.COACH_FIRST_NAME(coachRow, 3)
    coachLastName = await CoachTestData.COACH_LAST_NAME(coachRow, 3)
    coachName = `${coachFirstName} ${coachLastName}`
    coachID = await CoachTestData.COACH_ID(coachRow, 3)

    // Payment Information (for VISA = B, Mastercard = C, American Express = D and Discover = E) column of sheet 2 in excel file
    let ccCol = 'B'
    let sheet = 2
    nameOnCard = customerInfo.CUSTOMER_FIRST_NAME + ' ' + customerInfo.CUSTOMER_LAST_NAME
    cardNumber = await TestCreditCards.CARD_NUMBER(sheet, ccCol)
    cvv = await TestCreditCards.CVV(sheet, ccCol)
    month = await TestCreditCards.EXPIRATION_MONTH(sheet, ccCol)
    year = await TestCreditCards.EXPIRATION_YEAR(sheet, ccCol)
    expdt = `${month}/${year}`
  })

  it('Loads the Home Page', async function () {
    homePage = new HomePage()
    await homePage.verifyPageIsLoaded()
    header = new Header()
    await header.verifyPageIsLoaded()
  })

  it('Loads the Shop All Page', async function () {
    mainNavBar = new MainNavBar()
    await mainNavBar.clickShopAll()
    shopAllPage = new ShopAllPage()
    await shopAllPage.verifyPageIsLoaded()
  })

  it('Navigate to Choose Your Own Fuelings from Shop', async function () {
    await mainNavBar.clickChooseYourOwnFuelings()
    addItemsToCartPopup = new ChooseYourOwnFuelingsPopup()
    await addItemsToCartPopup.verifyPageIsLoaded()
  })

  it('Search and add searched product', async function () {
    await allureReporter.addDescription(`Search product SKU = ${searchProduct}`)
    await addItemsToCartPopup.searchForProduct(searchProduct)
    await addItemsToCartPopup.selectProduct()
    await addItemsToCartPopup.verifyPageIsLoaded()
    await addItemsToCartPopup.clickAddItemsToCart()
  })

  it('Loads the Shopping Cart Page', async function () {
    shoppingCartPage = new ShoppingCartPage()
    await shoppingCartPage.verifyPageIsLoaded()
  })

  it('Click on Proceed to Checkout button', async function () {
    await shoppingCartPage.clickProceedToCheckoutBtn()
  })

  it('Loads the Find a Coach page', async function () {
    findACoachPage = new FindACoachPage()
    await findACoachPage.verifyPageIsLoaded()
  })

  it('Search a coach', async function () {
    await allureReporter.addDescription(`Search a coach ${coachFirstName} ${coachLastName}`)
    await findACoachPage.enterFirstAndLastNameToSearchCoach(coachFirstName, coachLastName)
    await findACoachPage.clickSearchCoachFindACoachButton()
  })

  it('Loads the Search Result page', async function () {
    searchCoachResultsPage = new SearchCoachResultsPage()
    await searchCoachResultsPage.verifyPageIsLoaded()
  })

  it('Verify searched coach is in search result list', async function () {
    await allureReporter.addDescription(`Verify ${coachFirstName} ${coachLastName} is in search result list`)
    let expected = `${coachFirstName} ${coachLastName}`
    let actual = await searchCoachResultsPage.getCoachName()
    assert.strictEqual(actual, expected, `${expected} Coach Info is not displayed in Search result Page`)
  })

  it('Click on Connect Me next to searched coach', async function () {
    await allureReporter.addDescription(`Click on Connect Me next to ${coachFirstName} ${coachLastName}`)
    await searchCoachResultsPage.clickConnectMeButton()
  })

  it('Loads the Create Account page', async function () {
    createAccountPage = new CreateAccountPage()
    await createAccountPage.verifyPageIsLoaded()
  })

  it('Verify Coach Information', async function () {
    let expected = `${coachName}, ${coachID}`
    let actual = await header.getCoachInformation()
    assert.strictEqual(actual, expected, `${expected} Coach Info is not displayed in Create Account page`)
  })

  it('Create an Account', async function () {
    screencapture = true
    await allureReporter.addDescription(`Create an Account Full name: ${nameOnCard}, email: ${customerInfo.CUSTOMER_EMAIL}, password: ${GLOBAL_PASSWORD}`)
    await createAccountPage.createAnAccount(nameOnCard, customerInfo.CUSTOMER_EMAIL, GLOBAL_PASSWORD, TestData.SHIPPING_PHONE)
  })

  it('Loads the Checkout page', async function () {
    checkoutPage = new CheckoutPage()
    await checkoutPage.verifyPageIsLoaded()
  })

  it('Loads the Shipping Address page', async function () {
    shippingAddressTile = new ShippingAddressTile()
    await shippingAddressTile.verifyPageIsLoaded()
  })
  it('Enter Shipping Address information', async function () {
    await shippingAddressTile.addShippingAddressHK(TestData)
  })

  it('Check Default shipping check box', async function () {
    await shippingAddressTile.clickToCheckDefaultAddressCheckBox()
  })

  it('Shipping Address: click Okay to Proceed button', async function () {
    await shippingAddressTile.clickNext()
  })

  it('Loads the Suggested Address popup or Shipping Method page', async function () {
    popupSuggestedDeliveryAddressesForm = new PopupSuggestedDeliveryAddressesForm()
    let popup = await popupSuggestedDeliveryAddressesForm.isPopUpPresent()
    if (popup) {
      await popupSuggestedDeliveryAddressesForm.verifyPageIsLoaded()
      await popupSuggestedDeliveryAddressesForm.clickUseThisAddress()
    }
  })

  it('Loads the Payment page', async function () {
    paymentAndBillingAddressTile = new PaymentAndBillingAddressTile()
    await paymentAndBillingAddressTile.verifyPageIsLoadedHK()
  })

  it(`Enter Payment Details information`, async function () {
    await paymentAndBillingAddressTile.createPaymentDetailsHK(nameOnCard, cardNumber, expdt, cvv)
  })

  it('Click on Next button from Payment tile', async function () {
    await paymentAndBillingAddressTile.clickNextHK()
  })

  it('Loads the Final Review page', async function () {
    finalReviewTile = new FinalReviewTile()
    await finalReviewTile.verifyPageIsLoaded()
  })

  it('Click on Next button from Final Review tile', async function () {
    await finalReviewTile.clickPlaceOrderButton()
  })

  it('Loads the Order Confirmation page', async function () {
    screencapture = true
    orderConfirmationPage = new OrderConfirmationPage()
    await orderConfirmationPage.verifyPageIsLoaded()
  })

  afterEach(async function () {
    await driverutils.saveScreenshots(this.currentTest.state, screencapture)
    screencapture = false
  })
})
