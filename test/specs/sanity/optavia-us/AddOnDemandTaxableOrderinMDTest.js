// @flow
import {assert} from 'chai'
import driverutils from '@core-libs/DriverUtils'
import CustomerTestData from '@input-data/customers/CustomerTestData'
import ProductTestData, {attb, market, product} from '@input-data/products/ProductTestData'
import TestCreditCards from '@input-data/creditcards/CreditCardTestData'
import allureReporter from '@wdio/allure-reporter'
import HomePage from '@page-objects/home/HomePage'
import Header from '@page-objects/common/Header'
import MainNavBar from '@page-objects/common/MainNavBar'
import LoginPage from '@page-objects/loginPage/LoginPage'
import {GLOBAL_PASSWORD} from '@core-libs/ConstantUtil'
import MyAccountPage from '@page-objects/myaccount/MyAccountPage'
import ShopAllPage from '@page-objects/shop/ShopAllPage'
import ChooseYourOwnFuelingsPopup from '@page-objects/chooseyourownfuelings/ChooseYourOwnFuelingsPopup'
import ShoppingCartPage from '@page-objects/shoppingcart/ShoppingCartPage'
import FinalReviewTile from '@page-objects/checkout/FinalReviewTile'
import ShippingAddressTile from '@page-objects/checkout/ShippingAddressTile'
import PopupSuggestedDeliveryAddressesForm
  from '@page-objects/popupsuggesteddeliveryaddressesform/PopupSuggestedDeliveryAddressesForm'
import PaymentAndBillingAddressTile from '@page-objects/checkout/PaymentAndBillingAddressTile'
import OrderConfirmationPage from '@page-objects/orderconfirmation/OrderConfirmationPage'
import ShippingMethodTile from '@page-objects/checkout/ShippingMethodTile'

describe('C1448 - US OPTAVIA - Add OnDemand Taxable Order in MD Test', function () {
  let screencapture = false // capture screenshot when test step 'passed' also by default only capture screenshot of failed steps

  let homePage, header, mainNavBar, loginPage

  // Customer Login Info
  let TestData, email, myAccountPage

  // Coach Store
  let productSKU, productName

  // Shopping Cart

  let shoppingCartPage, shopAllPage

  // CheckOut Page

  let addItemsToCartPopup, finalReviewPage, firstname, lastname

  // Order Confirmation Page
  let orderConfirmationPage, shippingAddressTile, tax
  let shippingMethodTile, paymentAndBillingAddressTile, cardType, nameOnCard, cardNumber, cvv, month, year, popupSuggestedDeliveryAddressesForm

  // Coach Information
  let coachInformation, coachImage

  let testTitle = this.title

  before(async function () {
    await driverutils.goToOptaviaWebSiteHomePage()
  })

  beforeEach(async function () {
    await driverutils.addAllureReport(allureReporter, __dirname, 'US OPTAVIA Sanity', testTitle)
  })

  it('Preparing Test Data', async function () {
    TestData = await CustomerTestData.getCustomerInfo('779341462')

    firstname = TestData.CUSTOMER_FIRST_NAME
    lastname = TestData.CUSTOMER_LAST_NAME
    email = TestData.CUSTOMER_EMAIL

    productName = await ProductTestData.PRODUCT_INFO(attb.NAME, product.marketing_materials.OPT_SUCCESS_SYSTEM, market.US)
    productSKU = await ProductTestData.PRODUCT_INFO(attb.SKU, product.marketing_materials.OPT_SUCCESS_SYSTEM, market.US)

    // Payment Information (for VISA = B, Mastercard = C, American Express = D and Discover = E) column of sheet 2 in excel file
    let ccCol = 'C'
    let sheet = 1
    cardType = await TestCreditCards.CARD_TYPE(sheet, ccCol)
    nameOnCard = firstname + ' ' + lastname
    cardNumber = await TestCreditCards.CARD_NUMBER(sheet, ccCol)
    cvv = await TestCreditCards.CVV(sheet, ccCol)
    month = await TestCreditCards.EXPIRATION_MONTH(sheet, ccCol)
    year = await TestCreditCards.EXPIRATION_YEAR(sheet, ccCol)
    tax = 1.14
  })

  it('Loads the Home Page', async function () {
    homePage = new HomePage()
    await homePage.verifyPageIsLoaded()
    header = new Header()
    await header.verifyPageIsLoaded()
    mainNavBar = new MainNavBar()
    await mainNavBar.verifyPageIsLoaded()
  })

  it('Navigate to Login page', async function () {
    await header.clickLogIn()
    loginPage = new LoginPage()
    await loginPage.verifyPageIsLoaded()
  })

  it('Login as existing Coach', async function () {
    await allureReporter.addDescription(`Login as existing Coach email: ${email} and password: ${GLOBAL_PASSWORD}`)
    await loginPage.loginAsExistingCustomer(email, GLOBAL_PASSWORD)
  })

  it('Loads the My Account Page', async function () {
    myAccountPage = new MyAccountPage()
    await myAccountPage.verifyPageIsLoaded()
  })

  it('Get Coach info from avatar', async function () {
    coachInformation = await header.getCoachNameAndId()
    coachImage = await header.getCoachImageSrc()
  })

  it('Loads the Shop All Page', async function () {
    await mainNavBar.clickShopAll()
    shopAllPage = new ShopAllPage()
    await shopAllPage.verifyPageIsLoaded()
  })

  it('Navigate to Choose Your Own Fuelings from Shop', async function () {
    await mainNavBar.clickChooseYourOwnFuelings()
    addItemsToCartPopup = new ChooseYourOwnFuelingsPopup()
    await addItemsToCartPopup.verifyPageIsLoaded()
  })
  it('Search for product', async function () {
    screencapture = true
    await allureReporter.addDescription(`Search product SKU = ${productSKU}`)
    await browser.pause(4000)
    await addItemsToCartPopup.searchForProduct(productSKU)
  })

  it('Add searched product', async function () {
    screencapture = true
    await addItemsToCartPopup.selectProduct()
    await addItemsToCartPopup.verifyPageIsLoaded()
    await addItemsToCartPopup.clickAddItemsToCart()
  })

  it('Loads the Shopping Cart Page', async function () {
    screencapture = true
    shoppingCartPage = new ShoppingCartPage()
    await shoppingCartPage.verifyPageIsLoaded()
  })

  it('Verify Product Name in the cart', async function () {
    let expected = `${productName}`
    let actual = await shoppingCartPage.getProductNameText(expected)
    assert.strictEqual(actual, expected, `${expected} Product Name is not displayed in Cart page`)
  })

  it('Click on Proceed to Checkout button', async function () {
    await shoppingCartPage.clickProceedToCheckoutBtn()
  })

  it('Loads the Final Review page', async function () {
    finalReviewPage = new FinalReviewTile()
    await finalReviewPage.verifyPageIsLoaded()
  })

  it('Verify Coach Name and ID are the Same as the Previously Captured Values', async function () {
    let actual = await header.getCoachInformationASM()
    let expected = coachInformation
    assert.strictEqual(actual, expected, `${expected} Coach Info is not displayed`)
  })

  it('Verify Coach Image has the Same Source as the Previously Displayed Image', async function () {
    let actual = await header.getCoachImageSrc()
    let expected = coachImage
    assert.strictEqual(actual, expected, `${expected} Coach Image is not displayed`)
  })

  it('Click on Edit Button under Shipping Delivery Address', async function () {
    await finalReviewPage.clickDeliveryAddressEditBtn()
  })

  it('Loads the Shipping Address page', async function () {
    shippingAddressTile = new ShippingAddressTile()
    await shippingAddressTile.verifyPageIsLoaded()
  })

  it('Verify Coach Name and ID are the Same as the Previously Captured Values', async function () {
    let expected = coachInformation
    let actual = await header.getCoachInformationASM()
    assert.strictEqual(actual, expected, `${expected} Coach Info is not displayed`)
  })

  it('Verify Coach Image has the Same Source as the Previously Displayed Image', async function () {
    let actual = await header.getCoachImageSrc()
    let expected = coachImage
    assert.strictEqual(actual, expected, `${expected} Coach Image is not displayed`)
  })

  it('Clears the text in Delivery Address', async function () {
    let blankAddress = {
      SHIPPING_ADDRESS_LINE_1: '',
      SHIPPING_CITY: '',
      SHIPPING_FULL_STATE_NAME: '',
      SHIPPING_ZIP: '',
      SHIPPING_PHONE: '',
      SHIPPING_COUNTRY: '',
      SHIPPING_COUNTRY_CODE: ''
    }
    await shippingAddressTile.addShippingAddress(blankAddress)
  })

  it('Updating Delivery Address information', async function () {
    await shippingAddressTile.verifyPageIsLoaded()
    await shippingAddressTile.addShippingAddress(TestData, true)
  })

  it('Shipping Address: click Next button', async function () {
    if (TestData.SHIPPING_FULL_STATE_NAME === 'California') {
      await shippingAddressTile.clickOkayToProceed()
    } else {
      await shippingAddressTile.clickNext()
    }
  })

  it('Loads the Suggested Address popup', async function () {
    popupSuggestedDeliveryAddressesForm = new PopupSuggestedDeliveryAddressesForm()
    await popupSuggestedDeliveryAddressesForm.verifyPageIsLoaded()
  })

  it('Click on Use this address button', async function () {
    await shippingAddressTile.verifyPageIsLoaded()
    await popupSuggestedDeliveryAddressesForm.clickUseThisAddress()
  })

  it('Loads the Shipping Method page', async function () {
    shippingMethodTile = new ShippingMethodTile()
    await shippingMethodTile.verifyPageIsLoaded()
  })

  it('Click on Next button from Shipping Method tile', async function () {
    await shippingMethodTile.clickNext()
  })

  it('Verify Coach Name and ID are the Same as the Previously Captured Values', async function () {
    let actual = await header.getCoachInformationASM()
    let expected = coachInformation
    assert.strictEqual(actual, expected, `${expected} Coach Name is not displayed`)
  })

  it('Verify Coach Image has the Same Source as the Previously Displayed Image', async function () {
    let actual = await header.getCoachImageSrc()
    let expected = coachImage
    assert.strictEqual(actual, expected, `${expected} Coach Image is not displayed`)
  })

  it('Loads the Payment & Billing Address page', async function () {
    paymentAndBillingAddressTile = new PaymentAndBillingAddressTile()
    await paymentAndBillingAddressTile.verifyPageIsLoaded()
  })

  it('Verify Coach Name and ID are the Same as the Previously Captured Values', async function () {
    let actual = await header.getCoachInformationASM()
    let expected = coachInformation
    assert.strictEqual(actual, expected, `${expected} Coach Name is not displayed`)
  })

  it('Verify Coach Image has the Same Source as the Previously Displayed Image', async function () {
    let actual = await header.getCoachImageSrc()
    let expected = coachImage
    assert.strictEqual(actual, expected, `${expected} Coach Image is not displayed`)
  })

  it(`Enter Payment Details information`, async function () {
    await paymentAndBillingAddressTile.createPaymentDetails(cardType, nameOnCard, cardNumber, cvv, month, year)
  })

  it('Click Use my Delivery Address checkbox', async function () {
    await paymentAndBillingAddressTile.clickToCheckUseDeliveryAddressCheckBox()
  })

  it('Verify Use my Delivery Address checkbox checked', async function () {
    let actual = await paymentAndBillingAddressTile.isUseDeliveryAddressChecked()
    let expected = true
    assert.strictEqual(actual, expected, 'Use my Delivery Address checkbox is not checked')
  })

  it('Loads the Payment & Billing Address page', async function () {
    await paymentAndBillingAddressTile.verifyPageIsLoaded()
  })

  it('Click on Next button from Payment & Billing Address tile', async function () {
    await paymentAndBillingAddressTile.clickNext()
  })

  it('Verify Coach Name and ID are the Same as the Previously Captured Values', async function () {
    let actual = await header.getCoachInformationASM()
    let expected = coachInformation
    assert.strictEqual(actual, expected, `${expected} Coach Name is not displayed`)
  })

  it('Verify Coach Image has the Same Source as the Previously Displayed Image', async function () {
    let actual = await header.getCoachImageSrc()
    let expected = coachImage
    assert.strictEqual(actual, expected, `${expected} Coach Image is not displayed`)
  })

  it('Verify Shipping Delivery Address State is Maryland', async function () {
    let expected = `${firstname} ${lastname}\n${TestData.SHIPPING_ADDRESS_LINE_1}${TestData.SHIPPING_ADDRESS_LINE_2}\n${TestData.SHIPPING_CITY}, ${TestData.SHIPPING_FULL_STATE_NAME}\n${TestData.SHIPPING_ZIP}\n${TestData.SHIPPING_COUNTRY}\n${TestData.SHIPPING_PHONE}`
    let actual = await finalReviewPage.getDeliveryAddressSection()
    assert.strictEqual(actual.toString(), expected.toString(), `${expected} Shipping Delivery Address is not displayed`)
  })

  it('Order Summary: Tax', async function () {
    // Add $ sign and decimal upto 2 digits
    let expected = '$' + await tax.toFixed(2)
    let actual = await finalReviewPage.getTaxAmount()
    assert.strictEqual(actual, expected, `Order Summary: Actual Tax ${actual} is not matching ${expected}`)
  })

  it('Click on Submit Order button from Final Review tile', async function () {
    await finalReviewPage.clickPlaceOrderButton()
  })

  it('Loads the Order Confirmation page', async function () {
    orderConfirmationPage = new OrderConfirmationPage()
    await orderConfirmationPage.verifyPageIsLoaded()
  })

  it('Verify the Coach information is not displayed in Order Summary Section', async function () {
    screencapture = true
    assert.isFalse(await orderConfirmationPage.isCoachInfoFromAvatarDisplayed(), 'Coach information is displayed in Order confirmation Page')
  })

  afterEach(async function () {
    await driverutils.saveScreenshots(this.currentTest.state, screencapture)
    screencapture = false
  })
})
