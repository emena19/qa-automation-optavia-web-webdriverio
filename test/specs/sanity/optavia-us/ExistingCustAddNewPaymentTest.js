// @flow
import {assert} from 'chai'
import driverutils from '@core-libs/DriverUtils'
import CustomerTestData from '@input-data/customers/CustomerTestData'
import TestCreditCards from '@input-data/creditcards/CreditCardTestData'
import HomePage from '@page-objects/home/HomePage'
import Header from '@page-objects/common/Header'
import MainNavBar from '@page-objects/common/MainNavBar'
import LoginPage from '@page-objects/loginPage/LoginPage'
import MyAccountPage from '@page-objects/myaccount/MyAccountPage'
import MyAccountLeftNavigation from '@page-objects/myaccount/MyAccountLeftNavigation'
import PaymentDetailsPage from '@page-objects/myaccount/PaymentDetailsPage'
import allureReporter from '@wdio/allure-reporter'

describe('C15292/24274 - US OPTAVIA - My Account - Payment Details - Existing Customer adds new Payment Method', function () {
  let screencapture = false // capture screenshot when test step 'passed' also by default only capture screenshot of failed steps

  let homePage, header, mainNavBar
  let loginPage, paymentDetailsPage, myAccountLeftNavigation, currentDefaultCard

  // Customer Registration
  let firstName, lastName, email, password, myAccountPage

  // Shipping Address
  let addressLine1, city, state, zip

  // Payment Information
  let cardType, nameOnCard, cardNumber, cvv, month, year

  let testTitle = this.title

  before(async function () {
    await driverutils.goToOptaviaWebSiteHomePage()
  })

  beforeEach(async function () {
    await driverutils.addAllureReport(allureReporter, __dirname, 'US Sanity', testTitle)
  })

  it('Preparing Client data', async function () {
    let custID = '952674471'
    let TestData = await CustomerTestData.getCustomerInfo(custID)
    // Customer Registration
    email = TestData.CUSTOMER_EMAIL
    password = TestData.CUSTOMER_PASSWORD
    firstName = TestData.CUSTOMER_FIRST_NAME
    lastName = TestData.CUSTOMER_LAST_NAME
    addressLine1 = TestData.ADDITIONAL_ADDRESS_LINE_1
    city = TestData.ADDITIONAL_CITY
    state = TestData.ADDITIONAL_FULL_STATE_NAME
    zip = TestData.ADDITIONAL_ZIP
  })

  it('Preparing Test Data', async function () {
    screencapture = false
    let ccCol = 'B'
    let ccsheet = 1
    cardType = await TestCreditCards.CARD_TYPE(ccsheet, ccCol)
    nameOnCard = firstName + ' ' + lastName
    cardNumber = await TestCreditCards.CARD_NUMBER(ccsheet, ccCol)
    cvv = await TestCreditCards.CVV(ccsheet, ccCol)
    month = await TestCreditCards.EXPIRATION_MONTH(ccsheet, ccCol)
    year = await TestCreditCards.EXPIRATION_YEAR(ccsheet, ccCol)
  })

  it('Loads the Home Page', async function () {
    homePage = new HomePage()
    await homePage.verifyPageIsLoaded()
    header = new Header()
    await header.verifyPageIsLoaded()
    mainNavBar = new MainNavBar()
    await mainNavBar.verifyPageIsLoaded()
  })

  it('Click on Login', async function () {
    await header.clickLogIn()
  })

  it('Loads the Login and Create an Account page', async function () {
    loginPage = new LoginPage()
    await loginPage.verifyPageIsLoaded()
  })

  it('Login as existing Customer', async function () {
    screencapture = true
    await allureReporter.addDescription(`Login as existing Customer email: ${email} and password: ${password}`)
    await loginPage.loginAsExistingCustomer(email, password)
  })

  it('Verify left menu is loaded', async function () {
    myAccountPage = new MyAccountPage()
    await myAccountPage.verifyPageIsLoaded()
    myAccountLeftNavigation = new MyAccountLeftNavigation()
    await myAccountLeftNavigation.verifyPageIsLoaded()
  })

  it('Click on payment details', async function () {
    await myAccountLeftNavigation.clickPaymentDetails()
  })

  it('Loads Payment Details Page', async function () {
    paymentDetailsPage = new PaymentDetailsPage()
    await paymentDetailsPage.verifyPageIsLoaded()
  })

  it('Get current default card', async function () {
    currentDefaultCard = await paymentDetailsPage.getDefaultAddress()
  })

  it('Click on add a new card', async function () {
    await paymentDetailsPage.clickAddNewCardBTN()
  })

  it('Enter Payment Details information', async function () {
    await paymentDetailsPage.createPaymentDetails(nameOnCard, cardType, cardNumber, cvv, month, year)
  })

  it('Create Billing Address', async function () {
    await paymentDetailsPage.createBillingAddress(firstName, lastName, addressLine1, city, state, zip)
  })

  it('Click on Cancel Button', async function () {
    await paymentDetailsPage.verifyCancelButtonIsDisplayed()
    await paymentDetailsPage.clickCancelButton()
  })

  it('Loads Payment Details Page', async function () {
    await paymentDetailsPage.verifyPageIsLoaded()
  })

  it('Verify  Default address in Payment Details page is not changed', async function () {
    await allureReporter.addDescription('Default address in Payment Details page')
    let expected = currentDefaultCard
    let actual = await paymentDetailsPage.getDefaultAddress()
    assert.strictEqual(actual, expected, 'Default address in Payment Details page is changed')
  })

  it('Click on add a new card', async function () {
    await paymentDetailsPage.clickAddNewCardBTN()
  })

  it('Enter Payment Details information', async function () {
    await paymentDetailsPage.createPaymentDetails(nameOnCard, cardType, cardNumber, cvv, month, year)
  })

  it('Create Billing Address', async function () {
    await paymentDetailsPage.createBillingAddress(firstName, lastName, addressLine1, city, state, zip)
  })

  it('Click Save Payment Info button', async function () {
    await browser.pause(3000)
    await paymentDetailsPage.clickSavePaymentInfoButton()
  })

  it('Verify Confirmation Message', async function () {
    let actual = await paymentDetailsPage.getYourGlobalMessage()
    let expected = 'Payment Card added successfully'
    assert.equal(actual, expected, `${expected} Message is not displayed`)
  })

  // Extra steps to remove added Credit card

  it('Remove the Payment Info', async function () {
    await paymentDetailsPage.clickRemovePaymentInfo()
  })

  it('Verify Remove Payment Information Pop Up is loaded', async function () {
    await paymentDetailsPage.verifyRemovePaymentInfoPopUpIsLoaded()
  })

  it('Click on OK button to remove the Payment Info', async function () {
    await paymentDetailsPage.clickPopUpOKToRemove()
  })

  it('Verify Global message displays on top', async function () {
    let actual = await paymentDetailsPage.getGlobalSuccessMsg()
    let expected = 'Payment Card removed successfully'
    assert.strictEqual(actual, expected, `${expected} Global Message is not displayed on top`)
  })

  afterEach(async function () {
    await driverutils.saveScreenshots(this.currentTest.state, screencapture)
    screencapture = false
  })
})
