// @flow
import {assert} from 'chai'
import driverutils from '@core-libs/DriverUtils'
import CustomerTestData from '@input-data/customers/CustomerTestData'
import CoachTestData from '@input-data/coaches/CoachTestData'
import ProductTestData, {attb, product} from '@input-data/products/ProductTestData'
import TestCreditCards from '@input-data/creditcards/CreditCardTestData'
import HomePage from '@page-objects/home/HomePage'
import Header from '@page-objects/common/Header'
import MainNavBar from '@page-objects/common/MainNavBar'
import JoinUsPage from '@page-objects/joinus/JoinUsPage'
import AddedToYourShoppingCartPopup from '@page-objects/shop/AddedToYourShoppingCartPopup'
import ShoppingCartPage from '@page-objects/shoppingcart/ShoppingCartPage'
import ShopAllPage from '@page-objects/shop/ShopAllPage'
import CommonUtils from '@core-libs/CommonUtils'
import FindACoachPage from '@page-objects/findacoach/FindACoachPage'
import SearchCoachResultsPage from '@page-objects/searchcoachresults/SearchCoachResultsPage'
import CreateAccountPage from '@page-objects/createAccountPage/CreateAccountPage'
import {AGENT_USER, GLOBAL_PASSWORD} from '@core-libs/ConstantUtil'
import CheckoutPage from '@page-objects/checkout/CheckoutPage'
import ShippingAddressTile from '@page-objects/checkout/ShippingAddressTile'
import ShippingMethodTile from '@page-objects/checkout/ShippingMethodTile'
import PaymentAndBillingAddressTile from '@page-objects/checkout/PaymentAndBillingAddressTile'
import FinalReviewTile from '@page-objects/checkout/FinalReviewTile'
import OrderConfirmationPage from '@page-objects/orderconfirmation/OrderConfirmationPage'
import MyAccountPage from '@page-objects/myaccount/MyAccountPage'
import MyAccountLeftNavigation from '@page-objects/myaccount/MyAccountLeftNavigation'
import CoachStorePage from '@page-objects/coachstore/CoachStorePage'
import HybrisBackofficeLoginPage from '@page-objects/hybrisbackoffice/HybrisBackofficeLoginPage'
import HybrisBackofficeAutorityGroupPage from '@page-objects/hybrisbackoffice/HybrisBackofficeAutorityGroupPage'
import HybrisBackofficeHomePage from '@page-objects/hybrisbackoffice/HybrisBackofficeHomePage'
import HybrisBackofficeSearchPage from '@page-objects/hybrisbackoffice/HybrisBackofficeSearchPage'
import HybrisBackofficeOrderDetailsPage from '@page-objects/hybrisbackoffice/HybrisBackofficeOrderDetailsPage'
import allureReporter from '@wdio/allure-reporter'
import TestDataBaseMapping from '@db/TestDataBaseMapping'
import OptaviaPremierOrderPage from '@page-objects/myaccount/OptaviaPremierOrderPage'
import SelectFuelingsPage from '@page-objects/shop/SelectFuelingsPage'

describe('C1452-US-OPTAVIA-Add OPTAVIA Premier Customer with Health Coach Business Kit', function () {
  const payableTypeID = 5
  const customerTypeID = 1

  let screencapture = false // capture screenshot when test step 'passed' also by default only capture screenshot of failed steps

  let homePage, header, mainNavBar, joinUsPage, addedToYourShoppingCartPopup, shoppingCartPage, createAccountPage,
    findACoachPage, searchCoachResultsPage
  let checkoutPage, shippingAddressTile, shippingMethodTile, paymentAndBillingAddressTile, finalReviewTile,
    orderConfirmationPage, myAccountPage, orderConfirmationDate, accountID

  let actualGlobal, myAccountLeftNavigation, optaviaPremierOrderPage

  // Customer Registration
  let TestData, customerInfo

  // Coach (Enroller) Info
  let coachFirstName, coachLastName, coachID

  // Payment Details
  let cardType, nameOnCard, cardNumber, cvv, month, year

  // Shop Page
  let shopAllPage, selectFuelingsPage

  // Products
  let businessKit, heartyChoiceProduct, renewalKit

  // Shopping Cart Page
  let orderSubTotal, deliveryCharge, taxAmount, orderTotal, coachstorePage

  // Final Review
  let renewalKitSubtotal, renewalKitTotal, renewalKitTax, renewalDate

  // Order Confirmation page
  let orderNumber, commissonableVolume, qualifyingvolume, actualOrderNumber, decimalLength

  // Hybris Backoffice
  let agentEmail, agentPassword, hybrisBackofficeLoginPage, hybrisBackofficeAutorityGroupPage, hybrisBackofficeHomePage,
    hybrisBackofficeSearchPage, hybrisBackofficeOrderDetailsPage

  describe('C1452 New customer buy a Coach kit', function () {
    let testTitle = this.title

    before(async function () {
      await driverutils.goToOptaviaWebSiteHomePage()
    })

    beforeEach(async function () {
      await driverutils.addAllureReport(allureReporter, __dirname, 'US OPTAVIA Sanity', testTitle)
    })

    it('Preparing Test Data', async function () {
      TestData = await CustomerTestData.getCustomerInfo('90000000002')
      // Customer Registration
      customerInfo = await CustomerTestData.getNewCustomer()
      // Coach Info
      coachFirstName = await CoachTestData.COACH_FIRST_NAME()
      coachLastName = await CoachTestData.COACH_LAST_NAME()
      coachID = await CoachTestData.COACH_ID()

      renewalKit = await ProductTestData.PRODUCT_INFO(attb.UNIT_PRICE, product.renewal_certification.OPT_BUSINESS_RENEWAL)
      businessKit = await ProductTestData.PRODUCT_INFO(attb.UNIT_PRICE, product.kits.OPT_OPTAVIA_BUSINESS_KIT)
      heartyChoiceProduct = await ProductTestData.PRODUCT_INFO(attb.UNIT_PRICE, product.hearty_choices.OPT_CHEDDAR_BISCUIT)
      orderSubTotal = businessKit + heartyChoiceProduct
      deliveryCharge = 'FREE'
      taxAmount = 11.94
      orderTotal = orderSubTotal + taxAmount
      renewalKitSubtotal = renewalKit
      renewalKitTax = 0.00
      renewalKitTotal = renewalKitSubtotal + renewalKitTax

      // Payment Information (for VISA = B, Mastercard = C, American Express = D and Discover = E) column of sheet 2 in excel file
      let ccCol = 'C'
      let sheet = 1
      cardType = await TestCreditCards.CARD_TYPE(sheet, ccCol)
      nameOnCard = customerInfo.CUSTOMER_FIRST_NAME + ' ' + customerInfo.CUSTOMER_LAST_NAME
      cardNumber = await TestCreditCards.CARD_NUMBER(sheet, ccCol)
      cvv = await TestCreditCards.CVV(sheet, ccCol)
      month = await TestCreditCards.EXPIRATION_MONTH(sheet, ccCol)
      year = await TestCreditCards.EXPIRATION_YEAR(sheet, ccCol)
    })

    it('Loads the Home Page', async function () {
      homePage = new HomePage()
      await homePage.verifyPageIsLoaded()
      header = new Header()
      await header.verifyPageIsLoaded()
      mainNavBar = new MainNavBar()
      await mainNavBar.verifyPageIsLoaded()
    })

    it('Navigate to Join Us page', async function () {
      await mainNavBar.clickJoinUs()
      joinUsPage = new JoinUsPage()
      await joinUsPage.verifyPageIsLoaded()
    })

    it('Verify Add to Cart button is disabled', async function () {
      screencapture = true
      let actual = await joinUsPage.isAddToCartBtnEnabled()
      assert.isTrue(actual, 'Add to cart button is Enabled')
    })

    it('Click on Checkbox for accepting terms and conditions.', async function () {
      await joinUsPage.clickCheckBox()
    })

    it('Verify Add to Cart button is enabled', async function () {
      screencapture = true
      let actual = await joinUsPage.isAddToCartBtnEnabled()
      assert.isTrue(actual, 'Add to cart button is disabled')
    })

    it('Click on Add to Cart button', async function () {
      await joinUsPage.clickAddToCartBtn()
    })

    it('Loads the Added To Your Shopping Cart Popup', async function () {
      addedToYourShoppingCartPopup = new AddedToYourShoppingCartPopup()
      await addedToYourShoppingCartPopup.verifyPageIsLoaded()
    })

    it('Click on Checkout button on pop up window', async function () {
      await addedToYourShoppingCartPopup.clickCheckoutButton()
    })

    it('Loads the Shopping Cart Page', async function () {
      shoppingCartPage = new ShoppingCartPage()
      await shoppingCartPage.verifyPageIsLoaded()
    })

    it('Verify Join Premier checkbox is NOT checked', async function () {
      screencapture = true
      let actual = await shoppingCartPage.isJoinPremierContainerDisplayed()
      assert.isFalse(actual, 'Join Premier checkbox is checked')
    })

    it('Verify Coach Upsells banner displayed on Top', async function () {
      screencapture = true
      let actual = await shoppingCartPage.isCoachUpSellBannerLoaded()
      assert.isTrue(actual, 'Coach Upsell Banner is displayed')
    })

    it('Click on Continue shopping Button', async function () {
      await shoppingCartPage.clickContinueShoppingBtn()
    })

    it('Loads Shop All Page', async function () {
      shopAllPage = new ShopAllPage()
      await shopAllPage.verifyPageIsLoaded()
    })

    it('Click on Shop Button under Select Fuelings', async function () {
      await shopAllPage.clickSelectFuelingsShopBtn()
    })

    it('Loads Select Fuelings Page', async function () {
      selectFuelingsPage = new SelectFuelingsPage()
      await selectFuelingsPage.verifyPageIsLoaded()
    })

    it('Check on Meal Replacement Checkbox', async function () {
      await selectFuelingsPage.clickOnMealReplacementCheckBox()
    })

    it('Click on Add To Cart Button for Select Dark Chocolate Coconut Curry Bar with Almonds and Sea Salt', async function () {
      await selectFuelingsPage.clickOnSelectDarkChocCoconutCurryBar()
    })

    it('Click on Checkout button on pop up window', async function () {
      await addedToYourShoppingCartPopup.clickCheckoutButton()
    })

    it('Check the Checkbox for Join Optavia Premier Checkbox', async function () {
      await shoppingCartPage.clickToCheckJoinPremierCheckBox()
    })

    it('Verify OPTAVIA Free Guide is displayed as Free Item', async function () {
      await shoppingCartPage.verifyFreeOptaviaGuideIsLoaded()
    })

    it('Verify Journey Kickoff Card Insert is displayed as Free Item', async function () {
      await shoppingCartPage.verifyFreeJourneyKickoffCardInsertIsLoaded()
    })

    it('Verify OPTAVIA Guide Top Tips Insert is displayed as Free Item', async function () {
      await shoppingCartPage.verifyFreeOptaviaGuideTopTipsInsertIsLoaded()
    })

    it('Verify Order Subtotal in the cart', async function () {
      let expected = `${await CommonUtils.toCurrency(orderSubTotal)}`
      let actual = await shoppingCartPage.getOrderSubtotal()
      assert.strictEqual(actual, expected, `Order Subtotal : ${actual} is not matching with expected`)
    })

    it('Click on Proceed to Checkout button', async function () {
      await shoppingCartPage.clickProceedToCheckoutBtn()
    })

    it('Loads the Find a Coach page', async function () {
      findACoachPage = new FindACoachPage()
      await findACoachPage.verifyPageIsLoaded()
    })

    it('Verify text in First Name text box', async function () {
      let expected = 'First Name'
      let actual = await findACoachPage.getFirstNameTextboxText('placeholder')
      assert.strictEqual(actual, expected, `${expected} is not displayed`)
    })

    it('Verify text in Last Name text box', async function () {
      let expected = 'Last Name'
      let actual = await findACoachPage.getLastNameTextboxText('placeholder')
      assert.strictEqual(actual, expected, `${expected} is not displayed`)
    })

    it('Verify market is defaulted to United States', async function () {
      let expected = 'United States'
      let actual = await findACoachPage.getMarketDefaultText()
      assert.strictEqual(actual, expected, `${expected} is not displayed`)
    })

    it('Search a coach', async function () {
      await allureReporter.addDescription(`Search a coach ${coachFirstName} ${coachLastName}`)
      await findACoachPage.enterFirstAndLastNameToSearchCoach(coachFirstName, coachLastName)
      await findACoachPage.clickSearchCoachFindACoachButton()
    })

    it('Loads the Search Result page', async function () {
      searchCoachResultsPage = new SearchCoachResultsPage()
      await searchCoachResultsPage.verifyPageIsLoaded()
    })

    it('Verify searched coach is in search result list', async function () {
      screencapture = true
      await allureReporter.addDescription(`Verify ${coachFirstName} ${coachLastName} is in search result list`)
      let expected = `${coachFirstName} ${coachLastName}`
      let actual = await searchCoachResultsPage.getCoachName()
      assert.strictEqual(actual, expected, `Coach : ${expected} is not displayed in Search result Page`)
    })

    it('Click on Connect Me next to searched coach', async function () {
      await allureReporter.addDescription(`Click on Connect Me next to ${coachFirstName} ${coachLastName}`)
      await searchCoachResultsPage.clickConnectMeButton()
    })

    it('Loads the Create Account page', async function () {
      createAccountPage = new CreateAccountPage()
      await createAccountPage.verifyPageIsLoaded()
    })

    it('Verify Coach info displayed on top header', async function () {
      let actual = await header.getCoachInformation()
      let expected = `${coachFirstName} ${coachLastName}, ${coachID}`
      assert.strictEqual(actual, expected, `Coach : ${expected} is not displayed on top header`)
    })

    it('Create an Account', async function () {
      screencapture = true
      await allureReporter.addDescription(`Create an Account name: ${nameOnCard}, email: ${customerInfo.CUSTOMER_EMAIL}, password: ${GLOBAL_PASSWORD} and confirm password: ${GLOBAL_PASSWORD}`)
      await createAccountPage.createAnAccount(nameOnCard, customerInfo.CUSTOMER_EMAIL, GLOBAL_PASSWORD, TestData.SHIPPING_PHONE)
    })

    it('Loads the Checkout page', async function () {
      checkoutPage = new CheckoutPage()
      await checkoutPage.verifyPageIsLoaded()
    })

    it('Loads the Shipping Address page', async function () {
      shippingAddressTile = new ShippingAddressTile()
      await shippingAddressTile.verifyPageIsLoaded()
    })

    it('Enter Shipping Address information', async function () {
      await shippingAddressTile.addShippingAddress(TestData)
    })

    it('Check Default shipping check box', async function () {
      await shippingAddressTile.clickToCheckDefaultAddressCheckBox()
    })

    it('Shipping Address: click Next button', async function () {
      await shippingAddressTile.clickNext()
    })

    it('Loads the Shipping Method page', async function () {
      shippingMethodTile = new ShippingMethodTile()
      await shippingMethodTile.verifyPageIsLoaded()
    })

    it('Verify Standard shipping is selected', async function () {
      let actual = await shippingMethodTile.isStdShippingSelected()
      let expected = true
      assert.strictEqual(actual, expected, 'Standard shipping is not selected')
    })

    it('Click on Next button from Shipping Method tile', async function () {
      await shippingMethodTile.clickNext()
    })

    it('Loads the Payment & Billing Address page', async function () {
      paymentAndBillingAddressTile = new PaymentAndBillingAddressTile()
      await paymentAndBillingAddressTile.verifyPageIsLoaded()
    })

    it('Verify Use my Delivery Address checkbox is there and NOT checked', async function () {
      let actual = await paymentAndBillingAddressTile.isUseDeliveryAddressChecked()
      let expected = false
      assert.strictEqual(actual, expected, 'Delivery Address checkbox is not there and is checked')
    })

    it('Enter Payment Details information', async function () {
      await paymentAndBillingAddressTile.createPaymentDetails(cardType, nameOnCard, cardNumber, cvv, month, year)
    })

    it('Click Use my Delivery Address checkbox', async function () {
      await paymentAndBillingAddressTile.clickToCheckUseDeliveryAddressCheckBox()
    })

    it('Loads the Payment & Billing Address page', async function () {
      paymentAndBillingAddressTile = new PaymentAndBillingAddressTile()
      await paymentAndBillingAddressTile.verifyPageIsLoaded()
    })

    it('Click on Next button from Payment & Billing Address tile', async function () {
      await paymentAndBillingAddressTile.clickNext()
    })

    it('Loads the Final Review page', async function () {
      screencapture = true
      finalReviewTile = new FinalReviewTile()
      await finalReviewTile.verifyPageIsLoaded()
    })

    it('Click on Submit Order Button', async function () {
      screencapture = true
      await browser.pause(3000)
      await finalReviewTile.clickPlaceOrderButton()
    })

    it('Loads the Order Confirmation page', async function () {
      orderConfirmationPage = new OrderConfirmationPage()
      await orderConfirmationPage.verifyPageIsLoaded()
    })

    it('Verify Order Total in Confirmation Page', async function () {
      screencapture = true
      actualOrderNumber = await orderConfirmationPage.getOrderNumber()
      let resultArray = actualOrderNumber.split(' ')
      actualOrderNumber = await resultArray[4]
      let expected = `Total ${await CommonUtils.toCurrency(orderTotal)}\nSubtotal ${await CommonUtils.toCurrency(orderSubTotal)}\nDelivery Charge: ${deliveryCharge}\nTax: ${await CommonUtils.toCurrency(taxAmount)}`
      let actual = await orderConfirmationPage.getOrderTotals()
      let orderDateLine = await orderConfirmationPage.getOrderDate()
      orderConfirmationDate = orderDateLine.substring(10, orderDateLine.length)
      assert.strictEqual(actual, expected, `Order Summary ${expected} in Confirmation Page is different`)
    })

    it('Click on Continue Shopping button on Order Confirmation page', async function () {
      await orderConfirmationPage.clickContinueShopping()
    })

    it('Loads the Home page', async function () {
      await header.verifyMyAccountLinkIsLoaded()
      await mainNavBar.verifyPageIsLoaded()
    })

    it('Clicks on My Account menu and gets Client ID', async function () {
      await header.clickMyAccount()
      myAccountPage = new MyAccountPage()
      await myAccountPage.verifyPageIsLoaded()
      accountID = await myAccountPage.getAccountID()
      accountID = accountID.substring(11, accountID.length)
    })

    it('Verify My Account Left Navigation is loaded', async function () {
      myAccountLeftNavigation = new MyAccountLeftNavigation()
      await myAccountLeftNavigation.verifyPageIsLoaded()
    })

    it('Click on OPTAVIA Premier Order link under My Account', async function () {
      await myAccountLeftNavigation.clickOptaviaPremierOrder()
    })

    it('Loads the Subscriptions page', async function () {
      optaviaPremierOrderPage = new OptaviaPremierOrderPage()
      await optaviaPremierOrderPage.verifyPageIsLoaded()
      await browser.pause(180000) // Please remove after DB Sync and OPTAVIA Connect issue are fixed
    })
  })

  describe('C1452 Look up syncSQL database for above customers in Customers table', function () {
    it('Verify customer type ID is (Health Coach = 1) in DB', async function () {
      let expected = customerTypeID
      let actual = await TestDataBaseMapping.getCustomerTypeID_CustomersTable(accountID)
      assert.strictEqual(actual, expected, `Customer type ID : ${expected} is not matching with DB`)
    })

    it('Verify Business Kit order activation date in DB', async function () {
      let expected = orderConfirmationDate
      let activationDate = await TestDataBaseMapping.getActivationDate_CustomersTable(accountID)
      let actual = await CommonUtils.getActivationDateFormat(activationDate)
      assert.strictEqual(actual, expected, `Business Kit order activation date : ${expected} is not matching with DB`)
    })

    it('Verify Payable type ID is (Payment Card = 5) in DB', async function () {
      let expected = payableTypeID
      let actual = await TestDataBaseMapping.getPayableTypeID_CustomersTable(accountID)
      assert.strictEqual(actual, expected, `Payable type ID : ${expected} is not matching with DB`)
    })
  })

  describe('C1452 Look up CUSTOM.Health_Coach_Dates table for Renewal date', function () {
    it('Verify New Renewal Date is displayed as 365 days from order creation date in DB', async function () {
      let actual = await CommonUtils.getRenewalDateFormat(await TestDataBaseMapping.getRenewalDate_HCDatesTable(accountID))
      renewalDate = actual
      let expected = await CommonUtils.getRenewalDate(await TestDataBaseMapping.getActivationDate_HCDatesTable(accountID))
      assert.strictEqual(actual, expected, ` New Renewal Date : ${actual} is not 365 days from order creation date in DB`)
    })

    it('Verify Old Renewal Date is displayed as NULL in DB', async function () {
      assert.isNull(await TestDataBaseMapping.getOldRenewalDate_HCDatesTable(accountID), 'Old Renewal Date is not displayed as NULL')
    })

    it('Verify OPTAVIA Connect link is displayed in header', async function () {
      screencapture = true
      await myAccountLeftNavigation.clickOptaviaPremierOrder()
      await optaviaPremierOrderPage.verifyPageIsLoaded()
      actualGlobal = await header.isOptaviaConnectLinkFound()
      assert.isTrue(actualGlobal, 'OPTAVIA Connect link is not displayed')
    })
  })

  describe('C1452 Customer places an order with Renewal Kit', function () {
    it('Navigate to Join Us page', async function () {
      await mainNavBar.clickJoinUs()
      await joinUsPage.verifyPageIsLoaded()
    })

    it('Click on Coach store', async function () {
      await joinUsPage.clickVisitCoachStoreBtn()
    })

    it('Loads the Coach Store Page', async function () {
      coachstorePage = new CoachStorePage()
      await coachstorePage.verifyPageIsLoaded()
    })

    it('Click on Add To Cart for 12 Month Renewal kit', async function () {
      await coachstorePage.clickRenewalKitAddToCartbtn()
    })
    it('Loads the Added To Your Shopping Cart Popup', async function () {
      addedToYourShoppingCartPopup = new AddedToYourShoppingCartPopup()
      await addedToYourShoppingCartPopup.verifyPageIsLoaded()
    })

    it('Click on Checkout button on pop up window', async function () {
      await addedToYourShoppingCartPopup.clickCheckoutButton()
    })

    it('Click on Proceed to Checkout button', async function () {
      await shoppingCartPage.clickProceedToCheckoutBtn()
    })

    it('Loads the Final Review page', async function () {
      finalReviewTile = new FinalReviewTile()
      await finalReviewTile.verifyPageIsLoaded()
    })

    it('Click on Submit Order Button', async function () {
      screencapture = true
      await finalReviewTile.clickPlaceOrderButton()
    })

    it('Loads the Order Confirmation page', async function () {
      orderConfirmationPage = new OrderConfirmationPage()
      await orderConfirmationPage.verifyPageIsLoaded()
    })

    it('Verify Order Total in Confirmation Page', async function () {
      screencapture = true
      orderNumber = await orderConfirmationPage.getOrderNumber()
      let resultArray = orderNumber.split(' ')
      orderNumber = await resultArray[4]
      let expected = `Total ${await CommonUtils.toCurrency(renewalKitTotal)}\nSubtotal ${await CommonUtils.toCurrency(renewalKitSubtotal)}\nDelivery Charge: ${deliveryCharge}\nTax: ${await CommonUtils.toCurrency(renewalKitTax)}`
      let actual = await orderConfirmationPage.getOrderTotals()
      assert.strictEqual(actual, expected, 'Order Total in Confirmation Page is not matching with expected')
    })

    it('Verify Old Renewal Date is displayed as NULL in DB', async function () {
      assert.isNull(await TestDataBaseMapping.getOldRenewalDate_HCDatesTable(accountID), 'Old Renewal Date is not NULL')
    })

    it('Click on Continue Shopping button on Order Confirmation page', async function () {
      await orderConfirmationPage.clickContinueShopping()
    })

    it('Loads the Home page', async function () {
      await header.verifyMyAccountLinkIsLoaded()
      await mainNavBar.verifyPageIsLoaded()
    })

    it('Logout Coach', async function () {
      await header.verifyLogOutIsLoaded()
      await header.clickLogOut()
      await header.verifyPageIsLoaded()
    })

    afterEach(async function () {
      await driverutils.saveScreenshots(this.currentTest.state, screencapture)
      screencapture = false
    })
  })

  describe('C1452 Login to Hybris Backoffice as CSA to Verify QV/CV of Actual order with Additional food Product', function () {
    let testTitle = this.title

    before(async function () {
      await driverutils.goToHybrisBackofficeSitePage()
    })

    beforeEach(async function () {
      await driverutils.addAllureReport(allureReporter, __dirname, 'US OPTAVIA Sanity', testTitle)
    })

    it('Preparing Test Data', async function () {
      agentEmail = AGENT_USER
      agentPassword = GLOBAL_PASSWORD
    })

    it('Hybris Backoffice page is loaded ', async function () {
      hybrisBackofficeLoginPage = new HybrisBackofficeLoginPage()
      await hybrisBackofficeLoginPage.verifyPageIsLoaded()
    })

    it('Login as a CSA', async function () {
      await hybrisBackofficeLoginPage.loginAsCSR(agentEmail, agentPassword)
    })

    it('Backoffice Authority group page displays ', async function () {
      hybrisBackofficeAutorityGroupPage = new HybrisBackofficeAutorityGroupPage()
      await hybrisBackofficeAutorityGroupPage.verifyPageIsLoaded()
    })

    it('Click on Customer Support Admin Role radio button', async function () {
      await browser.pause(3000)
      await hybrisBackofficeAutorityGroupPage.proceedAsCSAdminRole()
      hybrisBackofficeHomePage = new HybrisBackofficeHomePage()
      await hybrisBackofficeHomePage.verifyPageIsLoaded()
    })

    it('Click on Orders link from Customer Admin section', async function () {
      await hybrisBackofficeHomePage.clickASMOrder()
    })

    it('Click on Order link from Customer Admin section', async function () {
      await browser.pause(3000)
      await hybrisBackofficeHomePage.clickOrder()
    })

    it('Search Textbox is loaded', async function () {
      hybrisBackofficeSearchPage = new HybrisBackofficeSearchPage()
      await hybrisBackofficeSearchPage.verifyPageIsLoaded()
    })

    it('Search the Renewal kit order number', async function () {
      await hybrisBackofficeSearchPage.searchOrder(actualOrderNumber)
    })

    it('Click on Search result', async function () {
      await hybrisBackofficeSearchPage.clickSearchedOrder()
    })

    it('Verify Order Details body title displays', async function () {
      hybrisBackofficeOrderDetailsPage = new HybrisBackofficeOrderDetailsPage()
      await hybrisBackofficeOrderDetailsPage.verifyPageIsLoaded(actualOrderNumber)
    })

    it('Click on Administration Report', async function () {
      await hybrisBackofficeOrderDetailsPage.clickAdminReportTab()
    })

    it('Verify the Order commissionable Volume is same as Order total', async function () {
      await browser.pause(3000)
      let expected = '24.25'
      let actual = await hybrisBackofficeOrderDetailsPage.getOrderCommissionalbleVolume()
      decimalLength = actual.split('.')
      assert.isTrue((decimalLength[1].length === 2), 'length is not 2')
      assert.strictEqual(actual, expected, `Order commissionable volume : ${expected} is not matched with Order Total`)
    })

    it('Verify the Qualifying Volume ', async function () {
      let expected = '21.00'
      let actual = await hybrisBackofficeOrderDetailsPage.getQualifyingVolume()
      decimalLength = actual.split('.')
      assert.isTrue((decimalLength[1].length === 2), 'length is not 2')
      assert.strictEqual(actual, expected, `Qualifying volume :${actual} is not matched with expected value`)
    })

    it('Click on Sign out button', async function () {
      await browser.pause(6000)
      await hybrisBackofficeHomePage.clickSignOut()
      await browser.pause(3000)
    })

    afterEach(async function () {
      await driverutils.saveScreenshots(this.currentTest.state, screencapture)
      screencapture = false
    })
  })

  // describe('Login to Hybris Backoffice as CSR to approve the order', function () {
  //   let testTitle = this.title

  //  before(async function () {
  //     driverBuilder = new DriverBuilder()
  //     driver = driverBuilder.driver
  //     await driverutils.goToHybrisBackofficeSitePage(driver)
  //   })
  //
  //   beforeEach(async function () {
  // await driverutils.addAllureReport(__dirname, 'US OPTAVIA Sanity', testTitle)
  //   })
  //
  //   it('Preparing Test Data', async function () {
  //     agentEmail = AGENT_USER
  //     agentPassword = GLOBAL_PASSWORD
  //   })
  //
  //   it('Hybris Backoffice page is loaded ', async function () {
  //     hybrisBackofficeLoginPage = new HybrisBackofficeLoginPage(driver)
  //     await hybrisBackofficeLoginPage.verifyPageIsLoaded()
  //   })
  //
  //   it('Login as a CSR', async function () {
  //     await hybrisBackofficeLoginPage.loginAsCSR(agentEmail, agentPassword)
  //   })
  //
  //   it('Backoffice Authority group page displays ', async function () {
  //     hybrisBackofficeAutorityGroupPage = new HybrisBackofficeAutorityGroupPage(driver)
  //     await hybrisBackofficeAutorityGroupPage.verifyPageIsLoaded()
  //   })
  //
  //   it('Click on Customer Support Agent Role radio button', async function () {
  //     await hybrisBackofficeAutorityGroupPage.proceedAsCSAgentRole()
  //     hybrisBackofficeHomePage = new HybrisBackofficeHomePage(driver)
  //     await hybrisBackofficeHomePage.verifyPageIsLoaded()
  //   })
  //
  //   it('Click on Session Context collapse arrow', async function () {
  //     await hybrisBackofficeHomePage.clickSessionContextCollapse()
  //   })
  //
  //   it('Click on Orders link from Customer Support section', async function () {
  //     await hybrisBackofficeHomePage.clickOrder()
  //   })
  //
  //   it('Click on Order All link from Customer Support section', async function () {
  //     await hybrisBackofficeHomePage.clickOrderAll()
  //   })
  //
  //   it('Search Textbox is loaded', async function () {
  //     hybrisBackofficeSearchPage = new HybrisBackofficeSearchPage(driver)
  //     hybrisBackofficeSearchPage.verifyPageIsLoaded()
  //   })
  //
  //   it('Search the Renewal kit order number', async function () {
  //     await hybrisBackofficeSearchPage.searchOrderNumber(orderNumber)
  //   })
  //
  //   it('Click on Search button', async function () {
  //     await hybrisBackofficeSearchPage.clickSearchBtn()
  //   })
  //
  //   it('Click on Search result', async function () {
  //     await hybrisBackofficeSearchPage.clickSearchResult(orderNumber)
  //   })
  //
  //   it('Verify Order Details body title displays', async function () {
  //     hybrisBackofficeOrderDetailsPage = new HybrisBackofficeOrderDetailsPage(driver)
  //     await hybrisBackofficeOrderDetailsPage.verifyPageIsLoaded(orderNumber)
  //   })
  //
  //   it('Click on Fraud Report', async function () {
  //     await hybrisBackofficeOrderDetailsPage.clickFraudReportTab()
  //   })
  //
  //   it('Click on Fraud Approved button', async function () {
  //     await hybrisBackofficeOrderDetailsPage.clickFraudActionButton()
  //     await browser.pause(6000)
  //   })
  //
  //   it('Search the Renewal kit order number', async function () {
  //     await hybrisBackofficeSearchPage.searchOrderNumber(actualOrderNumber)
  //   })
  //
  //   it('Click on Search button', async function () {
  //     await hybrisBackofficeSearchPage.clickSearchBtn()
  //   })
  //
  //   it('Click on Search result', async function () {
  //     await hybrisBackofficeSearchPage.clickSearchResult(actualOrderNumber)
  //   })
  //
  //   it('Verify Order Details body title displays', async function () {
  //     await hybrisBackofficeOrderDetailsPage.verifyPageIsLoaded(actualOrderNumber)
  //   })
  //
  //   it('Click on Fraud Approved button', async function () {
  //     await hybrisBackofficeOrderDetailsPage.clickFraudActionButton()
  //     await browser.pause(6000)
  //   })
  //
  //   it('Click on Sign out button', async function () {
  //     await browser.pause(6000)
  //     await hybrisBackofficeHomePage.clickSignOut()
  //   })
  //
  //   afterEach(async function () {
  //     await allure.addArgument('Page URL', await driver.getCurrentUrl())
  //     await allure.addArgument('Page Title', await driver.getTitle())
  //     await driverutils.saveScreenshots(this.currentTest.state, screencapture, driver)
  //     screencapture = false
  //   })
  //
  //   after(async function () {
  //     await driverBuilder.quit()
  //   })
  // })
  // // Original order is Renewal Kit so Qv/CV is 0
  describe('C1452 Login to Hybris Backoffice as CSA to Verify QV/CV of Actual order without any food product', function () {
    let testTitle = this.title

    before(async function () {
      await driverutils.goToHybrisBackofficeSitePage()
    })

    beforeEach(async function () {
      await driverutils.addAllureReport(allureReporter, __dirname, 'US OPTAVIA Sanity', testTitle)
    })

    it('Preparing Test Data', async function () {
      agentEmail = AGENT_USER
      agentPassword = GLOBAL_PASSWORD
    })

    it('Hybris Backoffice page is loaded ', async function () {
      hybrisBackofficeLoginPage = new HybrisBackofficeLoginPage()
      await hybrisBackofficeLoginPage.verifyPageIsLoaded()
    })

    it('Login as a CSR', async function () {
      await hybrisBackofficeLoginPage.loginAsCSR(agentEmail, agentPassword)
    })

    it('Backoffice Authority group page displays ', async function () {
      hybrisBackofficeAutorityGroupPage = new HybrisBackofficeAutorityGroupPage()
      await hybrisBackofficeAutorityGroupPage.verifyPageIsLoaded()
    })

    it('Click on Customer Support Agent Role radio button', async function () {
      await browser.pause(3000)
      await hybrisBackofficeAutorityGroupPage.proceedAsCSAdminRole()
      hybrisBackofficeHomePage = new HybrisBackofficeHomePage()
      await hybrisBackofficeHomePage.verifyPageIsLoaded()
    })

    it('Click on Orders link from Customer Admin section', async function () {
      await hybrisBackofficeHomePage.clickASMOrder()
    })

    it('Click on Order link from Customer Admin section', async function () {
      await browser.pause(3000)
      await hybrisBackofficeHomePage.clickOrder()
    })

    it('Search Textbox is loaded', async function () {
      hybrisBackofficeSearchPage = new HybrisBackofficeSearchPage()
      await hybrisBackofficeSearchPage.verifyPageIsLoaded()
    })

    it('Search the Renewal kit order number', async function () {
      await hybrisBackofficeSearchPage.searchOrder(orderNumber)
    })

    it('Click on Search result', async function () {
      await hybrisBackofficeSearchPage.clickSearchedOrder()
    })

    it('Verify Order Details body title displays', async function () {
      hybrisBackofficeOrderDetailsPage = new HybrisBackofficeOrderDetailsPage()
      await hybrisBackofficeOrderDetailsPage.verifyPageIsLoaded(orderNumber)
    })

    it('Click on Administration Report', async function () {
      await hybrisBackofficeOrderDetailsPage.clickAdminReportTab()
    })

    it('Verify the Order commissionable Volume is same as Order total', async function () {
      await browser.pause(3000)
      let expected = '0.00'
      let actual = await hybrisBackofficeOrderDetailsPage.getOrderCommissionalbleVolume()
      decimalLength = actual.split('.')
      assert.isTrue((decimalLength[1].length === 2), 'length is not 2')
      assert.strictEqual(actual, expected, `Order commissionable volume :${expected} is not matched with Order Total`)
    })

    it('Verify the Qualifying Volume ', async function () {
      let expected = '0.00'
      let actual = await hybrisBackofficeOrderDetailsPage.getQualifyingVolume()
      decimalLength = actual.split('.')
      assert.isTrue((decimalLength[1].length === 2), 'length is not 2')
      assert.strictEqual(actual, expected, `Qualifying volume :${actual} is not matched with expected value`)
    })

    it('Click on Sign out button', async function () {
      await browser.pause(6000)
      await hybrisBackofficeHomePage.clickSignOut()
      await browser.pause(3000)
    })

    afterEach(async function () {
      await driverutils.saveScreenshots(this.currentTest.state, screencapture)
      screencapture = false
    })
  })

  describe('C1452 Look up syncSQL database for Auto Order ID and QV,CV Values', function () {
    it('Get the Auto order ID from From DB', async function () {
      let db = await TestDataBaseMapping.getAutoOrderHeaderView(accountID)
      orderNumber = db.Auto_Order_ID
    })

    it('Get the Commissionable_Volume from From DB', async function () {
      let db = await TestDataBaseMapping.getAutoOrderHeaderView(accountID)
      commissonableVolume = db.Commissionable_Volume
      let expected = 23.04
      assert.strictEqual(commissonableVolume, expected, `Commissionable_Volume from From DB is not matching with expected : ${expected}`)
    })

    it('Get the Qualifying_Volume from From DB', async function () {
      let db = await TestDataBaseMapping.getAutoOrderHeaderView(accountID)
      qualifyingvolume = db.Qualifying_Volume
      let expected = 19.95
      assert.strictEqual(qualifyingvolume, expected, `Qualifying_Volume from From DB is not matching with expected : ${expected}`)
    })
  })

  describe('C1452 Login to Hybris Backoffice as CSA to verify template order', function () {
    let testTitle = this.title

    before(async function () {
      await driverutils.goToHybrisBackofficeSitePage()
    })

    beforeEach(async function () {
      await driverutils.addAllureReport(allureReporter, __dirname, 'US OPTAVIA Sanity', testTitle)
    })

    it('Preparing Test Data', async function () {
      agentEmail = AGENT_USER
      agentPassword = GLOBAL_PASSWORD
    })

    it('Hybris Backoffice page is loaded ', async function () {
      hybrisBackofficeLoginPage = new HybrisBackofficeLoginPage()
      await hybrisBackofficeLoginPage.verifyPageIsLoaded()
    })

    it('Login as a CSR', async function () {
      await hybrisBackofficeLoginPage.loginAsCSR(agentEmail, agentPassword)
    })

    it('Backoffice Authority group page displays ', async function () {
      hybrisBackofficeAutorityGroupPage = new HybrisBackofficeAutorityGroupPage()
      await hybrisBackofficeAutorityGroupPage.verifyPageIsLoaded()
    })

    it('Click on Customer Support Agent Role radio button', async function () {
      await browser.pause(3000)
      await hybrisBackofficeAutorityGroupPage.proceedAsCSAdminRole()
      hybrisBackofficeHomePage = new HybrisBackofficeHomePage()
      await hybrisBackofficeHomePage.verifyPageIsLoaded()
    })

    it('Click on Orders link from Customer Admin section', async function () {
      await hybrisBackofficeHomePage.clickASMOrder()
    })

    it('Click on Order link from Customer Admin section', async function () {
      await browser.pause(3000)
      await hybrisBackofficeHomePage.clickOrder()
    })

    it('Search Textbox is loaded', async function () {
      hybrisBackofficeSearchPage = new HybrisBackofficeSearchPage()
      hybrisBackofficeSearchPage.verifyPageIsLoaded()
    })

    it('Search the Renewal kit order number', async function () {
      await hybrisBackofficeSearchPage.searchOrder(orderNumber)
    })

    it('Click on Search result', async function () {
      await hybrisBackofficeSearchPage.clickSearchedOrder()
    })

    it('Verify Order Details body title displays', async function () {
      hybrisBackofficeOrderDetailsPage = new HybrisBackofficeOrderDetailsPage()
      await hybrisBackofficeOrderDetailsPage.verifyPageIsLoaded(orderNumber)
    })

    it('Click on Administration Report', async function () {
      await hybrisBackofficeOrderDetailsPage.clickAdminReportTab()
    })

    it('Verify the Order commissionable Volume is same as Order total', async function () {
      await browser.pause(3000)
      let expected = '23.04'
      let actual = await hybrisBackofficeOrderDetailsPage.getOrderCommissionalbleVolume()
      decimalLength = actual.split('.')
      assert.isTrue((decimalLength[1].length === 2), 'length is not 2')
      assert.strictEqual(actual, expected, `Order commissionable volume : ${expected} is not matched with Order Total`)
    })

    it('Verify the Qualifying Volume ', async function () {
      let expected = '19.95'
      let actual = await hybrisBackofficeOrderDetailsPage.getQualifyingVolume()
      decimalLength = actual.split('.')
      assert.isTrue((decimalLength[1].length === 2), 'length is not 2')
      assert.strictEqual(actual, expected, `Qualifying volume : ${actual} is not matched with expected value`)
    })

    it('Click on Sign out button', async function () {
      await browser.pause(6000)
      await hybrisBackofficeHomePage.clickSignOut()
    })

    afterEach(async function () {
      await driverutils.saveScreenshots(this.currentTest.state, screencapture)
      screencapture = false
    })
  })

  describe('C1452 Look up CUSTOM.Health_Coach_Dates table for Renewal date after buying renewal kit', function () {
    it('Verify New Renewal Date is displayed as 365 days from order creation date in DB', async function () {
      await browser.pause(60000) // Wait a minute approx. for the record to reach DB
      let actual = await CommonUtils.getRenewalDateFormat(await TestDataBaseMapping.getRenewalDate_HCDatesTable(accountID))
      let expected = await CommonUtils.getRenewalDate(await TestDataBaseMapping.getOldRenewalDate_HCDatesTable(accountID))
      assert.strictEqual(actual, expected, `New Renewal Date : ${actual} is not 365 days from order creation date in DB`)
    })

    it('Verify Old Renewal Date is displayed in DB', async function () {
      let actual = await CommonUtils.getRenewalDateFormat(await TestDataBaseMapping.getOldRenewalDate_HCDatesTable(accountID))
      let expected = renewalDate
      assert.strictEqual(actual, expected, `Old Renewal Date :${expected} is not matching with DB value ${actual}`)
    })
  })
})
