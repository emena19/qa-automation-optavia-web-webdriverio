// @flow
import {assert} from 'chai'
import driverutils from '@core-libs/DriverUtils'
import CustomerTestData from '@input-data/customers/CustomerTestData'
import ProductTestData, {attb, product} from '@input-data/products/ProductTestData'
import HomePage from '@page-objects/home/HomePage'
import Header from '@page-objects/common/Header'
import MainNavBar from '@page-objects/common/MainNavBar'
import LoginPage from '@page-objects/loginPage/LoginPage'
import MyAccountPage from '@page-objects/myaccount/MyAccountPage'
import MyAccountLeftNavigation from '@page-objects/myaccount/MyAccountLeftNavigation'
import OptaviaPremierOrderPage from '@page-objects/myaccount/OptaviaPremierOrderPage'
import DatePickerOptaviaPremier from '@page-objects/myaccount/DatePickerOptaviaPremier'
import ChooseYourOwnFuelingsPopup from '@page-objects/chooseyourownfuelings/ChooseYourOwnFuelingsPopup'
import OrderConfirmationPage from '@page-objects/orderconfirmation/OrderConfirmationPage'
import OrderDetailsPage from '@page-objects/myaccount/OrderDetailsPage'
import TestDataBaseMapping from '@db/TestDataBaseMapping'
import ShoppingCartPage from '@page-objects/shoppingcart/ShoppingCartPage'
import CommonUtils from '@core-libs/CommonUtils'
import allureReporter from '@wdio/allure-reporter'

describe('C1453 - OPTAVIA - My Account - OPTAVIA Premier Order - Create OPTAVIA Premier Order By Clicking Process Now', function () {
  let screencapture = false // True to capture screenshot when test step 'passed'. By default only capture screenshot of failed steps
  let homePage, header, loginPage, myAccountPage, optaviaPremierOrderPage, orderConfirmationPage,
    myAccountLeftNavigation, orderDetailsPage,
    shoppingCartPage, mainNavBar, addItemsToCartPopup, datePickerOptaviaPremier

  // Customer Registration
  let email, password
  let orderDate, lastOrderNumber, orderNumber, productSKU

  describe('C1453 - OPTAVIA - My Account - OPTAVIA Premier Order - Create OPTAVIA Premier Order By Clicking Process Now', function () {
    let testTitle = this.title

    before(async function () {
      await driverutils.goToOptaviaWebSiteHomePage()
    })

    beforeEach(async function () {
      await driverutils.addAllureReport(allureReporter, __dirname, 'US OPTAVIA Sanity', testTitle)
    })

    it('Preparing Test Data', async function () {
      let custID = '30214040'
      let TestData = await CustomerTestData.getCustomerInfo(custID)
      productSKU = await ProductTestData.PRODUCT_INFO(attb.SKU, product.bars.OPT_CRAN_NUT_BAR)
      email = TestData.CUSTOMER_EMAIL
      password = TestData.CUSTOMER_PASSWORD
    })

    it('Loads the Home Page', async function () {
      homePage = new HomePage()
      await homePage.verifyPageIsLoaded()
      header = new Header()
      await header.verifyPageIsLoaded()
      mainNavBar = new MainNavBar()
    })

    it('Navigate to Login and Register Page', async function () {
      await header.clickLogIn()
      loginPage = new LoginPage()
      await loginPage.verifyPageIsLoaded()
    })

    it('Login as existing Customer', async function () {
      screencapture = true
      await allureReporter.addDescription(`Login as existing Customer email: ${email} and password: ${password}`)
      await loginPage.loginAsExistingCustomer(email, password)
    })

    it('Verify My Account page is loaded', async function () {
      screencapture = true
      myAccountPage = new MyAccountPage()
      await myAccountPage.verifyPageIsLoaded()
      myAccountLeftNavigation = new MyAccountLeftNavigation()
      await myAccountLeftNavigation.verifyPageIsLoaded()
    })

    it('OPTAVIA Premier Order link displays', async function () {
      screencapture = true
      let actual = await myAccountLeftNavigation.isOptaviaPremierLinkPresent()
      assert.isTrue(actual, 'OPTAVIA Premier link is missing')
    })

    it('Get the last order number', async function () {
      lastOrderNumber = await myAccountPage.getLastOrderNumber()
      await allureReporter.addDescription(`The last order number is ${lastOrderNumber}`)
    })

    it('Navigate to Optavia Premier Order page', async function () {
      await myAccountLeftNavigation.clickOptaviaPremierOrder()
    })

    it('Loads the Subscriptions page', async function () {
      optaviaPremierOrderPage = new OptaviaPremierOrderPage()
      await optaviaPremierOrderPage.verifyPageIsLoaded()
    })

    it('Click on Modify Order button', async function () {
      await optaviaPremierOrderPage.clickModifyOrderBtn()
    })

    it('Update the autoship date if the Calendar popup window is loaded', async function () {
      datePickerOptaviaPremier = new DatePickerOptaviaPremier()
      if (await datePickerOptaviaPremier.isUpdateBtnDisplayed()) {
        await datePickerOptaviaPremier.clickDatePickerPastAutoshipUpdateBtn()
      }
    })
    it('OPTAVIA Premier Shopping Cart page is loaded', async function () {
      screencapture = true
      shoppingCartPage = new ShoppingCartPage()
      await shoppingCartPage.verifyPageIsLoaded()
    })

    it('Verify message displays on top', async function () {
      let actual = await shoppingCartPage.getSubscriptionEditMessageText()
      let expected = 'You are now editing your OPTAVIA Premier order.'
      assert.strictEqual(actual, expected, 'Message is not displayed on top')
    })

    it('Navigate to Choose Your Own Fuelings', async function () {
      screencapture = true
      await mainNavBar.clickChooseYourOwnFuelings()
      addItemsToCartPopup = new ChooseYourOwnFuelingsPopup()
      await addItemsToCartPopup.verifyPageIsLoaded()
    })

    it('Search and select searched product', async function () {
      screencapture = true
      await allureReporter.addDescription(`Searched product is ${productSKU}`)
      await addItemsToCartPopup.searchForProduct(productSKU)
      await addItemsToCartPopup.selectProduct()
      await addItemsToCartPopup.addItemsToCartButton()
    })

    it('Add searched product to Shopping Cart', async function () {
      screencapture = true
      await addItemsToCartPopup.clickAddItemsToCart()
      await shoppingCartPage.verifyPageIsLoaded()
    })

    it('Save the Subscription template', async function () {
      await shoppingCartPage.clickSaveSubscriptionBtn()
    })

    it('Verify Process Now button is enabled', async function () {
      let actual = await optaviaPremierOrderPage.getProcessNowBtnClassAttributeValue()
      assert.notInclude(actual, 'disabled', 'Process Now button is disabled')
    })

    it('Click on Process Now button', async function () {
      await optaviaPremierOrderPage.clickProcessNowBtn()
    })

    it('Verify Process Now popup displays', async function () {
      screencapture = true
      await optaviaPremierOrderPage.verifyProcessNowPopUpIsLoaded()
    })

    it('Click cancel button from Process Now popup', async function () {
      await optaviaPremierOrderPage.clickProcessNowPopUpCancelBtn()
    })

    it('Verify Subscriptions page displays', async function () {
      await optaviaPremierOrderPage.verifyPageIsLoaded()
    })

    it('Click on My Account Dashboard', async function () {
      await myAccountLeftNavigation.clickAccountDashboard()
      await myAccountPage.verifyPageIsLoaded()
    })

    it('Verify an Autoship order was not created', async function () {
      screencapture = true
      await allureReporter.addDescription(`Expected last order is ${lastOrderNumber}`)
      let actual = await myAccountPage.getLastOrderNumber()
      let expected = lastOrderNumber
      assert.strictEqual(actual, expected, `Autoship order Actual: ${actual} does not match expected: ${expected}`)
    })

    it('Navigate to Optavia Premier Order page', async function () {
      await myAccountLeftNavigation.clickOptaviaPremierOrder()
    })

    it('Click on Process Now button', async function () {
      await optaviaPremierOrderPage.clickProcessNowBtn()
    })

    it('Verify Process Now popup displays', async function () {
      screencapture = true
      await optaviaPremierOrderPage.verifyProcessNowPopUpIsLoaded()
    })

    it('Click Process Now button from Process Now popup', async function () {
      screencapture = true
      await browser.pause(2000)
      await optaviaPremierOrderPage.clickProcessNowPopUpProcessNowBtn()
      orderDate = await CommonUtils.getNextShippingDate()
    })

    it('Loads the Order Confirmation page', async function () {
      orderConfirmationPage = new OrderConfirmationPage()
      await orderConfirmationPage.verifyPageIsLoaded()
      let actual = await orderConfirmationPage.getOrderNumber()
      let resultArray = actual.split(' ')
      orderNumber = resultArray[4]
    })

    it('Click on Continue Shopping button on Order Confirmation page', async function () {
      await orderConfirmationPage.clickContinueShopping()
    })

    it('Click on My Account link from header', async function () {
      await header.clickMyAccount()
    })

    it('My Account Dashboard page is loaded', async function () {
      await myAccountPage.verifyPageIsLoaded()
      await myAccountLeftNavigation.verifyPageIsLoaded()
    })

    it('Verify Next order date is valid', async function () {
      let expected = await orderDate
      let actual = await myAccountPage.getOptaviaNextOrderDate()
      assert.strictEqual(actual, expected, `Next order date Actual: ${actual} does not match expected: ${expected}`)
    })

    it('Verify recently placed order is displayed in order history', async function () {
      screencapture = true
      let actual = await myAccountPage.getLastOrderNumber()
      let expected = orderNumber
      await allureReporter.addDescription(`Expected last order is ${expected}`)
      assert.strictEqual(actual, expected, `Recently placed order : ${expected} is not displayed in order history`)
    })

    it('Click on Last Order Number link', async function () {
      await myAccountPage.clickLastOrderNumberLink()
    })

    it('Loads the Order Details page', async function () {
      orderDetailsPage = new OrderDetailsPage()
      await orderDetailsPage.verifyPageIsLoaded()
    })

    it('Verify order details page displays for the order created above', async function () {
      let expected = `Order # ${orderNumber}`
      let actual = await orderDetailsPage.getOrderNumber()
      assert.strictEqual(actual, expected, `${expected} is not displayed in order details page`)
    })

    it('Verify the order type is Immediate Autoship', async function () {
      screencapture = true
      let expected = 'Type: Immediate_Autoship'
      let actual = await orderDetailsPage.getOrderType()
      assert.strictEqual(actual.trim(), expected, 'Order type is not Immediate Autoship')
    })

    it('Click on My Account link from header', async function () {
      await header.clickMyAccount()
    })

    it('Verify Cancel Order link displays', async function () {
      screencapture = true
      await allureReporter.addDescription('Verify My Account Dashboard displays the link to Cancel Order')
      let actual = await myAccountPage.getCancelOrderText()
      let expected = 'Cancel Order'
      assert.strictEqual(actual, expected, 'Cancel Order link is not displays')
    })

    // Process now to create second autoship order
    it('Navigate to Optavia Premier Order page', async function () {
      await myAccountLeftNavigation.clickOptaviaPremierOrder()
    })

    it('Click on Process Now button', async function () {
      await optaviaPremierOrderPage.clickProcessNowBtn()
    })

    it('Verify Process Now popup displays', async function () {
      screencapture = true
      await optaviaPremierOrderPage.verifyProcessNowPopUpIsLoaded()
    })

    it('Click Process Now button from Process Now popup', async function () {
      screencapture = true
      await optaviaPremierOrderPage.clickProcessNowPopUpProcessNowBtn()
      // orderDate = await CommonUtils.getNextShippingDate()
    })

    it('Loads the Order Confirmation page', async function () {
      orderConfirmationPage = new OrderConfirmationPage()
      await orderConfirmationPage.verifyPageIsLoaded()
      let actual = await orderConfirmationPage.getOrderNumber()
      let resultArray = actual.split(' ')
      orderNumber = resultArray[4]
    })

    it('Click on Continue Shopping button on Order Confirmation page', async function () {
      await orderConfirmationPage.clickContinueShopping()
    })

    it('Click on My Account link from header', async function () {
      await header.clickMyAccount()
    })

    it('Verify Next order date is valid', async function () {
      let expected = await orderDate
      let actual = await myAccountPage.getOptaviaNextOrderDate()
      assert.strictEqual(actual, expected, `Next order date Actual: ${actual} does not match expected: ${expected}`)
      await allureReporter.addDescription(`The Next Order Date should stay the same as ${orderDate}`)
    })

    // Temporal step, remove it when DB is syncing fast
    it('Wait for 2.5 minutes for DB to sync', async function () {
      await browser.pause(150000)
    })

    it('Verify Order ID in DB', async function () {
      let expected = orderNumber
      let actual = await TestDataBaseMapping.getOrderID(orderNumber)
      assert.strictEqual(actual, expected, `In DB Order ID: ${actual} does not match expected: ${expected}`)
    })

    afterEach(async function () {
      await driverutils.saveScreenshots(this.currentTest.state, screencapture)
      screencapture = false
    })
  })
})
/* Commented since fraud approval is not working

    describe('Login to Hybris to Approve order', function () {
      let testTitle = this.title

    before(async function () {
        driverBuilder = new DriverBuilder()
        driver = driverBuilder.driver
        await driverutils.goToHybrisBackofficeSitePage(driver)
      })

      beforeEach(async function () {
      await driverutils.addAllureReport(__dirname, 'US ASM Sanity', testTitle)
      })

      it('Preparing Test Data', async function () {
        agentEmail = AGENT_USER
        agentPassword = GLOBAL_PASSWORD
      })

      it('Hybris Backoffice page is loaded ', async function () {
        hybrisBackofficeLoginPage = new HybrisBackofficeLoginPage(driver)
        await hybrisBackofficeLoginPage.verifyPageIsLoaded()
      })

      it('Login to Hybris', async function () {
        await hybrisBackofficeLoginPage.loginAsCSR(agentEmail, agentPassword)
      })

      it('Backoffice Authority group page displays ', async function () {
        hybrisBackofficeAutorityGroupPage = new HybrisBackofficeAutorityGroupPage(driver)
        await hybrisBackofficeAutorityGroupPage.verifyPageIsLoaded()
      })

      it('Process as CSM Manager', async function () {
        await hybrisBackofficeAutorityGroupPage.proceedAsCsmRole()
        hybrisBackofficeCSMHomePage = new HybrisBackofficeCSMHomePage(driver)
        await hybrisBackofficeCSMHomePage.verifyCSIsLoaded()
      })

      it('Click on Orders ', async function () {
        await hybrisBackofficeCSMHomePage.clickOrder()
      })

      it('Loads Search page', async function () {
        hybrisBackOfficeSearchPage = new HybrisBackofficeSearchPage(driver)
        await hybrisBackOfficeSearchPage.verifyPageIsLoaded()
      })

      it('Search for Existing Order ', async function () {
        await driver.sleep(10000)
        hybrisBackofficeCustomerPage = new HybrisBackofficeCustomerPage(driver)
        await hybrisBackofficeCustomerPage.searchOrderIDOnCSR(orderNumber)
      })

      it('Click on first registry ', async function () {
        await driver.sleep(10000)
        await hybrisBackofficeCustomerPage.clickOnFirstRegistry()
      })

      it('Loads Order Page', async function () {
        await driver.sleep(10000)
        hybrisBackOfficeOrderDetailsPage = new HybrisBackofficeOrderDetailsPage(driver)
        await hybrisBackOfficeOrderDetailsPage.verifyPageIsLoaded(orderNumber)
      })

      it('Clicks on fraud report', async function () {
        await hybrisBackOfficeOrderDetailsPage.clickFraudReportTab()
      })

      it('Click on Process Now button', async function () {
        await optaviaPremierOrderPage.clickProcessNowBtn()
      })

      it('Verify Process Now popup displays', async function () {
        screencapture = true
        await optaviaPremierOrderPage.verifyProcessNowPopUpIsLoaded()
      })

      it('Click Process Now button from Process Now popup', async function () {
        screencapture = true
        await optaviaPremierOrderPage.clickProcessNowPopUpProcessNowBtn()
        orderDate = await CommonUtils.getNextShippingDate()
      })

      it('Loads the Order Confirmation page', async function () {
        orderConfirmationPage = new OrderConfirmationPage(driver)
        await orderConfirmationPage.verifyPageIsLoaded()
        let actual = await orderConfirmationPage.getOrderNumber()
        let resultArray = actual.split(' ')
        orderNumber = await resultArray[4]
      })

      it('Click on Continue Shopping button on Order Confirmation page', async function () {
        await orderConfirmationPage.clickContinueShopping()
      })

      it('Click on My Account link from header', async function () {
        await header.clickMyAccount()
      })

      it('My Account Dashboard page is loaded', async function () {
        await myAccountPage.verifyPageIsLoaded()
        await myAccountLeftNavigation.verifyPageIsLoaded()
      })

      it('Verify Next order date is valid', async function () {
        let expected = await orderDate
        let actual = await myAccountPage.getOptaviaNextOrderDate()
        assert.strictEqual(actual, expected, 'Actual should match expected tagline')
      })

      it('Verify recently placed order is displayed in order history', async function () {
        screencapture = true
        let actual = await myAccountPage.getLastOrderNumber()
        let expected = orderNumber
        await allureReporter.addDescription(`Expected last order is ${expected}`)
        assert.strictEqual(actual, expected, 'Actual should not include expected tagline')
      })

      it('Click on Last Order Number link', async function () {
        await myAccountPage.clickLastOrderNumberLink()
      })

      it('Loads the Order Details page', async function () {
        orderDetailsPage = new OrderDetailsPage(driver)
        await orderDetailsPage.verifyPageIsLoaded()
      })

      it('Verify order details page displays for the order created above', async function () {
        let expected = `Order # ${orderNumber}`
        let actual = await orderDetailsPage.getOrderNumber()
        assert.strictEqual(actual, expected, 'Actual should not include expected tagline')
      })

      it('Verify the order type is Immediate Autoship', async function () {
        screencapture = true
        let expected = 'Order type is Immediate_Autoship'
        let actual = await orderDetailsPage.getOrderType()
        assert.strictEqual(actual, expected, 'Actual should match expected tagline')
      })

      it('Click on My Account link from header', async function () {
        await header.clickMyAccount()
      })

      it('Verify Cancel Order link displays', async function () {
        screencapture = true
        await allureReporter.addDescription(`Verify My Account Dashboard displays the link to Cancel Order`)
        let actual = await myAccountPage.getCancelOrderText()
        let expected = 'Cancel Order'
        assert.strictEqual(actual, expected, 'Actual should match expected tagline')
      })

      // Process now to create second autoship order
      it('Navigate to Optavia Premier Order page', async function () {
        await myAccountLeftNavigation.clickOptaviaPremierOrder()
      })

      it('Click on Process Now button', async function () {
        await optaviaPremierOrderPage.clickProcessNowBtn()
      })

      it('Verify Process Now popup displays', async function () {
        screencapture = true
        await optaviaPremierOrderPage.verifyProcessNowPopUpIsLoaded()
      })

      it('Click Process Now button from Process Now popup', async function () {
        screencapture = true
        await optaviaPremierOrderPage.clickProcessNowPopUpProcessNowBtn()
        // orderDate = await CommonUtils.getNextShippingDate()
      })

      it('Loads the Order Confirmation page', async function () {
        orderConfirmationPage = new OrderConfirmationPage(driver)
        await orderConfirmationPage.verifyPageIsLoaded()
        let actual = await orderConfirmationPage.getOrderNumber()
        let resultArray = actual.split(' ')
        orderNumber = await resultArray[4]
      })

      // Temporal step, remove it when DB is syncing fast
      it('Wait for 2.5 minutes for DB to sync', async function () {
        await driver.sleep(150000)
      })

      it('Verify Order ID in DB', async function () {
        let expected = orderNumber
        let actual = await TestDataBaseMapping.getOrderID(orderNumber)
        assert.strictEqual(actual, expected, 'Actual should match expected tagline')
      })

      it('Click on Continue Shopping button on Order Confirmation page', async function () {
        await orderConfirmationPage.clickContinueShopping()
      })

      it('Click on My Account link from header', async function () {
        await header.clickMyAccount()
      })

      it('Verify Next order date is valid', async function () {
        let expected = await orderDate
        let actual = await myAccountPage.getOptaviaNextOrderDate()
        assert.strictEqual(actual, expected, 'Actual should match expected tagline')
        await allureReporter.addDescription(`The Next Order Date should stay the same as ${orderDate}`)

    })

      it('Click on Thumbs Up icon to approve the order', async function () {
        await hybrisBackOfficeOrderDetailsPage.clickFraudActionButton()
      })

    afterEach(async function () {
      await driverutils.saveScreenshots(this.currentTest.state, screencapture, driver)
      screencapture = false
    })

      after(async function () {
        await driverBuilder.quit()
      })
  })

     */
