// @flow
import {assert} from 'chai'

import driverutils from '@core-libs/DriverUtils'
import CustomerTestData from '@input-data/customers/CustomerTestData'
import CoachTestData from '@input-data/coaches/CoachTestData'
import ProductTestData, {attb, market, product, productUOM} from '@input-data/products/ProductTestData'
import TestCreditCards from '@input-data/creditcards/CreditCardTestData'
import HomePage from '@page-objects/home/HomePage'
import Header from '@page-objects/common/Header'
import MainNavBar from '@page-objects/common/MainNavBar'
import ShopAllPage from '@page-objects/shop/ShopAllPage'
import EssentialFuelingsPage from '@page-objects/shop/EssentialFuelingsPage'
import ProductDetailsPage from '@page-objects/shop/ProductDetailsPage'
import AddedToYourShoppingCartPopup from '@page-objects/shop/AddedToYourShoppingCartPopup'
import ShoppingCartPage from '@page-objects/shoppingcart/ShoppingCartPage'
import ShopPage from '@page-objects/shop/ShopPage'
import CommonUtils from '@core-libs/CommonUtils'
import FindACoachPage from '@page-objects/findacoach/FindACoachPage'
import SearchCoachResultsPage from '@page-objects/searchcoachresults/SearchCoachResultsPage'
import SiteMapTestData, {page} from '@input-data/various/MiscTestData'
import CreateAccountPage from '@page-objects/createAccountPage/CreateAccountPage'
import {GLOBAL_PASSWORD} from '@core-libs/ConstantUtil'
import CheckoutPage from '@page-objects/checkout/CheckoutPage'
import ShippingAddressTile from '@page-objects/checkout/ShippingAddressTile'
import PopupSuggestedDeliveryAddressesForm
  from '@page-objects/popupsuggesteddeliveryaddressesform/PopupSuggestedDeliveryAddressesForm'
import ShippingMethodTile from '@page-objects/checkout/ShippingMethodTile'
import PaymentAndBillingAddressTile from '@page-objects/checkout/PaymentAndBillingAddressTile'
import FinalReviewTile from '@page-objects/checkout/FinalReviewTile'
import RewardEarningsPopUp from '@page-objects/popuprewardearnings/RewardEarningsPopUp'
import OrderConfirmationPage from '@page-objects/orderconfirmation/OrderConfirmationPage'
import MyAccountLeftNavigation from '@page-objects/myaccount/MyAccountLeftNavigation'
import MyAccountPage from '@page-objects/myaccount/MyAccountPage'
import OrderDetailsPage from '@page-objects/myaccount/OrderDetailsPage'
import allureReporter from '@wdio/allure-reporter'

let screencapture = false // capture screenshot when test step 'passed' also by default only capture screenshot of failed steps

let homePage, header, mainNavBar, addedToYourShoppingCartPopup, shoppingCartPage, createAccountPage, findACoachPage, searchCoachResultsPage
let checkoutPage, shippingAddressTile, paymentAndBillingAddressTile, finalReviewTile, orderConfirmationPage, myAccountPage, popupSuggestedDeliveryAddressesForm
let productDetailsPage, shopPage, rewardsPopup, orderDetailsPage, myAccountLeftNavigation, shippingMethodTile, customerInfo, eligibleForNewRewards

let orderNumber

// Customer Registration
let TestData

// Coach (Enroller) Info
let coachFirstName, coachLastName, coachID

// Payment Details
let nameOnCard, cardNumber, cvv, month, year, cardType

// Shop Page
let shopAllPage, essentialFuelingsPage

// Shopping Cart Page
let orderSubTotal, orderTotal, earnedRewards, habitsOfHealth

// Product
let productUnitPriceArray, productList, productNamesArray, tax, productUnitPriceSum

describe('C1449 - US OPTAVIA - Add OPTAVIA Premier Customer and Order', function () {
  let testTitle = this.title

  before(async function () {
    await driverutils.goToOptaviaWebSiteHomePage(driver)
  })

  beforeEach(async function () {
    await driverutils.addAllureReport(allureReporter, __dirname, 'US OPTAVIA Sanity', testTitle)
  })

  it('Preparing Test Data', async function () {
    TestData = await CustomerTestData.getCustomerInfo('90000000013')
    // Customer Registration
    customerInfo = await CustomerTestData.getNewCustomer()

    // Coach Information
    coachFirstName = await CoachTestData.COACH_FIRST_NAME(2)
    coachLastName = await CoachTestData.COACH_LAST_NAME(2)
    coachID = await CoachTestData.COACH_ID(2)
    // Products info
    productList = [
      {prod: product.bars.OPT_CRAN_NUT_BAR},
      {prod: product.kits.OPT_OPTIMAL_KIT_5_1},
      {prod: product.tools_accessories.OPT_HABITS_HEALTH_SYSTEM}
    ]

    productUnitPriceArray = await ProductTestData.PRODUCT_LIST_INFO(attb.UNIT_PRICE, productList, market.US)
    productNamesArray = await ProductTestData.PRODUCT_LIST_INFO(attb.NAME, productList, market.US)
    habitsOfHealth = await ProductTestData.PRODUCT_INFO(attb.UNIT_PRICE, product.tools_accessories.OPT_HABITS_HEALTH_SYSTEM, market.US)
    productUnitPriceSum = productUnitPriceArray.reduce((a, b) => a + b, 0)
    orderSubTotal = productUnitPriceSum.toFixed(2) - 4.95 // HoH Discount

    // Calculations
    earnedRewards = (productUnitPriceSum - habitsOfHealth) * 0.1
    tax = 3
    orderTotal = (orderSubTotal + tax)
    eligibleForNewRewards = (productUnitPriceSum - habitsOfHealth)

    // Payment Information (for VISA = B, Mastercard = C, American Express = D and Discover = E) column of sheet 2 in excel file
    let ccCol = 'B'
    let sheet = 1
    nameOnCard = customerInfo.CUSTOMER_FIRST_NAME + ' ' + customerInfo.CUSTOMER_LAST_NAME
    cardType = await TestCreditCards.CARD_TYPE(sheet, ccCol)
    cardNumber = await TestCreditCards.CARD_NUMBER(sheet, ccCol)
    cvv = await TestCreditCards.CVV(sheet, ccCol)
    month = await TestCreditCards.EXPIRATION_MONTH(sheet, ccCol)
    year = await TestCreditCards.EXPIRATION_YEAR(sheet, ccCol)
  })

  it('Loads the Home Page', async function () {
    homePage = new HomePage()
    await homePage.verifyPageIsLoaded()
    header = new Header()
    await header.verifyPageIsLoaded()
    mainNavBar = new MainNavBar()
    await mainNavBar.verifyPageIsLoaded()
  })

  it('Loads Shop All Page', async function () {
    await mainNavBar.clickShopAll()
    shopAllPage = new ShopAllPage()
    await shopAllPage.verifyPageIsLoaded()
  })

  it('Click on Shop Button under Essentials Fuelings', async function () {
    await shopAllPage.clickEssentialFuelingsShopBtn()
  })

  it('Loads Select Fuelings Page', async function () {
    essentialFuelingsPage = new EssentialFuelingsPage()
    await essentialFuelingsPage.verifyPageIsLoaded()
  })

  it('Check on Bars Checkbox', async function () {
    await essentialFuelingsPage.checkBarsCheckbox()
  })

  it('Click the first product on the page', async function () {
    await essentialFuelingsPage.clickOnFirstProductImage()
  })

  it('Loads the product Detail Page', async function () {
    productDetailsPage = new ProductDetailsPage()
    await productDetailsPage.verifyPageIsLoaded()
  })

  it('Click Add to cart button', async function () {
    await productDetailsPage.clickAddToCartBtn()
  })

  it('Loads the Added To Your Shopping Cart Popup', async function () {
    addedToYourShoppingCartPopup = new AddedToYourShoppingCartPopup()
    await addedToYourShoppingCartPopup.verifyPageIsLoaded()
  })

  it('Click on Checkout button on pop up window', async function () {
    await addedToYourShoppingCartPopup.clickCheckoutButton()
  })

  it('Loads the Shopping Cart Page', async function () {
    shoppingCartPage = new ShoppingCartPage()
    await shoppingCartPage.verifyPageIsLoaded()
  })

  it('Verify Join Premier checkbox is NOT checked', async function () {
    screencapture = true
    if (await shoppingCartPage.isJoinPremierChecked()) {
      await shoppingCartPage.clickToCheckJoinPremierCheckBox()
    }
  })

  it('Click on Continue shopping Button', async function () {
    await shoppingCartPage.clickContinueShoppingBtn()
  })

  it('Loads Shop All Page', async function () {
    shopAllPage = new ShopAllPage(driver)
    await shopAllPage.verifyPageIsLoaded()
  })

  it('Click on View All button', async function () {
    await shopAllPage.clickViewAllBtn()
  })

  it('Loads Shop Page', async function () {
    shopPage = new ShopPage()
    await shopPage.verifyPageIsLoaded()
  })

  it('Click on Kits checkbox', async function () {
    await shopPage.clickKitsCheckbox()
  })

  it('Click on Add To Cart Button for Essential Optimal Kit for 5&1 Plan', async function () {
    await shopPage.clickOnTheGoKitFor5And1PlanUS()
  })

  it('Click on Checkout button on pop up window', async function () {
    await addedToYourShoppingCartPopup.clickBeginCheckoutButtonHK()
  })

  it('Check the Checkbox for Join Optavia Premier Checkbox', async function () {
    await shoppingCartPage.clickToCheckJoinPremierCheckBox()
  })

  it('Verify that 5 free items are displayed in cart', async function () {
    screencapture = true
    let actual = (await shoppingCartPage.getFreeMealsIncluded()).split('\n').slice(1)
    let expected = 5
    assert.strictEqual(actual.length, expected, `${expected} Free Items is Not Found`)
  })

  it('Verify Product Name in Edit Mode page', async function () {
    let actual
    let expected = productNamesArray.reverse()
    for (let i = 0; i < productNamesArray.length; i++) {
      actual = await shoppingCartPage.getProductNameTextArray(5 + i)
      if (i === 2) {
        expected[i] = `${expected[i]}${productUOM.BOX}`
      }
      assert.strictEqual(actual, expected[i])
    }
  })

  it('Verify Upsells Offers text', async function () {
    let actual
    let expected = [
      'Habits of Health Transformational System']
    for (let i = 0; i < expected.length; i++) {
      actual = await shoppingCartPage.getUpsellTextByIndex(i)
      assert.strictEqual(actual, expected[i], 'Upsell offer Not Found')
    }
  })

  it('Verify Order Subtotal in the cart', async function () {
    let expected = await CommonUtils.toCurrency(orderSubTotal, market.US)
    let actual = await shoppingCartPage.getOrderSubtotal()
    assert.strictEqual(actual, expected, `Order Subtotal actual : ${actual} is not matching with ${expected}`)
  })

  it('Verify Add to cart button for Habits of Health Transformational', async function () {
    let expected = 'Add to Cart'
    let actual = await shoppingCartPage.getTextSelectHabitsOfHealthTransPackAddBtn()
    assert.strictEqual(actual, expected, 'Add to cart button is not displayed for Habits of Health Transformational')
  })

  it('Verify Upsell QTY = 1', async function () {
    let actual = [
      await shoppingCartPage.getUpsellQtyHoHTransformational()
    ]
    let expected = '1'
    for (let i = 0; i < actual.length; i++) {
      assert.strictEqual(actual[i], expected)
    }
  })

  it('Click on Proceed to Checkout button', async function () {
    await shoppingCartPage.clickProceedToCheckoutBtn()
  })

  it('Loads the Find a Coach page', async function () {
    findACoachPage = new FindACoachPage()
    await findACoachPage.verifyPageIsLoaded()
  })

  it('Search a coach', async function () {
    await allureReporter.addDescription(`Search a coach ${coachFirstName} ${coachLastName}`)
    await findACoachPage.enterFirstAndLastNameToSearchCoach(coachFirstName, coachLastName)
    await findACoachPage.clickSearchCoachFindACoachButton()
  })

  it('Loads the Search Result page', async function () {
    searchCoachResultsPage = new SearchCoachResultsPage()
    await searchCoachResultsPage.verifyPageIsLoaded()
  })

  it('Verify Coach Search Result page body text', async function () {
    let expected = await SiteMapTestData.PAGE_BODY_TITLE(page.Home.SEARCH_COACH_RESULTS)
    let actual = await searchCoachResultsPage.getCoachSearchResultTitle()
    assert.strictEqual(actual, expected, 'Coach Search Result page body text is not matching with expected')
  })

  it('Verify searched coach is in search result list', async function () {
    screencapture = true
    await allureReporter.addDescription(`Verify ${coachFirstName} ${coachLastName} is in search result list`)
    let expected = `${coachFirstName} ${coachLastName}`
    let actual = await searchCoachResultsPage.getCoachName()
    assert.strictEqual(actual, expected, `Searched coach : ${expected} is not displayed`)
  })

  it('Click on Connect Me next to searched coach', async function () {
    await allureReporter.addDescription(`Click on Connect Me next to ${coachFirstName} ${coachLastName}`)
    await searchCoachResultsPage.clickConnectMeButton()
  })

  it('Loads the Create Account page', async function () {
    createAccountPage = new CreateAccountPage()
    await createAccountPage.verifyPageIsLoaded()
  })

  it('Verify Coach info displayed on top header', async function () {
    let actual = await header.getCoachInformation()
    let expected = `${coachFirstName} ${coachLastName}, ${coachID}`
    assert.strictEqual(actual, expected, `Coach : ${expected} is not displayed on top header`)
  })

  it('Create an Account', async function () {
    await allureReporter.addDescription(`Create an Account Full name: ${nameOnCard}, email: ${customerInfo.CUSTOMER_EMAIL}, password: ${GLOBAL_PASSWORD}`)
    await createAccountPage.createAnAccount(nameOnCard, customerInfo.CUSTOMER_EMAIL, GLOBAL_PASSWORD, TestData.SHIPPING_PHONE)
  })

  it('Loads the Checkout page', async function () {
    checkoutPage = new CheckoutPage()
    await checkoutPage.verifyPageIsLoaded()
  })

  it('Loads the Shipping Address page', async function () {
    shippingAddressTile = new ShippingAddressTile()
    await shippingAddressTile.verifyPageIsLoaded()
  })

  it('Enter Shipping Full Name and Address information', async function () {
    await shippingAddressTile.addShippingAddress(TestData, true)
  })

  it('Check Default shipping check box', async function () {
    await shippingAddressTile.clickToCheckDefaultAddressCheckBox()
  })

  it('Shipping Address: click Next button', async function () {
    await shippingAddressTile.clickNext()
  })

  it('Loads the Suggested Address popup', async function () {
    popupSuggestedDeliveryAddressesForm = new PopupSuggestedDeliveryAddressesForm()
    await popupSuggestedDeliveryAddressesForm.verifyPageIsLoaded()
  })

  it('Click on Use this address button', async function () {
    await shippingAddressTile.verifyPageIsLoaded()
    await popupSuggestedDeliveryAddressesForm.clickUseThisAddress()
  })

  it('Loads the Shipping Method page', async function () {
    shippingMethodTile = new ShippingMethodTile()
    await shippingMethodTile.verifyPageIsLoaded()
  })

  it('Click on Next button from Shipping Method tile', async function () {
    await shippingMethodTile.clickNext()
  })

  it('Loads the Payment & Billing Address page', async function () {
    paymentAndBillingAddressTile = new PaymentAndBillingAddressTile()
    await paymentAndBillingAddressTile.verifyPageIsLoaded()
  })

  it('Enter Payment Details information', async function () {
    await paymentAndBillingAddressTile.createPaymentDetails(cardType, nameOnCard, cardNumber, cvv, month, year)
  })

  it('Click Use my Delivery Address checkbox', async function () {
    await paymentAndBillingAddressTile.clickToCheckUseDeliveryAddressCheckBox()
  })

  it('Click on Next button from Payment tile', async function () {
    await paymentAndBillingAddressTile.clickNext()
  })

  it('Loads the Final Review page', async function () {
    finalReviewTile = new FinalReviewTile()
    await finalReviewTile.verifyPageIsLoaded()
  })

  it('Verify Estimated Rewards Link is displayed', async function () {
    let actual = await checkoutPage.isEstimatedRewardsLinkDisplayed()
    assert.isTrue(actual, 'Estimated Rewards Link is not displayed')
  })

  it('Verify Estimated Rewards Earned on this Order', async function () {
    let expected = await CommonUtils.toCurrency(earnedRewards, market.US)
    let actual = await checkoutPage.getEarningThisOrderText()
    assert.strictEqual(actual, expected, `Estimated Rewards Earned :${expected} is not matching with actual`)
  })

  it('Verify Eligible For New Rewards', async function () {
    let actual = await checkoutPage.getEligibleForRewardsText()
    let expected = await CommonUtils.toCurrency(eligibleForNewRewards, market.US)
    assert.strictEqual(actual, expected, `Eligible For New Rewards :${expected} is not matching with actual`)
  })

  it('Verify 10.00% rewards text is displayed', async function () {
    let actual = await checkoutPage.getRewardsTowardsNextOrderText()
    let expected = '10.0% rewards towards next Optavia Premier Order'
    assert.strictEqual(actual, expected, `${expected} is not displayed`)
  })

  it('Verify Premier Rewards Summary Pop Up is displayed', async function () {
    await checkoutPage.clickEstimatedRewards()
    rewardsPopup = new RewardEarningsPopUp()
    await rewardsPopup.verifyPageIsLoaded()
  })

  it('Close Rewards Earning pop up', async function () {
    await rewardsPopup.clickCloseRewardEarningsPopUp()
  })

  it('Verify that 5 free meals are displayed on Items to be delivered section', async function () {
    screencapture = true
    let actual = (await finalReviewTile.getFreeItemsNames()).split('\n').slice(1)
    let expected = 5
    assert.strictEqual(actual.length, expected, 'Free Item Not Found')
  })

  it('Verify purchased items on Items to be delivered section', async function () {
    let expected = productNamesArray
    let actual = await finalReviewTile.getItemsToDeliverText()
    for (let i = 0; i < productNamesArray.length; i++) {
      assert.include(actual, expected[i])
    }
  })

  it('Verify free items on Items to be delivered section', async function () {
    let actual = await finalReviewTile.getItemsToDeliverText()
    let expected = ['OPTAVIA Guide Top Tips Insert',
      'Journey Kickoff Card Insert',
      'OPTAVIA Guide'
    ]
    for (let i = 0; i < expected.length; i++) {
      assert.include(actual, expected[i], `${expected[i]} Free Item is not displayed`)
    }
  })

  it('Click on Submit order button', async function () {
    await browser.pause(2000)
    await finalReviewTile.clickPlaceOrderButton()
  })

  it('Loads order Confirmation page', async function () {
    orderConfirmationPage = new OrderConfirmationPage()
    orderConfirmationPage.verifyPageIsLoaded()
  })

  it('Thank you Message is Displayed', async function () {
    orderConfirmationPage = new OrderConfirmationPage()
    orderConfirmationPage.verifyPageIsLoaded()
    let actual = await orderConfirmationPage.getOrderConfirmationBodyText()
    let expected = 'Thank you for your Order!'
    assert.strictEqual(actual, expected, `${expected} Message is not displayed in Order confirmation Page`)
  })

  it('Capture Order Number', async function () {
    orderNumber = await orderConfirmationPage.getOrderNumber()
    await allureReporter.addDescription(`Order confirmation number is: ${orderNumber}`)
  })

  it('Click on Continue Shopping button on Order Confirmation page', async function () {
    await orderConfirmationPage.clickContinueShopping()
  })

  it('Loads the Home page', async function () {
    await header.verifyMyAccountLinkIsLoaded()
    await mainNavBar.verifyPageIsLoaded()
  })

  it('Click on My Account link', async function () {
    await header.clickMyAccount()
  })

  it('Loads My Account page', async function () {
    myAccountLeftNavigation = new MyAccountLeftNavigation()
    await myAccountLeftNavigation.verifyPageIsLoaded()
    myAccountPage = new MyAccountPage()
    await myAccountPage.verifyPageIsLoaded()
  })

  it('OPTAVIA Premier Order link displays', async function () {
    screencapture = true
    let actual = await myAccountLeftNavigation.isOptaviaPremierLinkPresent()
    assert.isTrue(actual, 'OPTAVIA Premier link is missing')
  })

  it('Verify recently placed order is displayed in order history', async function () {
    screencapture = true
    let actual = await myAccountPage.getLastOrderNumber()
    let expected = orderNumber.split(' ')[4]
    await allureReporter.addDescription(`Expected last order is ${expected}`)
    assert.strictEqual(actual, expected, `Order Number : ${expected} is not available in recently Placed order`)
  })

  it('Click on Last Order Number link', async function () {
    await myAccountPage.clickLastOrderNumberLink()
  })

  it('Loads the Order Details page', async function () {
    orderDetailsPage = new OrderDetailsPage()
    await orderDetailsPage.verifyPageIsLoaded()
  })

  it('Verify order details page displays for the order created above', async function () {
    let expected = orderNumber.substring(21)
    expected = `Order # ${expected}`
    let actual = await orderDetailsPage.getOrderNumber()
    assert.strictEqual(actual, expected, `${expected} is not available in Order details page`)
  })

  it('Verify Order Subtotal in Order Details Page', async function () {
    let actual = await orderDetailsPage.getOrderDetailsSubTotalText()
    let expected = await CommonUtils.toCurrency(orderSubTotal, market.US)
    assert.strictEqual(actual, expected, `Order Subtotal : ${actual} is not matching with expected value`)
  })

  it('Verify Order Total in Order Details Page', async function () {
    let actual = await orderDetailsPage.getOrderDetailsTotalText()
    let expected = await CommonUtils.toCurrency(orderTotal, market.US)
    assert.strictEqual(actual, expected, `Order Total : ${actual} is not matching with expected value`)
  })

  it('Verify Eligible for rewards value', async function () {
    let actual = await orderDetailsPage.getEligibleRewardsText()
    let expected = await CommonUtils.toCurrency(eligibleForNewRewards, market.US)
    assert.strictEqual(actual, expected, `Eligible for rewards value : ${actual} is not matching with expected value`)
  })

  it('Verify Earn This Order value', async function () {
    let actual = await orderDetailsPage.getEarningThisOrderText()
    let expected = await CommonUtils.toCurrency(earnedRewards, market.US)
    assert.strictEqual(actual, expected, `Earn This Order value : ${actual} is not matching with expected value`)
  })

  it('Verify Estimated Rewards Link is Displayed', async function () {
    let actual = await orderDetailsPage.isEstimatedRewardsLinkDisplayed()
    assert.isTrue(actual, 'Estimated Rewards Link is Not Displayed')
  })

  it('Verify that 5 free meals are displayed in the Items to Deliver section', async function () {
    screencapture = true
    let actual = (await orderDetailsPage.getFreeItemsNames()).length
    let expected = 5
    assert.strictEqual(actual, expected, 'Free Item Not Found')
  })

  it('Logout', async function () {
    await header.verifyLogOutIsLoaded()
    await header.clickLogOut()
    await header.verifyPageIsLoaded()
  })

  afterEach(async function () {
    await driverutils.saveScreenshots(this.currentTest.state, screencapture, driver)
    screencapture = false
  })
})
