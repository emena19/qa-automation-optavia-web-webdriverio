// @flow
import {assert} from 'chai'

import allureReporter from '@wdio/allure-reporter'
import driverutils from '@core-libs/DriverUtils'
import ProductTestData, {attb, market, product} from '@input-data/products/ProductTestData'
import CoachTestData from '@input-data/coaches/CoachTestData'
import CustomerTestData from '@input-data/customers/CustomerTestData'
import HomePage from '@page-objects/home/HomePage'
import Header from '@page-objects/common/Header'
import LoginPage from '@page-objects/loginPage/LoginPage'
import {GLOBAL_PASSWORD} from '@core-libs/ConstantUtil'
import MyAccountPage from '@page-objects/myaccount/MyAccountPage'
import ShoppingCartPage from '@page-objects/shoppingcart/ShoppingCartPage'
import ShopAllPage from '@page-objects/shop/ShopAllPage'
import ChooseYourOwnFuelingsPopup from '@page-objects/chooseyourownfuelings/ChooseYourOwnFuelingsPopup'
import PaymentAndBillingAddressTile from '@page-objects/checkout/PaymentAndBillingAddressTile'
import FinalReviewTile from '@page-objects/checkout/FinalReviewTile'
import ShippingAddressTile from '@page-objects/checkout/ShippingAddressTile'
import PopupSuggestedDeliveryAddressesForm
  from '@page-objects/popupsuggesteddeliveryaddressesform/PopupSuggestedDeliveryAddressesForm'
import OrderConfirmationPage from '@page-objects/orderconfirmation/OrderConfirmationPage'
import TestCreditCards from '@input-data/creditcards/CreditCardTestData'
import MainNavBar from '@page-objects/common/MainNavBar'
import ShopToolsAndAccessoriesPage from '@page-objects/shop/ShopToolsAndAccessoriesPage'
import AddedToYourShoppingCartPopup from '@page-objects/shop/AddedToYourShoppingCartPopup'
import CheckoutCommonSection from '@page-objects/checkout/CheckoutCommonSection'

describe('C5761 - SG OPTAVIA -Existing Customer- Add OnDemand Order', function () {
  let screencapture = false // capture screenshot when test step 'passed' also by default only capture screenshot of failed steps

  let homePage, header, loginPage, mainNavBar, shopAllPage,
    shoppingCartPage, shippingAddressTile, paymentAndBillingAddressTile, searchProduct, chooseYourOwnFuelings, checkoutCommonSection,
    myAccountPage, shopToolsAndAccessoriesPage, addedToYourShoppingCartPopup, finalReviewPage, popupSuggestedDeliveryAddressesForm

  // Customer Registration
  let TestData

  // Coach (Enroller) Info
  let coachFirstName, coachLastName, coachName, coachID

  // Product search
  let addItemsToCartPopup

  // Order Confirmation Page
  let orderConfirmationPage, orderNumber

  // Payment Details
  let cvv

  let testTitle = this.title

  before(async function () {
    await driverutils.goToOptaviaWebSiteHomePageSG()
  })

  beforeEach(async function () {
    await driverutils.addAllureReport(allureReporter, __dirname, 'SG OPTAVIA Sanity', testTitle)
  })

  it('Preparing Test Data', async function () {
    let sh = 2
    // Product Information
    searchProduct = await ProductTestData.PRODUCT_INFO(attb.NAME, product.tools_accessories.OPT_HABITS_HEALTH_SYSTEM, market.SG)
    // Coach Information
    let coachRow = 5
    coachID = await CoachTestData.COACH_ID(coachRow, sh)
    coachFirstName = await CoachTestData.COACH_FIRST_NAME(coachRow, sh)
    coachLastName = await CoachTestData.COACH_LAST_NAME(coachRow, sh)
    coachName = `${coachFirstName} ${coachLastName}`
    // Client Info
    TestData = await CustomerTestData.getCustomerInfo('6520000402035')

    // Payment Information (for VISA = B, Mastercard = C, American Express = D and Discover = E) column of sheet 2 in excel file
    let ccCol = 'B'
    let sheet = 2
    cvv = await TestCreditCards.CVV(sheet, ccCol)
  })

  it('Loads the Home Page', async function () {
    homePage = new HomePage()
    await homePage.verifyPageIsLoaded()
    header = new Header()
    await header.verifyPageIsLoaded()
    mainNavBar = new MainNavBar()
    await mainNavBar.verifyPageIsLoaded()
    await header.verifyFlagIsLoaded()
  })

  it('Navigate to Login Page', async function () {
    await header.clickLogIn()
    loginPage = new LoginPage()
    await loginPage.verifyPageIsLoaded()
  })

  it('Login as existing Singapore Coach', async function () {
    await allureReporter.addDescription(`Login as existing Singapore coach email: ${TestData.CUSTOMER_EMAIL} and password: ${GLOBAL_PASSWORD}`)
    await loginPage.loginAsExistingCustomer(TestData.CUSTOMER_EMAIL, GLOBAL_PASSWORD)
  })

  it('Loads the My Account Page', async function () {
    myAccountPage = new MyAccountPage()
    await myAccountPage.verifyPageIsLoaded()
  })

  it('Verify Coach Information', async function () {
    let expected = `${coachName}, ${coachID}`
    let actual = await header.getCoachInformation()
    assert.strictEqual(actual, expected, `Coach Info : ${expected} is not displayed on top header`)
  })

  it('Click on the Cart Icon from header', async function () {
    await header.clickCartIcon()
    shoppingCartPage = new ShoppingCartPage()
    await shoppingCartPage.verifyPageIsLoaded()
  })

  it('Remove items from the cart', async function () {
    let i = await shoppingCartPage.getCartItemsArrayListCount()
    for (let p = 0; p < (i); p++) {
      if (i) {
        await shoppingCartPage.clickRemoveProductIcon()
      }
    }
    await allureReporter.addDescription('No Products available in the cart')
  })

  it('Loads the Shop All Page', async function () {
    await mainNavBar.clickShopAll()
    shopAllPage = new ShopAllPage()
    await shopAllPage.verifyPageIsLoaded()
  })

  it('Navigate to Choose Your Own Fuelings from Shop', async function () {
    await mainNavBar.clickChooseYourOwnFuelings()
    addItemsToCartPopup = new ChooseYourOwnFuelingsPopup()
    await addItemsToCartPopup.verifyPageIsLoaded()
  })

  it('Loads the Choose Your Own Fuelings from Shop ', async function () {
    chooseYourOwnFuelings = new ChooseYourOwnFuelingsPopup()
    await chooseYourOwnFuelings.verifyPageIsLoaded()
    await chooseYourOwnFuelings.selectButton()
    await chooseYourOwnFuelings.removeAllButton()
    await chooseYourOwnFuelings.addItemsToCartButton()
  })

  it('Close choose  your own Fuelings ', async function () {
    await chooseYourOwnFuelings.clickClosebutton()
  })

  it('Clicks Shop Button under Tools and Accessories', async function () {
    await shopAllPage.clickToolsAndAccessoriesShopBtn()
  })

  it('Search and add searched product', async function () {
    await allureReporter.addDescription(`Search product SKU = ${searchProduct}`)
    shopToolsAndAccessoriesPage = new ShopToolsAndAccessoriesPage()
    await shopToolsAndAccessoriesPage.verifyPageIsLoaded()
    await shopToolsAndAccessoriesPage.clickProductsInToolsAndAccessories(searchProduct)
  })

  it('Loads the Added To Your Shopping Cart Popup', async function () {
    addedToYourShoppingCartPopup = new AddedToYourShoppingCartPopup()
    await addedToYourShoppingCartPopup.verifyPageIsLoaded()
  })

  it('Click on Checkout button on pop up window', async function () {
    await addedToYourShoppingCartPopup.clickCheckoutButton()
  })

  it('Loads the Shopping Cart Page', async function () {
    shoppingCartPage = new ShoppingCartPage(driver)
    await shoppingCartPage.verifyPageIsLoaded()
  })

  it('Click on Proceed to Checkout button', async function () {
    await shoppingCartPage.clickProceedToCheckoutBtn()
  })

  it('Loads the Payment page', async function () {
    paymentAndBillingAddressTile = new PaymentAndBillingAddressTile()
    await paymentAndBillingAddressTile.verifyPageIsLoadedSG()
  })

  it('Loads the CheckoutCommonSection page', async function () {
    checkoutCommonSection = new CheckoutCommonSection()
    await checkoutCommonSection.verifyPageIsLoaded()
  })

  it('Verify Coach info displayed on top header', async function () {
    let actual = await checkoutCommonSection.getCoachInfoFromAvatar()
    let expected = `${coachFirstName} ${coachLastName}, ${coachID}`
    assert.strictEqual(actual, expected, `Coach Info : ${expected} is not displayed on top header`)
  })

  it('Enter Existing Payment Details information', async function () {
    await paymentAndBillingAddressTile.clickOnExistingCardDetailsSG(cvv)
  })

  it('Click on Next button from Payment tile', async function () {
    await paymentAndBillingAddressTile.clickNextSG()
  })

  it('Loads the Final Review page', async function () {
    finalReviewPage = new FinalReviewTile()
    await finalReviewPage.verifyPageIsLoaded()
  })

  it('Verify Coach info displayed on top header', async function () {
    let actual = await checkoutCommonSection.getCoachInfoFromAvatar()
    let expected = `${coachFirstName} ${coachLastName}, ${coachID}`
    assert.strictEqual(actual, expected, `Coach Info : ${expected} is not displayed on top header`)
  })

  it('Click on Edit Button under Shipping Delivery Address', async function () {
    await finalReviewPage.clickDeliveryAddressEditBtn()
  })

  it('Loads the Shipping Address page', async function () {
    shippingAddressTile = new ShippingAddressTile()
    await shippingAddressTile.verifyPageIsLoaded()
  })

  it('Verify Coach info displayed on top header', async function () {
    let actual = await checkoutCommonSection.getCoachInfoFromAvatar()
    let expected = `${coachFirstName} ${coachLastName}, ${coachID}`
    assert.strictEqual(actual, expected, `Coach Info : ${expected} is not displayed on top header`)
  })

  it('Clears the text in Delivery Address', async function () {
    let blankAddress = {
      SHIPPING_ADDRESS_LINE_1: '',
      SHIPPING_ZIP: '',
      SHIPPING_PHONE: '',
      SHIPPING_COUNTRY_CODE: ''
    }
    await shippingAddressTile.addShippingAddressSG(blankAddress)
  })

  it('Updating Delivery Address information', async function () {
    await browser.pause(3000)
    await shippingAddressTile.verifyPageIsLoaded()
    await shippingAddressTile.addShippingAddressSG(TestData, true)
  })

  it('Shipping Address: click Next button', async function () {
    await shippingAddressTile.clickNext()
  })

  it('Loads the Suggested Address popup', async function () {
    popupSuggestedDeliveryAddressesForm = new PopupSuggestedDeliveryAddressesForm()
    await popupSuggestedDeliveryAddressesForm.verifyPageIsLoaded()
  })

  it('Click on Use this address button', async function () {
    await shippingAddressTile.verifyPageIsLoaded()
    await popupSuggestedDeliveryAddressesForm.clickUseThisAddress()
  })

  it('Loads the Payment page', async function () {
    await paymentAndBillingAddressTile.verifyPageIsLoadedSG()
  })

  it('Verify Coach info displayed on top header', async function () {
    let actual = await checkoutCommonSection.getCoachInfoFromAvatar()
    let expected = `${coachFirstName} ${coachLastName}, ${coachID}`
    assert.strictEqual(actual, expected, `Coach Info : ${expected} is not displayed on top header`)
  })

  it('Enter Existing Payment Details information', async function () {
    await paymentAndBillingAddressTile.clickOnExistingCardDetailsSG(cvv)
  })

  it('Click on Next button from Payment tile', async function () {
    await paymentAndBillingAddressTile.clickNextSG()
  })

  it('Verify Coach info displayed on top header', async function () {
    let actual = await checkoutCommonSection.getCoachInfoFromAvatar()
    let expected = `${coachFirstName} ${coachLastName}, ${coachID}`
    assert.strictEqual(actual, expected, `Coach Info : ${expected} is not displayed on top header`)
  })

  it('Click on Submit Order button from Final Review tile', async function () {
    await finalReviewPage.clickPlaceOrderButton()
  })

  it('Loads the Order Confirmation page', async function () {
    screencapture = true
    orderConfirmationPage = new OrderConfirmationPage()
    await orderConfirmationPage.verifyPageIsLoaded()
    let actual = await orderConfirmationPage.getOrderNumber()
    let resultArray = actual.split(' ')
    orderNumber = resultArray[4]
    await allureReporter.addDescription(`Order Number : ${orderNumber}`)
  })

  it('Verify the Coach information is not displayed in Order Summary Section', async function () {
    screencapture = true
    assert.isFalse(await orderConfirmationPage.isCoachInfoFromAvatarDisplayed(), 'Coach Information is Displayed')
  })

  it('Click on Continue Shopping button on Order Confirmation page', async function () {
    await orderConfirmationPage.clickContinueShopping()
  })

  it('Loads the Home page', async function () {
    await header.verifyMyAccountLinkIsLoaded()
    await mainNavBar.verifyPageIsLoaded()
  })

  it('Logout Customer', async function () {
    await header.verifyLogOutIsLoaded()
    await header.clickLogOut()
    await header.verifyPageIsLoaded()
  })

  it('Verify coach information is not displayed', async function () {
    assert.isFalse(await header.isCoachInformationDisplayed(), 'Coach Information is Displayed')
  })

  afterEach(async function () {
    await driverutils.saveScreenshots(this.currentTest.state, screencapture, driver)
    screencapture = false
  })
})
