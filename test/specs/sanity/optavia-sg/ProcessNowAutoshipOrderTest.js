// @flow
import {assert} from 'chai'
import driverutils from '@core-libs/DriverUtils'
import allureReporter from '@wdio/allure-reporter'
import CustomerTestData from '@input-data/customers/CustomerTestData'
import HomePage from '@page-objects/home/HomePage'
import Header from '@page-objects/common/Header'
import LoginPage from '@page-objects/loginPage/LoginPage'
import TestDataBaseMapping from '@db/TestDataBaseMapping'
import MyAccountPage from '@page-objects/myaccount/MyAccountPage'
import ShoppingCartPage from '@page-objects/shoppingcart/ShoppingCartPage'
import OptaviaPremierOrderPage from '@page-objects/myaccount/OptaviaPremierOrderPage'
import OrderConfirmationPage from '@page-objects/orderconfirmation/OrderConfirmationPage'
import OrderDetailsPage from '@page-objects/myaccount/OrderDetailsPage'

describe('C5764/C21734 SG OPTAVIA - Create Autoship Order Using Same Day Processing', function () {
  let screencapture = false // True to capture screenshot when test step 'passed'. By default only capture screenshot of failed steps
  let homePage, header, loginPage, myAccountPage, optaviaPremierOrderPage, shoppingCartPage, orderConfirmationPage, orderDetailsPage
  let subTotal, appliedRewards, deliveryCharge
  // Customer Registration
  let email, password

  let orderNumber

  let testTitle = this.title

  before(async function () {
    await driverutils.goToOptaviaWebSiteHomePageSG()
  })

  beforeEach(async function () {
    await driverutils.addAllureReport(allureReporter, __dirname, 'SG OPTAVIA Sanity', testTitle)
  })

  it('Preparing Test Data', async function () {
    let custID = '6520000435453'
    let TestData = await CustomerTestData.getCustomerInfo(custID)
    // Client Info
    email = TestData.CUSTOMER_EMAIL
    password = TestData.CUSTOMER_PASSWORD
  })

  it('Loads the Home Page', async function () {
    homePage = new HomePage()
    await homePage.verifyPageIsLoaded()
    header = new Header()
    await header.verifyPageIsLoaded()
    await header.verifyFlagIsLoaded()
  })

  it('Navigate to Login Page', async function () {
    await header.clickLogIn()
    loginPage = new LoginPage()
    await loginPage.verifyPageIsLoaded()
  })

  it('Login as existing Singapore Coach', async function () {
    await allureReporter.addDescription(`Login as existing Singapore coach email: ${email} and password: ${password}`)
    await browser.pause(4000)
    await loginPage.loginAsExistingCustomer(email, password)
  })

  it('Verify My Account page is loaded', async function () {
    screencapture = true
    myAccountPage = new MyAccountPage()
    await myAccountPage.verifyPageIsLoaded()
  })

  it('Click on Edit Order button', async function () {
    await myAccountPage.clickEditOrder()
  })

  it('Loads the Shopping Cart page', async function () {
    shoppingCartPage = new ShoppingCartPage()
    await shoppingCartPage.verifyPageIsLoaded()
  })

  it('Update Product quantity to 3', async function () {
    await shoppingCartPage.updateProductQTY(3)
    await shoppingCartPage.clickUpdateProductLink()
    shoppingCartPage = new ShoppingCartPage()
    await shoppingCartPage.verifyPageIsLoaded()
  })

  it('Click on Save order and Loads the Subscription page ', async function () {
    await shoppingCartPage.clickSaveOrderLink()
    optaviaPremierOrderPage = new OptaviaPremierOrderPage()
    await optaviaPremierOrderPage.verifyPageIsLoaded()
  })

  it('Click on Process Now button', async function () {
    await optaviaPremierOrderPage.clickProcessNowBtn()
  })

  it('Verify Process Now popup displays', async function () {
    screencapture = true
    await optaviaPremierOrderPage.verifyProcessNowPopUpIsLoaded()
  })

  it('Click Process Now button from Process Now popup', async function () {
    screencapture = true
    await optaviaPremierOrderPage.clickProcessNowPopUpProcessNowBtn()
  })

  it('Loads the Order Confirmation page', async function () {
    orderConfirmationPage = new OrderConfirmationPage()
    await orderConfirmationPage.verifyPageIsLoaded()
    let actual = await orderConfirmationPage.getOrderNumber()
    let resultArray = actual.split(' ')
    orderNumber = resultArray[4]
    await allureReporter.addDescription(`Order Number : ${orderNumber}`)
  })

  // Temporal step, remove it when DB is syncing fast
  it('Wait for 2.5 minutes for DB to sync', async function () {
    await browser.pause(150000)
  })

  it('Verify Order ID in DB', async function () {
    let expected = orderNumber
    let actual = await TestDataBaseMapping.getOrderID(orderNumber)
    assert.strictEqual(actual, expected, `In DB Order ID: ${actual} does not match expected: ${expected}`)
  })

  it('Click on Continue Shopping button on Order Confirmation page', async function () {
    await orderConfirmationPage.clickContinueShopping()
  })

  it('Click on My Account link from header', async function () {
    await header.clickMyAccount()
  })

  it('My Account Dashboard page is loaded', async function () {
    await myAccountPage.verifyPageIsLoaded()
  })

  it('Verify recently placed order is displayed in order history', async function () {
    screencapture = true
    let actual = await myAccountPage.getLastOrderNumber()
    let expected = orderNumber
    await allureReporter.addDescription(`Expected last order is ${expected}`)
    assert.strictEqual(actual, expected, `Recently placed order : ${expected} is not displayed in order history`)
  })

  it('Click on Last Order Number link', async function () {
    await myAccountPage.clickLastOrderNumberLink()
  })

  it('Loads the Order Details page', async function () {
    orderDetailsPage = new OrderDetailsPage()
    await orderDetailsPage.verifyPageIsLoaded()
  })

  it('Verify order details page displays for the order created above', async function () {
    let expected = `Order # ${orderNumber}`
    let actual = await orderDetailsPage.getOrderNumber()
    assert.strictEqual(actual, expected, `${expected} is not displayed in order details page`)
  })

  it('Verify the order type is Immediate Autoship', async function () {
    screencapture = true
    let expected = 'Type: Immediate_Autoship'
    let actual = await orderDetailsPage.getOrderType()
    assert.strictEqual(actual.trim(), expected, 'Order type is not Immediate Autoship')
  })

  it('Capture SubTotal , applied rewards and delivery under Order Summary', async function () {
    subTotal = (await orderDetailsPage.getOrderDetailsSubTotalText()).split('$')
    if (await orderDetailsPage.isPremierAppliedRewardsPresent()) {
      appliedRewards = (await orderDetailsPage.getPremierAppliedRewardsText()).split('$')
      appliedRewards = parseFloat(appliedRewards[1])
    } else {
      appliedRewards = 0.00
    }
    deliveryCharge = await orderDetailsPage.getOrderDetailsDeliveryChargeText()
    if (deliveryCharge === 'FREE') {
      deliveryCharge = 0.00
    } else {
      deliveryCharge = (await orderDetailsPage.getOrderDetailsDeliveryChargeText()).split('$')
      deliveryCharge = parseFloat(deliveryCharge[1])
    }
  })

  it('Verify the GST is 7% of Subtotal and Delivery charge', async function () {
    let expected = ((parseFloat(subTotal[1]) - appliedRewards + deliveryCharge)) / 107 * 7
    let actual = (await orderDetailsPage.getGSTAmt()).split('$')
    assert.isAtLeast(parseFloat(actual[1]), expected, 'GST is not 7% of Subtotal and Delivery charge')
  })

  it('Click on Log out Link from header', async function () {
    await header.clickLogOut()
  })

  afterEach(async function () {
    await driverutils.saveScreenshots(this.currentTest.state, screencapture)
    screencapture = false
  })
})
