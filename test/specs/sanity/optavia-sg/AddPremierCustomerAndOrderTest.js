// @flow
import {assert} from 'chai'
import driverutils from '@core-libs/DriverUtils'
import allureReporter from '@wdio/allure-reporter'
import CoachTestData from '@input-data/coaches/CoachTestData'
import CustomerTestData from '@input-data/customers/CustomerTestData'
import ProductTestData, {attb, market, product} from '@input-data/products/ProductTestData'
import HomePage from '@page-objects/home/HomePage'
import Header from '@page-objects/common/Header'
import MainNavBar from '@page-objects/common/MainNavBar'
import TestCreditCards from '@input-data/creditcards/CreditCardTestData'
import LoginPage from '@page-objects/loginPage/LoginPage'
import ShopAllPage from '@page-objects/shop/shopAllPage'
import EssentialFuelingsPage from '@page-objects/shop/EssentialFuelingsPage'
import ProductDetailsPage from '@page-objects/shop/ProductDetailsPage'
import CommonUtils from '@core-libs/CommonUtils'
import AddedToYourShoppingCartPopup from '@page-objects/shop/AddedToYourShoppingCartPopup'
import ShoppingCartPage from '@page-objects/shoppingcart/ShoppingCartPage'
import ShopPage from '@page-objects/shop/ShopPage'
import CheckoutCommonSection from '@page-objects/checkout/CheckoutCommonSection'
import FindACoachPage from '@page-objects/findacoach/FindACoachPage'
import SearchCoachResultsPage from '@page-objects/searchcoachresults/SearchCoachResultsPage'
import SiteMapTestData, {page} from '@input-data/various/MiscTestData'
import CreateAccountPage from '@page-objects/createAccountPage/CreateAccountPage'
import {AGENT_USER, GLOBAL_PASSWORD} from '@core-libs/ConstantUtil'
import CheckoutPage from '@page-objects/checkout/CheckoutPage'
import ShippingAddressTile from '@page-objects/checkout/ShippingAddressTile'
import PopupSuggestedDeliveryAddressesForm
  from '@page-objects/popupsuggesteddeliveryaddressesform/PopupSuggestedDeliveryAddressesForm'
import PaymentAndBillingAddressTile from '@page-objects/checkout/PaymentAndBillingAddressTile'
import RewardEarningsPopUp from '@page-objects/popuprewardearnings/RewardEarningsPopUp'
import OrderConfirmationPage from '@page-objects/orderconfirmation/OrderConfirmationPage'
import MyAccountPage from '@page-objects/myaccount/MyAccountPage'
import MyAccountLeftNavigation from '@page-objects/myaccount/MyAccountLeftNavigation'
import OrderHistoryPage from '@page-objects/myaccount/OrderHistoryPage'
import OptaviaPremierOrderPage from '@page-objects/myaccount/OptaviaPremierOrderPage'
import ChooseYourOwnFuelingsPopup from '@page-objects/chooseyourownfuelings/ChooseYourOwnFuelingsPopup'
import TestDataBaseMapping from '@db/TestDataBaseMapping'
import HybrisBackofficeLoginPage from '@page-objects/hybrisbackoffice/HybrisBackofficeLoginPage'
import HybrisBackofficeAutorityGroupPage from '@page-objects/hybrisbackoffice/HybrisBackofficeAutorityGroupPage'
import HybrisBackofficeHomePage from '@page-objects/hybrisbackoffice/HybrisBackofficeHomePage'
import HybrisBackofficeSearchPage from '@page-objects/hybrisbackoffice/HybrisBackofficeSearchPage'
import HybrisBackofficeOrderDetailsPage from '@page-objects/hybrisbackoffice/HybrisBackofficeOrderDetailsPage'
import FinalReviewTile from '@page-objects/checkout/FinalReviewTile'

let screencapture = false // capture screenshot when test step 'passed' also by default only capture screenshot of failed steps

let homePage, header, loginPage, mainNavBar, shopAllPage, productDetailPage, essentialFuelingsPage, createAccountPage,
  shoppingCartPage, checkoutPage, paymentAndBillingAddressTile, orderHistoryPage, shippingAddressTile, addItemsToCartPopup,
  popupSuggestedDeliveryAddressesForm, shopPage, addedToYourShoppingCartPopup, finalReviewTile, myAccountPage, optaviaPremierOrderPage, checkoutCommonSection, rewardsPopup

// Customer Registration
let TestData, customerInfo

// Coach (Enroller) Info
let coachFirstName, coachLastName, findACoachPage, searchCoachResultsPage, coachID

// Order Confirmation Page
let orderConfirmationPage

// Payment Details
let nameOnCard, cardNumber, cvv, expdt, month, year

// Shopping Cart Page
let orderSubTotal, deliveryCharge, orderNumber, orderTotal, myAccountLeftNavigation, accountID, gst, rewardsApplied, actualOrderNumber

// Order ID
let templateID, decimalLength
// product detail page
let earnThisOrder, eligibleForNewRewards, commissonableVolume, qualifyingvolume

let product1Name, product1UnitPrice, product1CasePrice, product2UnitPrice, product3UnitPrice, premierPageOrderTotal, premierPageOrderSubtotal, premierPageGst

// Hybris Backoffice
let agentEmail, agentPassword, hybrisBackofficeLoginPage, hybrisBackofficeAutorityGroupPage, hybrisBackofficeHomePage, hybrisBackofficeSearchPage, hybrisBackofficeOrderDetailsPage

describe('C5762 - SG OPTAVIA - Add OPTAVIA Premier Customer and Order', function () {
  let testTitle = this.title

  before(async function () {
    await driverutils.goToOptaviaWebSiteHomePageSG()
  })

  beforeEach(async function () {
    await driverutils.addAllureReport(allureReporter, __dirname, 'SG OPTAVIA Sanity', testTitle)
  })

  it('Preparing Test Data', async function () {
    let sheet = 2
    // Coach Information
    let coachRow = 3
    coachFirstName = await CoachTestData.COACH_FIRST_NAME(coachRow, sheet)
    coachLastName = await CoachTestData.COACH_LAST_NAME(coachRow, sheet)
    coachID = await CoachTestData.COACH_ID(coachRow, sheet)
    // Customer Registration
    TestData = await CustomerTestData.getCustomerInfo('6520000402035')
    customerInfo = await CustomerTestData.getNewCustomer()

    // Product Information
    product1Name = await ProductTestData.PRODUCT_INFO(attb.NAME, product.soups.OPT_WILD_RICE_CHICKEN_SOUP, market.SG)
    product1UnitPrice = await ProductTestData.PRODUCT_INFO(attb.UNIT_PRICE, product.soups.OPT_WILD_RICE_CHICKEN_SOUP, market.SG)
    product1CasePrice = await ProductTestData.PRODUCT_INFO(attb.CASE_PRICE, product.soups.OPT_WILD_RICE_CHICKEN_SOUP, market.SG)

    product2UnitPrice = await ProductTestData.PRODUCT_INFO(attb.UNIT_PRICE, product.kits.OPT_OPTIMAL_KIT_5_1_US_FLAVORS, market.SG)

    product3UnitPrice = await ProductTestData.PRODUCT_INFO(attb.UNIT_PRICE, product.tools_accessories.OPT_HABITS_HEALTH_SYSTEM, market.SG)

    orderSubTotal = product1UnitPrice + product2UnitPrice + product3UnitPrice // 754.00
    orderTotal = orderSubTotal // 754.00
    deliveryCharge = 'FREE'
    gst = 51.04

    premierPageOrderSubtotal = (product1UnitPrice + product2UnitPrice) // 684.00
    rewardsApplied = 66.45 // 66.45
    eligibleForNewRewards = 598.04
    earnThisOrder = eligibleForNewRewards * 0.1 // 61.56
    premierPageOrderTotal = premierPageOrderSubtotal - rewardsApplied// 620.07
    premierPageGst = 42.13

    // Payment Information (for VISA = B, Mastercard = C, American Express = D and Discover = E) column of sheet 2 in excel file
    let ccCol = 'B'
    nameOnCard = customerInfo.CUSTOMER_FIRST_NAME + ' ' + customerInfo.CUSTOMER_LAST_NAME
    cardNumber = await TestCreditCards.CARD_NUMBER(sheet, ccCol)
    cvv = await TestCreditCards.CVV(sheet, ccCol)
    month = await TestCreditCards.EXPIRATION_MONTH(sheet, ccCol)
    year = await TestCreditCards.EXPIRATION_YEAR(sheet, ccCol)
    expdt = `${month}/${year}`
  })

  it('Loads the Home Page', async function () {
    homePage = new HomePage()
    await homePage.verifyPageIsLoaded()
    header = new Header()
    await header.verifyPageIsLoaded()
    mainNavBar = new MainNavBar()
    await mainNavBar.verifyPageIsLoaded()
    await header.verifyFlagIsLoaded()
  })

  it('Navigate to Login Page', async function () {
    await header.clickLogIn()
    loginPage = new LoginPage()
    await loginPage.verifyPageIsLoaded()
  })

  it('Loads the Shop All Page', async function () {
    await mainNavBar.clickShopAll()
    shopAllPage = new ShopAllPage()
    await shopAllPage.verifyPageIsLoaded()
  })

  it('Clicks Shop Button under Essential Fuelings', async function () {
    await shopAllPage.clickEssentialFuelingsShopBtn()
  })

  it('Loads Essential Fuelings Page', async function () {
    essentialFuelingsPage = new EssentialFuelingsPage()
    await essentialFuelingsPage.verifyPageIsLoaded()
  })

  it('Check on Soups Checkbox', async function () {
    await essentialFuelingsPage.checkSoupsCheckbox()
  })

  it('Click on Wild Rice & Chicken Flavored Soup link', async function () {
    await essentialFuelingsPage.clickWildRiceAndChickenFlavoredSoupLink()
  })

  it('Loads the product Detail Page', async function () {
    productDetailPage = new ProductDetailsPage()
    await productDetailPage.verifyPageIsLoaded()
  })

  it('Verify items in Product drop down', async function () {
    let itemsArray = await productDetailPage.getProductDropDownText()
    let actual = itemsArray
    let actualResult = actual[1].trim() + ' ' + actual[2].trim()
    let expected = `CASE ${await CommonUtils.toCurrency(product1CasePrice, market.SG)} BOX ${await CommonUtils.toCurrency(product1UnitPrice, market.SG)}`
    assert.strictEqual(actualResult, expected, 'Items in Product drop down is not matching with expected')
  })

  it('Click on Add To Cart Button for Wild rice and Chicken Flavored Soup', async function () {
    await productDetailPage.clickAddToCartBtn()
  })

  it('Loads the Added To Your Shopping Cart Popup', async function () {
    addedToYourShoppingCartPopup = new AddedToYourShoppingCartPopup()
    await addedToYourShoppingCartPopup.verifyPageIsLoaded()
  })

  it('Click on Checkout button on pop up window', async function () {
    await addedToYourShoppingCartPopup.clickCheckoutButton()
  })

  it('Loads the Shopping Cart Page', async function () {
    shoppingCartPage = new ShoppingCartPage(driver)
    await shoppingCartPage.verifyPageIsLoaded()
  })

  /*  // Temporary fix
  it('Temporary Step - UnCheck the checkbox to Join Premier and Verify Join Premier checkbox is not checked', async function () {
    await shoppingCartPage.clickToCheckJoinPremierCheckBox()
    await shoppingCartPage.verifyPageIsLoaded()
    let actual = await shoppingCartPage.isJoinPremierChecked()
    assert.isFalse(actual, 'Join Premier checkbox did not get unchecked')
  })

  it('Verify Join Premier checkbox is NOT checked', async function () {
    screencapture = true
    let actual = await shoppingCartPage.isJoinPremierChecked()
    assert.isFalse(actual, 'Join Premier checkbox is checked')
  }) */

  it('Click on Continue shopping Button', async function () {
    await shoppingCartPage.clickContinueShoppingBtn()
  })

  it('Click the Shop Page and loads the shop page', async function () {
    await browser.pause(2000)
    await mainNavBar.clickShop()
    shopPage = new ShopPage()
    await shopPage.getPageTitle()
  })

  it('Click on Kits under Category', async function () {
    await allureReporter.addDescription(`Search Category = Kits`)
    await shopPage.clickKitsCheckbox()
  })

  it('Click on Add To Cart Button for Essential Optimal Kit for 5&1 Plan', async function () {
    await shopPage.clickOptimalKitFor5And1Plan()
  })

  it('Click on Checkout button on pop up window', async function () {
    await addedToYourShoppingCartPopup.clickBeginCheckoutButton()
  })

  it('Check the Checkbox for Join Optavia Premier Checkbox', async function () {
    screencapture = true
    if (await shoppingCartPage.isJoinPremierChecked() === false) {
      await shoppingCartPage.clickToCheckJoinPremierCheckBox()
    }
  })

  it('Verify that 5 free meals are displayed in cart', async function () {
    screencapture = true
    let freeItemsNames = await shoppingCartPage.getFreeItemsNames()
    let actual = freeItemsNames.filter(name => name.includes('OPTAVIA') === false)
    let expected = 5
    assert.strictEqual(actual.length, expected, 'Free meals Not Found')
  })

  it('Verify OPTAVIA BlenderBottle Classic is displayed as Free Item', async function () {
    await shoppingCartPage.verifyFreeBlenderBottleIsLoaded()
  })

  it('Verify OPTAVIA Free Guide is displayed as Free Item', async function () {
    await shoppingCartPage.verifyFreeOptaviaGuideIsLoaded()
  })

  it('Verify Journey Kickoff Card Insert is displayed as Free Item', async function () {
    await shoppingCartPage.verifyFreeJourneyKickoffCardInsertIsLoaded()
  })

  it('Verify OPTAVIA Guide Top Tips Insert is displayed as Free Item', async function () {
    await shoppingCartPage.verifyFreeOptaviaGuideTopTipsInsertIsLoaded()
  })

  it('Verify Order Subtotal in the cart', async function () {
    let expected = await CommonUtils.toCurrency(orderSubTotal, market.SG)
    let actual = await shoppingCartPage.getOrderSubtotal()
    assert.strictEqual(actual, expected, `Order Subtotal actual : ${actual} is not matching with ${expected}`)
  })

  it('Loads the CheckoutCommonSection page', async function () {
    checkoutCommonSection = new CheckoutCommonSection()
  })

  it('Verify Order Total in Shopping Cart Page', async function () {
    let expected = `Order Total: ${await CommonUtils.toCurrency(orderTotal, market.SG)}\nGST: ${await CommonUtils.toCurrency(gst, market.SG)}\nSubtotal ${await CommonUtils.toCurrency(orderSubTotal, market.SG)}\nDelivery Charge: ${deliveryCharge}`
    let actual = await checkoutCommonSection.getOrderSummaryTotals()
    assert.strictEqual(actual, expected, `Order Total actual : ${actual} is not matching with ${expected}`)
  })

  it('Click on Proceed to Checkout button', async function () {
    await shoppingCartPage.clickProceedToCheckoutBtn()
  })

  it('Loads the Find a Coach page', async function () {
    findACoachPage = new FindACoachPage()
    await findACoachPage.verifyPageIsLoaded()
  })

  it('Verify text in Given Name text box', async function () {
    let expected = `Given Name`
    let actual = await findACoachPage.getFirstNameTextboxText('placeholder')
    assert.strictEqual(actual, expected, `${expected} is not displayed`)
  })

  it('Verify text in Family Name text box', async function () {
    let expected = `Family Name`
    let actual = await findACoachPage.getLastNameTextboxText('placeholder')
    assert.strictEqual(actual, expected, `${expected} is not displayed`)
  })

  it('Verify text in City text box is not displayed', async function () {
    let actual = await findACoachPage.isCityTextboxDisplayed()
    assert.isFalse(actual, 'City Name is displayed')
  })

  it('Verify market is defaulted to Singapore', async function () {
    let expected = `Singapore`
    let actual = await findACoachPage.getMarketDefaultText()
    assert.strictEqual(actual, expected, `${expected} is not displayed`)
  })

  it('Search a coach', async function () {
    await allureReporter.addDescription(`Search a coach ${coachFirstName} ${coachLastName}`)
    await findACoachPage.enterFirstAndLastNameToSearchCoach(coachFirstName, coachLastName)
    await findACoachPage.enterMarketToSearchCoach('All Markets')
    await findACoachPage.clickSearchCoachFindACoachButton()
  })

  it('Loads the Search Result page', async function () {
    searchCoachResultsPage = new SearchCoachResultsPage()
    await searchCoachResultsPage.verifyPageIsLoaded()
  })

  it('Verify Coach Search Result page body text', async function () {
    let expected = await SiteMapTestData.PAGE_BODY_TITLE(page.Home.SEARCH_COACH_RESULTS)
    let actual = await searchCoachResultsPage.getCoachSearchResultTitle()
    assert.strictEqual(actual, expected, 'Coach Search Result page body text is not matching with expected')
  })

  it('Verify searched coach is in search result list', async function () {
    screencapture = true
    await allureReporter.addDescription(`Verify ${coachFirstName} ${coachLastName} is in search result list`)
    let expected = `${coachFirstName} ${coachLastName}`
    let actual = await searchCoachResultsPage.getCoachName()
    assert.strictEqual(actual, expected, `Searched coach : ${expected} is not displayed`)
  })

  it('Click on Connect Me next to searched coach', async function () {
    await allureReporter.addDescription(`Click on Connect Me next to ${coachFirstName} ${coachLastName}`)
    await searchCoachResultsPage.clickConnectMeButton()
  })

  it('Loads the Create Account page', async function () {
    createAccountPage = new CreateAccountPage()
    await createAccountPage.verifyPageIsLoaded()
  })

  it('Verify Coach info displayed on top header', async function () {
    let actual = await header.getCoachInformation()
    let expected = `${coachFirstName} ${coachLastName}, ${coachID}`
    assert.strictEqual(actual, expected, `Coach Info : ${expected} is not displayed on top header`)
  })

  it('Create an Account', async function () {
    screencapture = true
    await allureReporter.addDescription(`Create an Account Full name: ${nameOnCard}, email: ${customerInfo.CUSTOMER_EMAIL}, password: ${GLOBAL_PASSWORD}`)
    await createAccountPage.createAnAccount(nameOnCard, customerInfo.CUSTOMER_EMAIL, GLOBAL_PASSWORD, TestData.SHIPPING_PHONE)
  })

  it('Loads the Checkout page', async function () {
    checkoutPage = new CheckoutPage()
    await checkoutPage.verifyPageIsLoaded()
    await checkoutCommonSection.verifyPageIsLoaded()
  })

  it('Verify Coach info displayed on top header', async function () {
    let actual = await checkoutCommonSection.getCoachInfoFromAvatar()
    let expected = `${coachFirstName} ${coachLastName}, ${coachID}`
    assert.strictEqual(actual, expected, `Coach Info : ${expected} is not displayed on top header`)
  })

  it('Verify Rewards Summary: Eligible For New Rewards', async function () {
    let expected = 'S$664.49'
    let actual = await checkoutCommonSection.getEligibleForNewRewards()
    assert.strictEqual(actual, expected, `Eligible For New Rewards :${expected} is not matching with actual`)
  })

  it('Verify Rewards Summary: Earning this order', async function () {
    let expected = 'S$66.45'
    let actual = await checkoutCommonSection.getEarningRewards()
    assert.strictEqual(actual, expected, `Earning this order :${expected} is not matching with actual`)
  })

  it('Loads the Shipping Address page', async function () {
    shippingAddressTile = new ShippingAddressTile()
    await shippingAddressTile.verifyPageIsLoaded()
  })

  it('Verify Order Total in Shipping Address Page', async function () {
    screencapture = true
    let expected = `Order Total: ${await CommonUtils.toCurrency(orderTotal, market.SG)}\nGST: ${await CommonUtils.toCurrency(gst, market.SG)}\nSubtotal ${await CommonUtils.toCurrency(orderSubTotal, market.SG)}\nDelivery Charge: ${deliveryCharge}`
    let actual = await checkoutCommonSection.getOrderSummaryTotals()
    assert.strictEqual(actual, expected, 'Order Total in Shipping Address Page is not matching with expected')
  })

  it('Updating Delivery Address information', async function () {
    await shippingAddressTile.verifyPageIsLoaded()
    await shippingAddressTile.addShippingAddressSG(TestData, true)
  })

  it('Check Default shipping check box', async function () {
    await shippingAddressTile.clickToCheckDefaultAddressCheckBox()
  })

  it('Shipping Address: click Next button', async function () {
    await shippingAddressTile.clickNext()
  })

  it('Loads the Suggested Address popup', async function () {
    popupSuggestedDeliveryAddressesForm = new PopupSuggestedDeliveryAddressesForm()
    await popupSuggestedDeliveryAddressesForm.verifyPageIsLoaded()
  })

  it('Click on Use this address button', async function () {
    await shippingAddressTile.verifyPageIsLoaded()
    await popupSuggestedDeliveryAddressesForm.clickUseThisAddress()
  })

  it('Loads the Payment page', async function () {
    paymentAndBillingAddressTile = new PaymentAndBillingAddressTile()
    await paymentAndBillingAddressTile.verifyPageIsLoadedSG()
  })

  it('Verify Coach info displayed on top header', async function () {
    let actual = await checkoutCommonSection.getCoachInfoFromAvatar()
    let expected = `${coachFirstName} ${coachLastName}, ${coachID}`
    assert.strictEqual(actual, expected, `Coach Info : ${expected} is not displayed on top header`)
  })

  it('Enter Payment Details information', async function () {
    await browser.pause(3000)
    await paymentAndBillingAddressTile.createPaymentDetailsSG(nameOnCard, cardNumber, expdt, cvv)
  })

  it('Click on Next button from Payment tile', async function () {
    await paymentAndBillingAddressTile.clickNextSG()
  })

  it('Loads the Final Review page', async function () {
    finalReviewTile = new FinalReviewTile(driver)
    await finalReviewTile.verifyPageIsLoaded()
  })

  it('Verify Coach info displayed on top header', async function () {
    let actual = await checkoutCommonSection.getCoachInfoFromAvatar()
    let expected = `${coachFirstName} ${coachLastName}, ${coachID}`
    assert.strictEqual(actual, expected, `Coach Info : ${expected} is not displayed on top header`)
  })

  it('Verify Order Total in final Review Tile Page', async function () {
    screencapture = true
    await allureReporter.addDescription(`Order Total: ${await CommonUtils.toCurrency(orderTotal, market.SG)}\nGST: ${await CommonUtils.toCurrency(gst, market.SG)}\nSubtotal ${await CommonUtils.toCurrency(orderSubTotal, market.SG)}\nDelivery Charge: ${deliveryCharge}`)
    let expected = `Order Total: ${await CommonUtils.toCurrency(orderTotal, market.SG)}\nGST: ${await CommonUtils.toCurrency(gst, market.SG)}\nSubtotal ${await CommonUtils.toCurrency(orderSubTotal, market.SG)}\nDelivery Charge: ${deliveryCharge}`
    let actual = await checkoutCommonSection.getOrderSummaryTotals()
    assert.strictEqual(actual, expected, 'Order Total in final Review Tile Page is not matching with expected')
  })

  it('Click on Estimated Rewards link', async function () {
    await checkoutPage.clickEstimatedRewards()
  })

  it('Verify Premier Rewards Summary Pop Up is displayed', async function () {
    rewardsPopup = new RewardEarningsPopUp()
    await rewardsPopup.verifyPageIsLoaded()
  })

  it('verify the Rewards Earnings Popup Displays and close the popup', async function () {
    await rewardsPopup.clickCloseRewardEarningsPopUp()
  })

  it('Click on Submit Order Button', async function () {
    await browser.pause(2000)
    await finalReviewTile.clickPlaceOrderButton()
  })

  it('Loads the Order Confirmation page', async function () {
    orderConfirmationPage = new OrderConfirmationPage()
    await orderConfirmationPage.verifyPageIsLoaded()
  })

  it('Verify the Coach information is not displayed in Order Summary Section', async function () {
    screencapture = true
    assert.isFalse(await orderConfirmationPage.isCoachInfoFromAvatarDisplayed(), 'Coach Information is displayed')
  })

  it('Verify Order Total in Confirmation Page', async function () {
    screencapture = true
    let expected = `Total ${await CommonUtils.toCurrency(orderTotal, market.SG)}\nGST: ${await CommonUtils.toCurrency(gst, market.SG)}\nSubtotal ${await CommonUtils.toCurrency(orderSubTotal, market.SG)}\nDelivery Charge: ${deliveryCharge}`
    let actual = await orderConfirmationPage.getOrderTotals()
    assert.strictEqual(actual, expected, 'Actual should match expected tagline')
    let order = await orderConfirmationPage.getOrderNumber()
    let resultArray = order.split(' ')
    orderNumber = await resultArray[4]
    await allureReporter.addDescription(`Order Number : ${orderNumber}`)
  })

  it('Click on Continue Shopping button on Order Confirmation page', async function () {
    await orderConfirmationPage.clickContinueShopping()
  })

  it('Loads the Home page', async function () {
    await header.verifyMyAccountLinkIsLoaded()
    await mainNavBar.verifyPageIsLoaded()
  })

  it('Clicks on My Account menu and gets Client ID', async function () {
    // await header.isMyAccountLinkLoaded()
    await header.clickMyAccount()
    myAccountPage = new MyAccountPage()
    await myAccountPage.verifyPageIsLoaded()
    accountID = await myAccountPage.getAccountID()
    accountID = accountID.substring(11, accountID.length)
  })

  it('Loads My Account page', async function () {
    myAccountLeftNavigation = new MyAccountLeftNavigation()
    await myAccountLeftNavigation.verifyPageIsLoaded()
  })

  it('Click on Order History', async function () {
    await myAccountLeftNavigation.clickOrderHistory()
  })

  it('Loads the Order History page', async function () {
    orderHistoryPage = new OrderHistoryPage()
    await orderHistoryPage.verifyPageIsLoaded()
  })

  it('Verify P icon displays to the order', async function () {
    await orderHistoryPage.verifyPIconDisplays()
  })

  it('Clicks on the Order List', async function () {
    await orderHistoryPage.clickOnOrder()
  })

  it('Verify Order Total in Order History Page', async function () {
    let expected = `Total ${await CommonUtils.toCurrency(orderTotal, market.SG)}\nGST: ${await CommonUtils.toCurrency(gst, market.SG)}\nSubtotal ${await CommonUtils.toCurrency(orderSubTotal, market.SG)}\nDelivery Charge ${deliveryCharge}`
    let actual = await checkoutCommonSection.getOrderSummaryTotals()
    assert.strictEqual(actual, expected, 'Order Total in Order History Page is not matching with expected')
  })

  it('Verify My Account page is loaded', async function () {
    screencapture = true
    await myAccountLeftNavigation.verifyPageIsLoaded()
  })

  it('Click on OPTAVIA Premier Order link under My Account', async function () {
    await myAccountLeftNavigation.clickOptaviaPremierOrder()
  })

  it('Loads the Subscriptions page', async function () {
    optaviaPremierOrderPage = new OptaviaPremierOrderPage()
    await optaviaPremierOrderPage.verifyPageIsLoaded()
  })

  it('Verify Items in Order section title displays', async function () {
    let expected = await SiteMapTestData.PAGE_BODY_SECTION_3(page.MyAccount.OPTAVIA_PREMIER_ORDER)
    let actual = await optaviaPremierOrderPage.getOptaviaPremierItemsInOrderText()
    assert.strictEqual(actual, expected, 'Items in Order section title is not displayed')
  })

  it('Verify Order Total in Order Premier Page', async function () {
    let expected = `Order Total ${await CommonUtils.toCurrency(premierPageOrderTotal, market.SG)}\nGST: ${await CommonUtils.toCurrency(premierPageGst, market.SG)}\nSubtotal ${await CommonUtils.toCurrency(premierPageOrderSubtotal, market.SG)}\nApplied Rewards: -${await CommonUtils.toCurrency(rewardsApplied, market.SG)}\nDelivery Charge: ${deliveryCharge}`
    let actual = await checkoutCommonSection.getOrderSummaryTotals()
    assert.strictEqual(actual, expected, 'Order Total in Order Premier Page not matching with expected')
  })

  it('Order Summary: Order Total', async function () {
    let expected = await CommonUtils.toCurrency(premierPageOrderTotal, market.SG)
    let actual = await optaviaPremierOrderPage.getPremierOrderTotalText()
    assert.strictEqual(actual, expected, 'Order Summary: Order Total is not matching with expected')
  })

  it('Verify Rewards Summary: Eligible For New Rewards', async function () {
    let expected = await CommonUtils.toCurrency(eligibleForNewRewards, market.SG)
    let actual = await optaviaPremierOrderPage.getEligibleForRewardsText()
    assert.strictEqual(actual, expected, 'Rewards Summary: Eligible For New Rewards is not matching with expected')
  })

  it('Verify Rewards Summary: Earning this order', async function () {
    let expected = await CommonUtils.toCurrency(earnThisOrder, market.SG)
    let actual = await optaviaPremierOrderPage.getEarningThisOrderText()
    assert.strictEqual(actual, expected, 'Rewards Summary: Earning this order is not matching with expected')
  })

  it(`Navigate to Choose Your Own Fuelings`, async function () {
    screencapture = true
    await mainNavBar.clickChooseYourOwnFuelings()
    addItemsToCartPopup = new ChooseYourOwnFuelingsPopup()
    await addItemsToCartPopup.verifyPageIsLoaded()
  })

  it('Search and add searched product', async function () {
    await addItemsToCartPopup.searchForProduct(product1Name)
    await addItemsToCartPopup.increaseItemQuantity('10')
    await addItemsToCartPopup.selectProduct()
    await addItemsToCartPopup.clickAddItemsToCart()
  })

  it('Loads the Shopping Cart Page', async function () {
    shoppingCartPage = new ShoppingCartPage(driver)
    await shoppingCartPage.verifyPageIsLoaded()
  })

  it('Click on Proceed to Checkout button', async function () {
    await shoppingCartPage.clickProceedToCheckoutBtn()
  })

  it('Loads the Payment page', async function () {
    paymentAndBillingAddressTile = new PaymentAndBillingAddressTile(driver)
    await paymentAndBillingAddressTile.verifyPageIsLoadedSG()
  })

  it('Verify Coach info displayed on top header', async function () {
    let actual = await checkoutCommonSection.getCoachInfoFromAvatar()
    let expected = `${coachFirstName} ${coachLastName}, ${coachID}`
    assert.strictEqual(actual, expected, `Coach Info : ${expected} is not displayed on top header`)
  })

  it('Enter Existing Payment Details information', async function () {
    await paymentAndBillingAddressTile.clickOnExistingCardDetailsSG(cvv)
  })

  it('Click on Next button from Payment tile', async function () {
    await paymentAndBillingAddressTile.clickNextSG()
  })

  it('Loads the Final Review page', async function () {
    finalReviewTile = new FinalReviewTile()
    await finalReviewTile.verifyPageIsLoaded()
  })

  it('Verify Coach info displayed on top header', async function () {
    let actual = await checkoutCommonSection.getCoachInfoFromAvatar()
    let expected = `${coachFirstName} ${coachLastName}, ${coachID}`
    assert.strictEqual(actual, expected, `Coach Info : ${expected} is not displayed on top header`)
  })

  it('Click on Submit Order button from Final Review tile', async function () {
    await finalReviewTile.clickPlaceOrderButton()
  })

  it('Loads the Order Confirmation page', async function () {
    screencapture = true
    await orderConfirmationPage.verifyPageIsLoaded()
    let actual = await orderConfirmationPage.getOrderNumber()
    let resultArray = actual.split(' ')
    actualOrderNumber = await resultArray[4]
    await allureReporter.addDescription(`Order Number : ${actualOrderNumber}`)
  })

  it('Verify the Coach information is not displayed in Order Summary Section', async function () {
    screencapture = true
    assert.isFalse(await orderConfirmationPage.isCoachInfoFromAvatarDisplayed(), 'Coach Information is displayed')
  })

  it('Click on Continue Shopping button on Order Confirmation page', async function () {
    await orderConfirmationPage.clickContinueShopping()
  })

  it('Loads the Home page', async function () {
    await header.verifyMyAccountLinkIsLoaded()
    await mainNavBar.verifyPageIsLoaded()
  })

  it('Clicks on My Account menu ', async function () {
    await header.clickMyAccount()
    myAccountPage = new MyAccountPage()
    await myAccountPage.verifyPageIsLoaded()
    accountID = await myAccountPage.getAccountID()
    accountID = accountID.substring(11, accountID.length)
    await allureReporter.addDescription(`Account ID : ${accountID}`)
  })

  it('Verify My Account Left Navigation is loaded', async function () {
    myAccountLeftNavigation = new MyAccountLeftNavigation()
    await myAccountLeftNavigation.verifyPageIsLoaded()
  })

  it('Click on OPTAVIA Premier Order link under My Account', async function () {
    await myAccountLeftNavigation.clickOptaviaPremierOrder()
  })

  it('Loads the Subscriptions page', async function () {
    optaviaPremierOrderPage = new OptaviaPremierOrderPage()
    await optaviaPremierOrderPage.verifyPageIsLoaded()
  })

  it('Logout Coach', async function () {
    await header.verifyLogOutIsLoaded()
    await header.clickLogOut()
    await header.verifyPageIsLoaded()
  })

  it('Get the Auto order ID, Commissionable_Volume , Qualifying_Volume from DB', async function () {
    let db = await TestDataBaseMapping.getAutoOrderHeaderView(accountID)
    templateID = db.Auto_Order_ID
    commissonableVolume = db.Commissionable_Volume
    qualifyingvolume = db.Qualifying_Volume
  })

  it('Verify the Commissionable_Volume from template', async function () {
    let actual = commissonableVolume
    let expected = 602.39
    assert.strictEqual(actual, expected, `Commissionable_Volume from template is not matching with :${expected}`)
  })

  it('Verify the Qualifying_Volume from template', async function () {
    let actual = qualifyingvolume.toFixed(1)
    let expected = 342.6
    expected = expected.toFixed(1)
    assert.strictEqual(actual, expected, `Qualifying_Volume from template is not matching with :${expected}`)
  })

  afterEach(async function () {
    await driverutils.saveScreenshots(this.currentTest.state, screencapture, driver)
    screencapture = false
  })
})

describe('C5762 - Login to Hybris Backoffice as CSA to validate QV/CV the Template order', function () {
  let testTitle = this.title

  before(async function () {
    await driverutils.goToHybrisBackofficeSitePage()
  })

  beforeEach(async function () {
    await driverutils.addAllureReport(allureReporter, __dirname, 'SG OPTAVIA Sanity', testTitle)
  })

  it('Preparing Test Data', async function () {
    agentEmail = AGENT_USER
    agentPassword = GLOBAL_PASSWORD
  })

  it('Hybris Backoffice page is loaded ', async function () {
    hybrisBackofficeLoginPage = new HybrisBackofficeLoginPage()
    await hybrisBackofficeLoginPage.verifyPageIsLoaded()
  })

  it('Login to Backoffice', async function () {
    await hybrisBackofficeLoginPage.loginAsCSR(agentEmail, agentPassword)
  })

  it('Backoffice Authority group page displays ', async function () {
    hybrisBackofficeAutorityGroupPage = new HybrisBackofficeAutorityGroupPage()
    await hybrisBackofficeAutorityGroupPage.verifyPageIsLoaded()
  })

  it('Click on Customer Support Administrator Role radio button', async function () {
    await browser.pause(3000)
    await hybrisBackofficeAutorityGroupPage.proceedAsCSAdminRole()
    hybrisBackofficeHomePage = new HybrisBackofficeHomePage()
    await hybrisBackofficeHomePage.verifyPageIsLoaded()
  })

  it('Click on Orders link from Customer Admin section', async function () {
    await hybrisBackofficeHomePage.clickASMOrder()
  })

  it('Click on Order link from Customer Admin section', async function () {
    await browser.pause(3000)
    await hybrisBackofficeHomePage.clickOrder()
  })

  it('Search Textbox is loaded', async function () {
    hybrisBackofficeSearchPage = new HybrisBackofficeSearchPage()
    hybrisBackofficeSearchPage.verifyPageIsLoaded()
  })

  it('Search the template ID number', async function () {
    await hybrisBackofficeSearchPage.searchOrder(templateID)
  })

  it('Click on Searched result', async function () {
    await hybrisBackofficeSearchPage.clickSearchedOrder(templateID)
  })

  it('Verify Order Details body title displays', async function () {
    hybrisBackofficeOrderDetailsPage = new HybrisBackofficeOrderDetailsPage()
    await hybrisBackofficeOrderDetailsPage.verifyPageIsLoaded(templateID)
  })

  it('Click on Administration Report', async function () {
    await browser.pause(4000)
    await hybrisBackofficeOrderDetailsPage.clickAdminReportTab()
  })

  it('Verify the Order commissionable Volume is same as Order total', async function () {
    await browser.pause(3000)
    let expected = '602.39'
    let actual = await hybrisBackofficeOrderDetailsPage.getOrderCommissionalbleVolume()
    decimalLength = actual.split('.')
    assert.isTrue((decimalLength[1].length === 2), 'length is not 2')
    assert.strictEqual(actual, expected, `Order commissionable volume :${actual} is not matched with Order Total`)
  })

  it('Verify the Qualifying Volume ', async function () {
    let expected = 342.6
    expected = expected.toFixed(1)
    let actual = await hybrisBackofficeOrderDetailsPage.getQualifyingVolume()
    actual = parseFloat(actual).toFixed(1)
    decimalLength = actual.split('.')
    assert.isTrue((decimalLength[1].length === 1), 'length is not 2')
    assert.strictEqual(actual, expected, `Qualifying volume:${actual} is not matched with expected value`)
  })

  // verify Qv/CV for actual order
  it('Search the on Demand Order number', async function () {
    await hybrisBackofficeSearchPage.searchOrder(actualOrderNumber)
  })

  it('Click on Search result', async function () {
    await hybrisBackofficeSearchPage.clickSearchedOrder(actualOrderNumber)
  })

  it('Verify Order Details body title displays', async function () {
    await hybrisBackofficeOrderDetailsPage.verifyPageIsLoaded(actualOrderNumber)
  })

  it('Click on Administration Report', async function () {
    await hybrisBackofficeOrderDetailsPage.clickAdminReportTab()
  })

  it('Verify the Order commissionable Volume is same as Order total', async function () {
    await browser.pause(3000)
    let expected = '369.20'
    let actual = await hybrisBackofficeOrderDetailsPage.getOrderCommissionalbleVolume()
    decimalLength = actual.split('.')
    assert.isTrue((decimalLength[1].length === 2), 'length is not 2')
    assert.strictEqual(actual, expected, `Order commissionable volume :${actual} is not matched with Order Total`)
  })

  it('Verify the Qualifying Volume ', async function () {
    let expected = '210.00'
    let actual = await hybrisBackofficeOrderDetailsPage.getQualifyingVolume()
    decimalLength = actual.split('.')
    assert.isTrue((decimalLength[1].length === 2), 'length is not 2')
    assert.strictEqual(actual, expected, `Qualifying volume:${actual} is not matched with expected value`)
  })
  it('Click on Sign out button', async function () {
    await browser.pause(6000)
    await hybrisBackofficeHomePage.clickSignOut()
  })

  afterEach(async function () {
    await driverutils.saveScreenshots(this.currentTest.state, screencapture)
    screencapture = false
  })
})
