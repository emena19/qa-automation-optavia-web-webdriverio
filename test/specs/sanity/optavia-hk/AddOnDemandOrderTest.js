// @flow
import {assert} from 'chai'
import driverutils from '@core-libs/DriverUtils'
import ProductTestData, {attb, market, product} from '@input-data/products/ProductTestData'
import CustomerTestData from '@input-data/customers/CustomerTestData'
import CoachTestData from '@input-data/coaches/CoachTestData'
import TestCreditCards from '@input-data/creditcards/CreditCardTestData'
import HomePage from '@page-objects/home/HomePage'
import Header from '@page-objects/common/Header'
import MainNavBar from '@page-objects/common/MainNavBar'
import LoginPage from '@page-objects/loginPage/LoginPage'
import {AGENT_USER, GLOBAL_PASSWORD} from '@core-libs/ConstantUtil'
import MyAccountPage from '@page-objects/myaccount/MyAccountPage'
import ShoppingCartPage from '@page-objects/shoppingcart/ShoppingCartPage'
import ShopAllPage from '@page-objects/shop/ShopAllPage'
import ChooseYourOwnFuelingsPopup from '@page-objects/chooseyourownfuelings/ChooseYourOwnFuelingsPopup'
import ShopToolsAndAccessoriesPage from '@page-objects/shop/ShopToolsAndAccessoriesPage'
import AddedToYourShoppingCartPopup from '@page-objects/shop/AddedToYourShoppingCartPopup'
import PaymentAndBillingAddressTile from '@page-objects/checkout/PaymentAndBillingAddressTile'
import CheckoutCommonSection from '@page-objects/checkout/CheckoutCommonSection'
import FinalReviewTile from '@page-objects/checkout/FinalReviewTile'
import ShippingAddressTile from '@page-objects/checkout/ShippingAddressTile'
import PopupSuggestedDeliveryAddressesForm
  from '@page-objects/popupsuggesteddeliveryaddressesform/PopupSuggestedDeliveryAddressesForm'
import OrderConfirmationPage from '@page-objects/orderconfirmation/OrderConfirmationPage'
import allureReporter from '@wdio/allure-reporter'
import HybrisBackofficeLoginPage from '@page-objects/hybrisbackoffice/HybrisBackofficeLoginPage'
import HybrisBackofficeAutorityGroupPage from '@page-objects/hybrisbackoffice/HybrisBackofficeAutorityGroupPage'
import HybrisBackofficeHomePage from '@page-objects/hybrisbackoffice/HybrisBackofficeHomePage'
import HybrisBackofficeCustomerPage from '@page-objects/hybrisbackoffice/HybrisBackofficeCustomerPage'

let screencapture = false // capture screenshot when test step 'passed' also by default only capture screenshot of failed steps
let homePage, header, loginPage, mainNavBar, shopAllPage,
  shoppingCartPage, shippingAddressTile, paymentAndBillingAddressTile, searchProduct, chooseYourOwnFuelings,
  myAccountPage, shopToolsAndAccessoriesPage, addedToYourShoppingCartPopup, finalReviewPage, popupSuggestedDeliveryAddressesForm

// Customer Registration
let TestData, email

// Coach (Enroller) Info
let coachFirstName, coachLastName, coachName, coachID

// Product search
let addItemsToCartPopup

// Order Confirmation Page
let orderConfirmationPage, orderNumber, checkoutCommonSection

// Payment Details
let cvv

// Hybris Backoffice
let agentEmail, agentPassword, hybrisBackofficeLoginPage, hybrisBackofficeAutorityGroupPage, hybrisBackofficeHomePage, hybrisBackofficeCustomerPage

describe('C5755 - HK - OPTAVIA -Existing Customer- Add OnDemand Order', function () {
  let testTitle = this.title

  before(async function () {
    await driverutils.goToOptaviaWebSiteHomePageHK()
  })

  beforeEach(async function () {
    await driverutils.addAllureReport(allureReporter, __dirname, 'HK OPTAVIA Sanity', testTitle)
  })

  it('Preparing Test Data', async function () {
    let sh = 3
    // Product Information
    searchProduct = await ProductTestData.PRODUCT_INFO(attb.NAME, product.tools_accessories.OPT_HABITS_HEALTH_SYSTEM, market.HK)

    // Coach Information
    let coachRow = 6
    coachFirstName = await CoachTestData.COACH_FIRST_NAME(coachRow, sh)
    coachLastName = await CoachTestData.COACH_LAST_NAME(coachRow, sh)
    coachName = `${coachFirstName} ${coachLastName}`
    coachID = await CoachTestData.COACH_ID(coachRow, sh)

    // Client Info
    TestData = await CustomerTestData.getCustomerInfo('85220000404389')
    email = TestData.CUSTOMER_EMAIL

    // Payment Information
    let ccCol = 'B'
    let sheet = 2
    cvv = await TestCreditCards.CVV(sheet, ccCol)
  })

  it('Loads the Home Page', async function () {
    homePage = new HomePage()
    await homePage.verifyPageIsLoaded()
    header = new Header()
    await header.verifyPageIsLoaded()
    mainNavBar = new MainNavBar()
    await mainNavBar.verifyPageIsLoaded()
    await header.verifyFlagIsLoaded()
  })

  it('Navigate to Login Page', async function () {
    await header.clickLogIn()
    loginPage = new LoginPage()
    await loginPage.verifyPageIsLoaded()
  })

  it('Login as existing Hong Kong Coach', async function () {
    await allureReporter.addDescription(`Login as existing Singapore coach email: ${TestData.CUSTOMER_EMAIL} and password: ${GLOBAL_PASSWORD}`)
    await loginPage.loginAsExistingCustomer(TestData.CUSTOMER_EMAIL, GLOBAL_PASSWORD)
  })

  it('Loads My Account Page', async function () {
    myAccountPage = new MyAccountPage()
    await myAccountPage.verifyPageIsLoaded()
  })

  it('Verify Coach Information', async function () {
    let expected = `${coachName}, ${coachID}`
    let actual = await header.getCoachInformation()
    assert.strictEqual(actual, expected, `Coach Info : ${expected} is not displayed on top header`)
  })

  it('Click on the Cart Icon from header', async function () {
    await header.clickCartIcon()
    shoppingCartPage = new ShoppingCartPage()
    await shoppingCartPage.verifyPageIsLoaded()
  })

  it('Remove items from the cart', async function () {
    let i = await shoppingCartPage.getCartItemsArrayListCount()
    for (let p = 0; p < (i); p++) {
      if (i) {
        await shoppingCartPage.clickRemoveProductIcon()
      }
    }
    await allureReporter.addDescription('No Products available in the cart')
  })

  it('Loads the Shop All Page', async function () {
    await mainNavBar.clickShopAll()
    shopAllPage = new ShopAllPage()
    await shopAllPage.verifyPageIsLoaded()
  })

  it('Navigate to Choose Your Own Fuelings from Shop', async function () {
    await mainNavBar.clickChooseYourOwnFuelings()
    addItemsToCartPopup = new ChooseYourOwnFuelingsPopup()
    await addItemsToCartPopup.verifyPageIsLoaded()
  })

  it('Loads the Choose Your Own Fuelings from Shop ', async function () {
    chooseYourOwnFuelings = new ChooseYourOwnFuelingsPopup()
    await chooseYourOwnFuelings.verifyPageIsLoaded()
    await chooseYourOwnFuelings.selectButton()
    await chooseYourOwnFuelings.removeAllButton()
    await chooseYourOwnFuelings.addItemsToCartButton()
  })

  it('Close choose  your own Fuelings ', async function () {
    await chooseYourOwnFuelings.clickClosebutton()
  })

  it('Clicks Shop Button under Tools and Accessories', async function () {
    await shopAllPage.clickToolsAndAccessoriesShopBtn()
  })

  it('Search and add searched product', async function () {
    await allureReporter.addDescription(`Search product SKU = ${searchProduct}`)
    shopToolsAndAccessoriesPage = new ShopToolsAndAccessoriesPage()
    await shopToolsAndAccessoriesPage.verifyPageIsLoaded()
    await shopToolsAndAccessoriesPage.clickProductsInToolsAndAccessories(searchProduct)
  })

  it('Loads the Added To Your Shopping Cart Popup', async function () {
    addedToYourShoppingCartPopup = new AddedToYourShoppingCartPopup()
    await addedToYourShoppingCartPopup.verifyPageIsLoaded()
  })

  it('Click on Checkout button on pop up window', async function () {
    await addedToYourShoppingCartPopup.clickCheckoutButton()
  })

  it('Loads the Shopping Cart Page', async function () {
    shoppingCartPage = new ShoppingCartPage()
    await shoppingCartPage.verifyPageIsLoaded()
  })

  it('Click on Proceed to Checkout button', async function () {
    await shoppingCartPage.clickProceedToCheckoutBtn()
  })

  it('Loads the Payment page', async function () {
    paymentAndBillingAddressTile = new PaymentAndBillingAddressTile()
    await paymentAndBillingAddressTile.verifyPageIsLoadedSG()
  })

  it('Loads the CheckoutCommonSection page', async function () {
    checkoutCommonSection = new CheckoutCommonSection()
    await checkoutCommonSection.verifyPageIsLoaded()
  })

  it('Verify Coach info displayed on top header', async function () {
    let actual = await checkoutCommonSection.getCoachInfoFromAvatar()
    let expected = `${coachFirstName} ${coachLastName}, ${coachID}`
    assert.strictEqual(actual, expected, `Coach Info : ${expected} is not displayed on top header`)
  })

  it('Verify Discount of HK$40 not displayed on Order Summary', async function () {
    assert.isFalse(await checkoutCommonSection.isDiscountTextDisplayed(), 'Discount of HK$40 is displayed')
  })

  it('Enter Existing Payment Details information', async function () {
    await paymentAndBillingAddressTile.clickOnSpecificCardDetails('visa', '1111', cvv)
  })

  it('Click on Next button from Payment tile', async function () {
    await paymentAndBillingAddressTile.clickNextHK()
  })

  it('Loads the Final Review page', async function () {
    finalReviewPage = new FinalReviewTile()
    await finalReviewPage.verifyPageIsLoaded()
  })

  it('Verify Coach info displayed on top header', async function () {
    let actual = await checkoutCommonSection.getCoachInfoFromAvatar()
    let expected = `${coachFirstName} ${coachLastName}, ${coachID}`
    assert.strictEqual(actual, expected, `Coach Info : ${expected} is not displayed on top header`)
  })

  it('Click on Edit Button under Shipping Delivery Address', async function () {
    await finalReviewPage.clickDeliveryAddressEditBtn()
  })

  it('Loads the Shipping Address page', async function () {
    shippingAddressTile = new ShippingAddressTile()
    await shippingAddressTile.verifyPageIsLoaded()
  })

  // Covers test case C23135
  it('Verify text in the Country Value says "Hong Kong"', async function () {
    let actual = await shippingAddressTile.getDisabledCountryText()
    let expected = TestData.SHIPPING_FULL_STATE_NAME
    assert.strictEqual(actual, expected, 'Country Value is not Hong Kong')
  })

  it('Verify Coach info displayed on top header', async function () {
    let actual = await checkoutCommonSection.getCoachInfoFromAvatar()
    let expected = `${coachFirstName} ${coachLastName}, ${coachID}`
    assert.strictEqual(actual, expected, `Coach Info : ${expected} is not displayed on top header`)
  })

  it('Clears the text in Delivery Address', async function () {
    let blankAddress = {
      SHIPPING_ADDRESS_LINE_1: '',
      SHIPPING_DISTRICT: '',
      SHIPPING_PHONE: '',
      SHIPPING_COUNTRY_CODE: ''
    }
    await shippingAddressTile.addShippingAddressHK(blankAddress)
  })

  it('Updating Delivery Address information', async function () {
    await shippingAddressTile.verifyPageIsLoaded()
    await shippingAddressTile.addShippingAddressHK(TestData, true)
  })

  it('Shipping Address: click Next button', async function () {
    await shippingAddressTile.clickNext()
  })

  it('Loads the Suggested Address popup', async function () {
    popupSuggestedDeliveryAddressesForm = new PopupSuggestedDeliveryAddressesForm()
    await popupSuggestedDeliveryAddressesForm.verifyPageIsLoaded()
  })

  it('Click on Use this address button', async function () {
    await shippingAddressTile.verifyPageIsLoaded()
    await popupSuggestedDeliveryAddressesForm.clickUseThisAddress()
  })

  it('Loads the Payment page', async function () {
    await paymentAndBillingAddressTile.verifyPageIsLoadedSG()
  })

  it('Verify Coach info displayed on top header', async function () {
    let actual = await checkoutCommonSection.getCoachInfoFromAvatar()
    let expected = `${coachFirstName} ${coachLastName}, ${coachID}`
    assert.strictEqual(actual, expected, `Coach Info : ${expected} is not displayed on top header`)
  })

  it('Enter Existing Payment Details information', async function () {
    await paymentAndBillingAddressTile.clickOnSpecificCardDetails('visa', '1111', cvv)
  })

  it('Click on Next button from Payment tile', async function () {
    await paymentAndBillingAddressTile.clickNextSG()
  })

  it('Verify Coach info displayed on top header', async function () {
    let actual = await checkoutCommonSection.getCoachInfoFromAvatar()
    let expected = `${coachFirstName} ${coachLastName}, ${coachID}`
    assert.strictEqual(actual, expected, `Coach Info : ${expected} is not displayed on top header`)
  })

  it('Click on Submit Order button from Final Review tile', async function () {
    await finalReviewPage.clickPlaceOrderButton()
  })

  it('Loads the Order Confirmation page', async function () {
    screencapture = true
    orderConfirmationPage = new OrderConfirmationPage()
    await orderConfirmationPage.verifyPageIsLoaded()
    let actual = await orderConfirmationPage.getOrderNumber()
    let resultArray = actual.split(' ')
    orderNumber = resultArray[4]
    await allureReporter.addDescription(`Order Number : ${orderNumber}`)
  })

  it('Verify the Coach information is not displayed in Order Summary Section', async function () {
    screencapture = true
    assert.isFalse(await orderConfirmationPage.isCoachInfoFromAvatarDisplayed(), 'Coach Information is Displayed')
  })

  it('Click on Continue Shopping button on Order Confirmation page', async function () {
    await orderConfirmationPage.clickContinueShopping()
  })

  it('Loads the Home page', async function () {
    await header.verifyMyAccountLinkIsLoaded()
    await mainNavBar.verifyPageIsLoaded()
  })

  it('Logout Customer', async function () {
    await header.verifyLogOutIsLoaded()
    await header.clickLogOut()
    await header.verifyPageIsLoaded()
  })

  it('Verify coach information is not displayed', async function () {
    assert.isFalse(await header.isCoachInformationDisplayed(), 'Coach Information is Displayed')
  })

  afterEach(async function () {
    await driverutils.saveScreenshots(this.currentTest.state, screencapture)
    screencapture = false
  })
})

describe('C5755 - Login to Hybris Backoffice as customer Support Admin role and verify the county value is same as district in OPTAVIA Page', function () {
  before(async function () {
    await driverutils.goToHybrisBackofficeSitePage()
  })

  beforeEach(async function () {
    await driverutils.addAllureReport(allureReporter, __dirname, 'HK OPTAVIA Sanity', `C5755 - HK - OPTAVIA -Existing Customer- Add OnDemand Order ${email}`)
  })

  it('Preparing Test Data', async function () {
    agentEmail = AGENT_USER
    agentPassword = GLOBAL_PASSWORD
  })

  it('Hybris Backoffice page is loaded ', async function () {
    hybrisBackofficeLoginPage = new HybrisBackofficeLoginPage()
    await hybrisBackofficeLoginPage.verifyPageIsLoaded()
  })

  it('Login to Hybris', async function () {
    await hybrisBackofficeLoginPage.loginAsCSR(agentEmail, agentPassword)
  })

  it('Backoffice Authority group page displays ', async function () {
    hybrisBackofficeAutorityGroupPage = new HybrisBackofficeAutorityGroupPage()
    await hybrisBackofficeAutorityGroupPage.verifyPageIsLoaded()
  })

  it('Click on Admin Role', async function () {
    await hybrisBackofficeAutorityGroupPage.proceedAsCSAdminRole()
    hybrisBackofficeHomePage = new HybrisBackofficeHomePage()
    await hybrisBackofficeHomePage.verifyPageIsLoaded()
  })

  it('Click on User - Customers', async function () {
    await hybrisBackofficeHomePage.clickOnUserItem()
  })

  it('Click on customer Sub Item', async function () {
    await hybrisBackofficeHomePage.clickOnCustomerSubItem()
  })

  it('Search for Existing Customer ', async function () {
    await browser.pause(5000)
    hybrisBackofficeCustomerPage = new HybrisBackofficeCustomerPage()
    await hybrisBackofficeCustomerPage.searchCustomer(email)
  })

  it('Click on first registry', async function () {
    await browser.pause(5000)
    await hybrisBackofficeCustomerPage.clickOnFirstRegistry()
  })

  it('Click on Address Tab', async function () {
    await hybrisBackofficeCustomerPage.clickOnTopMenuAddress()
  })

  it('Click on first details on Address history', async function () {
    await browser.pause(5000)
    await hybrisBackofficeCustomerPage.doubleClickOnAddressHistory(4)
  })

  it('Click on Admin Tab on Address History pop up', async function () {
    await hybrisBackofficeCustomerPage.clickOnAdminTab()
  })

  it('Verify the County value is same as District in OPTAVIA page', async function () {
    let expected = TestData.SHIPPING_DISTRICT
    let actual = await hybrisBackofficeCustomerPage.getCountyValue()
    assert.strictEqual(actual.toString(), expected, 'County Value is not matched with expected value')
  })

  afterEach(async function () {
    await driverutils.saveScreenshots(this.currentTest.state, screencapture)
    screencapture = false
  })
})
