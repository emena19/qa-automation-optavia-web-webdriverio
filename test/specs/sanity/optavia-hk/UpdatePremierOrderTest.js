// @flow
import {assert} from 'chai'
import driverutils from '@core-libs/DriverUtils'
import CustomerTestData from '@input-data/customers/CustomerTestData'
import ProductTestData, {attb, market, product} from '@input-data/products/ProductTestData'
import HomePage from '@page-objects/home/HomePage'
import Header from '@page-objects/common/Header'
import MainNavBar from '@page-objects/common/MainNavBar'
import LoginPage from '@page-objects/loginPage/LoginPage'
import MyAccountPage from '@page-objects/myaccount/MyAccountPage'
import MyAccountLeftNavigation from '@page-objects/myaccount/MyAccountLeftNavigation'
import OptaviaPremierOrderPage from '@page-objects/myaccount/OptaviaPremierOrderPage'
import SiteMapTestData, {page} from '@input-data/various/MiscTestData'
import ShoppingCartPage from '@page-objects/shoppingcart/ShoppingCartPage'
import CommonUtils from '@core-libs/CommonUtils'
import DatePickerOptaviaPremier from '@page-objects/myaccount/DatePickerOptaviaPremier'
import TestDataBaseMapping from '@db/TestDataBaseMapping'
import allureReporter from '@wdio/allure-reporter'

describe('C5759 - HK OPTAVIA - My Account - OPTAVIA Premier Order - Update OPTAVIA Premier Order', function () {
  let screencapture = false // True to capture screenshot when test step 'passed'. By default only capture screenshot of failed steps

  let homePage, header, loginPage, myAccountPage, optaviaPremierOrderPage, myAccountLeftNavigation, mainNavBar
  let optaviaTemplateItems, shoppingCartPage, productUnitPriceArray, productQtyArray, productPriceArray,
    productSubtotal, deliveryCharge, orderTotal, CartOptaviaItems, datePickerOptaviaPremier
  let accountID, searchProductPrice
  // Customer Registration
  let email, password

  // Optavia Premier Schedule
  let orderDate, actualNextOrderDate

  let testTitle = this.title

  before(async function () {
    await driverutils.goToOptaviaWebSiteHomePageHK()
  })

  beforeEach(async function () {
    await driverutils.addAllureReport(allureReporter, __dirname, 'HK OPTAVIA Sanity', testTitle)
  })

  it('Preparing Test Data', async function () {
    let custID = '85220000500648'
    let TestData = await CustomerTestData.getCustomerInfo(custID)
    email = TestData.CUSTOMER_EMAIL
    password = TestData.CUSTOMER_PASSWORD
    searchProductPrice = await ProductTestData.PRODUCT_INFO(attb.UNIT_PRICE, product.bars.OPT_LEMON_CRISP_BAR, market.HK)
  })

  it('Loads the Home Page', async function () {
    homePage = new HomePage()
    await homePage.verifyPageIsLoaded()
    header = new Header()
    await header.verifyPageIsLoaded()
    mainNavBar = new MainNavBar()
    await mainNavBar.verifyPageIsLoaded()
  })

  it('Navigate to Login page', async function () {
    await header.clickLogIn()
    loginPage = new LoginPage()
    await loginPage.verifyPageIsLoaded()
  })

  it('Login as existing active autoship Customer', async function () {
    screencapture = true
    await allureReporter.addDescription(`Login as existing Customer email: ${email} and password: ${password}`)
    await loginPage.loginAsExistingCustomer(email, password)
  })

  it('Verify My Account page is loaded', async function () {
    screencapture = true
    myAccountPage = new MyAccountPage()
    await myAccountPage.verifyPageIsLoaded()
    myAccountLeftNavigation = new MyAccountLeftNavigation()
    await myAccountLeftNavigation.verifyPageIsLoaded()
    accountID = await myAccountPage.getAccountID()
    accountID = accountID.substring(11, accountID.length)
  })

  it('Click on OPTAVIA Premier Order link under My Account', async function () {
    await myAccountLeftNavigation.clickOptaviaPremierOrder()
  })

  it('Loads the Subscriptions page', async function () {
    optaviaPremierOrderPage = new OptaviaPremierOrderPage()
    await optaviaPremierOrderPage.verifyPageIsLoaded()
  })

  it('Verify Items in Order section title displays', async function () {
    let expected = await SiteMapTestData.PAGE_BODY_SECTION_3(page.MyAccount.OPTAVIA_PREMIER_ORDER)
    let actual = await optaviaPremierOrderPage.getOptaviaPremierItemsInOrderText()
    assert.strictEqual(actual, expected, 'Items in Order section is not displayed')
  })

  it('Verify Modify Order button is displayed', async function () {
    await optaviaPremierOrderPage.verifyModifyOrderBtnIsLoaded()
  })

  it('Click on Modify Order button', async function () {
    optaviaTemplateItems = await optaviaPremierOrderPage.getProductArrayList()
    productQtyArray = await optaviaPremierOrderPage.getPremierProductQtyArrayList()
    productUnitPriceArray = await optaviaPremierOrderPage.getPremierProductUnitPriceArrayList()
    productPriceArray = await optaviaPremierOrderPage.getPremierProductPriceArrayList()
    productSubtotal = await optaviaPremierOrderPage.getPremierSubtotalText()
    deliveryCharge = await optaviaPremierOrderPage.getPremierDeliveryChargeText()
    orderTotal = await optaviaPremierOrderPage.getPremierOrderTotalText()
    await optaviaPremierOrderPage.clickModifyOrderBtn()
  })

  it('OPTAVIA Premier Shopping Cart page is loaded', async function () {
    screencapture = true
    shoppingCartPage = new ShoppingCartPage()
    await shoppingCartPage.verifyPageIsLoaded()
  })

  it('Verify message displays on top', async function () {
    let actual = await shoppingCartPage.getSubscriptionEditMessageText()
    let expected = 'You are now editing your OPTAVIA Premier order.'
    assert.strictEqual(actual, expected, 'Global message is not displayed on top')
  })

  it('Verify order items displayed on Subscription page is displayed in edit mode', async function () {
    let actual = await shoppingCartPage.getCartItemsArrayList()
    let expected = await optaviaTemplateItems.reverse()
    assert.deepStrictEqual(actual, expected, 'Order items on Subscription page is not matching with edit mode')
  })

  it('Verify Product unit displayed on Subscription page is displayed in edit mode', async function () {
    let expected = (productUnitPriceArray.reverse()).map(price => price.replace(' / Kit', ''))
    let actual = await shoppingCartPage.getCartUnitPriceArrayList()
    assert.deepStrictEqual(actual, expected, 'Product unit on Subscription page is not matching with edit mode')
  })

  it('Verify Product Qty displayed on Subscription page is displayed in edit mode', async function () {
    let expected = productQtyArray.reverse()
    let actual = await shoppingCartPage.getCartQtyArrayList()
    assert.deepStrictEqual(actual, expected, 'Product Qty on Subscription page is not matching with edit mode')
  })

  it('Verify Price displayed on Subscription page is displayed in edit mode', async function () {
    let actual = await shoppingCartPage.getCartPriceArrayList()
    let expected = productPriceArray.reverse()
    assert.deepStrictEqual(actual, expected, 'Price displayed on Subscription page is not displayed in edit mode')
  })

  it('Verify Order Summary: Subtotal displayed on Subscription page is displayed in edit mode', async function () {
    let expected = productSubtotal
    let actual = await shoppingCartPage.getOrderSubtotal()
    assert.strictEqual(actual, expected, 'Order Summary: Subtotal displayed on Subscription page is not displayed in edit mode')
  })

  it('Verify Order Summary: Delivery Charge displayed on Subscription page is displayed in edit mode', async function () {
    let expected = deliveryCharge
    let actual = await shoppingCartPage.getPremierDeliveryChargeText()
    assert.strictEqual(actual, expected, 'Order Summary: Delivery Charge displayed on Subscription page is not displayed in edit mode')
  })

  it('Verify Order Summary: Order Total displayed on Subscription page is displayed in edit mode', async function () {
    let expected = orderTotal
    let actual = await shoppingCartPage.getPremierOrderTotalText()
    assert.strictEqual(actual, expected, 'Order Summary: Order Total displayed on Subscription page is not displayed in edit mode')
  })

  it('Update Product quantity to 6', async function () {
    await shoppingCartPage.updateProductQTY(6)
    await shoppingCartPage.clickUpdateProductLink()
    await shoppingCartPage.verifyPageIsLoaded()
  })

  it('Verify Order Summary: Subtotal increased by HK$205.00', async function () {
    await allureReporter.addDescription(`Subtotal: ${productSubtotal}, is increased by HK$205.00`)
    let subTotal = await productSubtotal.split('HK$')
    subTotal = await subTotal[1].replace(/[$,]+/gm, '')
    // convert string to Float
    subTotal = parseFloat(subTotal)
    // Add $ sign and decimal upto 2 digits
    searchProductPrice = parseFloat(searchProductPrice)
    let increasedNum = (subTotal + searchProductPrice)
    let expected = await CommonUtils.toCurrency(increasedNum, market.HK)
    let actual = await shoppingCartPage.getOrderSubtotal()
    CartOptaviaItems = await shoppingCartPage.getCartItemsArrayList()
    assert.strictEqual(actual, expected, `Order Summary: Subtotal not increased by ${increasedNum}`)
  })

  it('OPTAVIA Premier quantity updated message displays', async function () {
    screencapture = true
    let actual = await optaviaPremierOrderPage.getMessageAutoshipTemplateCancel()
    let expected = 'Product quantity has been updated.'
    assert.strictEqual(actual, expected, `"${expected}" message is not displayed`)
  })

  it('Save the Subscription template', async function () {
    await shoppingCartPage.clickSaveSubscriptionBtn()
  })

  it('Verify order items updated in edit mode is displayed on Subscription page', async function () {
    let actual = await optaviaPremierOrderPage.getProductArrayList()
    let expected = await CartOptaviaItems.reverse()
    assert.deepStrictEqual(actual, expected, 'Order items updated in edit mode is not displayed on Subscription page')
  })

  it('Verify Order Summary: Subtotal is increased by HK$205.00 in Subscription page', async function () {
    let subTotal = await productSubtotal.split('HK$')
    // convert string to Float
    subTotal = parseFloat(subTotal[1].replace(/,/g, ''))
    let increasedNum = (subTotal + searchProductPrice)
    let expected = await CommonUtils.toCurrency(increasedNum, market.HK)
    let actual = await optaviaPremierOrderPage.getPremierSubtotalText()
    assert.strictEqual(actual, expected, `Order Summary: Subtotal is not increased by ${increasedNum} in Subscription page`)
  })

  it('Click on Modify Order button', async function () {
    await optaviaPremierOrderPage.clickModifyOrderBtn()
  })

  it('OPTAVIA Premier Shopping Cart page is loaded', async function () {
    screencapture = true
    shoppingCartPage = new ShoppingCartPage()
    await shoppingCartPage.verifyPageIsLoaded()
  })

  it('Verify Modified Date from DB is displayed as System Current Date', async function () {
    let expected = await CommonUtils.getCurrentDate()
    let dateFromDB = await TestDataBaseMapping.getModifiedDate(accountID)
    let actual = await CommonUtils.getCustomFormatDate(dateFromDB, 'mm/dd/yyyy')
    assert.strictEqual(actual, expected, 'Modified Date from DB is not displayed as System Current Date')
  })

  it('Verify message displays on top', async function () {
    let actual = await shoppingCartPage.getSubscriptionEditMessageText()
    let expected = 'You are now editing your OPTAVIA Premier order.'
    assert.strictEqual(actual, expected, `"${expected}" message is not displayed`)
  })

  it('Decrease Product quantity to 2', async function () {
    await shoppingCartPage.updateProductQTY(2)
    await shoppingCartPage.clickUpdateProductLink()
    await shoppingCartPage.verifyPageIsLoaded()
  })

  it('Save the Subscription template', async function () {
    await shoppingCartPage.clickSaveSubscriptionBtn()
  })

  it('Verify order items unsaved in edit mode is displayed on Subscription page', async function () {
    let actual = await optaviaPremierOrderPage.getProductArrayList()
    let expected = await CartOptaviaItems
    assert.deepStrictEqual(actual, expected, 'Order items in edit mode is not displayed on Subscription page')
  })

  it('Verify Order Summary: Subtotal stays same with decreased HK$820.00 in Subscription page', async function () {
    let subTotal = await productSubtotal.split('HK$')
    // convert string to Float
    subTotal = parseFloat(subTotal[1].replace(/,/g, ''))
    let increasedNum = (subTotal - (3 * searchProductPrice))
    let expected = await CommonUtils.toCurrency(increasedNum, market.HK)
    let actual = await optaviaPremierOrderPage.getPremierSubtotalText()
    assert.strictEqual(actual, expected, 'Order Summary: Subtotal is not decreased in Subscription page')
  })

  it('Click on Edit OPTAVIA Premier Schedule button', async function () {
    await optaviaPremierOrderPage.clickEditOptaviaPremierBtn()
  })

  it('Verify the Calendar popup window is loaded', async function () {
    datePickerOptaviaPremier = new DatePickerOptaviaPremier()
    await datePickerOptaviaPremier.verifyPageIsLoaded()
  })

  it('Update Next Order Date to 30 days from last autoship order date from OPTAVIA Premier page', async function () {
    screencapture = true
    actualNextOrderDate = orderDate
    let nextOrderDate = await CommonUtils.getNextDate30DaysFromToday()
    await allureReporter.addDescription(`Updating Next order date from: ${actualNextOrderDate} to ${nextOrderDate}`)
    actualNextOrderDate = nextOrderDate
    let fullMonthDate = await CommonUtils.getDateFullMonth(nextOrderDate)
    let arrayResult = fullMonthDate.split('-')
    let monthYear = arrayResult[1]
    // Select Month
    for (let m = 0; m < 13; m++) {
      let selectMonth = await datePickerOptaviaPremier.getCalendarMonthText()
      let selectYear = await datePickerOptaviaPremier.getCalendarYearText()
      let actualMonthYear = `${selectMonth} ${selectYear}`
      if (monthYear === actualMonthYear) {
        // Select Day
        let nextMonthDay = await CommonUtils.getDayFromDate(nextOrderDate)
        let dLength = await datePickerOptaviaPremier.getCountofDateList()
        for (let d = 0; d < dLength; d++) {
          let days = await datePickerOptaviaPremier.getNextOrderDate(d)
          if (days === nextMonthDay) {
            await datePickerOptaviaPremier.clickAvailableDate(d)
            await datePickerOptaviaPremier.clickDatePickerSubmitBtn()
            return
          }
        }
      } else {
        await datePickerOptaviaPremier.clickNextMonthArrowBtn()
      }
    }
  })

  it('Verify Next Autoship Order Date from DB is same as Next Order Date in Optavia Premier Page', async function () {
    let dateFromDB = await TestDataBaseMapping.getNextAutoshipOrderDate(accountID)
    let expected = await CommonUtils.getCustomFormatDate(dateFromDB, 'mm/dd/yyyy')
    let actual = await optaviaPremierOrderPage.getOptaviaPremierNextDateText()
    let actualArray = actual.split(' ')
    assert.strictEqual(actualArray[5], expected, `Next Autoship Order Date ${actual} from DB is not same in Optavia premier Page`)
  })

  it('Verify order items unsaved in edit mode is displayed on Subscription page', async function () {
    let actual = await optaviaPremierOrderPage.getProductArrayList()
    let expected = await CartOptaviaItems
    assert.deepStrictEqual(actual, expected, 'Order items in edit mode is not matching on Subscription page')
  })

  it('Verify Order Summary: Subtotal is correct in Subscription page', async function () {
    let subTotal = await productSubtotal.split('HK$')
    // convert string to Float
    subTotal = parseFloat(subTotal[1].replace(/,/g, ''))
    let expected = subTotal - (3 * searchProductPrice)
    expected = await CommonUtils.toCurrency(expected, market.HK)
    let actual = await optaviaPremierOrderPage.getPremierSubtotalText()
    assert.strictEqual(actual, expected, 'Order Summary: Subtotal is not matching in Subscription page')
  })

  // Extra steps to reset product qty to 5

  it('Click on OPTAVIA Premier Order link under My Account', async function () {
    await myAccountLeftNavigation.clickOptaviaPremierOrder()
  })

  it('Click on Modify Order button', async function () {
    await optaviaPremierOrderPage.clickModifyOrderBtn()
  })

  it('OPTAVIA Premier Shopping Cart page is loaded', async function () {
    screencapture = true
    shoppingCartPage = new ShoppingCartPage()
    await shoppingCartPage.verifyPageIsLoaded()
  })

  it('Decrease Product quantity to 5', async function () {
    await shoppingCartPage.updateProductQTY(5)
    await shoppingCartPage.clickUpdateProductLink()
    await shoppingCartPage.verifyPageIsLoaded()
  })

  it('Save the Subscription template', async function () {
    await shoppingCartPage.clickSaveSubscriptionBtn()
  })

  afterEach(async function () {
    await driverutils.saveScreenshots(this.currentTest.state, screencapture)
    screencapture = false
  })
})
