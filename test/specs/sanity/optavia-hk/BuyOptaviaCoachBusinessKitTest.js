// @flow
import {assert} from 'chai'
import faker from 'faker'

import driverutils from '@core-libs/DriverUtils'
import CustomerTestData from '@input-data/customers/CustomerTestData'
import CoachTestData from '@input-data/coaches/CoachTestData'
import ProductTestData, {attb, market, product} from '@input-data/products/ProductTestData'
import TestCreditCards from '@input-data/creditcards/CreditCardTestData'
import HomePage from '@page-objects/home/HomePage'
import Header from '@page-objects/common/Header'
import MainNavBar from '@page-objects/common/MainNavBar'
import JoinUsPage from '@page-objects/joinus/JoinUsPage'
import LoginAndRegisterPage from '@page-objects/loginandregister/LoginAndRegisterPage'
import {GLOBAL_PASSWORD} from '@core-libs/ConstantUtil'
import AddedToYourShoppingCartPopup from '@page-objects/shop/AddedToYourShoppingCartPopup'
import ShoppingCartPage from '@page-objects/shoppingcart/ShoppingCartPage'
import ShopAllPage from '@page-objects/shop/ShopAllPage'
import EssentialFuelingsPage from '@page-objects/shop/EssentialFuelingsPage'
import CommonUtils from '@core-libs/CommonUtils'
import FindACoachPage from '@page-objects/findacoach/FindACoachPage'
import SearchCoachResultsPage from '@page-objects/searchcoachresults/SearchCoachResultsPage'
import CheckoutPage from '@page-objects/checkout/CheckoutPage'
import ShippingAddressTile from '@page-objects/checkout/ShippingAddressTile'
import PopupSuggestedDeliveryAddressesForm
  from '@page-objects/popupsuggesteddeliveryaddressesform/PopupSuggestedDeliveryAddressesForm'
import PaymentAndBillingAddressTile from '@page-objects/checkout/PaymentAndBillingAddressTile'
import FinalReviewTile from '@page-objects/checkout/FinalReviewTile'
import OrderConfirmationPage from '@page-objects/orderconfirmation/OrderConfirmationPage'
import MyAccountPage from '@page-objects/myaccount/MyAccountPage'
import TestDataBaseMapping from '@db/TestDataBaseMapping'
import CoachStorePage from '@page-objects/coachstore/CoachStorePage'
import allureReporter from '@wdio/allure-reporter'

describe('C5757- HK OPTAVIA - Add OPTAVIA Premier Customer with Health Coach Business Kit', function () {
  const payableTypeID = 5
  const customerTypeID = 1

  let screencapture = false // capture screenshot when test step 'passed' also by default only capture screenshot of failed steps

  let homePage, header, mainNavBar, joinUsPage, addedToYourShoppingCartPopup, shoppingCartPage, loginAndRegisterPage, findACoachPage, searchCoachResultsPage
  let checkoutPage, shippingAddressTile, paymentAndBillingAddressTile, finalReviewTile, orderConfirmationPage, myAccountPage, orderConfirmationDate, popupSuggestedDeliveryAddressesForm

  let actualGlobal

  // Customer Registration
  let TestData, firstName, lastName, email, nationalID, dob, accountID

  // Coach (Enroller) Info
  let coachFirstName, coachLastName

  // Payment Details
  let nameOnCard, cardNumber, cvv, month, year

  // Shop Page
  let shopAllPage, essentialFuelingsPage

  // Shopping Cart Page
  let orderSubTotal, deliveryCharge, orderTotal, coachstorePage

  // Final Review
  let renewalKitSubtotal, renewalKitTotal

  // Product
  let productPrice, renewalProdPrice, premierKitPrice

  let testTitle = this.title

  before(async function () {
    await driverutils.goToOptaviaWebSiteHomePageHK()
  })

  beforeEach(async function () {
    await driverutils.addAllureReport(allureReporter, __dirname, 'HK OPTAVIA Sanity', testTitle)
  })

  it('Preparing Test Data', async function () {
    TestData = await CustomerTestData.getCustomerInfo('90000000019')
    // Customer Registration
    firstName = await faker.name.firstName()
    lastName = await faker.name.lastName()
    email = `auto.${faker.internet.email()}`
    nationalID = TestData.NATIONAL_ID
    dob = TestData.DOB

    // Coach Information
    coachFirstName = await CoachTestData.COACH_FIRST_NAME(6, 3)
    coachLastName = await CoachTestData.COACH_LAST_NAME(6, 3)

    // Product Information
    productPrice = await ProductTestData.PRODUCT_INFO(attb.UNIT_PRICE, product.bars.OPT_PEANUT_CRISP_BAR, market.HK)
    renewalProdPrice = await ProductTestData.PRODUCT_INFO(attb.UNIT_PRICE, product.renewal_certification.OPT_BUSINESS_RENEWAL, market.HK)
    premierKitPrice = await ProductTestData.PRODUCT_INFO(attb.UNIT_PRICE, product.kits.OPT_OPTAVIA_BUSINESS_KIT, market.HK)
    orderSubTotal = productPrice + premierKitPrice
    deliveryCharge = 'FREE'
    orderTotal = orderSubTotal
    renewalKitSubtotal = renewalProdPrice
    renewalKitTotal = renewalProdPrice

    // Payment Information (for VISA = B, Mastercard = C, American Express = D and Discover = E) column of sheet 2 in excel file
    let ccCol = 'B'
    let sheet = 2
    nameOnCard = firstName + ' ' + lastName
    cardNumber = await TestCreditCards.CARD_NUMBER(sheet, ccCol)
    cvv = await TestCreditCards.CVV(sheet, ccCol)
    month = await TestCreditCards.EXPIRATION_MONTH(sheet, ccCol)
    year = await TestCreditCards.EXPIRATION_YEAR(sheet, ccCol)
  })

  it('Loads the Home Page', async function () {
    homePage = new HomePage()
    await homePage.verifyPageIsLoaded()
    header = new Header()
    await header.verifyPageIsLoaded()
    mainNavBar = new MainNavBar()
    await mainNavBar.verifyPageIsLoaded()
  })

  it('Navigate to Join Us page', async function () {
    await mainNavBar.clickJoinUs()
    joinUsPage = new JoinUsPage()
    await joinUsPage.verifyPageIsLoaded()
  })

  it('Click on Register button', async function () {
    await joinUsPage.clickRegisterBtn()
  })

  it('Loads the Create an Account for "Become An Optavia Coach" Popup', async function () {
    loginAndRegisterPage = new LoginAndRegisterPage()
    await loginAndRegisterPage.verifyBecomeAnOptaviaCoachPopUpIsLoaded()
    await loginAndRegisterPage.verifyCreateAnAccountPopUpIsLoaded()
  })

  it('Create an Account', async function () {
    await allureReporter.addDescription(`Create an Account first name: ${firstName}, last name: ${lastName}, email: ${email}, password: ${GLOBAL_PASSWORD} and confirm password: ${GLOBAL_PASSWORD}`)
    await loginAndRegisterPage.createAnAccountOtherCountry(firstName, lastName, email, GLOBAL_PASSWORD, TestData.SHIPPING_COUNTRY_CODE, TestData.SHIPPING_PHONE)
  })

  it('Verify Begin Enrollment section is displayed', async function () {
    await joinUsPage.verifyBeginEnrollmentIsLoaded()
  })

  it('Begin Enrollment', async function () {
    await allureReporter.addDescription(`Begin Enrollment first name: ${firstName}, last name: ${lastName}, national Id: ${nationalID}, dob: ${dob}`)
    await joinUsPage.beginEnrollment(nationalID, dob)
  })

  it('Loads the Added To Your Shopping Cart Popup', async function () {
    addedToYourShoppingCartPopup = new AddedToYourShoppingCartPopup()
    await addedToYourShoppingCartPopup.verifyPageIsLoaded()
  })

  it('Click on Checkout button on pop up window', async function () {
    await addedToYourShoppingCartPopup.clickCheckoutButton()
  })

  it('Loads the Shopping Cart Page', async function () {
    shoppingCartPage = new ShoppingCartPage()
    await shoppingCartPage.verifyPageIsLoaded()
  })

  it('Verify Join Premier checkbox is NOT present', async function () {
    screencapture = true
    let actual = await shoppingCartPage.isJoinPremierContainerDisplayed()
    assert.isFalse(actual, 'Join Premier checkbox is checked')
  })

  it('Click on Continue shopping Button', async function () {
    await shoppingCartPage.clickContinueShoppingBtn()
  })

  it('Loads Shop All Page', async function () {
    shopAllPage = new ShopAllPage()
    await shopAllPage.verifyPageIsLoaded()
  })

  it('Click on Shop Button under Essentials Fuelings', async function () {
    await shopAllPage.clickEssentialFuelingsShopBtn()
  })

  it('Loads Select Fuelings Page', async function () {
    essentialFuelingsPage = new EssentialFuelingsPage()
    await essentialFuelingsPage.verifyPageIsLoaded()
  })

  it('Check on Hearty Choice Checkbox', async function () {
    await essentialFuelingsPage.checkHeartyChoiceCheckbox()
  })

  it('Click on Add To Cart Button for the first product on the page', async function () {
    await essentialFuelingsPage.addFirstProductToCart()
  })

  it('Click on Checkout button on pop up window', async function () {
    await addedToYourShoppingCartPopup.clickCheckoutButton()
  })

  it('Check the Checkbox for Join Optavia Premier Checkbox', async function () {
    await shoppingCartPage.clickToCheckJoinPremierCheckBox()
  })

  it('Verify OPTAVIA BlenderBottle Classic is displayed as Free Item', async function () {
    await shoppingCartPage.verifyFreeBlenderBottleIsLoaded()
  })

  it('Verify OPTAVIA Free Guide is displayed as Free Item', async function () {
    await shoppingCartPage.verifyFreeOptaviaGuideIsLoaded()
  })

  it('Verify Journey Kickoff Card Insert is displayed as Free Item', async function () {
    await shoppingCartPage.verifyFreeJourneyKickoffCardInsertIsLoaded()
  })

  it('Verify OPTAVIA Guide Top Tips Insert is displayed as Free Item', async function () {
    await shoppingCartPage.verifyFreeOptaviaGuideTopTipsInsertIsLoaded()
  })

  it('Verify Order Subtotal in the cart', async function () {
    let expected = await CommonUtils.toCurrency(orderSubTotal, market.HK)
    let actual = await shoppingCartPage.getOrderSubtotal()
    assert.strictEqual(actual, expected, `Order Subtotal ${expected} is not matching with shopping cart`)
  })

  it('Click on Proceed to Checkout button', async function () {
    await shoppingCartPage.clickProceedToCheckoutBtn()
  })

  it('Loads the Find a Coach page', async function () {
    findACoachPage = new FindACoachPage()
    await findACoachPage.verifyPageIsLoaded()
  })

  it('Verify text in Given Name text box', async function () {
    let expected = 'Given Name'
    let actual = await findACoachPage.getFirstNameTextboxText('placeholder')
    assert.strictEqual(actual, expected, `${expected} is not displayed`)
  })

  it('Verify text in Family Name text box', async function () {
    let expected = 'Family Name'
    let actual = await findACoachPage.getLastNameTextboxText('placeholder')
    assert.strictEqual(actual, expected, `${expected} is not displayed`)
  })

  it('Verify market is defaulted to Hong Kong', async function () {
    let expected = 'Hong Kong'
    let actual = await findACoachPage.getMarketDefaultText()
    assert.strictEqual(actual, expected, `${expected} is not displayed`)
  })

  it('Search a coach', async function () {
    await allureReporter.addDescription(`Search a coach ${coachFirstName} ${coachLastName}`)
    await findACoachPage.enterFirstAndLastNameToSearchCoach(coachFirstName, coachLastName)
    await findACoachPage.clickSearchCoachFindACoachButton()
  })

  it('Loads the Search Result page', async function () {
    searchCoachResultsPage = new SearchCoachResultsPage()
    await searchCoachResultsPage.verifyPageIsLoaded()
  })

  it('Verify searched coach is in search result list', async function () {
    screencapture = true
    await allureReporter.addDescription(`Verify ${coachFirstName} ${coachLastName} is in search result list`)
    let expected = `${coachFirstName} ${coachLastName}`
    let actual = await searchCoachResultsPage.getCoachName()
    assert.strictEqual(actual, expected, 'Coach Search Result page body text is not matching with expected')
  })

  it('Click on Connect Me next to searched coach', async function () {
    await allureReporter.addDescription(`Click on Connect Me next to ${coachFirstName} ${coachLastName}`)
    await searchCoachResultsPage.clickConnectMeButton()
  })

  it('Loads the Checkout page', async function () {
    checkoutPage = new CheckoutPage()
    await checkoutPage.verifyPageIsLoaded()
  })

  it('Loads the Shipping Address page', async function () {
    shippingAddressTile = new ShippingAddressTile()
    await shippingAddressTile.verifyPageIsLoaded()
  })

  it('Enter Shipping Full Name and Address information', async function () {
    await shippingAddressTile.addShippingAddressHK(TestData, true)
  })

  it('Check Default shipping check box', async function () {
    await shippingAddressTile.clickToCheckDefaultAddressCheckBox()
  })

  it('Shipping Address: click Next button', async function () {
    await shippingAddressTile.clickNext()
  })

  it('Loads the Suggested Address popup', async function () {
    popupSuggestedDeliveryAddressesForm = new PopupSuggestedDeliveryAddressesForm()
    await popupSuggestedDeliveryAddressesForm.verifyPageIsLoaded()
  })

  it('Click on Use this address button', async function () {
    await shippingAddressTile.verifyPageIsLoaded()
    await popupSuggestedDeliveryAddressesForm.clickUseThisAddress()
  })

  it('Loads the Payment Details page', async function () {
    paymentAndBillingAddressTile = new PaymentAndBillingAddressTile()
    await paymentAndBillingAddressTile.verifyPageIsLoadedHK()
  })

  it('Enter Payment Details information', async function () {
    await paymentAndBillingAddressTile.createPaymentDetailsHK(nameOnCard, cardNumber, month + year, cvv)
  })

  it('Click on Next button from Payment Details', async function () {
    await paymentAndBillingAddressTile.clickNextHK()
  })

  it('Loads the Final Review page', async function () {
    finalReviewTile = new FinalReviewTile()
    await finalReviewTile.verifyPageIsLoaded()
  })

  it('Click on Submit Order Button', async function () {
    await finalReviewTile.clickPlaceOrderButton()
  })

  it('Loads the Order Confirmation page', async function () {
    orderConfirmationPage = new OrderConfirmationPage()
    await orderConfirmationPage.verifyPageIsLoaded()
  })

  it('Verify Order Total in Confirmation Page', async function () {
    let expected = `Total ${await CommonUtils.toCurrency(orderTotal, market.HK)}\nSubtotal ${await CommonUtils.toCurrency(orderSubTotal, market.HK)}\nDelivery Charge: ${deliveryCharge}`
    let actual = await orderConfirmationPage.getOrderTotals()
    let orderDateLine = await orderConfirmationPage.getOrderDate()
    orderConfirmationDate = orderDateLine.substring(10, orderDateLine.length)
    assert.strictEqual(actual, expected, 'Order Summary in Confirmation Page is not matching with expected')
  })

  it('Click on Continue Shopping button on Order Confirmation page', async function () {
    await orderConfirmationPage.clickContinueShopping()
  })

  it('Loads the Home page', async function () {
    await header.verifyMyAccountLinkIsLoaded()
    await mainNavBar.verifyPageIsLoaded()
  })

  it('Clicks on My Account menu and gets Client ID', async function () {
    await header.clickMyAccount()
    myAccountPage = new MyAccountPage()
    await myAccountPage.verifyPageIsLoaded()
    accountID = await myAccountPage.getAccountID()
    accountID = accountID.substring(11, accountID.length)
  })

  it('Wait for 2.5 minutes for DB to sync', async function () {
    await browser.pause(150000)
    await browser.refresh()
  })

  it('Verify customer type ID is (Health Coach = 1) in DB', async function () {
    let expected = customerTypeID
    let actual = await TestDataBaseMapping.getCustomerTypeID_CustomersTable(accountID)
    assert.strictEqual(actual, expected, `Customer type ID : ${expected} is not matching with DB`)
  })

  it('Verify Business Kit order activation date in DB', async function () {
    let expected = orderConfirmationDate
    let activationDate = await TestDataBaseMapping.getActivationDate_CustomersTable(accountID)
    let actual = await CommonUtils.getActivationDateFormat(activationDate)
    assert.strictEqual(actual, expected, `Business Kit order activation date : ${expected} is not matching with DB`)
  })

  it('Verify Payable type ID is (Payment Card = 5) in DB', async function () {
    let expected = payableTypeID
    let actual = await TestDataBaseMapping.getPayableTypeID_CustomersTable(accountID)
    assert.strictEqual(actual, expected, `Payable type ID : ${expected} is not matching with DB`)
  })

  it('Verify New Renewal Date is displayed as 365 days from order creation date in DB', async function () {
    let actual = await CommonUtils.getRenewalDateFormat(await TestDataBaseMapping.getRenewalDate_HCDatesTable(accountID))
    let expected = await CommonUtils.getRenewalDate(await TestDataBaseMapping.getActivationDate_HCDatesTable(accountID))
    assert.strictEqual(actual, expected, ` New Renewal Date : ${actual} is not 365 days from order creation date in DB`)
  })

  it('Verify Old Renewal Date is displayed as NULL in DB', async function () {
    assert.isNull(await TestDataBaseMapping.getOldRenewalDate_HCDatesTable(accountID), 'Old Renewal Date should be NULL')
  })

  it('Verify OPTAVIA Connect link is displayed in header', async function () {
    screencapture = true
    await header.clickMyAccount()
    await myAccountPage.verifyPageIsLoaded()
    actualGlobal = await header.isOptaviaConnectLinkFound()
    assert.isTrue(actualGlobal, 'OPTAVIA Connect link is not displayed')
  })

  it('Navigate to Join Us page', async function () {
    await mainNavBar.clickJoinUs()
    await joinUsPage.verifyPageIsLoaded()
  })

  it('Click on Coach store', async function () {
    await joinUsPage.clickVisitCoachStoreBtn()
  })

  it('Loads the Coach Store Page', async function () {
    coachstorePage = new CoachStorePage()
    await coachstorePage.verifyPageIsLoaded()
  })

  it('Click on Add To Cart for 12 Month Renewal kit', async function () {
    await coachstorePage.clickRenewalKitAddToCartbtn()
  })
  it('Loads the Added To Your Shopping Cart Popup', async function () {
    addedToYourShoppingCartPopup = new AddedToYourShoppingCartPopup()
    await addedToYourShoppingCartPopup.verifyPageIsLoaded()
  })

  it('Click on Checkout button on pop up window', async function () {
    await addedToYourShoppingCartPopup.clickCheckoutButton()
  })

  it('Click on Proceed to Checkout button', async function () {
    await shoppingCartPage.clickProceedToCheckoutBtn()
  })

  it('Enter Existing Payment Details information', async function () {
    await paymentAndBillingAddressTile.clickOnExistingCardDetailsSG(cvv)
  })

  it('Click on Next button from Payment tile', async function () {
    await paymentAndBillingAddressTile.clickNextHK()
  })

  it('Loads the Final Review page', async function () {
    finalReviewTile = new FinalReviewTile()
    await finalReviewTile.verifyPageIsLoaded()
  })

  it('Click on Submit Order Button', async function () {
    await finalReviewTile.clickPlaceOrderButton()
  })

  it('Loads the Order Confirmation page', async function () {
    orderConfirmationPage = new OrderConfirmationPage()
    await orderConfirmationPage.verifyPageIsLoaded()
  })

  it('Verify Order Total in Confirmation Page', async function () {
    screencapture = true
    let expected = `Total ${await CommonUtils.toCurrency(renewalKitTotal, market.HK)}\nSubtotal ${await CommonUtils.toCurrency(renewalKitSubtotal, market.HK)}\nDelivery Charge: ${deliveryCharge}`
    let actual = await orderConfirmationPage.getOrderTotals()
    assert.strictEqual(actual, expected, 'Order Total in Confirmation Page is not matching with expected')
  })

  it('Click on Continue Shopping button on Order Confirmation page', async function () {
    await orderConfirmationPage.clickContinueShopping()
  })

  it('Loads the Home page', async function () {
    await header.verifyMyAccountLinkIsLoaded()
    await mainNavBar.verifyPageIsLoaded()
  })

  it('Logout Coach', async function () {
    await header.verifyLogOutIsLoaded()
    await header.clickLogOut()
    await header.verifyPageIsLoaded()
  })

  afterEach(async function () {
    await driverutils.saveScreenshots(this.currentTest.state, screencapture)
    screencapture = false
  })
})
