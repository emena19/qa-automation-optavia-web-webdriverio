// @flow
import { assert } from 'chai'
import driverutils from '@core-libs/DriverUtils'
import allureReporter from '@wdio/allure-reporter'
import CustomerTestData from '@input-data/customers/CustomerTestData'
import TestCreditCards from '@input-data/creditcards/CreditCardTestData'
import HomePage from '@page-objects/home/HomePage'
import Header from '@page-objects/common/Header'
import MainNavBar from '@page-objects/common/MainNavBar'
import LoginPage from '@page-objects/loginPage/LoginPage'
import MyAccountPage from '@page-objects/myaccount/MyAccountPage'
import MyAccountLeftNavigation from '@page-objects/myaccount/MyAccountLeftNavigation'
import PaymentDetailsPage from '@page-objects/myaccount/PaymentDetailsPage'
import PaymentAndBillingAddressTile from '@page-objects/checkout/PaymentAndBillingAddressTile'

describe('C16883/24276 - HK OPTAVIA -My Account - Payment Details - Existing Customer adds new Payment Method', function () {
  let screencapture = false // capture screenshot when test step 'passed' also by default only capture screenshot of failed steps

  let homePage, header, myAccountPage, paymentDetailsPage, firstName, lastName, currentDefaultCard
  let myAccountLeftNavigation
  let nameOnCard, cardNumber, cvv, month, year

  // ASM Agent info
  let loginPage, email, password, mainNavBar, paymentAndBillingAddressTile
  let testTitle = this.title

  before(async function () {
    await driverutils.goToOptaviaWebSiteHomePageHK()
  })

  beforeEach(async function () {
    await driverutils.addAllureReport(allureReporter, __dirname, 'HK OPTAVIA Sanity', testTitle)
  })

  it('Preparing Test Data', async function () {
    let custID = '85220000404305'
    let TestData = await CustomerTestData.getCustomerInfo(custID)
    // Client Info
    firstName = TestData.CUSTOMER_FIRST_NAME
    lastName = TestData.CUSTOMER_LAST_NAME
    email = TestData.CUSTOMER_EMAIL
    password = TestData.CUSTOMER_PASSWORD

    // CC Data
    let ccCol = 'B'
    let ccsheet = 2
    nameOnCard = firstName + ' ' + lastName
    cardNumber = await TestCreditCards.CARD_NUMBER(ccsheet, ccCol)
    cvv = await TestCreditCards.CVV(ccsheet, ccCol)
    month = await TestCreditCards.EXPIRATION_MONTH(ccsheet, ccCol)
    year = await TestCreditCards.EXPIRATION_YEAR(ccsheet, ccCol)
  })

  it('Loads the Home Page', async function () {
    homePage = new HomePage()
    await homePage.verifyPageIsLoaded()
    header = new Header()
    await header.verifyPageIsLoaded()
    mainNavBar = new MainNavBar()
    await mainNavBar.verifyPageIsLoaded()
  })

  it('Click on Login', async function () {
    await header.clickLogIn()
  })

  it('Loads the Login page', async function () {
    loginPage = new LoginPage()
    await loginPage.verifyPageIsLoaded()
  })

  it('Login as existing Customer', async function () {
    screencapture = true
    await allureReporter.addDescription(`Login as existing Customer email: ${email} and password: ${password}`)
    await loginPage.loginAsExistingCustomer(email, password)
  })

  it('Verify left menu is loaded', async function () {
    myAccountPage = new MyAccountPage()
    await myAccountPage.verifyPageIsLoaded()
    myAccountLeftNavigation = new MyAccountLeftNavigation()
    await myAccountLeftNavigation.verifyPageIsLoaded()
  })

  it('Click on payment details', async function () {
    await myAccountLeftNavigation.clickPaymentDetails()
  })

  it('Loads Payment Details Page', function () {
    paymentDetailsPage = new PaymentDetailsPage()
    paymentDetailsPage.verifyPageIsLoaded()
  })

  it('Get current default card', async function () {
    currentDefaultCard = await paymentDetailsPage.getDefaultAddress()
  })
  it('Click on add a new card', async function () {
    await paymentDetailsPage.clickAddNewCardBTN()
  })

  it('Loads the Payment Details page', async function () {
    paymentAndBillingAddressTile = new PaymentAndBillingAddressTile()
    paymentDetailsPage.verifyPageIsLoaded()
  })

  it('Enter Payment Details information', async function () {
    await paymentAndBillingAddressTile.createPaymentDetailsHK(nameOnCard, cardNumber, month + year, cvv)
  })

  it('Click on Cancel Button', async function () {
    await paymentDetailsPage.verifyCancelButtonIsDisplayed()
    await paymentDetailsPage.clickCancelButton()
  })

  it('Loads Payment Details Page', async function () {
    await paymentDetailsPage.verifyPageIsLoaded()
  })

  it('Verify  Default address in Payment Details page is not changed', async function () {
    await allureReporter.addDescription('Default address in Payment Details page')
    let expected = currentDefaultCard
    let actual = await paymentDetailsPage.getDefaultAddress()
    assert.strictEqual(actual, expected, 'Default address in Payment Details page is changed')
  })

  it('Click on add a new card', async function () {
    await paymentDetailsPage.clickAddNewCardBTN()
  })
  it('Enter Payment Details information', async function () {
    await paymentAndBillingAddressTile.createPaymentDetailsHK(nameOnCard, cardNumber, month + year, cvv)
  })

  it('Click Save Payment Info button', async function () {
    await browser.pause(3000)
    await paymentDetailsPage.clickSavePaymentInfoButtonSG()
  })

  it('Verify Confirmation Message', async function () {
    let actual = await paymentDetailsPage.getYourGlobalMessage()
    let expected = 'Payment Card added successfully'
    assert.equal(actual, expected, `${expected} Message is not displayed`)
  })

  // Extra steps to removed added cc

  it('Remove the Payment Info', async function () {
    await paymentDetailsPage.clickRemovePaymentInfo()
  })

  it('Verify Remove Payment Information Pop Up is loaded', async function () {
    await paymentDetailsPage.verifyRemovePaymentInfoPopUpIsLoaded()
  })

  it('Click on OK button to remove the Payment Info', async function () {
    await paymentDetailsPage.clickPopUpOKToRemove()
  })

  it('Verify Global message displays on top', async function () {
    let actual = await paymentDetailsPage.getGlobalSuccessMsg()
    let expected = 'Payment Card removed successfully'
    assert.strictEqual(actual, expected, `${expected} Global Message is not displayed on top`)
  })

  afterEach(async function () {
    await driverutils.saveScreenshots(this.currentTest.state, screencapture)
    screencapture = false
  })
})
