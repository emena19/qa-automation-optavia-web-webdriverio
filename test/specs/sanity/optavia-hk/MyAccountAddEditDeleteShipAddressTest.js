// @flow
import {assert} from 'chai'
import driverutils from '@core-libs/DriverUtils'
import CustomerTestData from '@input-data/customers/CustomerTestData'
import HomePage from '@page-objects/home/HomePage'
import Header from '@page-objects/common/Header'
import LoginPage from '@page-objects/loginPage/LoginPage'
import MyAccountPage from '@page-objects/myaccount/MyAccountPage'
import MyAccountLeftNavigation from '@page-objects/myaccount/MyAccountLeftNavigation'
import AddressBookPage from '@page-objects/myaccount/AddressBookPage'
import PopupSuggestedDeliveryAddressesForm
  from '@page-objects/popupsuggesteddeliveryaddressesform/PopupSuggestedDeliveryAddressesForm'
import AddressDetailsPage from '@page-objects/myaccount/AddressDetailsPage'
import allureReporter from '@wdio/allure-reporter'

describe('C16777 -HK OPTAVIA - My Account -Address Book - Existing customer Add, Edit, Delete address in Address book', function () {
  declare var allure: any;

  let screencapture = false // True to capture screenshot when test step 'passed'. By default only capture screenshot of failed steps
  let homePage, header, loginPage, myAccountPage, myAccountLeftNavigation, addressBookPage,
    popupSuggestedDeliveryAddressesForm, addressDetailsPage

  // Customer Registration
  let email, password, firstName, lastName, suggestedAddress

  // Shipping Information
  let shippingAddressLine1, shippingAddressLine2, shippingDistrict, shippingState, shippingPhone,
    additionalAddress1, additionalCountryCode, additionalPhoneNumber, additionalFullStateName

  let testTitle = this.title

  before(async function () {
    await driverutils.goToOptaviaWebSiteHomePageHK(driver)
  })

  beforeEach(async function () {
    await driverutils.addAllureReport(allureReporter, __dirname, 'HK OPTAVIA Sanity', testTitle)
  })

  it('Preparing Test Data', async function () {
    let custID = '85220000480940'
    let TestData = await CustomerTestData.getCustomerInfo(custID)
    email = TestData.CUSTOMER_EMAIL
    password = TestData.CUSTOMER_PASSWORD
    firstName = TestData.CUSTOMER_FIRST_NAME
    lastName = TestData.CUSTOMER_LAST_NAME
    shippingAddressLine1 = TestData.SHIPPING_ADDRESS_LINE_1
    shippingAddressLine2 = TestData.SHIPPING_ADDRESS_LINE_2
    shippingDistrict = TestData.SHIPPING_DISTRICT
    shippingState = TestData.SHIPPING_FULL_STATE_NAME
    shippingPhone = TestData.SHIPPING_PHONE
    // Additional shipping/Billing Address
    additionalAddress1 = TestData.ADDITIONAL_ADDRESS_LINE_1
    additionalFullStateName = TestData.ADDITIONAL_FULL_STATE_NAME
    additionalCountryCode = TestData.ADDITIONAL_COUNTRY_CODE
    additionalPhoneNumber = TestData.ADDITIONAL_PHONE
  })

  it('Loads the Home Page', async function () {
    homePage = new HomePage(driver)
    await homePage.verifyPageIsLoaded()
    header = new Header(driver)
    await header.verifyPageIsLoaded()
  })

  it('Navigate to Login Page', async function () {
    await header.clickLogIn()
    loginPage = new LoginPage(driver)
    await loginPage.verifyPageIsLoaded()
  })

  it('Login as existing Customer', async function () {
    screencapture = true
    await allureReporter.addDescription(`Login as existing Customer email: ${email} and password: ${password}`)
    await loginPage.loginAsExistingCustomer(email, password)
  })

  it('Verify My Account page is loaded', async function () {
    screencapture = true
    myAccountPage = new MyAccountPage(driver)
    await myAccountPage.verifyPageIsLoaded()
    myAccountLeftNavigation = new MyAccountLeftNavigation(driver)
    await myAccountLeftNavigation.verifyPageIsLoaded()
  })

  it('Navigate to Address Book page', async function () {
    await myAccountLeftNavigation.clickAddressBook()
  })

  it('Loads the Address Book page', async function () {
    screencapture = true
    addressBookPage = new AddressBookPage(driver)
    await addressBookPage.verifyPageIsLoaded()
  })

  it('Click on Add Address button from Address Book page', async function () {
    await addressBookPage.clickAddNewAddressButton()
  })

  it('Verify Address Details page is loaded', async function () {
    addressDetailsPage = new AddressDetailsPage(driver)
    await addressDetailsPage.verifyPageIsLoaded()
  })

  // Covers test case C23132
  it('Verify text in the Country Value says "Hong Kong"', async function () {
    let actual = await addressDetailsPage.getDisabledCountryText()
    let expected = shippingState
    assert.strictEqual(actual, expected, 'Country Value is not "Hong Kong"')
  })

  it('Enter Shipping Address information without Phone value', async function () {
    await addressDetailsPage.createShippingAddressHK(shippingAddressLine1, shippingAddressLine2, shippingDistrict, additionalCountryCode, '')
  })

  it('Click on Proceed to Save Address button', async function () {
    await addressDetailsPage.clickAddAddressSaveButton()
  })

  it('Verify the error message is displayed under Phone', async function () {
    let expected = 'This field is required'
    let actual = await addressDetailsPage.getPhoneErrorMessage()
    assert.strictEqual(actual, expected, 'Error Message is not displayed under Phone')
  })

  it('Enter Shipping Address information with Phone value', async function () {
    await addressDetailsPage.createShippingAddressHK(shippingAddressLine1, shippingAddressLine2, shippingDistrict, additionalCountryCode, shippingPhone)
  })

  it('Click on Proceed to Save Address button', async function () {
    await addressDetailsPage.clickAddAddressSaveButton()
  })

  it('Loads the Suggested Address popup or Shipping Method page', async function () {
    screencapture = true
    popupSuggestedDeliveryAddressesForm = new PopupSuggestedDeliveryAddressesForm(driver)
    let popup = await popupSuggestedDeliveryAddressesForm.isPopUpPresent()
    if (popup) {
      await popupSuggestedDeliveryAddressesForm.verifyPageIsLoaded()
      await popupSuggestedDeliveryAddressesForm.clickSubmitAsIsButton()
    }
  })

  it('Verify Global message displays on top', async function () {
    let actual = await addressBookPage.getYourGlobalMessage()
    let expected = 'Address created successfully'
    assert.strictEqual(actual, expected, 'Global Message is not displayed on top')
  })

  it('Verify new address added is not Default address in Address Book page', async function () {
    await allureReporter.addDescription('Verify new address added is not Default address in Address Book page')
    let expected = `${firstName} ${lastName}\n${shippingAddressLine1}\n${shippingAddressLine2}\n${shippingDistrict},\n${shippingState}\n${shippingPhone}`
    let actual = await addressBookPage.getNonDefaultShippingAddress()
    assert.strictEqual(actual, expected, 'New address added is Default address in Address Book page')
  })

  it('Click on non default Edit Address button', async function () {
    await addressBookPage.clickEditButtonByIndex(1)
  })

  it('Click on Proceed to Save Address button', async function () {
    await addressDetailsPage.verifyPageIsLoaded()
    await addressDetailsPage.clickAddAddressSaveButton()
  })

  it('Loads the Suggested Address popup or Shipping Method page', async function () {
    screencapture = true
    popupSuggestedDeliveryAddressesForm = new PopupSuggestedDeliveryAddressesForm(driver)
    let popup = await popupSuggestedDeliveryAddressesForm.isPopUpPresent()
    if (popup) {
      await popupSuggestedDeliveryAddressesForm.verifyPageIsLoaded()
      await popupSuggestedDeliveryAddressesForm.clickSubmitAsIsButton()
    }
  })

  it('Verify Global message displays on top', async function () {
    let actual = await addressBookPage.getYourGlobalMessage()
    let expected = 'Address updated successfully'
    assert.strictEqual(actual, expected, 'Global message is not displayed on top')
  })

  it('Verify new address added is not Default address in Address Book page', async function () {
    await allureReporter.addDescription('Verify new address added is not Default address in Address Book page')
    let expected = `${firstName} ${lastName}\n${shippingAddressLine1}\n${shippingAddressLine2}\n${shippingDistrict},\n${shippingState}\n${shippingPhone}`
    let actual = await addressBookPage.getNonDefaultShippingAddress()
    assert.strictEqual(actual, expected, 'New address added is Default address in Address Book page')
  })

  it('Click on Edit Address button', async function () {
    await addressBookPage.clickEditButtonByIndex(1)
  })

  it('Enter New Shipping Address information', async function () {
    shippingDistrict = 'Chai Wan'
    await addressDetailsPage.createShippingAddressHK(additionalAddress1, '', shippingDistrict, additionalCountryCode, additionalPhoneNumber)
  })

  it('Click on Proceed to Save Address button', async function () {
    await addressDetailsPage.clickAddAddressSaveButton()
  })

  it('Loads the Suggested Address popup', async function () {
    popupSuggestedDeliveryAddressesForm = new PopupSuggestedDeliveryAddressesForm(driver)
    await popupSuggestedDeliveryAddressesForm.verifyPageIsLoaded()
  })

  it('Capture the suggested address from pop up ', async function () {
    suggestedAddress = await popupSuggestedDeliveryAddressesForm.getSuggestedDeliveryAddressSection()
    suggestedAddress = suggestedAddress.split('\n')
    additionalAddress1 = suggestedAddress[0]
    additionalCountryCode = suggestedAddress[3]
  })

  it('Click on Use this Address button', async function () {
    await popupSuggestedDeliveryAddressesForm.clickUseThisAddress()
  })

  it('Loads the Address Book page', async function () {
    screencapture = true
    await addressBookPage.verifyPageIsLoaded()
  })

  it('Verify Global message displays on top', async function () {
    let actual = await addressBookPage.getYourGlobalMessage()
    let expected = 'Address updated successfully'
    assert.strictEqual(actual, expected, ' Global message is not displayed on top')
  })

  it('Verify new address added is not Default address in Address Book page', async function () {
    await allureReporter.addDescription('Verify new address added is not Default address in Address Book page')
    let expected = `${firstName} ${lastName}\n${additionalAddress1}\n${shippingDistrict},\n${additionalFullStateName}\n${additionalPhoneNumber}`
    let actual = await addressBookPage.getNonDefaultShippingAddress()
    assert.strictEqual(actual, expected, 'New address added is Default address in Address Book page')
  })

  it('Remove non default shipping address', async function () {
    await addressBookPage.clickRemoveNonDefaultShipAddress()
  })

  it('Verify Remove Shipping Address Pop Up is loaded', async function () {
    await addressBookPage.verifyRemoveAddressPopUpIsLoaded()
  })

  it('Click NO button on remove the Shipping Address Popup', async function () {
    await addressBookPage.clickRemoveShipAddressNoBtn()
  })

  it('Verify new address is not removed in Address Book page', async function () {
    await allureReporter.addDescription('Verify new address is not removed in Address Book page')
    let expected = `${firstName} ${lastName}\n${additionalAddress1}\n${shippingDistrict},\n${additionalFullStateName}\n${additionalPhoneNumber}`
    let actual = await addressBookPage.getNonDefaultShippingAddress()
    assert.strictEqual(actual, expected, 'New address is removed in Address Book page')
  })

  it('Remove non default shipping address', async function () {
    await addressBookPage.clickRemoveNonDefaultShipAddress()
  })

  it('Verify Remove Shipping Address Pop Up is loaded', async function () {
    await addressBookPage.verifyRemoveAddressPopUpIsLoaded()
  })

  it('Click Yes button on remove the Shipping Address Popup', async function () {
    await addressBookPage.clickRemoveShipAddress()
  })

  it('Verify Global message displays on top', async function () {
    screencapture = true
    let actual = await addressBookPage.getYourGlobalMessage()
    let expected = 'Address removed successfully'
    assert.strictEqual(actual, expected, 'Global message is not displayed on top')
  })

  it('Logout Customer', async function () {
    await header.verifyLogOutIsLoaded()
    await header.clickLogOut()
    await header.verifyPageIsLoaded()
  })

  afterEach(async function () {
    await driverutils.saveScreenshots(this.currentTest.state, screencapture, driver)
    screencapture = false
  })
})
