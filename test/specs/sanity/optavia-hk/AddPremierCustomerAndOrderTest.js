// @flow
import {assert} from 'chai'
import driverutils from '@core-libs/DriverUtils'
import allureReporter from '@wdio/allure-reporter'
import CustomerTestData from '@input-data/customers/CustomerTestData'
import CoachTestData from '@input-data/coaches/CoachTestData'
import ProductTestData, {attb, market, product} from '@input-data/products/ProductTestData'
import TestCreditCards from '@input-data/creditcards/CreditCardTestData'
import ProductDetailsPage from '@page-objects/shop/ProductDetailsPage'
import CommonUtils from '@core-libs/CommonUtils'
import AddedToYourShoppingCartPopup from '@page-objects/shop/AddedToYourShoppingCartPopup'
import ShoppingCartPage from '@page-objects/shoppingcart/ShoppingCartPage'
import ShopAllPage from '@page-objects/shop/ShopAllPage'
import ShopPage from '@page-objects/shop/ShopPage'
import FindACoachPage from '@page-objects/findacoach/FindACoachPage'
import SearchCoachResultsPage from '@page-objects/searchcoachresults/SearchCoachResultsPage'
import SiteMapTestData, {page} from '@input-data/various/MiscTestData'
import MainNavBar from '@page-objects/common/MainNavBar'
import Header from '@page-objects/common/Header'
import HomePage from '@page-objects/home/HomePage'
import EssentialFuelingsPage from '@page-objects/shop/EssentialFuelingsPage'
import CreateAccountPage from '@page-objects/createAccountPage/CreateAccountPage'
import {AGENT_USER, GLOBAL_PASSWORD} from '@core-libs/ConstantUtil'
import CheckoutPage from '@page-objects/checkout/CheckoutPage'
import CheckoutCommonSection from '@page-objects/checkout/CheckoutCommonSection'
import ShippingAddressTile from '@page-objects/checkout/ShippingAddressTile'
import PaymentAndBillingAddressTile from '@page-objects/checkout/PaymentAndBillingAddressTile'
import FinalReviewTile from '@page-objects/checkout/FinalReviewTile'
import OrderConfirmationPage from '@page-objects/orderconfirmation/OrderConfirmationPage'
import MyAccountPage from '@page-objects/myaccount/MyAccountPage'
import MyAccountLeftNavigation from '@page-objects/myaccount/MyAccountLeftNavigation'
import OptaviaPremierOrderPage from '@page-objects/myaccount/OptaviaPremierOrderPage'
import ChooseYourOwnFuelingsPopup from '@page-objects/chooseyourownfuelings/ChooseYourOwnFuelingsPopup'
import TestDataBaseMapping from '@db/TestDataBaseMapping'
import HybrisBackofficeLoginPage from '@page-objects/hybrisbackoffice/HybrisBackofficeLoginPage'
import HybrisBackofficeAutorityGroupPage from '@page-objects/hybrisbackoffice/HybrisBackofficeAutorityGroupPage'
import HybrisBackofficeHomePage from '@page-objects/hybrisbackoffice/HybrisBackofficeHomePage'
import HybrisBackofficeSearchPage from '@page-objects/hybrisbackoffice/HybrisBackofficeSearchPage'
import HybrisBackofficeOrderDetailsPage from '@page-objects/hybrisbackoffice/HybrisBackofficeOrderDetailsPage'
import PopupSuggestedDeliveryAddressesForm
  from '@page-objects/popupsuggesteddeliveryaddressesform/PopupSuggestedDeliveryAddressesForm'
import OrderDetailsPage from '@page-objects/myaccount/OrderDetailsPage'
import RewardEarningsPopUp from '@page-objects/popuprewardearnings/RewardEarningsPopUp'

let screencapture = false // capture screenshot when test step 'passed' also by default only capture screenshot of failed steps

let homePage, header, mainNavBar, addedToYourShoppingCartPopup, shoppingCartPage, findACoachPage, searchCoachResultsPage, createAccountPage
let checkoutPage, shippingAddressTile, paymentAndBillingAddressTile, finalReviewTile, orderConfirmationPage, myAccountPage, popupSuggestedDeliveryAddressesForm
let productDetailsPage, shopPage, rewardsPopup, orderDetailsPage, optaviaPremierOrderPage, myAccountLeftNavigation, addItemsToCartPopup, checkoutCommonSection

let orderNumber, orderDate, orderStatus

// Customer Registration
let TestData, customerInfo

// Coach (Enroller) Info
let coachFirstName, coachLastName

// order ID
let templateID

// Payment Details
let nameOnCard, cardNumber, cvv, month, year

// Shop Page
let shopAllPage, essentialFuelingsPage

// Shopping Cart Page
let orderSubTotal, deliveryCharge, orderTotal, earnedRewards, actualOrderNumber, accountID, habitsOfHealth, coachID

// Hybris Backoffice
let agentEmail, agentPassword, hybrisBackofficeLoginPage, hybrisBackofficeAutorityGroupPage, hybrisBackofficeHomePage, hybrisBackofficeSearchPage, hybrisBackofficeOrderDetailsPage
let templateCV, templateQV, orderCV, orderQV, decimalLength
// Product
let productUnitPrice, productCasePrice, productKitCombinedPrice

describe('C5756 - HK OPTAVIA - Add OPTAVIA Premier Customer and Order', function () {
  let testTitle = this.title

  before(async function () {
    await driverutils.goToOptaviaWebSiteHomePageHK()
  })

  beforeEach(async function () {
    await driverutils.addAllureReport(allureReporter, __dirname, 'HK OPTAVIA Sanity', testTitle)
  })

  it('Preparing Test Data', async function () {
    TestData = await CustomerTestData.getCustomerInfo('90000000020')
    customerInfo = await CustomerTestData.getNewCustomer()

    // Coach Information
    coachFirstName = await CoachTestData.COACH_FIRST_NAME(6, 3)
    coachLastName = await CoachTestData.COACH_LAST_NAME(6, 3)
    coachID = await CoachTestData.COACH_ID(6, 3)

    // Product Information
    productUnitPrice = await ProductTestData.PRODUCT_INFO(attb.UNIT_PRICE, product.soups.OPT_WILD_RICE_CHICKEN_SOUP, market.HK)
    productCasePrice = await ProductTestData.PRODUCT_INFO(attb.CASE_PRICE, product.soups.OPT_WILD_RICE_CHICKEN_SOUP, market.HK)
    productKitCombinedPrice = await ProductTestData.PRODUCT_INFO(attb.UNIT_PRICE, product.kits.OPT_OPTIMAL_KIT_5_1_US_FLAVORS, market.HK)
    habitsOfHealth = await ProductTestData.PRODUCT_INFO(attb.UNIT_PRICE, product.tools_accessories.OPT_HABITS_HEALTH_SYSTEM, market.HK)
    orderSubTotal = productUnitPrice + productKitCombinedPrice + habitsOfHealth
    deliveryCharge = 'FREE'
    earnedRewards = (productUnitPrice + productKitCombinedPrice) * 0.1
    orderTotal = (orderSubTotal - habitsOfHealth) - earnedRewards

    // Payment Information (for VISA = B, Mastercard = C, American Express = D and Discover = E) column of sheet 2 in excel file
    let ccCol = 'B'
    let sheet = 2
    nameOnCard = customerInfo.CUSTOMER_FIRST_NAME + ' ' + customerInfo.CUSTOMER_LAST_NAME
    cardNumber = await TestCreditCards.CARD_NUMBER(sheet, ccCol)
    cvv = await TestCreditCards.CVV(sheet, ccCol)
    month = await TestCreditCards.EXPIRATION_MONTH(sheet, ccCol)
    year = await TestCreditCards.EXPIRATION_YEAR(sheet, ccCol)
  })

  it('Loads the Home Page', async function () {
    homePage = new HomePage()
    await homePage.verifyPageIsLoaded()
    header = new Header()
    await header.verifyPageIsLoaded()
    mainNavBar = new MainNavBar()
    await mainNavBar.verifyPageIsLoaded()
  })

  it('Loads Shop All Page', async function () {
    await mainNavBar.clickShopAll()
    shopAllPage = new ShopAllPage()
    await shopAllPage.verifyPageIsLoaded()
  })

  it('Click on Shop Button under Essentials Fuelings', async function () {
    await shopAllPage.clickEssentialFuelingsShopBtn()
  })

  it('Loads Select Fuelings Page', async function () {
    essentialFuelingsPage = new EssentialFuelingsPage()
    await essentialFuelingsPage.verifyPageIsLoaded()
  })

  it('Check on Soups Checkbox', async function () {
    await essentialFuelingsPage.checkSoupsCheckbox()
  })

  it('Click on Wild Rice & Chicken Flavored Soup link', async function () {
    await essentialFuelingsPage.clickWildRiceAndChickenFlavoredSoupLink()
  })

  it('Loads the product Detail Page', async function () {
    productDetailsPage = new ProductDetailsPage()
    await productDetailsPage.verifyPageIsLoaded()
  })

  it('Verify items in Product drop down', async function () {
    let itemsArray = await productDetailsPage.getProductDropDownText()
    let actual = itemsArray
    let actualResult = actual[1].trim() + ' ' + actual[2].trim()
    let expected = `CASE ${await CommonUtils.toCurrency(productCasePrice, market.HK)} BOX ${await CommonUtils.toCurrency(productUnitPrice, market.HK)}`
    assert.strictEqual(actualResult, expected, 'Items in Product drop down is not matching with expected')
  })

  it('Click Add to cart button', async function () {
    await productDetailsPage.clickAddToCartBtn()
  })

  it('Loads the Added To Your Shopping Cart Popup', async function () {
    addedToYourShoppingCartPopup = new AddedToYourShoppingCartPopup()
    await addedToYourShoppingCartPopup.verifyPageIsLoaded()
  })

  it('Click on Checkout button on pop up window', async function () {
    await addedToYourShoppingCartPopup.clickCheckoutButton()
  })

  it('Loads the Shopping Cart Page', async function () {
    shoppingCartPage = new ShoppingCartPage()
    await shoppingCartPage.verifyPageIsLoaded()
  })
  /*
  // Temporary fix
  it('Temporary Step - UnCheck the checkbox to Join Premier and Verify Join Premier checkbox is not checked', async function () {
    await shoppingCartPage.clickToCheckJoinPremierCheckBox()
    await shoppingCartPage.verifyPageIsLoaded()
    let actual = await shoppingCartPage.isJoinPremierChecked()
    assert.isFalse(actual, 'Join Premier checkbox did not get unchecked')
  })

  it('Verify Join Premier checkbox is NOT checked', async function () {
    screencapture = true
    let actual = await shoppingCartPage.isJoinPremierChecked()
    assert.isFalse(actual, 'Join Premier checkbox is checked')
  })
*/
  it('Click on Continue shopping Button', async function () {
    await shoppingCartPage.clickContinueShoppingBtn()
  })

  it('Loads Shop All Page', async function () {
    shopAllPage = new ShopAllPage()
    await shopAllPage.verifyPageIsLoaded()
  })

  it('Click on View All button', async function () {
    await shopAllPage.clickViewAllBtn()
  })

  it('Loads Shop Page', async function () {
    shopPage = new ShopPage()
    await shopPage.verifyPageIsLoaded()
  })

  it('Click on Kits checkbox', async function () {
    await shopPage.clickKitsCheckbox()
  })

  it('Click on Add To Cart Button for Essential Optimal Kit for 5&1 Plan', async function () {
    await shopPage.clickOptimalKitFor5And1Plan()
  })

  it('Click on Checkout button on pop up window', async function () {
    await addedToYourShoppingCartPopup.clickBeginCheckoutButtonHK()
  })

  it('Check the Checkbox for Join Optavia Premier Checkbox', async function () {
    screencapture = true
    if (await shoppingCartPage.isJoinPremierChecked() === false) {
      await shoppingCartPage.clickToCheckJoinPremierCheckBox()
    }
  })

  it('Verify that 5 free meals are displayed in cart', async function () {
    screencapture = true
    let freeItemsNames = await shoppingCartPage.getFreeItemsNames()
    let actual = freeItemsNames.filter(name => name.includes('OPTAVIA') === false)
    let expected = 5
    assert.strictEqual(actual.length, expected, 'Free meals Not Found')
  })

  it('Verify OPTAVIA BlenderBottle Classic is displayed as Free Item', async function () {
    await shoppingCartPage.verifyFreeBlenderBottleIsLoaded()
  })

  it('Verify OPTAVIA Free Guide is displayed as Free Item', async function () {
    await shoppingCartPage.verifyFreeOptaviaGuideIsLoaded()
  })

  it('Verify Journey Kickoff Card Insert is displayed as Free Item', async function () {
    await shoppingCartPage.verifyFreeJourneyKickoffCardInsertIsLoaded()
  })

  it('Verify OPTAVIA Guide Top Tips Insert is displayed as Free Item', async function () {
    await shoppingCartPage.verifyFreeOptaviaGuideTopTipsInsertIsLoaded()
  })

  it('Verify Order Subtotal in the cart', async function () {
    let expected = await CommonUtils.toCurrency(orderSubTotal, market.HK)
    let actual = await shoppingCartPage.getOrderSubtotal()
    assert.strictEqual(actual, expected, `Order Subtotal actual : ${actual} is not matching with ${expected}`)
  })

  it('Click on Proceed to Checkout button', async function () {
    await shoppingCartPage.clickProceedToCheckoutBtn()
  })

  it('Loads the Find a Coach page', async function () {
    findACoachPage = new FindACoachPage()
    await findACoachPage.verifyPageIsLoaded()
  })

  it('Verify text in Given Name text box', async function () {
    let expected = 'Given Name'
    let actual = await findACoachPage.getFirstNameTextboxText('placeholder')
    assert.strictEqual(actual, expected, `${expected} is not displayed`)
  })

  it('Verify text in Family Name text box', async function () {
    let expected = 'Family Name'
    let actual = await findACoachPage.getLastNameTextboxText('placeholder')
    assert.strictEqual(actual, expected, `${expected} is not displayed`)
  })

  it('Verify text in City text box is not displayed', async function () {
    let actual = await findACoachPage.isCityTextboxDisplayed()
    assert.isFalse(actual, 'City Name is displayed')
  })

  it('Verify market is defaulted to Hong Kong', async function () {
    let expected = 'Hong Kong'
    let actual = await findACoachPage.getMarketDefaultText()
    assert.strictEqual(actual, expected, `${expected} is not displayed`)
  })

  it('Search a coach', async function () {
    await allureReporter.addDescription(`Search a coach ${coachFirstName} ${coachLastName}`)
    await findACoachPage.enterFirstAndLastNameToSearchCoach(coachFirstName, coachLastName)
    await findACoachPage.clickSearchCoachFindACoachButton()
  })

  it('Loads the Search Result page', async function () {
    searchCoachResultsPage = new SearchCoachResultsPage()
    await searchCoachResultsPage.verifyPageIsLoaded()
  })

  it('Verify Coach Search Result page body text', async function () {
    let expected = await SiteMapTestData.PAGE_BODY_TITLE(page.Home.SEARCH_COACH_RESULTS)
    let actual = await searchCoachResultsPage.getCoachSearchResultTitle()
    assert.strictEqual(actual, expected, 'Coach Search Result page body text is not matching with expected')
  })

  it('Verify searched coach is in search result list', async function () {
    screencapture = true
    await allureReporter.addDescription(`Verify ${coachFirstName} ${coachLastName} is in search result list`)
    let expected = `${coachFirstName} ${coachLastName}`
    let actual = await searchCoachResultsPage.getCoachName()
    assert.strictEqual(actual, expected, `Searched coach : ${expected} is not displayed`)
  })

  it('Click on Connect Me next to searched coach', async function () {
    await allureReporter.addDescription(`Click on Connect Me next to ${coachFirstName} ${coachLastName}`)
    await searchCoachResultsPage.clickConnectMeButton()
  })

  it('Loads the Create Account page', async function () {
    createAccountPage = new CreateAccountPage()
    await createAccountPage.verifyPageIsLoaded()
  })

  it('Verify Coach info displayed on top header', async function () {
    let actual = await header.getCoachInformation()
    let expected = `${coachFirstName} ${coachLastName}, ${coachID}`
    assert.strictEqual(actual, expected, `Coach Info : ${expected} is not displayed on top header`)
  })

  it('Create an Account', async function () {
    screencapture = true
    await allureReporter.addDescription(`Create an Account Full name: ${nameOnCard}, email: ${customerInfo.CUSTOMER_EMAIL}, password: ${GLOBAL_PASSWORD}`)
    await createAccountPage.createAnAccount(nameOnCard, customerInfo.CUSTOMER_EMAIL, GLOBAL_PASSWORD, TestData.SHIPPING_PHONE)
  })

  it('Loads the Checkout page', async function () {
    checkoutPage = new CheckoutPage()
    await checkoutPage.verifyPageIsLoaded()
  })

  it('Loads the CheckoutCommonSection page', async function () {
    checkoutCommonSection = new CheckoutCommonSection()
    await checkoutCommonSection.verifyPageIsLoaded()
  })

  it('Verify Coach info displayed on top header', async function () {
    let actual = await checkoutCommonSection.getCoachInfoFromAvatar()
    let expected = `${coachFirstName} ${coachLastName}, ${coachID}`
    assert.strictEqual(actual, expected, `Coach Info : ${expected} is not displayed on top header`)
  })

  it('Loads the Shipping Address page', async function () {
    shippingAddressTile = new ShippingAddressTile()
    await shippingAddressTile.verifyPageIsLoaded()
  })

  // Cover test case C23133
  it('Verify text in the Country Value says "Hong Kong"', async function () {
    let actual = await shippingAddressTile.getDisabledCountryText()
    let expected = await TestData.SHIPPING_FULL_STATE_NAME
    assert.strictEqual(actual, expected, 'Country value is not Hong Kong')
  })

  it('Enter Shipping Full Name and Address information', async function () {
    await shippingAddressTile.addShippingAddressHK(TestData, true)
  })

  it('Check Default shipping check box', async function () {
    await shippingAddressTile.clickToCheckDefaultAddressCheckBox()
  })

  it('Shipping Address: click Next button', async function () {
    await shippingAddressTile.clickNext()
  })

  it('Loads the Suggested Address popup', async function () {
    popupSuggestedDeliveryAddressesForm = new PopupSuggestedDeliveryAddressesForm()
    await popupSuggestedDeliveryAddressesForm.verifyPageIsLoaded()
  })

  it('Click on Use this address button', async function () {
    await shippingAddressTile.verifyPageIsLoaded()
    await popupSuggestedDeliveryAddressesForm.clickUseThisAddress()
  })

  it('Verify Estimated Rewards Earned on this Order is HK$369.00', async function () {
    let expected = await CommonUtils.toCurrency(earnedRewards, market.HK)
    let actual = await checkoutPage.getEarningThisOrderText()
    assert.strictEqual(actual, expected, `Estimated Rewards Earned :${expected} is not matching with actual`)
  })

  it('Loads the Payment Details page', async function () {
    paymentAndBillingAddressTile = new PaymentAndBillingAddressTile()
    await paymentAndBillingAddressTile.verifyPageIsLoadedHK()
  })

  it('Verify Coach info displayed on top header', async function () {
    let actual = await checkoutCommonSection.getCoachInfoFromAvatar()
    let expected = `${coachFirstName} ${coachLastName}, ${coachID}`
    assert.strictEqual(actual, expected, `Coach Info : ${expected} is not displayed on top header`)
  })

  it('Enter Payment Details information', async function () {
    await paymentAndBillingAddressTile.createPaymentDetailsHK(nameOnCard, cardNumber, month + year, cvv)
  })

  it('Click on Next button from Payment Details', async function () {
    await paymentAndBillingAddressTile.clickNextHK()
  })

  it('Loads the Final Review page', async function () {
    finalReviewTile = new FinalReviewTile()
    await finalReviewTile.verifyPageIsLoaded()
  })

  it('Verify Coach info displayed on top header', async function () {
    let actual = await checkoutCommonSection.getCoachInfoFromAvatar()
    let expected = `${coachFirstName} ${coachLastName}, ${coachID}`
    assert.strictEqual(actual, expected, `Coach Info : ${expected} is not displayed on top header`)
  })

  /* /!* it('Verify Items to be delivered', async function () {
       //TODO
    }) *!/ */

  it('Verify Premier Rewards Summary Pop Up is displayed', async function () {
    await checkoutPage.clickEstimatedRewards()
    rewardsPopup = new RewardEarningsPopUp()
    await rewardsPopup.verifyPageIsLoaded()
  })

  it('Verify Total Rewards earned in Pop Up', async function () {
    let actual = await rewardsPopup.getTotalRewardsYouEarned()
    let expected = await CommonUtils.toCurrency(earnedRewards, market.HK)
    assert.strictEqual(actual, expected, `Total Rewards earned :${expected} is not matching with actual`)
  })

  it('Close Rewards Earning pop up', async function () {
    await rewardsPopup.clickCloseRewardEarningsPopUp()
  })

  it('Click on Submit Order Button', async function () {
    await browser.pause(2000)
    await finalReviewTile.clickPlaceOrderButton()
  })

  it('Loads the Order Confirmation page', async function () {
    orderConfirmationPage = new OrderConfirmationPage()
    await orderConfirmationPage.verifyPageIsLoaded()
  })

  it('Verify the Coach information is not displayed in Order Summary Section', async function () {
    screencapture = true
    assert.isFalse(await orderConfirmationPage.isCoachInfoFromAvatarDisplayed(), 'Coach Information is displayed')
  })

  it('Capture Confirmation Page Details', async function () {
    orderNumber = await orderConfirmationPage.getOrderNumber()
    orderDate = await orderConfirmationPage.getOrderDate()
    orderStatus = await orderConfirmationPage.getOrderStatus()
    await allureReporter.addDescription(`Order confirmation number is: ${orderNumber}`)
  })

  it('Click on Continue Shopping button on Order Confirmation page', async function () {
    await orderConfirmationPage.clickContinueShopping()
  })

  it('Loads the Home page', async function () {
    await header.verifyMyAccountLinkIsLoaded()
    await mainNavBar.verifyPageIsLoaded()
  })

  it('Navigate to My Account menu', async function () {
    await header.clickMyAccount()
    myAccountPage = new MyAccountPage()
    await myAccountPage.verifyPageIsLoaded()
    await browser.pause(10000)
  })

  it('Verify recently placed order is displayed in order history', async function () {
    screencapture = true
    let actual = await myAccountPage.getLastOrderNumber()
    let expected = orderNumber.split(' ')[4]
    await allureReporter.addDescription(`Expected last order is ${expected}`)
    assert.strictEqual(actual, expected, `Order Number : ${expected} is not available in recently Placed order`)
  })

  it('Verify recently placed order is displayed as Premier', async function () {
    await myAccountPage.verifyPremierOrderIconIsVisible()
  })

  it('Click on Last Order Number link', async function () {
    await myAccountPage.clickLastOrderNumberLink()
  })

  it('Loads the Order Details page', async function () {
    orderDetailsPage = new OrderDetailsPage()
    await orderDetailsPage.verifyPageIsLoaded()
  })

  it('Verify Order Details match Order Confirmation details', async function () {
    orderNumber = orderNumber.substring('Your Order Number is '.length)
    orderDate = new Intl.DateTimeFormat().format(new Date(orderDate))
    orderStatus = orderStatus.substring('The order is '.length)
    let expected = orderNumber + '\n' + orderDate + '\n' + orderStatus
    let actualOrderNumber = (await orderDetailsPage.getOrderNumber()).substring('Order # '.length)
    let actualDate = new Intl.DateTimeFormat().format(new Date(await orderDetailsPage.getOrderDate()))
    let actualOrderStatus = await orderDetailsPage.getOrderStatus()
    let actual = actualOrderNumber + '\n' + actualDate + '\n' + actualOrderStatus
    assert.strictEqual(actual, expected, 'Order Details not matching with Order Confirmation details')
  })

  it('Navigate to OPTAVIA Premier Order Page', async function () {
    myAccountLeftNavigation = new MyAccountLeftNavigation()
    await myAccountLeftNavigation.clickOptaviaPremierOrder()
    optaviaPremierOrderPage = new OptaviaPremierOrderPage()
    await optaviaPremierOrderPage.verifyPageIsLoaded()
  })

  it('Verify Order Summary: Subtotal displayed on Subscription page is displayed', async function () {
    let expected = await CommonUtils.toCurrency(productUnitPrice + productKitCombinedPrice, market.HK)
    let actual = await optaviaPremierOrderPage.getPremierSubtotalText()
    assert.strictEqual(actual, expected, 'Order Summary: Subtotal is not matching with expected')
  })

  it('Verify Order Summary: Applied Rewards displayed on Subscription page is displayed in edit mode', async function () {
    let expected = '-' + await CommonUtils.toCurrency(earnedRewards, market.HK)
    let actual = await optaviaPremierOrderPage.getPremierAppliedRewardsText()
    assert.strictEqual(actual, expected, 'Order Summary: Applied Rewards is not matching with expected')
  })

  it('Verify Order Summary: Delivery Charge displayed on Subscription page is displayed in edit mode', async function () {
    let expected = deliveryCharge
    let actual = await optaviaPremierOrderPage.getPremierDeliveryChargeText()
    assert.strictEqual(actual, expected, 'Order Summary: Delivery Charge is not matching with expected')
  })

  it('Verify Order Summary: Total displayed on Subscription page is displayed', async function () {
    let expected = await CommonUtils.toCurrency(orderTotal, market.HK)
    let actual = await optaviaPremierOrderPage.getPremierOrderTotalText()
    assert.strictEqual(actual, expected, 'Order Summary: Total is not matching with expected')
  })

  it('Navigate to Choose Your Own Fuelings from Shop', async function () {
    screencapture = true
    await mainNavBar.clickChooseYourOwnFuelings()
    addItemsToCartPopup = new ChooseYourOwnFuelingsPopup()
    await addItemsToCartPopup.verifyPageIsLoaded()
  })

  it('Click on Essential Bars', async function () {
    await addItemsToCartPopup.clickEssentialBars()
  })

  it('Update quantity of Essential Bar to 10', async function () {
    await addItemsToCartPopup.increaseItemQuantity(10)
  })

  it('Select the products', async function () {
    await addItemsToCartPopup.selectProduct()
    await addItemsToCartPopup.verifyPageIsLoaded()
  })

  it('Click on Add items to Cart button', async function () {
    await addItemsToCartPopup.clickAddItemsToCart()
  })

  it('Loads the Shopping Cart page', async function () {
    shoppingCartPage = new ShoppingCartPage()
    await shoppingCartPage.verifyPageIsLoaded()
  })

  it('Click on Proceed to Checkout button', async function () {
    await shoppingCartPage.clickProceedToCheckoutBtn()
  })

  it('Verify Coach info displayed on top header', async function () {
    let actual = await checkoutCommonSection.getCoachInfoFromAvatar()
    let expected = `${coachFirstName} ${coachLastName}, ${coachID}`
    assert.strictEqual(actual, expected, `Coach Info : ${expected} is not displayed on top header`)
  })

  it('Enter Existing Payment Details information', async function () {
    await paymentAndBillingAddressTile.clickOnExistingCardDetailsSG(cvv)
  })

  it('Click on Next button from Payment tile', async function () {
    await paymentAndBillingAddressTile.clickNextHK()
  })

  it('Loads the Final Review page', async function () {
    finalReviewTile = new FinalReviewTile()
    await finalReviewTile.verifyPageIsLoaded()
  })

  it('Verify Coach info displayed on top header', async function () {
    let actual = await checkoutCommonSection.getCoachInfoFromAvatar()
    let expected = `${coachFirstName} ${coachLastName}, ${coachID}`
    assert.strictEqual(actual, expected, `Coach Info : ${expected} is not displayed on top header`)
  })

  it('Click on Submit Order Button', async function () {
    await finalReviewTile.clickPlaceOrderButton()
  })

  it('Loads the Order Confirmation page', async function () {
    orderConfirmationPage = new OrderConfirmationPage()
    await orderConfirmationPage.verifyPageIsLoaded()
    actualOrderNumber = await orderConfirmationPage.getOrderNumber()
    let resultArray = actualOrderNumber.split(' ')
    actualOrderNumber = resultArray[4]
    await allureReporter.addDescription(`Order Number : ${actualOrderNumber}`)
  })

  it('Verify the Coach information is not displayed in Order Summary Section', async function () {
    screencapture = true
    assert.isFalse(await orderConfirmationPage.isCoachInfoFromAvatarDisplayed(), 'Coach Information is displayed')
  })

  it('Click on Continue Shopping button', async function () {
    await orderConfirmationPage.clickContinueShopping()
  })

  it('Loads the Home page', async function () {
    await header.verifyMyAccountLinkIsLoaded()
    await mainNavBar.verifyPageIsLoaded()
  })

  it('Clicks on My Account menu and gets Client ID', async function () {
    // await header.isMyAccountLinkLoaded()
    await header.clickMyAccount()
    myAccountPage = new MyAccountPage()
    await myAccountPage.verifyPageIsLoaded()
    accountID = await myAccountPage.getAccountID()
    accountID = accountID.substring(11, accountID.length)
  })
  it('Logout Coach', async function () {
    await header.verifyLogOutIsLoaded()
    await header.clickLogOut()
    await header.verifyPageIsLoaded()
  })

  it('Get the Auto order ID from DB', async function () {
    let db = await TestDataBaseMapping.getAutoOrderHeaderView(accountID)
    templateID = db.Auto_Order_ID
  })

  afterEach(async function () {
    await driverutils.saveScreenshots(this.currentTest.state, screencapture)
    screencapture = false
  })
})

describe('C5756 - Login to Hybris Backoffice as CSA to validate QV/CV the Template order', function () {
  let testTitle = this.title

  before(async function () {
    await driverutils.goToHybrisBackofficeSitePage()
  })

  beforeEach(async function () {
    await driverutils.addAllureReport(allureReporter, __dirname, 'HK OPTAVIA Sanity', testTitle)
  })

  it('Preparing Test Data', async function () {
    templateCV = '3450.60'
    templateQV = '340.20'
    orderCV = '2130.00'
    orderQV = '210.00'
    agentEmail = AGENT_USER
    agentPassword = GLOBAL_PASSWORD
  })

  it('Hybris Backoffice page is loaded ', async function () {
    hybrisBackofficeLoginPage = new HybrisBackofficeLoginPage()
    await hybrisBackofficeLoginPage.verifyPageIsLoaded()
  })

  it('Login into Backoffice', async function () {
    await hybrisBackofficeLoginPage.loginAsCSR(agentEmail, agentPassword)
  })

  it('Backoffice Authority group page displays ', async function () {
    hybrisBackofficeAutorityGroupPage = new HybrisBackofficeAutorityGroupPage()
    await hybrisBackofficeAutorityGroupPage.verifyPageIsLoaded()
  })

  it('Click on Customer Support Admin radio button', async function () {
    await browser.pause(3000)
    await hybrisBackofficeAutorityGroupPage.proceedAsCSAdminRole()
    hybrisBackofficeHomePage = new HybrisBackofficeHomePage()
    await hybrisBackofficeHomePage.verifyPageIsLoaded()
  })

  it('Click on Orders link from Customer Admin section', async function () {
    await hybrisBackofficeHomePage.clickASMOrder()
  })

  it('Click on Order link from Customer Admin section', async function () {
    await browser.pause(3000)
    await hybrisBackofficeHomePage.clickOrder()
  })

  it('Search Textbox is loaded', async function () {
    hybrisBackofficeSearchPage = new HybrisBackofficeSearchPage()
    await hybrisBackofficeSearchPage.verifyPageIsLoaded()
  })

  it('Search the template ID number', async function () {
    await hybrisBackofficeSearchPage.searchOrder(templateID)
  })

  it('Click on Search result', async function () {
    await hybrisBackofficeSearchPage.clickSearchedOrder()
  })

  it('Verify Order Details body title displays', async function () {
    hybrisBackofficeOrderDetailsPage = new HybrisBackofficeOrderDetailsPage()
    await hybrisBackofficeOrderDetailsPage.verifyPageIsLoaded(templateID)
  })

  it('Click on Administration Report', async function () {
    await hybrisBackofficeOrderDetailsPage.clickAdminReportTab()
  })

  it('Verify the Order commissionable Volume is same as Order total', async function () {
    await browser.pause(3000)
    let expected = templateCV.toString()
    let actual = await hybrisBackofficeOrderDetailsPage.getOrderCommissionalbleVolume()
    decimalLength = actual.split('.')
    assert.isTrue((decimalLength[1].length === 2), 'length is not 2')
    assert.strictEqual(actual, expected, `Order commissionable volume:${actual} is not matched with Order Total`)
  })

  it('Verify the Qualifying Volume ', async function () {
    let expected = templateQV.toString()
    let actual = await hybrisBackofficeOrderDetailsPage.getQualifyingVolume()
    decimalLength = actual.split('.')
    assert.isTrue((decimalLength[1].length === 2), 'length is not 2')
    assert.strictEqual(actual, expected, `Qualifying volume :${actual} is not matched with expected value`)
  })

  // verify Qv/CV for actual order
  it('Search the on Demand number', async function () {
    await hybrisBackofficeSearchPage.searchOrder(actualOrderNumber)
  })

  it('Click on Search result', async function () {
    await hybrisBackofficeSearchPage.clickSearchedOrder()
  })

  it('Verify Order Details body title displays', async function () {
    hybrisBackofficeOrderDetailsPage = new HybrisBackofficeOrderDetailsPage()
    await hybrisBackofficeOrderDetailsPage.verifyPageIsLoaded(actualOrderNumber)
  })

  it('Click on Administration Report', async function () {
    await hybrisBackofficeOrderDetailsPage.clickAdminReportTab()
  })

  it('Verify the Order commissionable Volume is same as Order total', async function () {
    await browser.pause(3000)
    let expected = orderCV
    let actual = await hybrisBackofficeOrderDetailsPage.getOrderCommissionalbleVolume()
    decimalLength = actual.split('.')
    assert.isTrue((decimalLength[1].length === 2), 'length is not 2')
    assert.strictEqual(actual, expected, `Order commissionable volume:${actual} is not matched with Order Total`)
  })

  it('Verify the Qualifying Volume ', async function () {
    let expected = orderQV
    let actual = await hybrisBackofficeOrderDetailsPage.getQualifyingVolume()
    decimalLength = actual.split('.')
    assert.isTrue((decimalLength[1].length === 2), 'length is not 2')
    assert.strictEqual(actual, expected, `Qualifying volume :${actual} is not matched with expected value`)
  })
  it('Click on Sign out button', async function () {
    await browser.pause(6000)
    await hybrisBackofficeHomePage.clickSignOut()
  })

  afterEach(async function () {
    await driverutils.saveScreenshots(this.currentTest.state, screencapture)
    screencapture = false
  })
})
