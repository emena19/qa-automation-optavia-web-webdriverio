// @flow
import {assert} from 'chai'
import driverutils from '@core-libs/DriverUtils'
import allureReporter from '@wdio/allure-reporter'
import CustomerTestData from '@input-data/customers/CustomerTestData'
import TestProductData, {attb, market, product, productUOM} from '@input-data/products/ProductTestData'
import {AGENT_USER} from '@core-libs/ConstantUtil'
import HomePage from '@page-objects/home/HomePage'
import Header from '@page-objects/common/Header'
import MainNavBar from '@page-objects/common/MainNavBar'
import ASMHeader from '@page-objects/common/ASMHeader'
import MyAccountLeftNavigation from '@page-objects/myaccount/MyAccountLeftNavigation'
import MyAccountPage from '@page-objects/myaccount/MyAccountPage'
import OptaviaPremierOrderPage from '@page-objects/myaccount/OptaviaPremierOrderPage'
import SiteMapTestData, {page} from '@input-data/various/MiscTestData'
import DatePickerOptaviaPremier from '@page-objects/myaccount/DatePickerOptaviaPremier'
import ShoppingCartPage from '@page-objects/shoppingcart/ShoppingCartPage'
import ChooseYourOwnFuelingsPopup from '@page-objects/chooseyourownfuelings/ChooseYourOwnFuelingsPopup'
import CommonUtils from '@core-libs/CommonUtils'
import TestDataBaseMapping from '@db/TestDataBaseMapping'

describe(' C16878 HK ASM OPTAVIA - Update OPTAVIA Premier Order', function () {
  let screencapture = false // "True" to capture screenshot when test step 'passed'. By default only capture screenshot of failed steps

  let homePage, header, myAccountPage, optaviaPremierOrderPage, myAccountLeftNavigation, mainNavBar, expectedNextOrderDate
  let optaviaTemplateItems, shoppingCartPage, productUnitPriceArray, productQtyArray, productPriceArray,
    productSubtotal, deliveryCharge, accountID, orderTotal, addItemsToCartPopup, CartOptaviaItems, datePickerOptaviaPremier
  let searchProductSKU, asmHeader, asmAgentEmail, searchProductName
  // Customer Registration
  let email, password, productUnitPrice

  let productQuantity = 6

  let testTitle = this.title

  before(async function () {
    await driverutils.goToOptaviaAsmWebSiteHomePageHK()
  })

  beforeEach(async function () {
    await driverutils.addAllureReport(allureReporter, __dirname, 'HK ASM Sanity', testTitle)
  })

  it('Preparing Test Data', async function () {
    let custID = '85220000436781'
    let TestData = await CustomerTestData.getCustomerInfo(custID)
    email = await TestData.CUSTOMER_EMAIL
    password = TestData.CUSTOMER_PASSWORD
    searchProductSKU = await TestProductData.PRODUCT_INFO(attb.SKU, product.bars.OPT_PEANUT_CRISP_BAR, market.HK)
    searchProductName = await TestProductData.PRODUCT_INFO(attb.NAME, product.bars.OPT_PEANUT_CRISP_BAR, market.HK)
    productUnitPrice = await TestProductData.PRODUCT_INFO(attb.UNIT_PRICE, product.bars.OPT_PEANUT_CRISP_BAR, market.HK)
    asmAgentEmail = AGENT_USER
  })

  it('Loads the Home Page', async function () {
    homePage = new HomePage()
    await homePage.verifyPageIsLoaded()
    header = new Header()
    await header.verifyPageIsLoaded()
    mainNavBar = new MainNavBar()
    await mainNavBar.verifyPageIsLoaded()
    asmHeader = new ASMHeader()
    await asmHeader.verifyPageIsLoaded()
  })

  it('Log in ASM agent ', async function () {
    await asmHeader.loginASMAsCSR(asmAgentEmail, password)
  })

  it('Verify ASM login is successful', async function () {
    await asmHeader.verifyASMLoginIsSuccessful()
  })

  it('Verify LogIn link is displaying in header', async function () {
    await asmHeader.verifyLoginLinkIsDisplayed()
  })

  it('Start ASM Session for existing Customer', async function () {
    await browser.pause(4000)
    await asmHeader.startASMSessionForCustomer(email)
  })

  it('Verify My Account page is loaded', async function () {
    screencapture = true
    myAccountPage = new MyAccountPage()
    await myAccountPage.verifyPageIsLoaded()
    myAccountLeftNavigation = new MyAccountLeftNavigation()
    await myAccountLeftNavigation.verifyPageIsLoaded()
    accountID = await myAccountPage.getAccountID()
    accountID = accountID.substring(11, accountID.length)
  })

  it('Click on OPTAVIA Premier Order link under My Account', async function () {
    await myAccountLeftNavigation.clickOptaviaPremierOrder()
  })

  it('Loads the Subscriptions page', async function () {
    optaviaPremierOrderPage = new OptaviaPremierOrderPage()
    await optaviaPremierOrderPage.verifyPageIsLoaded()
  })

  it('Verify Items in Order section title displays', async function () {
    let expected = await SiteMapTestData.PAGE_BODY_SECTION_3(page.MyAccount.OPTAVIA_PREMIER_ORDER)
    let actual = await optaviaPremierOrderPage.getOptaviaPremierItemsInOrderText()
    assert.strictEqual(actual, expected, 'Items in Order section is not displayed')
  })

  it('Verify Modify Order button is displayed', async function () {
    await optaviaPremierOrderPage.verifyModifyOrderBtnIsLoaded()
  })

  it('Click on Modify Order button', async function () {
    optaviaTemplateItems = await optaviaPremierOrderPage.getProductArrayList()
    productQtyArray = await optaviaPremierOrderPage.getPremierProductQtyArrayList()
    productUnitPriceArray = await optaviaPremierOrderPage.getPremierProductUnitPriceArrayList()
    productPriceArray = await optaviaPremierOrderPage.getPremierProductPriceArrayList()
    productSubtotal = await optaviaPremierOrderPage.getPremierSubtotalText()
    deliveryCharge = await optaviaPremierOrderPage.getPremierDeliveryChargeText()
    orderTotal = await optaviaPremierOrderPage.getPremierOrderTotalText()
    await optaviaPremierOrderPage.clickModifyOrderBtn()
  })

  it('Update Next Order Date to tomorrow date if needed', async function () {
    screencapture = true
    datePickerOptaviaPremier = new DatePickerOptaviaPremier()
    if (await datePickerOptaviaPremier.isCalendarPickerDisplayed()) {
      await datePickerOptaviaPremier.verifyPageIsLoaded()
      await datePickerOptaviaPremier.clickDatePickerPastAutoshipUpdateBtn()
    }
  })

  it('OPTAVIA Premier Shopping Cart page is loaded', async function () {
    screencapture = true
    shoppingCartPage = new ShoppingCartPage()
    await shoppingCartPage.verifyPageIsLoaded()
  })

  it('Verify message displays on top', async function () {
    let actual = await shoppingCartPage.getSubscriptionEditMessageText()
    let expected = 'You are now editing your OPTAVIA Premier order.'
    assert.strictEqual(actual, expected, 'Global message is not displayed on top')
  })

  it('Verify order items displayed on Subscription page is displayed in edit mode', async function () {
    let actual = await shoppingCartPage.getCartItemsArrayList()
    let expected = await optaviaTemplateItems.reverse()
    assert.deepStrictEqual(actual, expected, 'Order items on Subscription page is not matching with edit mode')
  })

  it('Verify Product unit displayed on Subscription page is displayed in edit mode', async function () {
    let expected = (productUnitPriceArray.reverse()).map(price => price.replace(' / Kit', ''))
    let actual = await shoppingCartPage.getCartUnitPriceArrayList()
    assert.deepStrictEqual(actual, expected, 'Product unit on Subscription page is not matching with edit mode')
  })

  it('Verify Product Qty displayed on Subscription page is displayed in edit mode', async function () {
    let expected = productQtyArray.reverse()
    let actual = await shoppingCartPage.getCartQtyArrayList()
    assert.deepStrictEqual(actual, expected, 'Product Qty on Subscription page is not matching with edit mode')
  })

  it('Verify Price displayed on Subscription page is displayed in edit mode', async function () {
    let actual = await shoppingCartPage.getCartPriceArrayList()
    let expected = productPriceArray.reverse()
    assert.deepStrictEqual(actual, expected, 'Price displayed on Subscription page is not displayed in edit mode')
  })

  it('Verify Order Summary: Subtotal displayed on Subscription page is displayed in edit mode', async function () {
    let expected = productSubtotal
    let actual = await shoppingCartPage.getOrderSubtotal()
    assert.strictEqual(actual, expected, 'Order Summary: Subtotal displayed on Subscription page is not displayed in edit mode')
  })

  it('Verify Order Summary: Delivery Charge displayed on Subscription page is displayed in edit mode', async function () {
    let expected = deliveryCharge
    let actual = await shoppingCartPage.getPremierDeliveryChargeText()
    assert.strictEqual(actual, expected, 'Order Summary: Delivery Charge displayed on Subscription page is not displayed in edit mode')
  })

  it('Verify Order Summary: Order Total displayed on Subscription page is displayed in edit mode', async function () {
    let expected = orderTotal
    let actual = await shoppingCartPage.getPremierOrderTotalText()
    assert.strictEqual(actual, expected, 'Order Summary: Order Total displayed on Subscription page is not displayed in edit mode')
  })

  it('Navigate to Choose Your Own Fuelings', async function () {
    screencapture = true
    await mainNavBar.clickChooseYourOwnFuelings()
    addItemsToCartPopup = new ChooseYourOwnFuelingsPopup()
    await addItemsToCartPopup.verifyPageIsLoaded()
    let expected = await SiteMapTestData.PAGE_BODY_TITLE(page.Home.CHOOSE_YOUR_OWN_FUELINGS)
    let actual = await addItemsToCartPopup.getAddItemsToCartPopupTitle()
    assert.strictEqual(actual, expected, `${expected} is not dislayed`)
  })

  it('Search and select searched product', async function () {
    screencapture = true
    await allureReporter.addDescription(`Searched product is ${searchProductSKU}`)
    await addItemsToCartPopup.searchForProduct(searchProductSKU)
    await browser.pause(4000)
    await addItemsToCartPopup.selectProduct()
    await addItemsToCartPopup.verifyPageIsLoaded()
  })

  it('Add searched product to Shopping Cart', async function () {
    screencapture = true
    await addItemsToCartPopup.clickAddItemsToCart()
    await shoppingCartPage.verifyPageIsLoaded()
  })

  it('Verify added product is in the cart', async function () {
    await allureReporter.addDescription(`Added product is ${searchProductName}`)
    let expected = `${searchProductName}${productUOM.BOX}`
    let actual = await shoppingCartPage.getProductNameText(`${searchProductName}${productUOM.BOX}`)
    assert.strictEqual(actual, expected, `${expected} Product is not added in the cart`)
  })

  it('Update Product quantity to 6', async function () {
    await shoppingCartPage.updateProductQTY(productQuantity)
    await shoppingCartPage.clickUpdateProductLink()
    await shoppingCartPage.verifyPageIsLoaded()
  })

  it('Verify Order Summary: Subtotal increased by HK$1230.00', async function () {
    await allureReporter.addDescription(`Subtotal: ${productSubtotal}, is increased by HK$1230.00`)
    let subTotal = await productSubtotal.split('HK$')
    subTotal = await subTotal[1].replace(/[$,]+/gm, '')
    // convert string to Float
    subTotal = parseFloat(subTotal)
    // Add $ sign and decimal upto 2 digits
    let increasedNum = (subTotal + (productQuantity * productUnitPrice)).toFixed(2)
    let expected = await CommonUtils.toCurrency(increasedNum, market.HK)
    let actual = await shoppingCartPage.getOrderSubtotal()
    CartOptaviaItems = await shoppingCartPage.getCartItemsArrayList()
    assert.strictEqual(actual, expected, `Order Summary: Subtotal not increased by ${expected}`)
  })

  it('OPTAVIA Premier quantity updated message displays', async function () {
    screencapture = true
    let actual = await optaviaPremierOrderPage.getMessageAutoshipTemplateCancel()
    let expected = 'Product quantity has been updated.'
    assert.strictEqual(actual, expected, `"${expected}" message is not displayed`)
  })

  it('Save the Subscription template', async function () {
    await shoppingCartPage.clickSaveSubscriptionBtn()
  })

  it('Verify order items updated in edit mode is displayed on Subscription page', async function () {
    let actual = await optaviaPremierOrderPage.getProductArrayList()
    let expected = await CartOptaviaItems.reverse()
    assert.deepStrictEqual(actual, expected, 'Order items updated in edit mode is not displayed on Subscription page')
  })

  it('Verify Order Summary: Subtotal is increased by HK$1230.00 in Subscription page', async function () {
    let subTotal = await productSubtotal.split('HK$')
    // convert string to Float
    subTotal = parseFloat(subTotal[1].replace(/,/g, ''))
    let increasedNum = (subTotal + (productQuantity * productUnitPrice)).toFixed(2)
    let expected = await CommonUtils.toCurrency(increasedNum, market.HK)
    let actual = await optaviaPremierOrderPage.getPremierSubtotalText()
    assert.strictEqual(actual, expected, `Order Summary: Subtotal is not increased by ${increasedNum} in Subscription page`)
  })

  it('Click on Modify Order button', async function () {
    await optaviaPremierOrderPage.clickModifyOrderBtn()
  })

  it('OPTAVIA Premier Shopping Cart page is loaded', async function () {
    screencapture = true
    shoppingCartPage = new ShoppingCartPage(driver)
    await shoppingCartPage.verifyPageIsLoaded()
  })

  it('Verify message displays on top', async function () {
    let actual = await shoppingCartPage.getSubscriptionEditMessageText()
    let expected = 'You are now editing your OPTAVIA Premier order.'
    assert.strictEqual(actual, expected, `"${expected}" message is not displayed`)
  })

  it('Remove Essential Creamy Double Peanut Butter Crisp Bar', async function () {
    // get count of items in array list
    const pLength = await CartOptaviaItems.length
    for (let p = 0; p < (pLength); p++) {
      // get the value result from the excel value and appending $ sign in front
      let itemName = await CartOptaviaItems[p]
      if (itemName === searchProductName + productUOM.BOX) {
        await shoppingCartPage.clickRemoveIcon()
        return
      }
    }
  })

  it('Cancel the updates made to Subscription template', async function () {
    await shoppingCartPage.clickCancelSubscriptionBtn()
  })

  it('Verify order items unsaved in edit mode is displayed on Subscription page', async function () {
    let actual = await optaviaPremierOrderPage.getProductArrayList()
    let expected = await CartOptaviaItems
    assert.deepStrictEqual(actual, expected, 'Order items unsaved in edit mode is not displayed on Subscription page')
  })

  it('Verify Order Summary: Subtotal stays same with increased HK$1230.00 in Subscription page', async function () {
    let subTotal = await productSubtotal.split('HK$')
    // convert string to Float
    subTotal = parseFloat(subTotal[1].replace(/,/g, ''))
    let increasedNum = (subTotal + (productQuantity * productUnitPrice)).toFixed(2)
    let expected = await CommonUtils.toCurrency(increasedNum, market.HK)
    let actual = await optaviaPremierOrderPage.getPremierSubtotalText()
    assert.strictEqual(actual, expected, 'Order Summary: Subtotal is not same as in Subscription page')
  })

  it('Verify Modified Date from DB is displayed as System Current Date', async function () {
    let expected = await CommonUtils.getCurrentDate()
    let dateFromDB = await TestDataBaseMapping.getModifiedDate(accountID)
    let actual = await CommonUtils.getCustomFormatDate(dateFromDB, 'mm/dd/yyyy')
    assert.strictEqual(actual, expected, 'Modified Date from DB is not displayed as System Current Date')
  })

  it('Click on Edit OPTAVIA Premier Schedule button', async function () {
    await optaviaPremierOrderPage.clickEditOptaviaPremierBtn()
  })

  it('Verify the Calendar popup window is loaded', async function () {
    datePickerOptaviaPremier = new DatePickerOptaviaPremier(driver)
    await datePickerOptaviaPremier.verifyPageIsLoaded()
  })

  it(`Update Next Order Date to tomorrow/'s date in OPTAVIA Premier page`, async function () {
    let nextDate = await CommonUtils.getTomorrowDate()
    expectedNextOrderDate = nextDate + 1
    await allureReporter.addDescription(`Updating Next order date from: ${await expectedNextOrderDate} to ${nextDate}`)
    nextDate = await CommonUtils.getDayFromDate(nextDate)
    let dLength = await datePickerOptaviaPremier.getCountofDateList()
    for (let d = 0; d < dLength; d++) {
      let days = await datePickerOptaviaPremier.getNextOrderDate(d)
      if (days === nextDate) {
        await datePickerOptaviaPremier.clickAvailableDate(d)
        await datePickerOptaviaPremier.clickDatePickerSubmitBtn()
        return
      }
    }
  })

  it('Verify Next Autoship Order Date from DB is same as Next Order Date in Optavia Premier Page', async function () {
    await browser.pause(4000)
    let dateFromDB = await TestDataBaseMapping.getNextAutoshipOrderDate(accountID)
    let expected = await CommonUtils.getCustomFormatDate(dateFromDB, 'mm/dd/yyyy')
    let actual = await optaviaPremierOrderPage.getOptaviaPremierNextDateText()
    let actualArray = actual.split(' ')
    assert.strictEqual(actualArray[5], expected, `Next Autoship Order Date ${actual} from DB is not same in Optavia premier Page`)
  })
  it('Click on Modify Order button', async function () {
    await optaviaPremierOrderPage.clickModifyOrderBtn()
  })

  it('OPTAVIA Premier Shopping Cart page is loaded', async function () {
    screencapture = true
    shoppingCartPage = new ShoppingCartPage(driver)
    await shoppingCartPage.verifyPageIsLoaded()
  })

  it('Verify message displays on top', async function () {
    let actual = await shoppingCartPage.getSubscriptionEditMessageText()
    let expected = 'You are now editing your OPTAVIA Premier order.'
    assert.strictEqual(actual, expected, `"${expected}" message is not displayed`)
  })

  it('Remove Essential Creamy Double Peanut Butter Crisp Bar ', async function () {
    // get count of items in array list
    const pLength = await CartOptaviaItems.length
    for (let p = 0; p < (pLength); p++) {
      // get the value result from the excel value and appending $ sign in front
      let itemName = await CartOptaviaItems[p]
      if (itemName === searchProductName + productUOM.BOX) {
        await shoppingCartPage.clickRemoveIcon()
        return
      }
    }
  })

  it('Product removed from template message displays', async function () {
    screencapture = true
    let actual = await optaviaPremierOrderPage.getMessageAutoshipTemplateCancel()
    let expected = 'Product has been removed from your cart.'
    assert.strictEqual(actual, expected, 'Product removed from template message is not displayed')
  })

  it('Verify Order Summary: Subtotal is reduced by HK$1230.00', async function () {
    screencapture = true
    let expected = productSubtotal
    let actual = await shoppingCartPage.getOrderSubtotal()
    CartOptaviaItems = await shoppingCartPage.getCartItemsArrayList()
    assert.strictEqual(actual, expected, 'Order Summary: Subtotal is not reduced')
  })

  it('Save the Subscription template', async function () {
    await shoppingCartPage.clickSaveSubscriptionBtn()
  })

  it('Verify order items unsaved in edit mode is displayed on Subscription page', async function () {
    let actual = await optaviaPremierOrderPage.getProductArrayList()
    let expected = await CartOptaviaItems.reverse()
    assert.deepStrictEqual(actual, expected, 'Order items in edit mode is not matching with items in Subscription page')
  })

  it('Verify Order Summary: Subtotal is correct in Subscription page', async function () {
    let expected = productSubtotal
    let actual = await optaviaPremierOrderPage.getPremierSubtotalText()
    assert.strictEqual(actual, expected, `Order Summary: Subtotal ${expected} is not matching in Subcription Page`)
  })

  it('Click on end ASM session ', async function () {
    asmHeader.verifyPageIsLoaded()
    await asmHeader.clickASMEndSession()
  })

  it('Verify Login is displayed', async function () {
    screencapture = true
    header.verifyPageIsLoaded()
    await header.verifyLoginLinkIsLoaded
  })

  it('Verify coach info is NOT Displayed', async function () {
    screencapture = true
    let coachInfo

    try {
      coachInfo = await header.getCoachInformation()
    } catch (NoSuchElementException) {
      coachInfo = false
    }
    assert.isFalse(coachInfo, 'Coach info is Displayed')
  })

  afterEach(async function () {
    await driverutils.saveScreenshots(this.currentTest.state, screencapture, driver)
    screencapture = false
  })
})
