// @flow
import { assert } from 'chai'
import driverutils from '@core-libs/DriverUtils'
import CustomerTestData from '@input-data/customers/CustomerTestData'
import {AGENT_USER, GLOBAL_PASSWORD} from '@core-libs/ConstantUtil'
import HomePage from '@page-objects/home/HomePage'
import Header from '@page-objects/common/Header'
import MainNavBar from '@page-objects/common/MainNavBar'
import ASMHeader from '@page-objects/common/ASMHeader'
import MyAccountPage from '@page-objects/myaccount/MyAccountPage'
import DatePickerOptaviaPremier from '@page-objects/myaccount/DatePickerOptaviaPremier'
import ShoppingCartPage from '@page-objects/shoppingcart/ShoppingCartPage'
import OptaviaPremierOrderPage from '@page-objects/myaccount/OptaviaPremierOrderPage'
import OrderConfirmationPage from '@page-objects/orderconfirmation/OrderConfirmationPage'
import TestDataBaseMapping from '@db/TestDataBaseMapping'
import OrderDetailsPage from '@page-objects/myaccount/OrderDetailsPage'
import allureReporter from '@wdio/allure-reporter'

let screencapture = false // capture screenshot when test step 'passed' also by default only capture screenshot of failed steps
let orderNumber

describe('C16877 - HK ASM OPTAVIA - Create Autoship Order Using Same Day Processing', function () {
  let homePage, header, myAccountPage, asmHeader, optaviaPremierOrderPage, orderConfirmationPage, orderDetailsPage
  let datePickerOptaviaPremier

  // ASM Agent info
  let asmAgentEmail, asmAgentPassword, clientSearch, mainNavBar, shoppingCartPage

  let testTitle = this.title

  before(async function () {
    await driverutils.goToOptaviaAsmWebSiteHomePageHK(driver)
  })

  beforeEach(async function () {
    await driverutils.addAllureReport(allureReporter, __dirname, 'HK ASM Sanity', testTitle)
  })

  it('Preparing Test Data', async function () {
    let custID = '85220000436781'
    let TestData = await CustomerTestData.getCustomerInfo(custID)
    // ASM Agent
    asmAgentEmail = AGENT_USER
    asmAgentPassword = GLOBAL_PASSWORD
    // Client Info
    clientSearch = TestData.CUSTOMER_EMAIL
  })

  it('Loads the Home Page', async function () {
    homePage = new HomePage()
    await homePage.verifyPageIsLoaded()
    header = new Header()
    mainNavBar = new MainNavBar()
    await header.verifyPageIsLoaded()
    asmHeader = new ASMHeader()
    await asmHeader.verifyPageIsLoaded()
    await mainNavBar.verifyPageIsLoaded()
  })

  it('Log in ASM agent ', async function () {
    await asmHeader.loginASMAsCSR(asmAgentEmail, asmAgentPassword)
  })

  it('Verify ASM login is successful', async function () {
    await asmHeader.verifyASMLoginIsSuccessful()
  })

  it('Verify LogIn link is displaying in header', async function () {
    await asmHeader.verifyLoginLinkIsDisplayed()
  })

  it('Start ASM Session for existing Customer', async function () {
    await asmHeader.startASMSessionForCustomer(clientSearch)
  })

  it('Click on My Account', async function () {
    header.verifyPageIsLoaded()
    await header.clickMyAccount()
  })

  it('My Account is loaded', async function () {
    myAccountPage = new MyAccountPage()
    myAccountPage.verifyPageIsLoaded()
    await browser.pause(5000)
  })

  it('Click on edit order link', async function () {
    await myAccountPage.clickEditOrder()
  })

  it('Update Next Order Date to tomorrow date if needed', async function () {
    screencapture = true
    datePickerOptaviaPremier = new DatePickerOptaviaPremier()
    if (await datePickerOptaviaPremier.isCalendarPickerDisplayed()) {
      await datePickerOptaviaPremier.clickDatePickerPastAutoshipUpdateBtn()
    }
  })

  it('Loads the Shopping Cart page', async function () {
    shoppingCartPage = new ShoppingCartPage()
    await shoppingCartPage.verifyPageIsLoaded()
  })

  it('Update item Qty = +1 ', async function () {
    await browser.pause(5000)
    let actualQty = await shoppingCartPage.getProductQty()
    actualQty = parseInt(actualQty)
    await shoppingCartPage.updateProductQTY(actualQty + 1)
    await shoppingCartPage.clickUpdateProductLink()
    shoppingCartPage = new ShoppingCartPage()
    await shoppingCartPage.verifyPageIsLoaded()
  })

  it('Click on Save order and Loads the Subscription page ', async function () {
    await shoppingCartPage.clickSaveOrderLink()
    optaviaPremierOrderPage = new OptaviaPremierOrderPage()
    await optaviaPremierOrderPage.verifyPageIsLoaded()
  })

  it('Click on Process Now button', async function () {
    await optaviaPremierOrderPage.clickProcessNowBtn()
  })

  it('Verify Process Now popup displays', async function () {
    screencapture = true
    await optaviaPremierOrderPage.verifyProcessNowPopUpIsLoaded()
  })

  it('Click Process Now button from Process Now popup', async function () {
    screencapture = true
    await optaviaPremierOrderPage.clickProcessNowPopUpProcessNowBtn()
  })

  it('Loads the Order Confirmation page', async function () {
    screencapture = true
    orderConfirmationPage = new OrderConfirmationPage()
    await orderConfirmationPage.verifyPageIsLoaded()
    let actual = await orderConfirmationPage.getOrderNumber()
    let resultArray = actual.split(' ')
    orderNumber = resultArray[4]
    await allureReporter.addDescription(`Order Number : ${orderNumber}`)
  })

  // Temporal step, remove it when DB is syncing fast
  it('Wait for 2.5 minutes for DB to sync', async function () {
    await browser.pause(150000)
  })

  it('Verify Order ID in DB', async function () {
    let expected = orderNumber
    let actual = await TestDataBaseMapping.getOrderID(orderNumber)
    assert.strictEqual(actual, expected, `Order ID in DB not matching with expected :${expected}`)
  })

  it('Click on Continue Shopping button on Order Confirmation page', async function () {
    await orderConfirmationPage.clickContinueShopping()
  })

  it('Click on My Account link from header', async function () {
    await header.clickMyAccount()
  })

  it('My Account Dashboard page is loaded', async function () {
    await myAccountPage.verifyPageIsLoaded()
  })

  it('Verify recently placed order is displayed in order history', async function () {
    screencapture = true
    let actual = await myAccountPage.getLastOrderNumber()
    let expected = orderNumber
    await allureReporter.addDescription(`Expected last order is ${expected}`)
    assert.strictEqual(actual, expected, `Recently placed order No : ${expected} is not displayed in order history`)
  })

  it('Click on Last Order Number link', async function () {
    await myAccountPage.clickLastOrderNumberLink()
  })

  it('Loads the Order Details page', async function () {
    orderDetailsPage = new OrderDetailsPage()
    await orderDetailsPage.verifyPageIsLoaded()
  })

  it('Verify order details page displays for the order created above', async function () {
    let expected = `Order # ${orderNumber}`
    let actual = await orderDetailsPage.getOrderNumber()
    assert.strictEqual(actual, expected, `Order No : ${expected} details is not displayed in order detail page`)
  })

  it('Verify the order type is Immediate Autoship', async function () {
    screencapture = true
    let expected = 'Type: Immediate_Autoship'
    let actual = await orderDetailsPage.getOrderType()
    assert.strictEqual(actual.trim(), expected, 'Order type is not Immediate Autoship')
  })

  it('Click on Log out Link from header', async function () {
    await header.clickLogOut()
  })

  afterEach(async function () {
    await driverutils.saveScreenshots(this.currentTest.state, screencapture, driver)
    screencapture = false
  })
})
