// @flow
import {assert} from 'chai'
import driverutils from '@core-libs/DriverUtils'
import CustomerTestData from '@input-data/customers/CustomerTestData'
import {AGENT_USER} from '@core-libs/ConstantUtil'
import HomePage from '@page-objects/home/HomePage'
import Header from '@page-objects/common/Header'
import ASMHeader from '@page-objects/common/ASMHeader'
import MyAccountPage from '@page-objects/myaccount/MyAccountPage'
import MyAccountLeftNavigation from '@page-objects/myaccount/MyAccountLeftNavigation'
import OptaviaPremierOrderPage from '@page-objects/myaccount/OptaviaPremierOrderPage'
import AddressBookPage from '@page-objects/myaccount/AddressBookPage'
import AddressDetailsPage from '@page-objects/myaccount/AddressDetailsPage'
import PopupSuggestedDeliveryAddressesForm
  from '@page-objects/popupsuggesteddeliveryaddressesform/PopupSuggestedDeliveryAddressesForm'
import allureReporter from '@wdio/allure-reporter'

describe('C16880 -HK ASM OPTAVIA - My Account -Address Book - Existing customer Add, Edit, Delete address in Address book', function () {
  let screencapture = false // True to capture screenshot when test step 'passed'. By default only capture screenshot of failed steps

  let homePage, header, asmHeader, myAccountPage, myAccountLeftNavigation, addressBookPage,
    popupSuggestedDeliveryAddressesForm, addressDetailsPage

  // Customer Registration
  let email, password, firstName, lastName, suggestedAddress, asmAgentEmail

  // Shipping Information
  let shippingAddressLine1, shippingState, shippingPhone, shippingDistrict, shippingAddressLine2,
    addBillingAddressLine1, addBillingCountryCode, addBillPhone, optaviaPremierOrderPage, addBillingDistrict

  let testTitle = this.title

  before(async function () {
    await driverutils.goToOptaviaAsmWebSiteHomePageHK()
  })

  beforeEach(async function () {
    await driverutils.addAllureReport(allureReporter, __dirname, 'HK ASM Sanity', testTitle)
  })

  it('Preparing Test Data', async function () {
    let custID = '85220000405168'
    let TestData = await CustomerTestData.getCustomerInfo(custID)
    email = TestData.CUSTOMER_EMAIL
    password = TestData.CUSTOMER_PASSWORD
    firstName = TestData.CUSTOMER_FIRST_NAME
    lastName = TestData.CUSTOMER_LAST_NAME
    shippingAddressLine1 = TestData.SHIPPING_ADDRESS_LINE_1
    shippingAddressLine2 = TestData.SHIPPING_ADDRESS_LINE_2
    shippingDistrict = TestData.SHIPPING_DISTRICT
    shippingState = TestData.SHIPPING_FULL_STATE_NAME
    shippingPhone = TestData.SHIPPING_PHONE
    // Add Billing Address
    addBillingAddressLine1 = TestData.ADDITIONAL_ADDRESS_LINE_1
    addBillingDistrict = TestData.ADDITIONAL_DISTRICT
    addBillingCountryCode = TestData.ADDITIONAL_COUNTRY_CODE
    addBillPhone = TestData.ADDITIONAL_PHONE
    asmAgentEmail = AGENT_USER
  })

  it('Loads the Home Page', async function () {
    homePage = new HomePage()
    await homePage.verifyPageIsLoaded()
    header = new Header()
    await header.verifyPageIsLoaded()
    asmHeader = new ASMHeader()
    await asmHeader.verifyPageIsLoaded()
  })

  it('Log in ASM agent ', async function () {
    await asmHeader.loginASMAsCSR(asmAgentEmail, password)
  })

  it('Verify ASM login is successful', async function () {
    await asmHeader.verifyASMLoginIsSuccessful()
  })

  it('Verify LogIn link is displaying in header', async function () {
    await asmHeader.verifyLoginLinkIsDisplayed()
  })

  it('Start ASM Session for existing Customer', async function () {
    await browser.pause(5000)
    await asmHeader.startASMSessionForCustomer(email)
  })

  it('Verify My Account page is loaded', async function () {
    screencapture = true
    myAccountPage = new MyAccountPage()
    await myAccountPage.verifyPageIsLoaded()
    myAccountLeftNavigation = new MyAccountLeftNavigation()
    await myAccountLeftNavigation.verifyPageIsLoaded()
  })

  it('Click on the Optavia Premier Order link', async function () {
    await myAccountLeftNavigation.clickOptaviaPremierOrder()
  })

  it('Loads the Subscriptions page', async function () {
    optaviaPremierOrderPage = new OptaviaPremierOrderPage()
    await optaviaPremierOrderPage.verifyPageIsLoaded()
  })

  it('Navigate to Address Book page', async function () {
    await myAccountLeftNavigation.clickAddressBook()
  })

  it('Loads the Address Book page', async function () {
    screencapture = true
    addressBookPage = new AddressBookPage()
    await addressBookPage.verifyPageIsLoaded()
  })

  it('Click on Add Address button from Address Book page', async function () {
    await addressBookPage.clickAddNewAddressButton()
  })

  it('Verify Address Details page is loaded', async function () {
    addressDetailsPage = new AddressDetailsPage()
    await addressDetailsPage.verifyPageIsLoaded()
  })

  // Covers test case C23170
  it('Verify text in the Country Value says "Hong Kong"', async function () {
    let actual = await addressDetailsPage.getDisabledCountryText()
    let expected = shippingState
    assert.strictEqual(actual, expected, 'Country Value Is not "Hong Kong"')
  })

  it('Enter Shipping Address information without Phone value', async function () {
    await addressDetailsPage.createShippingAddressHK(shippingAddressLine1, shippingAddressLine2, shippingDistrict, addBillingCountryCode, '')
  })

  it('Click on Proceed to Save Address button', async function () {
    await addressDetailsPage.clickAddAddressSaveButton()
  })

  it('Verify the error message is displayed under Phone', async function () {
    let expected = 'This field is required'
    let actual = await addressDetailsPage.getPhoneErrorMessage()
    assert.strictEqual(actual, expected, 'Error message is not displayed under Phone')
  })

  it('Enter valid data in all name and address fields and Phone field', async function () {
    await addressDetailsPage.createShippingAddressHK(shippingAddressLine1, shippingAddressLine2, shippingDistrict, addBillingCountryCode, shippingPhone)
  })

  it('Click on Proceed to Save Address button', async function () {
    await addressDetailsPage.clickAddAddressSaveButton()
  })

  it('Loads the Suggested Address popup or Shipping Method page', async function () {
    popupSuggestedDeliveryAddressesForm = new PopupSuggestedDeliveryAddressesForm()
    let popup = await popupSuggestedDeliveryAddressesForm.isPopUpPresent()
    if (popup) {
      await popupSuggestedDeliveryAddressesForm.verifyPageIsLoaded()
      await popupSuggestedDeliveryAddressesForm.clickSubmitAsIsButton()
    }
  })

  it('Verify Global message displays on top', async function () {
    let actual = await addressBookPage.getYourGlobalMessage()
    let expected = 'Address created successfully'
    assert.strictEqual(actual, expected, `"${expected}" Global message is not displayed on top`)
  })

  it('Verify new address added is not displayed as Default address anymore in address Details page', async function () {
    await allureReporter.addDescription('Verify new address added is not displayed as Default address anymore in address Details page')
    let expected = `${firstName} ${lastName}\n${shippingAddressLine1}\n${shippingAddressLine2}\n${shippingDistrict},\n${shippingState}\n${shippingPhone}`
    let actual = await addressBookPage.getNonDefaultShippingAddress()
    assert.strictEqual(actual, expected, 'New address added is displayed as Default address in address Details page')
  })

  it('Click on non default Edit Address button', async function () {
    await addressBookPage.clickEditButtonByIndex(1)
  })

  it('Click on Proceed to Save Address button', async function () {
    await addressDetailsPage.verifyPageIsLoaded()
    await addressDetailsPage.clickAddAddressSaveButton()
  })

  it('Loads the Suggested Address popup or Shipping Method page', async function () {
    popupSuggestedDeliveryAddressesForm = new PopupSuggestedDeliveryAddressesForm(driver)
    let popup = await popupSuggestedDeliveryAddressesForm.isPopUpPresent()
    if (popup) {
      await popupSuggestedDeliveryAddressesForm.verifyPageIsLoaded()
      await popupSuggestedDeliveryAddressesForm.clickSubmitAsIsButton()
    }
  })

  it('Verify Global message displays on top', async function () {
    let actual = await addressBookPage.getYourGlobalMessage()
    let expected = 'Address updated successfully'
    assert.strictEqual(actual, expected, 'Global message is not displayed on top')
  })

  it('Verify  new address added is not changed in address book page', async function () {
    await allureReporter.addDescription('Verify  new address added is not changed in address book page')
    let expected = `${firstName} ${lastName}\n${shippingAddressLine1}\n${shippingAddressLine2}\n${shippingDistrict},\n${shippingState}\n${shippingPhone}`
    let actual = await addressBookPage.getNonDefaultShippingAddress()
    assert.strictEqual(actual, expected, 'New address added is changed in address book page')
  })

  it('Click on Edit Address button', async function () {
    await addressBookPage.clickEditButtonByIndex(1)
  })

  it('Enter New Shipping Address information', async function () {
    await addressDetailsPage.createShippingAddressHK(addBillingAddressLine1, '', addBillingDistrict, addBillingCountryCode, addBillPhone)
  })

  it('Click on Proceed to Save Address button', async function () {
    await addressDetailsPage.clickAddAddressSaveButton()
  })

  it('Loads the Suggested Address popup', async function () {
    popupSuggestedDeliveryAddressesForm = new PopupSuggestedDeliveryAddressesForm(driver)
    await popupSuggestedDeliveryAddressesForm.verifyPageIsLoaded()
  })

  it('Capture the suggested address from pop up ', async function () {
    suggestedAddress = await popupSuggestedDeliveryAddressesForm.getSuggestedDeliveryAddressSection()
    suggestedAddress = suggestedAddress.split('\n')
    addBillingAddressLine1 = suggestedAddress[0]
    addBillingCountryCode = suggestedAddress[3]
  })

  it('Click on Use this Address button', async function () {
    await popupSuggestedDeliveryAddressesForm.clickUseThisAddress()
  })

  it('Loads the Address Book page', async function () {
    screencapture = true
    await addressBookPage.verifyPageIsLoaded()
  })

  it('Verify Global message displays on top', async function () {
    let actual = await addressBookPage.getYourGlobalMessage()
    let expected = 'Address updated successfully'
    assert.strictEqual(actual, expected, `"${expected}" Global message is not displayed`)
  })

  it('Verify Changed address is displayed in the address book under non default section', async function () {
    await allureReporter.addDescription('Verify Changed address is displayed in the address book under non default section')
    let expected = `${firstName} ${lastName}\n${addBillingAddressLine1}\n${addBillingDistrict},\n${shippingState}\n${addBillPhone}`
    let actual = await addressBookPage.getNonDefaultShippingAddress()
    assert.strictEqual(actual, expected, 'Changed address is not displayed in the address book under non default section')
  })

  it('Remove non default shipping address', async function () {
    await addressBookPage.clickRemoveNonDefaultShipAddress()
  })

  it('Verify Remove Shipping Address Pop Up is loaded', async function () {
    await addressBookPage.verifyRemoveAddressPopUpIsLoaded()
  })

  it('Click NO button on remove the Shipping Address Popup', async function () {
    await addressBookPage.clickRemoveShipAddressNoBtn()
  })

  it('Verify new address is not removed in Address Book page', async function () {
    await allureReporter.addDescription('Verify new address is not removed in Address Book page')
    let expected = `${firstName} ${lastName}\n${addBillingAddressLine1}\n${addBillingDistrict},\n${shippingState}\n${addBillPhone}`
    let actual = await addressBookPage.getNonDefaultShippingAddress()
    assert.strictEqual(actual, expected, 'New address is removed in Address Book page')
  })

  it('Remove non default shipping address', async function () {
    await addressBookPage.clickRemoveNonDefaultShipAddress()
  })

  it('Verify Remove Shipping Address Pop Up is loaded', async function () {
    await addressBookPage.verifyRemoveAddressPopUpIsLoaded()
  })

  it('Click Yes button on remove the Shipping Address Popup', async function () {
    await addressBookPage.clickRemoveShipAddress()
  })

  it('Verify Global message displays on top', async function () {
    screencapture = true
    let actual = await addressBookPage.getYourGlobalMessage()
    let expected = 'Address removed successfully'
    assert.strictEqual(actual, expected, `"${expected}" Global message is not displayed`)
  })

  it('Click on end ASM session ', async function () {
    asmHeader.verifyPageIsLoaded()
    await asmHeader.clickASMEndSession()
  })

  it('Verify Login is displayed', async function () {
    screencapture = true
    header.verifyPageIsLoaded()
    await header.verifyLoginLinkIsLoaded
  })

  it('Verify coach info is NOT Displayed', async function () {
    screencapture = true
    let coachInfo

    try {
      coachInfo = await header.getCoachInformation()
    } catch (NoSuchElementException) {
      coachInfo = false
    }
    assert.isFalse(coachInfo, 'Coach info is Displayed')
  })

  afterEach(async function () {
    await driverutils.saveScreenshots(this.currentTest.state, screencapture, driver)
    screencapture = false
  })
})
