// @flow
import { assert } from 'chai'
import driverutils from '@core-libs/DriverUtils'
import allureReporter from '@wdio/allure-reporter'
import CustomerTestData from '@input-data/customers/CustomerTestData'
import ProductTestData, {attb, product} from '@input-data/products/ProductTestData'
import HomePage from '@page-objects/home/HomePage'
import Header from '@page-objects/common/Header'
import MainNavBar from '@page-objects/common/MainNavBar'
import ASMHeader from '@page-objects/common/ASMHeader'
import {AGENT_USER, GLOBAL_PASSWORD} from '@core-libs/ConstantUtil'
import MyAccountPage from '@page-objects/myaccount/MyAccountPage'
import SearchResultsPage from '@page-objects/search/SearchResultsPage'
import AddedToYourShoppingCartPopup from '@page-objects/shop/AddedToYourShoppingCartPopup'
import ShoppingCartPage from '@page-objects/shoppingcart/ShoppingCartPage'
import FinalReviewTile from '@page-objects/checkout/FinalReviewTile'
import ShippingAddressTile from '@page-objects/checkout/ShippingAddressTile'
import PopupSuggestedDeliveryAddressesForm
  from '@page-objects/popupsuggesteddeliveryaddressesform/PopupSuggestedDeliveryAddressesForm'
import ShippingMethodTile from '@page-objects/checkout/ShippingMethodTile'
import PaymentAndBillingAddressTile from '@page-objects/checkout/PaymentAndBillingAddressTile'
import OrderConfirmationPage from '@page-objects/orderconfirmation/OrderConfirmationPage'

describe('C16782 - US ASM OPTAVIA - Add OnDemand Taxable Order in MD', function () {
  let screencapture = false // capture screenshot when test step 'passed' also by default only capture screenshot of failed steps

  let homePage, header, myAccountPage, asmHeader, shippingMethodTile, paymentAndBillingAddressTile
  let TestData, productSKU, popupSuggestedDeliveryAddressesForm, orderConfirmationPage

  // ASM Agent info
  let clientSearch, mainNavBar, searchResultPage, addedToYourShoppingCartPopup, shoppingCartPage, shippingAddressTile, finalReviewTile

  let coachInfoMyAccount

  let testTitle = this.title

  before(async function () {
    await driverutils.goToASMWebSitePage()
  })

  beforeEach(async function () {
    await driverutils.addAllureReport(allureReporter, __dirname, 'US ASM Sanity', testTitle)
  })

  it('Preparing Test Data', async function () {
    TestData = await CustomerTestData.getCustomerInfo('924830263')

    // Client Info
    clientSearch = TestData.CUSTOMER_EMAIL

    // Product Info
    productSKU = await ProductTestData.PRODUCT_INFO(attb.SKU, product.tools_accessories.OPT_BLENDER_BOTTLE)
  })

  it('Loads the Home Page', async function () {
    homePage = new HomePage()
    await homePage.verifyPageIsLoaded()
    header = new Header()
    mainNavBar = new MainNavBar()
    await header.verifyPageIsLoaded()
    asmHeader = new ASMHeader()
    await asmHeader.verifyPageIsLoaded()
    await mainNavBar.verifyPageIsLoaded()
  })

  it('Log in ASM agent ', async function () {
    await asmHeader.loginASMAsCSR(AGENT_USER, GLOBAL_PASSWORD)
  })

  it('Verify ASM login is successful', async function () {
    await asmHeader.verifyASMLoginIsSuccessful()
  })

  it('Verify LogIn link is displaying in header', async function () {
    await asmHeader.verifyLoginLinkIsDisplayed()
  })

  it('Start ASM Session for existing Customer', async function () {
    await asmHeader.startASMSessionForCustomer(clientSearch)
  })

  it('My Account is loaded', async function () {
    myAccountPage = new MyAccountPage()
    myAccountPage.verifyPageIsLoaded()
    await browser.pause(5000)
  })

  it('Get Coach Info from My Account', async function () {
    coachInfoMyAccount = await header.getCoachInformation()
  })

  it('Search product', async function () {
    await allureReporter.addDescription(`Search product SKU = ${productSKU}`)
    await mainNavBar.verifyPageIsLoaded()
    await mainNavBar.searchProduct(productSKU)
    searchResultPage = new SearchResultsPage()
    await searchResultPage.verifyPageIsLoaded()
  })

  it('Add Product', async function () {
    await searchResultPage.clickProductAddToCartButton()
    addedToYourShoppingCartPopup = new AddedToYourShoppingCartPopup()
    await addedToYourShoppingCartPopup.verifyPageIsLoaded()
    await browser.pause(4000)
    await addedToYourShoppingCartPopup.clickCheckoutButton()
  })

  it('Loads the Shopping Cart Page', async function () {
    shoppingCartPage = new ShoppingCartPage()
    await shoppingCartPage.verifyPageIsLoaded()
  })

  it('Verify Join Premier checkbox is NOT checked if it is displayed', async function () {
    screencapture = true
    let actual = await shoppingCartPage.isJoinPremierContainerDisplayed()
    if (actual) {
      if (await shoppingCartPage.isJoinPremierChecked()) {
        await shoppingCartPage.clickToCheckJoinPremierCheckBox()
      }
    } else {
      assert.isFalse(actual, 'Join Premier checkbox is not displayed')
    }
  })

  it('Click on Proceed to Checkout button', async function () {
    await shoppingCartPage.clickProceedToCheckoutBtn()
  })

  it('Get Coach Info from Final Review and compare with Coach info from My Account', async function () {
    let actual = await header.getCoachInformationASM()
    let expected = coachInfoMyAccount
    assert.strictEqual(actual, expected, 'Coach is different')
  })

  it('Loads final review page', async function () {
    await browser.pause(4000)
    finalReviewTile = new FinalReviewTile()
    await finalReviewTile.verifyPageIsLoaded()
  })

  it('Click on edit address Button', async function () {
    await finalReviewTile.clickDeliveryAddressEditBtn()
  })

  it('Get Coach Info from Edit Address and compare with Coach info from My Account', async function () {
    let actual = await header.getCoachInformationASM()
    let expected = coachInfoMyAccount
    assert.strictEqual(actual, expected, 'Coach is different')
  })

  it('Shipping Address section is loaded', async function () {
    shippingAddressTile = new ShippingAddressTile()
    await shippingAddressTile.verifyPageIsLoaded()
  })

  it('Enter Shipping Address information', async function () {
    await shippingAddressTile.addShippingAddress(TestData, true)
  })

  it('Shipping Address: click Next button', async function () {
    if (TestData.SHIPPING_FULL_STATE_NAME === 'California') {
      await shippingAddressTile.clickOkayToProceed()
    } else {
      await shippingAddressTile.clickNext()
    }
  })

  it('Loads the Suggested Address popup and click on use this address button', async function () {
    popupSuggestedDeliveryAddressesForm = new PopupSuggestedDeliveryAddressesForm()
    await browser.pause(5000)
    let Popupisdisplayed = await popupSuggestedDeliveryAddressesForm.isSuggestedDeliveryAddressPopupDisplayed()
    if (Popupisdisplayed) {
      await popupSuggestedDeliveryAddressesForm.clickUseThisAddress()
    }
  })

  it('Loads shipping method page', async function () {
    shippingMethodTile = new ShippingMethodTile()
    await shippingMethodTile.verifyPageIsLoaded()
  })

  it('Get Coach Info from Shipping Method and compare with Coach info from My Account', async function () {
    let actual = await header.getCoachInformationASM()
    let expected = coachInfoMyAccount
    assert.strictEqual(actual, expected, 'Coach is different')
  })

  it('Click on Next button from Shipping Method tile', async function () {
    await shippingMethodTile.clickNext()
  })

  it('Loads the Payment page', async function () {
    paymentAndBillingAddressTile = new PaymentAndBillingAddressTile()
    await paymentAndBillingAddressTile.verifyPageIsLoaded()
  })

  it('Get Coach Info from Payment Page and compare with Coach info from My Account', async function () {
    let actual = await header.getCoachInformationASM()
    let expected = coachInfoMyAccount
    assert.strictEqual(actual, expected, 'Coach is different')
  })

  it('Add existent payment method', async function () {
    await paymentAndBillingAddressTile.clickViewSavedPaymentsButton()
  })

  it('Loads Pop up', async function () {
    await paymentAndBillingAddressTile.selectExistingPaymentFromPopUp()
  })

  it('Loads Final review page', function () {
    finalReviewTile = new FinalReviewTile(driver)
    finalReviewTile.verifyPageIsLoaded()
  })

  it('Get Coach Info from Final Review and compare with Coach info from My Account', async function () {
    let actual = await header.getCoachInformationASM()
    let expected = coachInfoMyAccount
    assert.strictEqual(actual, expected, 'Coach is different')
  })

  it('Verify Tax amount', async function () {
    let actual = await finalReviewTile.getTaxAmount()
    let expected = '$0.45'
    assert.strictEqual(actual, expected, 'Tax charge is not as expected')
  })

  it('Click on Submit Order Button', async function () {
    await finalReviewTile.clickPlaceOrderButton()
  })

  it('Loads the Order Confirmation page', async function () {
    screencapture = true
    orderConfirmationPage = new OrderConfirmationPage()
    await orderConfirmationPage.verifyPageIsLoaded()
  })

  it('Verify order was submitted successfully', async function () {
    let actual = await orderConfirmationPage.getOrderConfirmationBodyText()
    let expected = 'Thank you for your Order!'
    assert.strictEqual(actual, expected, 'Order not submitted successfully')
  })

  it('Verify Coach info is NOT Displayed', async function () {
    let actual = await header.isCoachInformationDisplayed()
    assert.isFalse(actual, 'Coach Information Displayed')
  })

  it('Click on end ASM session ', async function () {
    asmHeader.verifyPageIsLoaded()
    await asmHeader.clickASMEndSession()
  })

  it('Verify Login is displayed', async function () {
    screencapture = true
    header.verifyPageIsLoaded()
    await header.verifyLoginLinkIsLoaded
  })

  it('Verify coach info is NOT Displayed', async function () {
    screencapture = true
    let coachInfo

    try {
      coachInfo = await header.getCoachInformation()
    } catch (NoSuchElementException) {
      coachInfo = false
    }
    assert.isFalse(coachInfo, 'Coach info is Displayed')
  })

  afterEach(async function () {
    await driverutils.saveScreenshots(this.currentTest.state, screencapture)
    screencapture = false
  })
})
