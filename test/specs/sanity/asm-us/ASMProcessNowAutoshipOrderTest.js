// @flow
import { assert } from 'chai'
// import numeral from 'numeral'

import driverutils from '@core-libs/DriverUtils'
import allureReporter from '@wdio/allure-reporter'
import CustomerTestData from '@input-data/customers/CustomerTestData'
import {AGENT_USER, GLOBAL_PASSWORD} from '@core-libs/ConstantUtil'
import HomePage from '@page-objects/home/HomePage'
import Header from '@page-objects/common/Header'
import MainNavBar from '@page-objects/common/MainNavBar'
import ASMHeader from '@page-objects/common/ASMHeader'
import MyAccountPage from '@page-objects/myaccount/MyAccountPage'
import ShoppingCartPage from '@page-objects/shoppingcart/ShoppingCartPage'
import OptaviaPremierOrderPage from '@page-objects/myaccount/OptaviaPremierOrderPage'
import OrderConfirmationPage from '@page-objects/orderconfirmation/OrderConfirmationPage'
import OrderDetailsPage from '@page-objects/myaccount/OrderDetailsPage'
import TestDataBaseMapping from '@db/TestDataBaseMapping'

let screencapture = false // capture screenshot when test step 'passed' also by default only capture screenshot of failed steps
let orderNumber

describe('C16783 - US ASM OPTAVIA - Create Autoship Order Using Same Day Processing', function () {
  let homePage, header, myAccountPage, asmHeader, optaviaPremierOrderPage, orderConfirmationPage, orderDetailsPage

  // ASM Agent info
  let asmAgentEmail, asmAgentPassword, clientSearch, mainNavBar, shoppingCartPage

  let testTitle = this.title

  before(async function () {
    await driverutils.goToASMWebSitePage()
  })

  beforeEach(async function () {
    await driverutils.addAllureReport(allureReporter, __dirname, 'US ASM Sanity', testTitle)
  })

  it('Preparing Test Data', async function () {
    let custID = '120000631443'
    let TestData = await CustomerTestData.getCustomerInfo(custID)
    // ASM Agent
    asmAgentEmail = AGENT_USER
    asmAgentPassword = GLOBAL_PASSWORD
    // Client Info
    clientSearch = TestData.CUSTOMER_EMAIL
  })

  it('Loads the Home Page', async function () {
    homePage = new HomePage()
    await homePage.verifyPageIsLoaded()
    header = new Header()
    mainNavBar = new MainNavBar()
    await header.verifyPageIsLoaded()
    asmHeader = new ASMHeader()
    await asmHeader.verifyPageIsLoaded()
    await mainNavBar.verifyPageIsLoaded()
  })

  it('Log in ASM agent ', async function () {
    await asmHeader.loginASMAsCSR(asmAgentEmail, asmAgentPassword)
  })

  it('Verify ASM login is successful', async function () {
    await asmHeader.verifyASMLoginIsSuccessful()
  })

  it('Verify LogIn link is displaying in header', async function () {
    await asmHeader.verifyLoginLinkIsDisplayed()
  })

  it('Start ASM Session for existing Customer', async function () {
    await asmHeader.startASMSessionForCustomer(clientSearch)
  })

  it('Click on My Account', async function () {
    header.verifyPageIsLoaded()
    await header.clickMyAccount()
  })

  it('My Account is loaded', async function () {
    myAccountPage = new MyAccountPage()
    myAccountPage.verifyPageIsLoaded()
    await browser.pause(5000)
  })

  it('Click on edit order link', async function () {
    await myAccountPage.clickEditOrder()
  })

  it('Loads the Shopping Cart page', async function () {
    shoppingCartPage = new ShoppingCartPage()
    await shoppingCartPage.verifyPageIsLoaded()
  })

  it('Update item Qty = +1 ', async function () {
    await browser.pause(5000)
    let actualQty = await shoppingCartPage.getProductQty()
    actualQty = parseInt(actualQty)
    await shoppingCartPage.updateProductQTY(actualQty + 1)
    await shoppingCartPage.clickUpdateProductLink()
    shoppingCartPage = new ShoppingCartPage()
    await shoppingCartPage.verifyPageIsLoaded()
  })

  it('Click on Save order and Loads the Subscription page ', async function () {
    await shoppingCartPage.clickSaveOrderLink()
    optaviaPremierOrderPage = new OptaviaPremierOrderPage()
    await optaviaPremierOrderPage.verifyPageIsLoaded()
  })

  it('Click on Process Now button', async function () {
    await optaviaPremierOrderPage.clickProcessNowBtn()
  })

  it('Verify Process Now popup displays', async function () {
    screencapture = true
    await optaviaPremierOrderPage.verifyProcessNowPopUpIsLoaded()
  })

  it('Click Process Now button from Process Now popup', async function () {
    screencapture = true
    await optaviaPremierOrderPage.clickProcessNowPopUpProcessNowBtn()
  })

  it('Loads the Order Confirmation page', async function () {
    screencapture = true
    orderConfirmationPage = new OrderConfirmationPage()
    await orderConfirmationPage.verifyPageIsLoaded()
    let actual = await orderConfirmationPage.getOrderNumber()
    let resultArray = actual.split(' ')
    orderNumber = resultArray[4]
    await allureReporter.addDescription(`Order Number : ${orderNumber}`)
  })

  it('Click on Continue Shopping button on Order Confirmation page', async function () {
    await orderConfirmationPage.clickContinueShopping()
  })

  it('Click on My Account link from header', async function () {
    await header.clickMyAccount()
  })

  it('My Account Dashboard page is loaded', async function () {
    await myAccountPage.verifyPageIsLoaded()
  })

  it('Verify recently placed order is displayed in order history', async function () {
    screencapture = true
    let actual = await myAccountPage.getLastOrderNumber()
    let expected = orderNumber
    await allureReporter.addDescription(`Expected last order is ${expected}`)
    assert.strictEqual(actual, expected, `Recently placed order No : ${expected} is not displayed in order history`)
  })

  it('Click on Last Order Number link', async function () {
    await myAccountPage.clickLastOrderNumberLink()
  })

  it('Loads the Order Details page', async function () {
    orderDetailsPage = new OrderDetailsPage()
    await orderDetailsPage.verifyPageIsLoaded()
  })

  it('Verify order details page displays for the order created above', async function () {
    let expected = `Order # ${orderNumber}`
    let actual = await orderDetailsPage.getOrderNumber()
    assert.strictEqual(actual, expected, `Order No : ${expected} details is not displayed in order detail page`)
  })

  it('Verify the order type is Immediate Autoship', async function () {
    screencapture = true
    let expected = 'Type: Immediate_Autoship'
    let actual = await orderDetailsPage.getOrderType()
    assert.strictEqual(actual.trim(), expected, 'Order type is not Immediate Autoship')
  })

  it('Click on Log out Link from header', async function () {
    await header.clickLogOut()
  })

  afterEach(async function () {
    await driverutils.saveScreenshots(this.currentTest.state, screencapture, driver)
    screencapture = false
  })
})
/* Commented since Fraud check is not working
describe('Login to Hybris to Approve order', function () {
  let testTitle = this.title

before(async function () {
    driverBuilder = new DriverBuilder()
    driver = driverBuilder.driver
    await driverutils.goToHybrisBackofficeSitePage(driver)
  })

  beforeEach(async function () {
  await driverutils.addAllureReport(__dirname, 'US ASM Sanity', testTitle)
  })

  it('Preparing Test Data', async function () {
    agentEmail = AGENT_USER
    agentPassword = GLOBAL_PASSWORD
  })

  it('Hybris Backoffice page is loaded ', async function () {
    hybrisBackofficeLoginPage = new HybrisBackofficeLoginPage(driver)
    await hybrisBackofficeLoginPage.verifyPageIsLoaded()
  })

  it('Login to Hybris', async function () {
    await hybrisBackofficeLoginPage.loginAsCSR(agentEmail, agentPassword)
  })

  it('Backoffice Authority group page displays ', async function () {
    hybrisBackofficeAutorityGroupPage = new HybrisBackofficeAutorityGroupPage(driver)
    await hybrisBackofficeAutorityGroupPage.verifyPageIsLoaded()
  })

  it('Process as CSM Manager', async function () {
    await hybrisBackofficeAutorityGroupPage.proceedAsCsmRole()
    hybrisBackofficeCSMHomePage = new HybrisBackofficeCSMHomePage(driver)
    await hybrisBackofficeCSMHomePage.verifyCSIsLoaded()
  })

  it('Click on Orders ', async function () {
    await hybrisBackofficeCSMHomePage.clickOrder()
  })

  it('Loads Search page', async function () {
    hybrisBackOfficeSearchPage = new HybrisBackofficeSearchPage(driver)
    await hybrisBackOfficeSearchPage.verifyPageIsLoaded()
  })

  it('Search for Existing Order ', async function () {
    await browser.pause(10000)
    hybrisBackofficeCustomerPage = new HybrisBackofficeCustomerPage(driver)
    await hybrisBackofficeCustomerPage.searchOrderIDOnCSR(orderNumber)
  })

  it('Click on first registry ', async function () {
    await browser.pause(10000)
    await hybrisBackofficeCustomerPage.clickOnFirstRegistry()
  })

  it('Loads Order Page', async function () {
    await browser.pause(10000)
    hybrisBackOfficeOrderDetailsPage = new HybrisBackofficeOrderDetailsPage(driver)
    await hybrisBackOfficeOrderDetailsPage.verifyPageIsLoaded(orderNumber)
  })

  it('Clicks on fraud report', async function () {
    await hybrisBackOfficeOrderDetailsPage.clickFraudReportTab()
  })

  it('Click on Thumbs Up icon to approve the order', async function () {
    await hybrisBackOfficeOrderDetailsPage.clickFraudActionButton()
  })

  afterEach(async function () {
    await allure.addArgument('Page URL', await driver.getCurrentUrl())
    await allure.addArgument('Page Title', await driver.getTitle())

    await driverutils.saveScreenshots(this.currentTest.state, screencapture, driver)

    screencapture = false
  })

  after(async function () {
    await driverBuilder.quit()
  })
})

 */
describe('Look up syncSQL database for Order number in dbo.Orders table', function () {
  // Temporal step, remove it when DB is syncing faster
  it('Wait for 5 minutes for DB to sync', async function () {
    await browser.pause(300000)
  })

  it(`Verify order number in DB`, async function () {
    let expected = orderNumber
    let actual = TestDataBaseMapping.getOrderID(orderNumber)
    assert.strictEqual(actual, expected, `Order No in DB not matching with expected :${expected}`)
  })
})
