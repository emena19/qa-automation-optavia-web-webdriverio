// @flow
import { assert } from 'chai'
import driverutils from '@core-libs/DriverUtils'
import TestCreditCards from '@input-data/creditcards/CreditCardTestData'
import {AGENT_USER, GLOBAL_PASSWORD} from '@core-libs/ConstantUtil'
import CustomerTestData from '@input-data/customers/CustomerTestData'
import HomePage from '@page-objects/home/HomePage'
import Header from '@page-objects/common/Header'
import MainNavBar from '@page-objects/common/MainNavBar'
import ASMHeader from '@page-objects/common/ASMHeader'
import MyAccountPage from '@page-objects/myaccount/MyAccountPage'
import MyAccountLeftNavigation from '@page-objects/myaccount/MyAccountLeftNavigation'
import PaymentDetailsPage from '@page-objects/myaccount/PaymentDetailsPage'
import PaymentAndBillingAddressTile from '@page-objects/checkout/PaymentAndBillingAddressTile'
import allureReporter from '@wdio/allure-reporter'

describe('C16955 - SG ASM OPTAVIA -My Account - Payment Details - Existing Customer adds new Payment Method', function () {
  declare var allure: any;

  let screencapture = false // capture screenshot when test step 'passed' also by default only capture screenshot of failed steps

  let homePage, header, myAccountPage, asmHeader, paymentDetailsPage, firstName, lastName, currentDefaultCard
  let myAccountLeftNavigation
  let nameOnCard, cardNumber, cvv, month, year

  // ASM Agent info
  let asmAgentEmail, asmAgentPassword, customerEmail, mainNavBar, paymentAndBillingAddressTile
  let testTitle = this.title

  before(async function () {
    await driverutils.goToOptaviaAsmWebSiteHomePageSG()
  })

  beforeEach(async function () {
    await driverutils.addAllureReport(allureReporter, __dirname, 'SG ASM Sanity', testTitle)
  })

  it('Preparing Test Data', async function () {
    // ASM Agent
    asmAgentEmail = AGENT_USER
    asmAgentPassword = GLOBAL_PASSWORD
    // Client Info
    let custID = '6520000532281'
    let TestData = await CustomerTestData.getCustomerInfo(custID)
    firstName = TestData.CUSTOMER_FIRST_NAME
    lastName = TestData.CUSTOMER_LAST_NAME
    customerEmail = TestData.CUSTOMER_EMAIL

    // CC Data
    let ccCol = 'B'
    let ccsheet = 2
    nameOnCard = firstName + ' ' + lastName
    cardNumber = await TestCreditCards.CARD_NUMBER(ccsheet, ccCol)
    cvv = await TestCreditCards.CVV(ccsheet, ccCol)
    month = await TestCreditCards.EXPIRATION_MONTH(ccsheet, ccCol)
    year = await TestCreditCards.EXPIRATION_YEAR(ccsheet, ccCol)
  })

  it('Loads the Home Page', async function () {
    homePage = new HomePage()
    await homePage.verifyPageIsLoaded()
    header = new Header()
    mainNavBar = new MainNavBar()
    await header.verifyPageIsLoaded()
    asmHeader = new ASMHeader()
    await asmHeader.verifyPageIsLoaded()
    await mainNavBar.verifyPageIsLoaded()
  })

  it('Log in ASM agent ', async function () {
    await asmHeader.loginASMAsCSR(asmAgentEmail, asmAgentPassword)
  })

  it('Verify ASM login is successful', async function () {
    await asmHeader.verifyASMLoginIsSuccessful()
  })

  it('Verify LogIn link is displaying in header', async function () {
    await asmHeader.verifyLoginLinkIsDisplayed()
  })

  it('Start ASM Session for existing Customer', async function () {
    await asmHeader.startASMSessionForCustomer(customerEmail)
  })

  it('My Account is loaded', async function () {
    myAccountPage = new MyAccountPage()
    myAccountPage.verifyPageIsLoaded()
    myAccountLeftNavigation = new MyAccountLeftNavigation()
    await myAccountLeftNavigation.verifyPageIsLoaded()
    await browser.pause(5000)
  })

  it('Click on payment details', async function () {
    await myAccountLeftNavigation.clickPaymentDetails()
  })

  it('Loads Payment Details Page', function () {
    paymentDetailsPage = new PaymentDetailsPage()
    paymentDetailsPage.verifyPageIsLoaded()
  })

  it('Get current default card', async function () {
    currentDefaultCard = await paymentDetailsPage.getDefaultAddress()
  })
  it('Click on add a new card', async function () {
    await paymentDetailsPage.clickAddNewCardBTN()
  })
  it('Loads the Payment Details page', async function () {
    paymentAndBillingAddressTile = new PaymentAndBillingAddressTile()
  })

  it('Enter Payment Details information', async function () {
    await paymentAndBillingAddressTile.createPaymentDetailsSG(nameOnCard, cardNumber, month + year, cvv)
  })

  it('Click on Cancel Button', async function () {
    await paymentDetailsPage.verifyCancelButtonIsDisplayed()
    await paymentDetailsPage.clickCancelButton()
  })

  it('Loads Payment Details Page', async function () {
    await paymentDetailsPage.verifyPageIsLoaded()
  })

  it('Verify  Default address in Payment Details page is not changed', async function () {
    await allureReporter.addDescription('Default address in Payment Details page')
    let expected = currentDefaultCard
    let actual = await paymentDetailsPage.getDefaultAddress()
    assert.strictEqual(actual, expected, 'Default address in Payment Details page is changed')
  })

  it('Click on add a new card', async function () {
    await paymentDetailsPage.clickAddNewCardBTN()
  })
  it('Enter Payment Details information', async function () {
    await paymentAndBillingAddressTile.createPaymentDetailsSG(nameOnCard, cardNumber, month + year, cvv)
  })
  it('Click Save Payment Info button', async function () {
    await browser.pause(3000)
    await paymentDetailsPage.clickSavePaymentInfoButtonSG()
  })

  it('Verify Confirmation Message', async function () {
    let actual = await paymentDetailsPage.getYourGlobalMessage()
    let expected = 'Payment Card added successfully'
    assert.equal(actual, expected, `"${expected}" Confirmation message is not displayed`)
  })

  it('Click on end ASM session ', async function () {
    asmHeader.verifyPageIsLoaded()
    await asmHeader.clickASMEndSession()
  })

  it('Verify Login is displayed', async function () {
    screencapture = true
    header.verifyPageIsLoaded()
    await header.verifyLoginLinkIsLoaded
  })

  it('Verify coach info is NOT Displayed', async function () {
    screencapture = true
    let coachInfo

    try {
      coachInfo = await header.getCoachInformation()
    } catch (NoSuchElementException) {
      coachInfo = false
    }
    assert.isFalse(coachInfo, 'Coach Info is Displayed')
  })

  afterEach(async function () {
    await driverutils.saveScreenshots(this.currentTest.state, screencapture)
    screencapture = false
  })
})
