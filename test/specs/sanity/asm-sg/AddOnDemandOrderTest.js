// @flow
import { assert } from 'chai'
import driverutils from '@core-libs/DriverUtils'
import CustomerTestData from '@input-data/customers/CustomerTestData'
import ProductTestData, {attb, market, product} from '@input-data/products/ProductTestData'
import CoachTestData from '@input-data/coaches/CoachTestData'
import HomePage from '@page-objects/home/HomePage'
import Header from '@page-objects/common/Header'
import MainNavBar from '@page-objects/common/MainNavBar'
import ASMHeader from '@page-objects/common/ASMHeader'
import {AGENT_USER, GLOBAL_PASSWORD} from '@core-libs/ConstantUtil'
import MyAccountPage from '@page-objects/myaccount/MyAccountPage'
import ShopAllPage from '@page-objects/shop/ShopAllPage'
import ShopToolsAndAccessoriesPage from '@page-objects/shop/ShopToolsAndAccessoriesPage'
import AddedToYourShoppingCartPopup from '@page-objects/shop/AddedToYourShoppingCartPopup'
import ShoppingCartPage from '@page-objects/shoppingcart/ShoppingCartPage'
import CheckoutCommonSection from '@page-objects/checkout/CheckoutCommonSection'
import PaymentAndBillingAddressTile from '@page-objects/checkout/PaymentAndBillingAddressTile'
import FinalReviewTile from '@page-objects/checkout/FinalReviewTile'
import ShippingAddressTile from '@page-objects/checkout/ShippingAddressTile'
import PopupSuggestedDeliveryAddressesForm
  from '@page-objects/popupsuggesteddeliveryaddressesform/PopupSuggestedDeliveryAddressesForm'
import OrderConfirmationPage from '@page-objects/orderconfirmation/OrderConfirmationPage'
import TestCreditCards from '@input-data/creditcards/CreditCardTestData'
import allureReporter from '@wdio/allure-reporter'

describe('C16952 - SG ASM OPTAVIA - Add OnDemand Order', function () {
  let screencapture = false // capture screenshot when test step 'passed' also by default only capture screenshot of failed steps

  let homePage, header, myAccountPage, asmHeader, shopAllPage, shopToolsAndAccessoriesPage, paymentAndBillingAddressTile, finalReviewPage
  let TestData, productName, cvv, popupSuggestedDeliveryAddressesForm, checkoutCommonSection

  // ASM Agent info
  let mainNavBar, orderConfirmationPage, addedToYourShoppingCartPopup, shoppingCartPage, shippingAddressTile

  // Coach (Enroller) Info
  let coachFirstName, coachLastName, coachID
  let testTitle = this.title

  before(async function () {
    await driverutils.goToOptaviaAsmWebSiteHomePageSG()
  })

  beforeEach(async function () {
    await driverutils.addAllureReport(allureReporter, __dirname, 'SG ASM Sanity', testTitle)
  })

  it('Preparing Test Data', async function () {
    // Client Info
    TestData = await CustomerTestData.getCustomerInfo('6520000406149')

    // Product Info
    productName = await ProductTestData.PRODUCT_INFO(attb.NAME, product.tools_accessories.OPT_HABITS_HEALTH_SYSTEM, market.SG)

    // Coach Information
    coachFirstName = await CoachTestData.COACH_FIRST_NAME(6, 2)
    coachLastName = await CoachTestData.COACH_LAST_NAME(6, 2)
    coachID = await CoachTestData.COACH_ID(4, 2)

    // Payment Info
    let ccCol = 'B'
    cvv = await TestCreditCards.CVV(2, ccCol)
  })

  it('Loads the Home Page', async function () {
    homePage = new HomePage()
    await homePage.verifyPageIsLoaded()
    header = new Header()
    mainNavBar = new MainNavBar()
    await header.verifyPageIsLoaded()
    asmHeader = new ASMHeader()
    await asmHeader.verifyPageIsLoaded()
    await mainNavBar.verifyPageIsLoaded()
  })

  it('Log in ASM agent ', async function () {
    await asmHeader.loginASMAsCSR(AGENT_USER, GLOBAL_PASSWORD)
  })

  it('Verify ASM login is successful', async function () {
    await asmHeader.verifyASMLoginIsSuccessful()
  })

  it('Verify LogIn link is displaying in header', async function () {
    await asmHeader.verifyLoginLinkIsDisplayed()
  })

  it('Start ASM Session for existing Customer', async function () {
    await asmHeader.startASMSessionForCustomer(TestData.CUSTOMER_EMAIL)
  })

  it('My Account is loaded', async function () {
    myAccountPage = new MyAccountPage(driver)
    myAccountPage.verifyPageIsLoaded()
    await browser.pause(5000)
  })

  it('Verify Coach info displayed on top header', async function () {
    let actual = await header.getCoachInformation()
    let expected = `${coachFirstName} ${coachLastName}, ${coachID}`
    assert.strictEqual(actual, expected, `Coach Info : ${expected} is not displayed on top header`)
  })

  it('Loads the Shop All Page', async function () {
    await mainNavBar.clickShopAll()
  })

  it('Loads the Shop All Page', async function () {
    await mainNavBar.clickShopAll()
    shopAllPage = new ShopAllPage()
    await shopAllPage.verifyPageIsLoaded()
  })

  it('Clicks Shop Button under Tools and Accessories', async function () {
    await shopAllPage.clickToolsAndAccessoriesShopBtn()
  })

  it('Search and add searched product', async function () {
    await allureReporter.addDescription(`Search product SKU = ${productName}`)
    shopToolsAndAccessoriesPage = new ShopToolsAndAccessoriesPage()
    await shopToolsAndAccessoriesPage.verifyPageIsLoaded()
    await shopToolsAndAccessoriesPage.clickOnHabitsForHealth()
  })

  it('Loads the Added To Your Shopping Cart Popup', async function () {
    addedToYourShoppingCartPopup = new AddedToYourShoppingCartPopup()
    await addedToYourShoppingCartPopup.verifyPageIsLoaded()
  })

  it('Click on Checkout button on pop up window', async function () {
    await addedToYourShoppingCartPopup.clickCheckoutButton()
  })

  it('Loads the Shopping Cart Page', async function () {
    shoppingCartPage = new ShoppingCartPage()
    await shoppingCartPage.verifyPageIsLoaded()
  })

  it('Click on Proceed to Checkout button', async function () {
    await shoppingCartPage.clickProceedToCheckoutBtn()
  })

  it('Loads the CheckoutCommonSection page', async function () {
    checkoutCommonSection = new CheckoutCommonSection()
    await checkoutCommonSection.verifyPageIsLoaded()
  })

  it('Loads the Payment page', async function () {
    paymentAndBillingAddressTile = new PaymentAndBillingAddressTile()
    await paymentAndBillingAddressTile.verifyPageIsLoadedSG()
  })

  it('Verify Coach info displayed on top header', async function () {
    let actual = await checkoutCommonSection.getCoachInfoFromAvatar()
    let expected = `${coachFirstName} ${coachLastName}, ${coachID}`
    assert.strictEqual(actual, expected, `Coach Info : ${expected} is not displayed on top header`)
  })

  it('Enter Existing Payment Details information', async function () {
    await paymentAndBillingAddressTile.clickOnExistingCardDetailsSG(cvv)
  })

  it('Click on Next button from Payment tile', async function () {
    await paymentAndBillingAddressTile.clickNextHK()
  })

  it('Loads the Final Review page', async function () {
    finalReviewPage = new FinalReviewTile()
    await finalReviewPage.verifyPageIsLoaded()
  })

  it('Verify Coach info displayed on top header', async function () {
    let actual = await checkoutCommonSection.getCoachInfoFromAvatar()
    let expected = `${coachFirstName} ${coachLastName}, ${coachID}`
    assert.strictEqual(actual, expected, `Coach Info : ${expected} is not displayed on top header`)
  })

  it('Click on Edit Button under Shipping Delivery Address', async function () {
    await finalReviewPage.clickDeliveryAddressEditBtn()
  })

  it('Loads the Shipping Address page', async function () {
    shippingAddressTile = new ShippingAddressTile()
    await shippingAddressTile.verifyPageIsLoaded()
  })

  it('Clears the text in Delivery Address', async function () {
    let blankAddress = {
      SHIPPING_ADDRESS_LINE_1: '',
      SHIPPING_ZIP: '',
      SHIPPING_PHONE: '',
      SHIPPING_COUNTRY_CODE: ''
    }
    await shippingAddressTile.addShippingAddressSG(blankAddress)
  })

  it('Updating Delivery Address information', async function () {
    await shippingAddressTile.verifyPageIsLoaded()
    await shippingAddressTile.addShippingAddressSG(TestData)
  })

  it('Shipping Address: click Next button', async function () {
    await shippingAddressTile.clickNext()
  })

  it('Loads the Suggested Address popup', async function () {
    popupSuggestedDeliveryAddressesForm = new PopupSuggestedDeliveryAddressesForm()
    await popupSuggestedDeliveryAddressesForm.verifyPageIsLoaded()
  })

  it('Click on Use this address button', async function () {
    await shippingAddressTile.verifyPageIsLoaded()
    await popupSuggestedDeliveryAddressesForm.clickUseThisAddress()
  })

  it('Loads the Payment page', async function () {
    await paymentAndBillingAddressTile.verifyPageIsLoadedSG()
  })

  it('Verify Coach info displayed on top header', async function () {
    let actual = await checkoutCommonSection.getCoachInfoFromAvatar()
    let expected = `${coachFirstName} ${coachLastName}, ${coachID}`
    assert.strictEqual(actual, expected, `Coach Info : ${expected} is not displayed on top header`)
  })

  it('Enter Existing Payment Details information', async function () {
    await paymentAndBillingAddressTile.clickOnExistingCardDetailsSG(cvv)
  })

  it('Click on Next button from Payment tile', async function () {
    await paymentAndBillingAddressTile.clickNextSG()
  })

  it('Verify Coach info displayed on top header', async function () {
    let actual = await checkoutCommonSection.getCoachInfoFromAvatar()
    let expected = `${coachFirstName} ${coachLastName}, ${coachID}`
    assert.strictEqual(actual, expected, `Coach Info : ${expected} is not displayed on top header`)
  })

  it('Click on Submit Order button from Final Review tile', async function () {
    await finalReviewPage.clickPlaceOrderButton()
  })

  it('Loads the Order Confirmation page', async function () {
    screencapture = true
    orderConfirmationPage = new OrderConfirmationPage()
    await orderConfirmationPage.verifyPageIsLoaded()
  })

  it('Loads the Order Confirmation page', async function () {
    screencapture = true
    orderConfirmationPage = new OrderConfirmationPage(driver)
    await orderConfirmationPage.verifyPageIsLoaded()
  })

  it('Verify the Coach information is not displayed in Order Summary Section', async function () {
    screencapture = true
    assert.isFalse(await orderConfirmationPage.isCoachInfoFromAvatarDisplayed(), 'Coach information is displayed')
  })

  it('Verify order was submitted successfully', async function () {
    let actual = await orderConfirmationPage.getOrderConfirmationBodyText()
    let expected = 'Thank you for your Order!'
    assert.strictEqual(actual, expected, 'Order not submitted successfully')
  })

  it('Click on end ASM session ', async function () {
    asmHeader.verifyPageIsLoaded()
    await asmHeader.clickASMEndSession()
  })

  it('Verify Login is displayed', async function () {
    screencapture = true
    header.verifyPageIsLoaded()
    await header.verifyLoginLinkIsLoaded
  })

  it('Verify coach info is NOT Displayed', async function () {
    screencapture = true
    let coachInfo

    try {
      coachInfo = await header.getCoachInformation()
    } catch (NoSuchElementException) {
      coachInfo = false
    }
    assert.isFalse(coachInfo, 'Coach info is Displayed')
  })

  afterEach(async function () {
    await driverutils.saveScreenshots(this.currentTest.state, screencapture, driver)
    screencapture = false
  })
})
