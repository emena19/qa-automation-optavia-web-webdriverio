// @flow
declare class ExceljsWorkbook {
  constructor(): ExceljsWorkbook;
  static getWorksheet(value: any): ExceljsWorkbook | Function;
  static getCell(value: any): ExceljsWorkbook | Function;
  static xlxs(value: any): ExceljsWorkbook | Function;
  static readFile(value: any): ExceljsWorkbook | Function;
}

declare module 'exceljs' {
  // libs

  // classes
  declare var Workbook: typeof ExceljsWorkbook;

  // props
}
