// @flow
// let sql = require('mssql')
import sql from 'mssql'
import EnvConfig from '@input-data/EnvConfig'

export default {
  async sqlExecuteProcedure (sqlQuery: string): Promise<any> {
    try {
      await sql.connect(await EnvConfig.getDBConnection())
      let recordset = await sql.execute(sqlQuery)
      sql.close()
      return recordset
    } catch (err) {
      sql.close()
      throw new Error(err.message)
    }
  },

  async sqlExecute (sqlQuery: string, sqlParams: any = []): Promise<any> {
    try {
      let pool = await sql.connect(await EnvConfig.getDBConnection())
      let req = await pool.request()
      for (const param of sqlParams) {
        await req.input(param.name, param.type, param.value)
      }
      let result = await req.query(sqlQuery)
      sql.close()
      return result
    } catch (err) {
      sql.close()
      throw new Error(err.message)
    }
  }
}
