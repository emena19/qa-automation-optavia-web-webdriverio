// @flow
import fs from 'fs'
import path from 'path'
let fileList = []

export default {
  async readFile (filePath: string, fileEncoding: string = 'utf8'): Promise<any> {
    try {
      let textContent = await fs.readFileSync(filePath, fileEncoding)
      return textContent
    } catch (err) {
      throw new Error(err.toString())
    }
  },

  async getAllFilesFromFolder (folderName: string) {
    const currentFolderPath = path.join('./', folderName)
    try {
      let currentFolder = await fs.readdirSync(currentFolderPath, 'utf8')
      await currentFolder.forEach(file => {
        let currentItemPath = path.join('./', folderName, '/', file)
        if (fs.statSync(currentItemPath).isFile()) {
          fileList.push(currentItemPath)
        } else {
          let folderPath = path.join(folderName, '/', file)
          this.getAllFilesFromFolder(folderPath)
        }
      })
      return fileList
    } catch (err) {
      throw new Error(err.toString())
    }
  }
}
