// @flow
const credentials = require('/src/testdata/config/GoogleSheetAuth.json')
const { GoogleSpreadsheet } = require('google-spreadsheet')

// spreadsheet key is the long id in the sheets URL
const doc = new GoogleSpreadsheet('1JQAqzyS9Zh0aTtIW6LnW1VRTObl1cSgl7rlRNO8SND0')

export default {
  async getGoogleSheetConnection () {
    // Connects Google Sheets by using a Service Account previously created
    // The credentials are retrieved from a generated json file (GloogleSheetAuth.json)
    await doc.useServiceAccountAuth(credentials)
  },

  async loadGoogleSheetInfo () {
    // Loads document properties and worksheets
    await doc.loadInfo()
  },

  async getGoogleSheetCellData (sheetID: number, cellID: string, retries: number = 1) {
    try {
      await this.getGoogleSheetConnection()
      await this.loadGoogleSheetInfo()
      // Gets sheet by index, this is the order they appear in the Google sheets UI
      const sheet = doc.sheetsByIndex[sheetID]
      // Will load ALL cells in the sheet
      await sheet.loadCells()
      const text = sheet.getCellByA1(cellID)
      return text.value
    } catch (err) {
      if (retries === 0) {
        throw new Error(`Cell '${cellID}' value was not found`)
      }
      return this.getGoogleSheetCellData(sheetID, cellID, retries - 1)
    }
  },

  async getGoogleSheetRowData (sheetID: number, column: string, valueToSearch: string, retries: number = 1) {
    try {
      let rowData
      await this.getGoogleSheetConnection()
      await this.loadGoogleSheetInfo()
      const sheet = doc.sheetsByIndex[sheetID]
      let rows = await sheet.getRows()
      rows.forEach(row => {
        if (row[column] === valueToSearch) {
          rowData = row
        }
      })
      return rowData
    } catch (err) {
      if (retries === 0) {
        throw new Error(err.message)
      }
    }
  },

  async getGoogleSheetMultipleRows (sheetID: number, column: string, valuesToSearch: [], retries: number = 1) {
    try {
      let rowData = []
      await this.getGoogleSheetConnection()
      await this.loadGoogleSheetInfo()
      const sheet = doc.sheetsByIndex[sheetID]
      let rows = await sheet.getRows()
      valuesToSearch.forEach(value => {
        rows.forEach(row => {
          if (row[column] === value) {
            rowData.push(row)
          }
        })
      })
      return rowData
    } catch (err) {
      if (retries === 0) {
        throw new Error(err.message)
      }
    }
  }
}
