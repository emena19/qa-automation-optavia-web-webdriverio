// @flow

// LOCAL SERVER for local development
const localServer = 'localhost'

// REMOTE SERVER for Jenkins use
const remoteServer = 'http://selenium.subprod-optavia.com'

// BROWSERSTACK SERVER
// const browserStackServer = 'http://hub-cloud.browserstack.com/wd/hub'

export default class DriverBuilder {
  static getSeleniumServer () {
    return {
      hostname: process.env.SELENIUM_SERVER === 'LOCAL' ? localServer : remoteServer,
      port: 4444,
      path: '/'
    }
  }

  static getCapabilities () {
    let localCapabilities = {
      browserName: 'chrome',
      acceptInsecureCerts: true,
      'goog:chromeOptions': {
        args: ['--ignore-certificate-errors', '--window-size=1920,1080']
      }
    }

    let remoteCapabilities = {
      browserName: 'chrome',
      acceptInsecureCerts: true,
      'goog:chromeOptions': {
        'w3c': false,
        args: ['--ignore-certificate-errors', '--headless', '--disable-gpu', '--window-size=1440,1080']
      }
    }
    return (process.env.SELENIUM_SERVER === 'LOCAL') ? localCapabilities : remoteCapabilities
  }
}
