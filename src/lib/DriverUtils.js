// @flow
import EnvConfig from '../testdata/inputdata/EnvConfig'
import CommonUtils from '@core-libs/CommonUtils'
import allureReporter from '@wdio/allure-reporter'

declare var allure: any
const usUrl = '/us/en'
const sgUrl = '/sg/en'
const hkUrl = '/hk/en'
const joinUsUrl = '/become-a-coach'
const asm = '?asm=true'
const login = '/sign-in'
const createAccount = '/create-account'
class DriverUtils {
  async baseUrl () {
    return EnvConfig.BASE_URL()
  }

  async baseUrlUS () {
    return await this.baseUrl() + usUrl
  }

  async baseUrlSG () {
    return await this.baseUrl() + sgUrl
  }

  async baseUrlHK () {
    return await this.baseUrl() + hkUrl
  }

  async goToOptaviaWebSiteHomePage (): Promise<void> {
    return browser.url(await this.baseUrl() + usUrl)
  }

  async goToOptaviaWebSiteHomePageSG (): Promise<void> {
    return browser.url(await this.baseUrl() + sgUrl)
  }

  async goToOptaviaWebSiteHomePageHK (): Promise<void> {
    return browser.url(await this.baseUrl() + hkUrl)
  }

  async goToReplicatedWebSiteHomePage (): Promise<void> {
    return browser.url(EnvConfig.REPLICATED_SITE())
  }

  async goToASMWebSitePage (): Promise<void> {
    return browser.url(await this.baseUrl() + usUrl + asm)
  }

  async goToHybrisBackofficeSitePage (): Promise<void> {
    return browser.url(await EnvConfig.HYBRIS_BACKOFFICE_URL())
  }

  async goToOptaviaConnectWebSitePage (): Promise<void> {
    return browser.url(EnvConfig.OPTAVIA_CONNECT_URL())
  }

  async goToJoinUsPage (): Promise<void> {
    return browser.url(await this.baseUrl() + joinUsUrl)
  }

  async goToLoginPage (market: number = 1): Promise<void> {
    switch (market) {
      case 1: return browser.url(await this.baseUrl() + usUrl + login)
      case 2: return browser.url(await this.baseUrl() + sgUrl + login)
      case 3: return browser.url(await this.baseUrl() + hkUrl + login)
    }
  }

  async goToWebSite (url: string): Promise<void> {
    return browser.url(url)
  }

  async browserName () {
    return EnvConfig.BROWSER()
  }

  async goToOptaviaAsmWebSiteHomePageHK (): Promise<void> {
    return browser.url(await this.baseUrl() + hkUrl + asm)
  }

  async goToOptaviaAsmWebSiteHomePageSG (): Promise<void> {
    return browser.url(await this.baseUrl() + sgUrl + asm)
  }

  async goToMedDirectSite (): Promise<void> {
    return browser.url(EnvConfig.MED_DIRECT_SITE_URL())
  }

  async goToMedDirectASMSite (): Promise<void> {
    return browser.url(await EnvConfig.MED_DIRECT_SITE_URL() + asm)
  }

  async takeFullPageScreenshots (state: string) {
    let totalHeight = await browser.execute('return document.body.offsetHeight')
    let windowHeight = await browser.execute('return window.outerHeight')

    for (let i = 0; i <= totalHeight / windowHeight; i++) {
      await browser.execute(`window.scrollTo(0, window.outerHeight * ${i})`)
      browser.takeScreenshot()
      // TODO: 'Add Full page screenshot functionality'
    }
  }

  async saveScreenshots (state: string, screencapture: boolean) {
    await allureReporter.addArgument('Page URL' + browser.getUrl())
    await allureReporter.addArgument('Page Title' + browser.getTitle())
    if (state === 'passed' && screencapture) {
      browser.takeScreenshot()
    } else if (state === 'failed') {
      browser.takeScreenshot()
    }
  }

  async addAllureReport (allureReporter, dirName: string, feature: string, testTitle: string) {
    await allureReporter.addFeature(feature)
    await allureReporter.addStory(testTitle)
    await allureReporter.addEnvironment('Browser', await this.browserName())
    await allureReporter.addEnvironment('Base URL', await this.baseUrl())
    await allureReporter.addEnvironment('Test folder', await CommonUtils.getCurrentFolder(dirName))
    await allureReporter.addEnvironment('Full path', await CommonUtils.getFullPath(dirName))
  }

  async goToCreateAccountPage (market: number = 1): Promise<void> {
    switch (market) {
      case 1: return browser.url(await this.baseUrl() + createAccount)
      case 2: return browser.url(await this.baseUrl() + sgUrl + createAccount)
      case 3: return browser.url(await this.baseUrl() + hkUrl + createAccount)
    }
  }
}
export default new DriverUtils()
