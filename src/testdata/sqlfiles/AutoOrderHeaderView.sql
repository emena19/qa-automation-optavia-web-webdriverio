SELECT [Auto_Order_ID]
      ,[Created_Date]
      ,[Modified_Date]
      ,[Medifast_Customer_Number]
      ,[Anticipated_Next_Autoship_Total]
      ,[Next_Autoship_Date]
      ,[Status_Code]
      ,[Commissionable_Volume]
      ,[Qualifying_Volume]
      ,[Channel]
  FROM [dbo].[v_auto_order_header]
  WHERE Medifast_Customer_Number = @MCN
ORDER BY Modified_Date DESC
