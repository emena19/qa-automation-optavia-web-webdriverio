SELECT TOP 1
[Medifast_Customer_Number],
[Order_No],
[Order_Date],
[Quantity],
[Item_Code],
[Item_Description],
[Timestamp],
[Activation_Date],
[Old_Renewal_Date],
[New_Renewal_Date],
[DataSource]
FROM [CUSTOM].Health_Coach_Dates
WHERE Medifast_Customer_Number = @MCN
ORDER BY [Timestamp] DESC
