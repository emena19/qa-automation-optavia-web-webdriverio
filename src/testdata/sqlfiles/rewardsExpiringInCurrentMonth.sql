SELECT TOP 10 [Medifast_Customer_Number]
, c.email
,[Available_Rewards_Balance]
,[Rewards_Expiration_Date]
 FROM [v_custom_customer_info] vc
 JOIN dbo.Customers c (nolock) on c.Field1= vc.Medifast_Customer_Number
 WHERE month(Rewards_Expiration_Date) = month(getdate())
 AND YEAR(Rewards_Expiration_Date) = YEAR(getdate())
 AND Available_Rewards_Balance > 0
 AND c.MainCountry like @COUNTRY
