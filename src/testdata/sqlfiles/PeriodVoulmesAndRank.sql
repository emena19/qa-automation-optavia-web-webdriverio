SELECT Volume2 As FQV
,Volume1 as PQV
,Volume3 as GQV
,CurrentRank as HAR,
CASE PaidRankID
		WHEN '1' THEN 'Optavia Coach'
		WHEN '10' THEN 'Senior Coach'
		WHEN '20' THEN 'Manager'
		WHEN '30' THEN 'Associate Director'
		WHEN '40' THEN 'Director'
		WHEN '50' THEN 'Executive Director'
		WHEN '60' THEN 'Integrated Executive Director'
		WHEN '70' THEN 'Regional Director'
		WHEN '80' THEN 'Integrated Regional Director'
		WHEN '90' THEN 'National Director'
		WHEN '100' THEN 'Integrated National Director'
		WHEN '110' THEN 'Global Directo'
		WHEN '120' THEN 'Integrated Global Director'
		WHEN '130' THEN 'Presidential Director'
		WHEN '140' THEN 'Integrated Presidential Director'
	END AS CurrentRank
FROM periodvolumes pv
JOIN CUSTOM.RankAdvancementLegend ra on ra.CurrentRankID = pv.Volume13
WHERE periodid=@periodId
AND periodtypeid=1
AND customerid in
( SELECT customerid FROM customers WHERE Field1=@MCN)
