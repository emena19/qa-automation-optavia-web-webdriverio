SELECT TOP 10 [Medifast_Customer_Number]
, c.email
, c.MainCountry
,[Next_Autoship_Date]
,[Status_Code]
,[Auto_Order_ID]
 FROM [dbo].[v_auto_order_header] va
 JOIN dbo.Customers c (nolock) on c.Field1= va.Medifast_Customer_Number
 WHERE Next_Autoship_Date < getdate() - 1
 AND Status_Code = 1
 AND MainCountry = @COUNTRY
 ORDER BY Next_Autoship_Date DESC
