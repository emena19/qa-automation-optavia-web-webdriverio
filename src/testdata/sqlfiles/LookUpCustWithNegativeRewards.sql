SELECT TOP 2 vc.[Medifast_Customer_Number],
[Email],
[Auto_Order_ID],
[Available_Rewards_Balance],
[Rewards_Expiration_Date]
FROM [v_custom_customer_info] vc JOIN dbo.Customers c (nolock) ON
c.Field1= vc.Medifast_Customer_Number JOIN dbo.v_auto_order_header oh (nolock) ON oh.Medifast_Customer_Number = vc.Medifast_Customer_Number
WHERE
Available_Rewards_Balance < 0
