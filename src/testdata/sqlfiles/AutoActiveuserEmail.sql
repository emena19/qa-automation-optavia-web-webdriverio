
SELECT TOP 10 [Auto_Order_ID]
,[Created_Date]
,[Modified_Date]
,[Medifast_Customer_Number]
,Email
,[Anticipated_Next_Autoship_Total]
,[Next_Autoship_Date]
,[Status_Code]
,[Commissionable_Volume]
,[Qualifying_Volume]
,[Channel]
,[MainCountry]
FROM [dbo].[v_auto_order_header] vo
JOIN dbo.Customers c (NOLOCK) ON c.Field1= vo.Medifast_Customer_Number
WHERE Next_Autoship_Date < getdate() -1
AND Status_Code = 1
AND MainCountry = @COUNTRY
ORDER BY Modified_Date DESC
