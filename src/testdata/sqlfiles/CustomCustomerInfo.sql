SELECT [Medifast_Customer_Number]
      ,[Available_Rewards_Balance]
      ,[Rewards_Expiration_Date]
  FROM [dbo].[v_custom_customer_info]
  WHERE Medifast_Customer_Number = @MCN
