// @flow
import DatabaseUtil from '@core-libs/DatabaseUtils'
import TextFileUtil from '@core-libs/TextFileUtils'

let sql = require('mssql')

const sqlCustomersDataFile = 'src/testdata/sqlfiles/buycoachkit_CustomersTable.sql'
const sqlHealthCoachDatesFile = 'src/testdata/sqlfiles/renewaldate_HealthCoachDatesTable.sql'
const sqlOrdersFile = 'src/testdata/sqlfiles/processNowAutoshipOrder_OrdersTable.sql'
const sqlAutoShipOrdersFile = 'src/testdata/sqlfiles/autoshiporder_AutoOrderHeaderTable.sql'
const sqlAutoOrderHeaderViewFile = 'src/testdata/sqlfiles/AutoOrderHeaderView.sql'
const sqlCustomCustomerInfoViewFile = 'src/testdata/sqlfiles/CustomCustomerInfo.sql'
const sqlAutoActiveuserEmail = 'src/testdata/sqlfiles/AutoActiveuserEmail.sql'
const sqlrewardsExipringNextMonth = 'src/testdata/sqlfiles/rewardsExpiringNextMonth.sql'
const sqlRewardsExpiringInCurrentMonth = 'src/testdata/sqlfiles/rewardsExpiringInCurrentMonth.sql'
const sqlRewardsExpDateAndaBalance = 'src/testdata/sqlfiles/RewardsExpDateAndBalance.sql'
const sqlExpiredAutoshipOrders = 'src/testdata/sqlfiles/expiredAutoshipOrders.sql'
const sqlLookUpCustWithNegativeRewards = 'src/testdata/sqlfiles/LookUpCustWithNegativeRewards.sql'
const sqlPeriodIDDetails = 'src/testdata/sqlfiles/PeriodIDDetails.sql'
const sqlPeriodVolumesAndRank = 'src/testdata/sqlfiles/PeriodVoulmesAndRank.sql'
const sqlCustomerCoachInfoViewFile = 'src/testdata/sqlfiles/CustomerCoachInfo.sql'
export default {

  async getCustomerTypeID_CustomersTable (accountID: string): Promise<any> {
    try {
      let sqlQuery = await TextFileUtil.readFile(sqlCustomersDataFile)
      let sqlParams = [
        {name: 'MCN', type: sql.VarChar, value: accountID}
      ]
      let db = await DatabaseUtil.sqlExecute(sqlQuery, sqlParams)
      return db.recordset[0].CustomerTypeID
    } catch (err) {
      throw new Error(err.message)
    }
  },

  async getActivationDate_CustomersTable (accountID: string): Promise<any> {
    try {
      let sqlQuery = await TextFileUtil.readFile(sqlCustomersDataFile)
      let sqlParams = [
        {name: 'MCN', type: sql.VarChar, value: accountID}
      ]
      let db = await DatabaseUtil.sqlExecute(sqlQuery, sqlParams)
      return db.recordset[0].ActivationDate
    } catch (err) {
      throw new Error(err.message)
    }
  },

  async getPayableTypeID_CustomersTable (accountID: string): Promise<any> {
    try {
      let sqlQuery = await TextFileUtil.readFile(sqlCustomersDataFile)
      let sqlParams = [
        {name: 'MCN', type: sql.VarChar, value: accountID}
      ]
      let db = await DatabaseUtil.sqlExecute(sqlQuery, sqlParams)
      return db.recordset[0].PayableTypeID
    } catch (err) {
      throw new Error(err.message)
    }
  },

  async getRenewalDate_HCDatesTable (accountID: string): Promise<any> {
    try {
      let sqlQuery = await TextFileUtil.readFile(sqlHealthCoachDatesFile)
      let sqlParams = [
        {name: 'MCN', type: sql.VarChar, value: accountID}
      ]
      let db = await DatabaseUtil.sqlExecute(sqlQuery, sqlParams)
      return db.recordset[0].New_Renewal_Date
    } catch (err) {
      throw new Error(err.message)
    }
  },

  async getActivationDate_HCDatesTable (accountID: string): Promise<any> {
    try {
      let sqlQuery = await TextFileUtil.readFile(sqlHealthCoachDatesFile)
      let sqlParams = [
        {name: 'MCN', type: sql.VarChar, value: accountID}
      ]
      let db = await DatabaseUtil.sqlExecute(sqlQuery, sqlParams)
      return db.recordset[0].Activation_Date
    } catch (err) {
      throw new Error(err.message)
    }
  },

  async getOldRenewalDate_HCDatesTable (accountID: string): Promise<any> {
    try {
      let sqlQuery = await TextFileUtil.readFile(sqlHealthCoachDatesFile)
      let sqlParams = [
        {name: 'MCN', type: sql.VarChar, value: accountID}
      ]
      let db = await DatabaseUtil.sqlExecute(sqlQuery, sqlParams)
      return db.recordset[0].Old_Renewal_Date
    } catch (err) {
      throw new Error(err.message)
    }
  },

  async getOrderID (orderNumber: string): Promise<any> {
    try {
      let sqlQuery = await TextFileUtil.readFile(sqlOrdersFile)
      let sqlParams = [
        {name: 'orderID', type: sql.VarChar, value: orderNumber}
      ]
      let db = await DatabaseUtil.sqlExecute(sqlQuery, sqlParams)
      return db.recordset[0].other13
    } catch (err) {
      throw new Error(err.message)
    }
  },

  async getModifiedDate (accountID: string): Promise<any> {
    try {
      let sqlQuery = await TextFileUtil.readFile(sqlAutoOrderHeaderViewFile)
      let sqlParams = [
        {name: 'MCN', type: sql.VarChar, value: accountID}
      ]
      let db = await DatabaseUtil.sqlExecute(sqlQuery, sqlParams)
      return db.recordset[0].Modified_Date
    } catch (err) {
      throw new Error(err.message)
    }
  },

  async getAutoOrderHeaderRecordset (accountID: string): Promise<any> {
    try {
      let sqlQuery = await TextFileUtil.readFile(sqlAutoShipOrdersFile)
      let sqlParams = [
        {name: 'MCN', type: sql.VarChar, value: accountID}
      ]
      let db = await DatabaseUtil.sqlExecute(sqlQuery, sqlParams)
      return db.recordset
    } catch (err) {
      throw new Error(err.message)
    }
  },

  async getAutoshipOrderID (accountID: string): Promise<any> {
    try {
      let sqlQuery = await TextFileUtil.readFile(sqlAutoShipOrdersFile)
      let sqlParams = [
        {name: 'MCN', type: sql.VarChar, value: accountID}
      ]
      let db = await DatabaseUtil.sqlExecute(sqlQuery, sqlParams)
      return db.recordset[0].Auto_Order_ID
    } catch (err) {
      throw new Error(err.message)
    }
  },

  async getNextAutoshipOrderDate (accountID: string): Promise<any> {
    try {
      let sqlQuery = await TextFileUtil.readFile(sqlAutoOrderHeaderViewFile)
      let sqlParams = [
        {name: 'MCN', type: sql.VarChar, value: accountID}
      ]
      let db = await DatabaseUtil.sqlExecute(sqlQuery, sqlParams)
      return db.recordset[0].Next_Autoship_Date
    } catch (err) {
      throw new Error(err.message)
    }
  },

  async getAutoOrderHeaderView (accountID: string): Promise<any> {
    try {
      let sqlQuery = await TextFileUtil.readFile(sqlAutoOrderHeaderViewFile)
      let sqlParams = [
        {name: 'MCN', type: sql.VarChar, value: accountID}
      ]
      let db = await DatabaseUtil.sqlExecute(sqlQuery, sqlParams)
      return db.recordset[0]
    } catch (err) {
      throw new Error(err.message)
    }
  },

  async getCustomCustomerInfoView (accountID: string): Promise<any> {
    try {
      let sqlQuery = await TextFileUtil.readFile(sqlCustomCustomerInfoViewFile)
      let sqlParams = [
        {name: 'MCN', type: sql.VarChar, value: accountID}
      ]
      let db = await DatabaseUtil.sqlExecute(sqlQuery, sqlParams)
      return db.recordset[0]
    } catch (err) {
      throw new Error(err.message)
    }
  },

  async getActiverUserEmailID (market: any = 1): Promise<any> {
    try {
      let country = market === 1 ? 'US' : market === 2 ? 'SG' : 'HK'
      let sqlQuery = await TextFileUtil.readFile(sqlAutoActiveuserEmail)
      let sqlParams = [
        {name: 'COUNTRY', type: sql.VarChar, value: country}
      ]
      let db = await DatabaseUtil.sqlExecute(sqlQuery, sqlParams)
      return db.recordset[0]
    } catch (err) {
      throw new Error(err.message)
    }
  },

  async getTop10ExpiredAutoshipOrders (market: any = 1): Promise<any> {
    try {
      let country = market === 1 ? 'US' : market === 2 ? 'SG' : 'HK'
      let sqlQuery = await TextFileUtil.readFile(sqlExpiredAutoshipOrders)
      let sqlParams = [
        {name: 'COUNTRY', type: sql.VarChar, value: country}
      ]
      let db = await DatabaseUtil.sqlExecute(sqlQuery, sqlParams)
      return db.recordset
    } catch (err) {
      throw new Error(err.message)
    }
  },

  async getExpiringRewardsUserDetails (market: any = 1): Promise<any> {
    try {
      let country = market === 1 ? 'US' : market === 2 ? 'SG' : 'HK'
      let sqlQuery = await TextFileUtil.readFile(sqlRewardsExpiringInCurrentMonth)
      let sqlParams = [
        {name: 'COUNTRY', type: sql.VarChar, value: country}
      ]
      let db = await DatabaseUtil.sqlExecute(sqlQuery, sqlParams)
      return db.recordset[0]
    } catch (err) {
      throw new Error(err.message)
    }
  },

  async getRewardsExpiringNextMonth (market: any = 1): Promise<any> {
    try {
      let country = market === 1 ? 'US' : market === 2 ? 'SG' : 'HK'
      let sqlQuery = await TextFileUtil.readFile(sqlrewardsExipringNextMonth)
      let sqlParams = [
        {name: 'COUNTRY', type: sql.VarChar, value: country}
      ]
      let db = await DatabaseUtil.sqlExecute(sqlQuery, sqlParams)
      return db.recordset[0]
    } catch (err) {
      throw new Error(err.message)
    }
  },

  async getOrderNumber_HCDatesTable (accountID: string): Promise<any> {
    try {
      let sqlQuery = await TextFileUtil.readFile(sqlHealthCoachDatesFile)
      let sqlParams = [
        {name: 'MCN', type: sql.VarChar, value: accountID}
      ]
      let db = await DatabaseUtil.sqlExecute(sqlQuery, sqlParams)
      return db.recordset[0].Order_No
    } catch (err) {
      throw new Error(err.message)
    }
  },

  async getRewardsExpDateAndBalance (accountID: string): Promise<any> {
    try {
      let sqlQuery = await TextFileUtil.readFile(sqlRewardsExpDateAndaBalance)
      let sqlParams = [
        {name: 'MCN', type: sql.VarChar, value: accountID}
      ]
      let db = await DatabaseUtil.sqlExecute(sqlQuery, sqlParams)
      return db.recordset[0]
    } catch (err) {
      throw new Error(err.message)
    }
  },

  async getCustomerWithNegativeRewardBalance (): Promise<any> {
    try {
      let sqlQuery = await TextFileUtil.readFile(sqlLookUpCustWithNegativeRewards)
      let db = await DatabaseUtil.sqlExecute(sqlQuery)
      return db.recordset[1]
    } catch (err) {
      throw new Error(err.message)
    }
  },

  async getPeriodID (monthYear: string): Promise<any> {
    try {
      let sqlQuery = await TextFileUtil.readFile(sqlPeriodIDDetails)
      let sqlParams = [
        {name: 'monthYear', type: sql.VarChar, value: monthYear}
      ]
      let db = await DatabaseUtil.sqlExecute(sqlQuery, sqlParams)
      return db.recordset[0].PeriodID
    } catch (err) {
      throw new Error(err.message)
    }
  },

  async getPeriodVolumesAndRank (periodID: string, accountID: string): Promise<any> {
    try {
      let sqlQuery = await TextFileUtil.readFile(sqlPeriodVolumesAndRank)
      let sqlParams = [
        {name: 'periodId', type: sql.VarChar, value: periodID},
        {name: 'MCN', type: sql.VarChar, value: accountID}
      ]
      let db = await DatabaseUtil.sqlExecute(sqlQuery, sqlParams)
      return db.recordset[0]
    } catch (err) {
      throw new Error(err.message)
    }
  },

  async getCustomerCoachInfoView (email: string): Promise<any> {
    try {
      let sqlQuery = await TextFileUtil.readFile(sqlCustomerCoachInfoViewFile)
      let sqlParams = [
        {name: 'MCN', type: sql.VarChar, value: email}
      ]
      let db = await DatabaseUtil.sqlExecute(sqlQuery, sqlParams)
      return db.recordset[0].Field2
    } catch (err) {
      throw new Error(err.message)
    }
  }
}
