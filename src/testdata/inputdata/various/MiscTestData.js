// @flow
import ExcelUtil from 'src/lib/ExcelUtil'
import {GLOBAL_PASSWORD} from 'src/lib/ConstantUtil'
const excelFileName = './src/testdata/inputdata/various/MiscTestData.xlsx'

export const page = {
  Home: {
    MAIN: 'Home',
    CREATE_ACCOUNT_LOGIN: 'Log In / Create an Account',
    SHOPPING_CART: 'Shopping Cart',
    SHOP: 'SHOP',
    JOIN_US: 'JOIN US',
    FIND_A_COACH: 'Find a Coach',
    SEARCH_COACH_RESULTS: 'Coach search results',
    CHECKOUT: 'Checkout',
    SHIPPING_ADDRESS: 'Shipping Address',
    SUGGESTED_ADDRESS: 'Suggested Address popup',
    SHIPPING_METHOD: 'Shipping Method',
    PAYMENT_AND_BILLING_ADDRESS: 'Payment & Billing Address',
    FINAL_REVIEW: 'Final Review',
    ORDER_CONFIRMATION: 'Order Confirmation',
    CHOOSE_YOUR_OWN_FUELINGS: 'Choose Your Own Fuelings',
    ADDED_TO_YOUR_SHOPPING_CART_TITLE: 'Added to Your Shopping Cart Popup',
    WHAT_MATTERS_MOST: 'What Matters Most',
    PROCEED_TO_CHECKOUT: 'Proceed To Checkout',
    CHANNEL: 'Channel',
    MAIN_HK: 'Home HK',
    SHOP_HK: 'SHOP HK',
    SHOPPING_CART_HK: 'Shopping Cart HK',
    PROCEED_TO_CHECKOUT_HK: 'Proceed To Checkout HK',
    MAIN_SG: 'Home SG',
    SHOP_SG: 'SHOP SG',
    SHOPPING_CART_SG: 'Shopping Cart SG',
    PROCEED_TO_CHECKOUT_SG: 'Proceed To Checkout SG',
    ORDER_CONFIRMATION_HK: 'Order Confirmation HK',
    ORDER_CONFIRMATION_SG: 'Order Confirmation SG',
    PRODUCTS_AND_PROGRAMS: 'PRODUCTS & PROGRAMS',
    MAIN_ASM: 'ASM Homepage',
    COACH_STORE_BUSINESS_MATERIALS: 'Coach Store Business Materials',
    CREATE_ACCOUNT: 'Create Account',
    SIGN_IN: 'Sign in'
  },
  MyAccount: {
    MAIN: 'My Account',
    YOUR_PROFILE: 'Your Profile',
    ADDRESS_BOOK: 'Address Book',
    PAYMENT_DETAILS: 'Payment Details',
    ORDER_HISTORY: 'Order History',
    OPTAVIA_PREMIER_ORDER: 'OPTAVIA Premier Order',
    RETURN_STATUS: 'Return Status'
  },
  ShoppingCart: {
    PROMO_HEADER: 'Promo Header'
  },
  Coach: {
    MAIN: 'Coach',
    COACH_ADMIN_FEE: 'Admin Fee',
    ESTABLISHED_COACH_BUSINESS_KIT: 'Established Coach Business Kit',
    BUSINESS_RENEWAL: 'Business Renewal'
  },
  Header: {
    LOGIN: 'Log In',
    GET_IN_TOUCH: 'get in touch',
    BANNER: 'Banner',
    ACCESSIBILITY_ICON: 'Accessibility Icon'
  },
  Footer: {
    ABOUT_US: 'about us',
    IN_THE_NEWS: 'in the news',
    FAQS: 'faqs',
    COMMUNITY: 'Community',
    TERMS_AND_CONDITIONS: 'Terms & Conditions',
    PRIVACY_POLICY: 'Privacy Policy',
    TRADEMARKS: 'Trademarks',
    DISCLAMIER: 'Disclaimer',
    FACEBOOK: 'facebook',
    TWITTER: 'twitter',
    YOUTUBE: 'YouTube',
    PINTEREST: 'pinterest',
    INSTAGRAM: 'instagram',
    DSA_CODE_OF_ETHICS: 'DSA Code of Ethics',
    DSEF: 'DSeF',
    DSA_CODE_OF_ETHICS_AND_COMPLAINT_PROCESS: 'DSA Code of Ethichs and Complaint Process',
    DIRECT_SELLING_ASSOCIATION: 'Direct Selling Association (DSA)',
    DSA: 'DSA'
  },
  NavBar: {
    PROGRAM_5_1: 'Optimal Weight 5&1 Program',
    PROGRAM_4_2_1: 'Optimal Weight 4&2&1 Program',
    PROGRAM_3_3: 'Optimal Health 3&3 Program',
    PURPOSEFUL_HYDRATION: 'Purposeful Hydration',
    TRANSFORMATIONAL_STORIES: 'Transformational Stories',
    PURPOSEFUL_HYDRATION_SHOP: 'Purposeful Hydration Shop',
    WELLNESS_CREDITS: 'Wellness Credits',
    MOMENTUM_ACTIVE: 'Momentum Active'
  },
  ProductDetailPage: {
    NUTRITION_FACTS: 'Nutrition Facts'
  },
  Shop: {
    CLASSIC_LEMON_MERINGE: 'Classic Lemon Meringue Crunch Bar - Naturally Flavored (Box)',
    CLASSIC_PEANUT_BUTTER_CHOCOLATE_CHIP: 'Classic Peanut Butter Chocolate Chip Chewy Bar - Naturally Flavored',
    CLASSIC_DUTCH_CHOCOLATE_SHAKE: 'Classic Dutch Chocolate Shake - Naturally and Artificially Flavored (Box)',
    CLASSIC_CALORIE_BURN_CAPUCCINO: 'Classic Calorie Burn Cappuccino - Naturally and Artificially Flavored (Box)',
    CLASSIC_MACARONI_CHEESE: 'Classic Macaroni & Cheese - Naturally Flavored (Box)',
    CLASSIC_CHILI_NACHO_CHEESE_PUFF: 'Classic Chili Nacho Cheese Puffs - Naturally Flavored (Box)'
  },
  OptaviaCategory: {
    CLASSIC_BARS: 'Classic Bars',
    CLASSIC_SHAKES: 'Classic Shakes',
    CLASSIC_DRINKS: 'Classic Drinks',
    CLASSIC_CRUNCHERS: 'Classic Crunchers',
    CLASSIC_HEARTY: 'Classic Hearty',
    CLASSIC_SOUPS: 'Classic Soups',
    CLASSIC_BREAKFAST: 'Classic Breakfast',
    CLASSIC_DESSERT: 'Classic Dessert'
  }
}

export default {
  // Wellness credit
  CLIENT_ID: async (row: number) => ExcelUtil.getCellData(excelFileName, 1, `A${await row}`),
  CLIENT_FIRST_NAME: async (row: number) => ExcelUtil.getCellData(excelFileName, 1, `B${await row}`),
  CLIENT_LAST_NAME: async (row: number) => ExcelUtil.getCellData(excelFileName, 1, `C${await row}`),
  CLIENT_EMAIL: async (row: number) => ExcelUtil.getCellData(excelFileName, 1, `D${await row}`),
  WELLNESS_CREDIT_QTY: async (row: number) => ExcelUtil.getCellData(excelFileName, 1, `E${await row}`),
  COACH_BOUGHT_WC: async (row: number) => ExcelUtil.getCellData(excelFileName, 1, `F${await row}`),
  GLOBAL_PASSWORD: async () => GLOBAL_PASSWORD,
  TAX_AMOUNT: async (row: number) => ExcelUtil.getCellData(excelFileName, 1, `H${await row}`),
  DELIVERY_CHARGE: async (row: number) => ExcelUtil.getCellData(excelFileName, 1, `I${await row}`),

  // Create - Register an Account
  FIRST_NAME_PLACEHOLDER: () => ExcelUtil.getCellData(excelFileName, 2, 'C2'),
  FIRST_NAME_REQUIRED_MESSAGE: () => ExcelUtil.getCellData(excelFileName, 2, 'D2'),
  FIRST_NAME_MORE_THAN_MAX_ERROR_MESSAGE: () => ExcelUtil.getCellData(excelFileName, 2, 'E2'),
  LAST_NAME_PLACEHOLDER: () => ExcelUtil.getCellData(excelFileName, 2, 'C3'),
  LAST_NAME_REQUIRED_MESSAGE: () => ExcelUtil.getCellData(excelFileName, 2, 'D3'),
  LAST_NAME_MORE_THAN_MAX_ERROR_MESSAGE: () => ExcelUtil.getCellData(excelFileName, 2, 'E3'),
  EMAIL_PLACEHOLDER: () => ExcelUtil.getCellData(excelFileName, 2, 'C4'),
  EMAIL_ADDRESS_REQUIRED_MESSAGE: () => ExcelUtil.getCellData(excelFileName, 2, 'D4'),
  INVALID_EMAIL_ADDRESS_ERROR_MESSAGE: () => ExcelUtil.getCellData(excelFileName, 2, 'E4'),
  EMAIL_EXISTING_ERROR: () => ExcelUtil.getCellData(excelFileName, 2, 'F4'),
  EMAIL_EXISTING_GLOBAL_ERROR: () => ExcelUtil.getCellData(excelFileName, 2, 'G4'),
  PASSWORD_PLACEHOLDER: () => ExcelUtil.getCellData(excelFileName, 2, 'C5'),
  PASSWORD_REQUIRED_MESSAGE: () => ExcelUtil.getCellData(excelFileName, 2, 'D5'),
  INVALID_PASSWORD_ERROR_MESSAGE: () => ExcelUtil.getCellData(excelFileName, 2, 'E5'),
  CONFIRM_PASSWORD_PLACEHOLDER: () => ExcelUtil.getCellData(excelFileName, 2, 'C6'),
  CONFIRM_PASSWORD_REQUIRED_MESSAGE: () => ExcelUtil.getCellData(excelFileName, 2, 'D6'),
  INVALID_CONFIRM_PASSWORD_ERROR_MESSAGE: () => ExcelUtil.getCellData(excelFileName, 2, 'E6'),
  CONFIRM_PASSWORD_NOT_MATCH_ERROR_MESSAGE: () => ExcelUtil.getCellData(excelFileName, 2, 'F6'),
  FIRST_NAME_PLACEHOLDER_SG_HK: () => ExcelUtil.getCellData(excelFileName, 2, 'C24'),
  LAST_NAME_PLACEHOLDER_SG_HK: () => ExcelUtil.getCellData(excelFileName, 2, 'C25'),
  EMAIL_PLACEHOLDER_SG_HK: () => ExcelUtil.getCellData(excelFileName, 2, 'C26'),
  COUNTRY_CODE_PLACEHOLDER_SG_HK: () => ExcelUtil.getCellData(excelFileName, 2, 'C27'),
  COUNTRY_CODE_ERROR_MESSAGE_SG_HK: () => ExcelUtil.getCellData(excelFileName, 2, 'D27'),
  PHONE_PLACEHOLDER_SG_HK: () => ExcelUtil.getCellData(excelFileName, 2, 'C28'),
  PHONE_ERROR_MESSAGE_SG_HK: () => ExcelUtil.getCellData(excelFileName, 2, 'D28'),
  INVALID_PHONE_ERROR_MESSAGE: () => ExcelUtil.getCellData(excelFileName, 2, 'E17'),
  // Create - Account
  FULL_NAME_ERROR_MESSAGE: async (row: number) => ExcelUtil.getCellData(excelFileName, 2, `E${await row}`),
  FULL_NAME_OTHER_MESSAGE: async (row: number) => ExcelUtil.getCellData(excelFileName, 2, `F${await row}`),
  CREATE_ACC_INVALID_PHONE_ERROR_MESSAGE: () => ExcelUtil.getCellData(excelFileName, 2, 'F17'),

  // Login Section Messages Field related and common
  LOGIN_USERNAME_PLACEHOLDER: () => ExcelUtil.getCellData(excelFileName, 2, 'C10'),
  LOGIN_USERNAME_EMPTY: () => ExcelUtil.getCellData(excelFileName, 2, 'D10'),
  LOGIN_PASSWORD_PLACEHOLDER: () => ExcelUtil.getCellData(excelFileName, 2, 'C11'),
  LOGIN_PASSWORD_EMPTY: () => ExcelUtil.getCellData(excelFileName, 2, 'D11'),
  LOGIN_INCORRECT_USERNAME_PASSWORD_COMMON_MESSAGE: () => ExcelUtil.getCellData(excelFileName, 2, 'E13'),
  LOGIN_USERNAME_PASSWORD_EMPTY_COMMON_MESSAGE: () => ExcelUtil.getCellData(excelFileName, 2, 'F13'),
  NEW_LOGIN_ERROR_MESSAGE: () => ExcelUtil.getCellData(excelFileName, 2, 'E34'),
  NEW_LOGIN_LOCKED_ERROR_MESSAGE: () => ExcelUtil.getCellData(excelFileName, 2, 'E35'),
  NEW_LOGIN_OKTA_ERROR_MESSAGE: () => ExcelUtil.getCellData(excelFileName, 2, 'F35'),
  // Logout Message
  LOGOUT_SUCCESSFUL_MESSAGE: () => ExcelUtil.getCellData(excelFileName, 2, 'F14'),

  // Reset Password
  LOGIN_RESET_PASSWORD_POPUP_TITLE: () => ExcelUtil.getCellData(excelFileName, 2, 'A12'),
  LOGIN_RESET_USERNAME_EMAIL_PLACEHOLDER: () => ExcelUtil.getCellData(excelFileName, 2, 'C12'),
  LOGIN_RESET_USERNAME_EMAIL_REQUIRED_MESSAGE: () => ExcelUtil.getCellData(excelFileName, 2, 'D12'),
  LOGIN_RESET_USERNAME_EMAIL_INVALID_MESSAGE: () => ExcelUtil.getCellData(excelFileName, 2, 'E12'),
  LOGIN_RESET_USERNAME_EMAIL_NOT_FOUND_MESSAGE: () => ExcelUtil.getCellData(excelFileName, 2, 'F12'),
  LOGIN_RESET_USERNAME_EMAIL_SENT_MESSAGE: () => ExcelUtil.getCellData(excelFileName, 2, 'G12'),

  // Your Profile
  PROFILE_FIRST_NAME_PLACEHOLDER: () => ExcelUtil.getCellData(excelFileName, 2, 'C15'),
  PROFILE_FIRST_NAME_REQUIRED: () => ExcelUtil.getCellData(excelFileName, 2, 'D15'),
  PROFILE_FIRST_NAME_MAX_ERROR: () => ExcelUtil.getCellData(excelFileName, 2, 'E15'),
  PROFILE_LAST_NAME_PLACEHOLDER: () => ExcelUtil.getCellData(excelFileName, 2, 'C16'),
  PROFILE_LAST_NAME_REQUIRED: () => ExcelUtil.getCellData(excelFileName, 2, 'D16'),
  PROFILE_LAST_NAME_MAX_ERROR: () => ExcelUtil.getCellData(excelFileName, 2, 'E16'),
  PROFILE_MOBILE_NUMBER_PLACEHOLDER: () => ExcelUtil.getCellData(excelFileName, 2, 'C17'),
  PROFILE_MOBILE_NUMBER_REQUIRED: () => ExcelUtil.getCellData(excelFileName, 2, 'D17'),
  PROFILE_MOBILE_NUMBER_INVALID: () => ExcelUtil.getCellData(excelFileName, 2, 'E17'),
  PROFILE_SECONDARY_PHONE_NUMBER_PLACEHOLDER: () => ExcelUtil.getCellData(excelFileName, 2, 'C18'),
  PROFILE_SECONDARY_PHONE_NUMBER_INVALID: () => ExcelUtil.getCellData(excelFileName, 2, 'E18'),

  // Coach Page
  COACH_FIRST_NAME_PLACEHOLDER: () => ExcelUtil.getCellData(excelFileName, 2, 'C19'),
  COACH_FIRST_NAME_PLACEHOLDER_SG_HK: () => ExcelUtil.getCellData(excelFileName, 2, 'C29'),
  COACH_FIRST_NAME_REQUIRED: () => ExcelUtil.getCellData(excelFileName, 2, 'D19'),
  COACH_LAST_NAME_PLACEHOLDER: () => ExcelUtil.getCellData(excelFileName, 2, 'C20'),
  COACH_LAST_NAME_PLACEHOLDER_SG_HK: () => ExcelUtil.getCellData(excelFileName, 2, 'C30'),
  COACH_LAST_NAME_REQUIRED: () => ExcelUtil.getCellData(excelFileName, 2, 'D20'),
  COACH_CITY_NAME_PLACEHOLDER: () => ExcelUtil.getCellData(excelFileName, 2, 'C21'),
  COACH_MARKET_NAME_PLACE_HOLDER: () => ExcelUtil.getCellData(excelFileName, 2, 'C22'),
  COACH_MARKET_NAME_PLACE_HOLDER_HK: () => ExcelUtil.getCellData(excelFileName, 2, 'C31'),
  COACH_MARKET_NAME_PLACE_HOLDER_SG: () => ExcelUtil.getCellData(excelFileName, 2, 'C32'),
  COACH_NEARBY_PLACEHOLDER: () => ExcelUtil.getCellData(excelFileName, 2, 'C23'),
  COACH_NEARBY_REQUIRED: () => ExcelUtil.getCellData(excelFileName, 2, 'D23'),

  // Acquisition Offer
  TEST_CASE_ID: async (col: string) => ExcelUtil.getCellData(excelFileName, 3, `${await col}1`),
  TEST_CASE_SCENARIO: async (col: string) => ExcelUtil.getCellData(excelFileName, 3, `${await col}2`),
  PRODUCT1_SKU: async (col: string) => ExcelUtil.getCellData(excelFileName, 3, `${await col}13`),
  PRODUCT1_NAME: async (col: string) => ExcelUtil.getCellData(excelFileName, 3, `${await col}14`),
  PRODUCT1_UNIT_PRICE: async (col: string) => ExcelUtil.getCellData(excelFileName, 3, `${await col}15`),
  PRODUCT1_QTY: async (col: string) => ExcelUtil.getCellData(excelFileName, 3, `${await col}16`),
  PRODUCT1_UOM: async (col: string) => ExcelUtil.getCellData(excelFileName, 3, `${await col}17`),
  ORDER_SHIPPING: async (col: string) => ExcelUtil.getCellData(excelFileName, 3, `${await col}32`),
  ORDER_SURCHARGE: async (col: string) => ExcelUtil.getCellData(excelFileName, 3, `${await col}33`),
  ORDER_TAX: async (col: string) => ExcelUtil.getCellData(excelFileName, 3, `${await col}34`),
  ORDER_REWARD_EARNED: async (col: string) => ExcelUtil.getCellData(excelFileName, 3, `${await col}39`),

  // Site Map Test Data
  async getRow (page) {
    let row = await ExcelUtil.getRowNumberByGivenCellValue(excelFileName, 4, 'B', page)
    return row
  },
  PAGE_NAME: async function (page) { return ExcelUtil.getCellData(excelFileName, 4, `B${await this.getRow(page)}`) },
  PAGE_TITLE: async function (page) { return ExcelUtil.getCellData(excelFileName, 4, `C${await this.getRow(page)}`) },
  PAGE_URL: async function (page) { return ExcelUtil.getCellData(excelFileName, 4, `D${await this.getRow(page)}`) },
  PAGE_BODY_TITLE: async function (page) { return ExcelUtil.getCellData(excelFileName, 4, `E${await this.getRow(page)}`) },
  PAGE_BODY_SECTION_1: async function (page) { return ExcelUtil.getCellData(excelFileName, 4, `F${await this.getRow(page)}`) },
  PAGE_BODY_SECTION_2: async function (page) { return ExcelUtil.getCellData(excelFileName, 4, `G${await this.getRow(page)}`) },
  PAGE_BODY_SECTION_3: async function (page) { return ExcelUtil.getCellData(excelFileName, 4, `H${await this.getRow(page)}`) },
  PAGE_MESSAGE: async function (page) { return ExcelUtil.getCellData(excelFileName, 4, `I${await this.getRow(page)}`) },

  // sms subscribe
  SMS_FIRST_NAME_REQUIRED_MESSAGE: () => ExcelUtil.getCellData(excelFileName, 2, 'D36'),
  SMS_LAST_NAME_REQUIRED_MESSAGE: () => ExcelUtil.getCellData(excelFileName, 2, 'D37'),
  SMS_MOBILE_NUMBER_REQUIRED_MESSAGE: () => ExcelUtil.getCellData(excelFileName, 2, 'D38'),
  SMS_MOBILE_NUMBER_ERROR_MESSAGE: () => ExcelUtil.getCellData(excelFileName, 2, 'E38'),

  // New Register page
  NEW_EMAIL_PLACEHOLDER: () => ExcelUtil.getCellData(excelFileName, 2, 'C39'),
  NEW_PASSWORD_PLACEHOLDER: () => ExcelUtil.getCellData(excelFileName, 2, 'C40'),
  NEW_PHONE_PLACEHOLDER: () => ExcelUtil.getCellData(excelFileName, 2, 'C41'),
  NEW_FULL_NAME_PLACEHOLDER: () => ExcelUtil.getCellData(excelFileName, 2, 'C42'),

  // New Login Page
  NEW_LOGIN_EMAIL_PLACEHOLDER: () => ExcelUtil.getCellData(excelFileName, 2, 'C34'),
  NEW_LOGIN_PASSWORD_PLACEHOLDER: () => ExcelUtil.getCellData(excelFileName, 2, 'C35')
}
