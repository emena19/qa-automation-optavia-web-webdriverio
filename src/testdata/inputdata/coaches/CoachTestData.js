// @flow
import ExcelUtil from 'src/lib/ExcelUtil'
const excelFileName = './src/testdata/inputdata/coaches/CoachTestData.xlsx'

export default {
  // Coach info
  COACH_ID: async (row: number = 2, sheet: number = 1) => ExcelUtil.getCellData(excelFileName, sheet, `A${await row}`),
  COACH_FIRST_NAME: async (row: number = 2, sheet: number = 1) => ExcelUtil.getCellData(excelFileName, sheet, `B${await row}`),
  COACH_LAST_NAME: async (row: number = 2, sheet: number = 1) => ExcelUtil.getCellData(excelFileName, sheet, `C${await row}`),
  COACH_EMAIL: async (row: number = 2, sheet: number = 1) => ExcelUtil.getCellData(excelFileName, sheet, `D${await row}`),
  COACH_SHORT_ADDRESS: async (row: number = 2, sheet: number = 1) => ExcelUtil.getCellData(excelFileName, sheet, `E${await row}`),
  COACH_PHONE: async (row: number = 2, sheet: number = 1) => ExcelUtil.getCellData(excelFileName, sheet, `F${await row}`)
}
