// @flow
import ExcelUtil from '/src/lib/ExcelUtil'
const testCreditCard = './src/testdata/inputdata/creditcards/CreditCardTestData.xlsx'

export default {
  // Payment Information (Test Credit Cards)
  CARD_TYPE: async (sh: number, col: string) => ExcelUtil.getCellData(testCreditCard, sh, `${await col}1`),
  CARD_NUMBER: async (sh: number, col: string) => ExcelUtil.getCellData(testCreditCard, sh, `${await col}2`),
  CVV: async (sh: number, col: string) => ExcelUtil.getCellData(testCreditCard, sh, `${await col}3`),
  EXPIRATION_MONTH: async (sh: number, col: string) => ExcelUtil.getCellData(testCreditCard, sh, `${await col}4`),
  EXPIRATION_YEAR: async (sh: number, col: string) => ExcelUtil.getCellData(testCreditCard, sh, `${await col}5`),
  LAST_4_CARD_NUMBER: async (sh: number, col: string) => ExcelUtil.getCellData(testCreditCard, sh, `${await col}6`),
  CARD_TYPE_PAYMENT_DETAIL: async (sh: number, col: string) => ExcelUtil.getCellData(testCreditCard, sh, `${await col}7`)
}
