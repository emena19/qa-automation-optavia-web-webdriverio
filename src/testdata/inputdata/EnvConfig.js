// @flow
import ExcelUtil from '/src/lib/ExcelUtil'

require('dotenv').config()
const excelFileName = './src/testdata/inputdata/EnvConfig.xlsx'

export default {
  HOST: function (): string { return process.env.SELENIUM_SERVER },

  BROWSER: function (): string { return process.env.TEST_BROWSER_NAME },

  ROW: async function () { return this.getEnvRow() },

  HYBRIS_NAME: async function () {
    return ExcelUtil.getCellData(excelFileName, 1, `A${await this.ROW()}`)
  },
  ENV: async function () {
    return ExcelUtil.getCellData(excelFileName, 1, `B${await this.ROW()}`)
  },
  BASE_URL: async function () {
    return ExcelUtil.getCellData(excelFileName, 1, `C${await this.ROW()}`)
  },
  REPLICATED_SITE: async function () {
    return ExcelUtil.getCellData(excelFileName, 1, `D${await this.ROW()}`)
  },
  OPTAVIA_CONNECT_URL: async function () {
    return ExcelUtil.getCellData(excelFileName, 1, `E${await this.ROW()}`)
  },
  HYBRIS_BACKOFFICE_URL: async function () {
    return ExcelUtil.getCellData(excelFileName, 1, `F${await this.ROW()}`)
  },
  ENV_SANDBOX: async function () {
    return ExcelUtil.getCellData(excelFileName, 1, `G${await this.ROW()}`)
  },
  MULESOFT_INSTANCE: async function () {
    return ExcelUtil.getCellData(excelFileName, 1, `H${await this.ROW()}`)
  },
  TRACK: async function () {
    return ExcelUtil.getCellData(excelFileName, 1, `I${await this.ROW()}`)
  },
  DB_SERVER: async function () {
    return ExcelUtil.getCellData(excelFileName, 1, `J${await this.ROW()}`)
  },
  DB_INSTANCE: async function () {
    return ExcelUtil.getCellData(excelFileName, 1, `K${await this.ROW()}`)
  },
  DB_USER: async function () {
    return ExcelUtil.getCellData(excelFileName, 1, `L${await this.ROW()}`)
  },
  DB_PASSWORD: async function () {
    return ExcelUtil.getCellData(excelFileName, 1, `M${await this.ROW()}`)
  },

  MED_DIRECT_SITE_URL: async function () {
    return ExcelUtil.getCellData(excelFileName, 1, `N${await this.ROW()}`)
  },

  DB_CONFIG: async function () {
    return this.getDBConnection()
  },

  async getEnvRow () {
    let row = await ExcelUtil.getRowNumberByGivenCellValue(excelFileName, 1, 'B', process.env.TEST_ENVIRONMENT)
    return row
  },

  async getDBConnection () {
    return {
      user: await this.DB_USER(),
      password: await this.DB_PASSWORD(),
      server: await this.DB_SERVER(),
      database: await this.DB_INSTANCE(),
      connectionTimeout: 360000,
      requestTimeout: 360000
    }
  }
}
