// @flow
import {GLOBAL_PASSWORD} from '/src/lib/ConstantUtil'
import GoogleSheetsUtil from '/src/lib/GoogleSheetUtil'
import faker from 'faker'

class BasicInfo {
  constructor (rowInfo) {
    this.CUSTOMER_ID = rowInfo['Customer ID']
    this.CUSTOMER_FIRST_NAME = rowInfo['First Name']
    this.CUSTOMER_LAST_NAME = rowInfo['Last Name']
    this.CUSTOMER_EMAIL = rowInfo['Email']
    this.CUSTOMER_PASSWORD = GLOBAL_PASSWORD
    this.CUSTOMER_MARKET = rowInfo['Market']
  }
}

class ShippingAddress {
  constructor (rowInfo) {
    this.SHIPPING_ADDRESS_LINE_1 = rowInfo['Shp Addr Line 1']
    this.SHIPPING_ADDRESS_LINE_2 = rowInfo['Shp Addr Line 2']
    this.SHIPPING_DISTRICT = rowInfo['Shipping District']
    this.SHIPPING_CITY = rowInfo['Shipping City']
    this.SHIPPING_STATE = rowInfo['Shipping State']
    this.SHIPPING_FULL_STATE_NAME = rowInfo['Shipping State Full Name']
    this.SHIPPING_ZIP = rowInfo['Shipping Zip']
    this.SHIPPING_PHONE = rowInfo['Shipping Phone']
    this.SHIPPING_COUNTRY = rowInfo['Shipping Country']
    this.SHIPPING_COUNTRY_CODE = rowInfo['Shipping Country Code']
    this.SHIPPING_DELIVERY_METHOD = rowInfo['Shipping Method']
  }
}

class BillingAddress {
  constructor (rowInfo) {
    this.BILLING_FIRST_NAME = rowInfo['Billing First Name']
    this.BILLING_LAST_NAME = rowInfo['Billing Last Name']
    this.BILLING_ADDRESS_LINE_1 = rowInfo['Billing Addr Line 1']
    this.BILLING_ADDRESS_LINE_2 = rowInfo['Billing Addr Line 2']
    this.BILLING_DISTRICT = rowInfo['Billing District']
    this.BILLING_CITY = rowInfo['Billing City']
    this.BILLING_STATE = rowInfo['Billing State']
    this.BILLING_FULL_STATE_NAME = rowInfo['Billing State Full Name']
    this.BILLING_ZIP = rowInfo['Billing Zip']
    this.BILLING_PHONE = rowInfo['Billing Phone']
    this.BILLING_COUNTRY = rowInfo['Billing Country']
    this.BILLING_COUNTRY_CODE = rowInfo['Billing Country Code']
  }
}

class AdditionalAddress {
  constructor (rowInfo) {
    this.ADDITIONAL_ADDRESS_LINE_1 = rowInfo['Additional Addr Line 1']
    this.ADDITIONAL_ADDRESS_LINE_2 = rowInfo['Additional Addr Line 2']
    this.ADDITIONAL_DISTRICT = rowInfo['Additional District']
    this.ADDITIONAL_CITY = rowInfo['Additional City']
    this.ADDITIONAL_STATE = rowInfo['Additional State']
    this.ADDITIONAL_FULL_STATE_NAME = rowInfo['Additional State Full Name']
    this.ADDITIONAL_ZIP = rowInfo['Additional Zip']
    this.ADDITIONAL_PHONE = rowInfo['Additional Phone']
    this.ADDITIONAL_COUNTRY = rowInfo['Additional Country']
    this.ADDITIONAL_COUNTRY_CODE = rowInfo['Additional Country Code']
  }
}

class AdditionalInfo {
  constructor (rowInfo) {
    this.NATIONAL_ID = rowInfo['National ID']
    this.MOBILE = rowInfo['Mobile Number']
    this.SECONDARY_PHONE = rowInfo['Secondary Phone Number']
    this.DESIRED_WEIGHT_LOSS = rowInfo['Desired Weight Loss Goal']
    this.DOB = rowInfo['Date of Birth']
    this.DIETARY_PREFERENCES = rowInfo['Dietary Preferences']
    this.GENDER = rowInfo['Gender']
  }
}

async function _getCustomerRowInfo (customerID) {
  let rowInfo = await GoogleSheetsUtil.getGoogleSheetRowData(0, 'Customer ID', customerID)
  if (rowInfo === undefined) {
    throw new Error(`Searched customer ID: '${customerID}' was not found`)
  }
  return rowInfo
}

export default {
  async getCustomerInfo (custID) {
    let rowInfo = await _getCustomerRowInfo(custID)
    let basicInfo = new BasicInfo(rowInfo)
    let shippingAddress = new ShippingAddress(rowInfo)
    let billingAddress = new BillingAddress(rowInfo)
    let additionalAddress = new AdditionalAddress(rowInfo)
    let additionalInfo = new AdditionalInfo(rowInfo)
    return {...basicInfo, ...shippingAddress, ...billingAddress, ...additionalAddress, ...additionalInfo}
  },

  async getNewCustomer (market: string = 'US', key: string = 'auto') {
    let rowInfo = []
    rowInfo['First Name'] = faker.name.firstName()
    rowInfo['Last Name'] = faker.name.lastName()
    rowInfo['Email'] = `${key}.${faker.internet.email()}`
    rowInfo['Password'] = GLOBAL_PASSWORD
    rowInfo['Market'] = market
    return new BasicInfo(rowInfo)
  },

  async getCustomerShippingAddress (custID) {
    let rowInfo = await _getCustomerRowInfo(custID)
    return new ShippingAddress(rowInfo)
  },

  async getCustomerBillingAddress (custID) {
    let rowInfo = await _getCustomerRowInfo(custID)
    return new BillingAddress(rowInfo)
  },

  async getCustomerAdditionalAddress (custID) {
    let rowInfo = await _getCustomerRowInfo(custID)
    return new AdditionalAddress(rowInfo)
  },

  async getCustomerAdditionalInfo (custID) {
    let rowInfo = await _getCustomerRowInfo(custID)
    return new AdditionalInfo(rowInfo)
  }
}
