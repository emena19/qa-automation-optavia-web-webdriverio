// @flow
import ExcelUtil from 'src/lib/ExcelUtil'
const excelFileName = './src/testdata/inputdata/products/ProductTestData.xlsx'

export const product = {
  bars: {
    OPT_CRAN_NUT_BAR: 'opt-cran-nut-bar',
    OPT_CHOC_CURRY_BAR: 'opt-choc-curry-bar',
    OPT_PEANUT_CRISP_BAR: 'opt-peanut-crisp-bar',
    OPT_SMORE_CRISP_BAR: 'opt-smore-crisp-bar',
    OPT_COOKIE_CHEWY_BAR: 'cookie-chewy-bar',
    OPT_CHOC_CHIP_CHEWY_BAR: 'pb-choc-chip-chewy-bar',
    OPT_CHOC_CRISP_BAR: 'opt-choc-crisp-bar',
    OPT_RAISIN_CINN_CRISP_BAR: 'opt-raisin-cinn-crisp-bar',
    OPT_CHOC_CHERRY_BAR: 'opt-choc-cherry-bar',
    OPT_CHOC_MINT_CRISP_BAR: 'opt-choc-mint-crisp-bar',
    STRAW_OPTIMAL_BAR: 'straw-optimal-bar',
    OPT_LEMON_CRISP_BAR: 'opt-lemon-crisp-bar',
    OPT_BERRY_CRISP_BAR: 'opt-berry-crisp-bar',
    OPT_PB_CHOC_CHIP_BAR: 'opt-pb-choc-chip-bar',
    OPT_CAR_CRISP_BAR: 'opt-car-crisp-bar',
    OPT_CHOC_CRUNCH_BAR: 'choc-crunch-bar',
    OPT_CINN_SPICE_CRISP_BAR: 'opt-cinn-spice-crisp-bar'
  },
  drinks: {
    OPT_HOT_COCOA: 'hot-cocoa',
    OPT_VELVET_HOT_CHOCOLATE: 'opt-velvet-hot-chocolate',
    OPT_CAPPUCCINO: 'opt-cappuccino'
  },
  crunchers_poppers_sticks: {
    OPT_CHEDDAR_HERB_CRUNCH: 'opt-cheddar-herb-crunch',
    OPT_BBQ_CRUNCH: 'opt-bbq-crunch',
    OPT_TOMATO_PENNE: 'opt-tomato-penne',
    OPT_JALAPENO_POPPERS: 'opt-jalapeno-poppers',
    OPT_MUSTARD_ONION_STICKS: 'opt-mustard-onion-sticks',
    OPT_SPICY_POPPERS: 'opt-spicy-poppers'
  },
  dessert_style: {
    OPT_DARK_CHOC_YOGURT_BROWNIE: 'opt-dark-choc-yogurt-brownie',
    OPT_CHOC_CHIP_COOKIE: 'opt-choc-chip-cookie',
    OPT_CINN_CHEESE_CAKE: 'opt-cinn-cheese-cake',
    OPT_COFFEE_SOFT_SERVE: 'opt-coffee-soft-serve'
  },
  breakfast_style: {
    OPT_BLUEBERRY_ALMOND_CEREAL: 'opt-blueberry-oatmeal',
    OPT_MIXED_BERRY_CEREAL: 'mixed-berry-cereal',
    OPT_CHOC_CHIP_PANCAKES: 'opt-choc-chip-pancakes',
    OPT_ORIG_PANCAKES: 'opt-orig-pancakes',
    OPT_BERRY_CEREAL: 'opt-berry-cereal',
    OPT_CINN_SUGAR_CEREAL: 'opt-cinn-sugar-cereal-bx'
  },
  hearty_choices: {
    OPT_CHEDDAR_BISCUIT: 'opt-cheddar-biscuit',
    OPT_GARLIC_POTATOES: 'opt-garlic-potatoes',
    OPT_SC_CHIVE_POTATOES: 'opt-sc-chive-potatoes'
  },
  kits: {
    OPT_ON_THE_GO_KIT_5_1: 'EssentialOnTheGoKit_Product',
    OPT_OPTIMAL_KIT_5_1: 'EssentialOptimalKit_Product',
    OPT_OPTIMAL_KIT_4_2_1: '421EssentialOptimalKit_Product',
    OPT_OPTIMAL_KIT_5_1_US_FLAVORS: '81805EssentialOptimalHalalKit_Product',
    OPT_OPTIMAL_KIT_5_1_ASIA_ESPECIALS: '81812EssentialOptimalKitAsiaSpecials_Product',
    OPT_OPTIMAL_KIT_5_1_HABITS_HEALTH: 'opt-optimal-kit-5-1-habits-health',
    OPT_OPTAVIA_BUSINESS_KIT: 'optavia-business-kit'
  },
  lean_green_meals: {
    OPT_CHILI_LIME_CHICKEN: 'chili-lime-foh',
    OPT_TURKEY_MEATBALL_MARINARA: 'turkey-meatball-foh',
    OPT_BEEF_STEW: 'beef-stew-foh',
    OPT_CHICKEN_CACCIATORE: 'chicken-cacc-foh',
    OPT_GINGER_LEMONGRASS_CHICKEN: 'ginger-lemon-foh',
    OPT_CHICKEN_RICE_AND_VEGETABLES: 'chicken-rice-foh'
  },
  spanish_materials: {
    OPT_SUCCESS_SYSTEM_SPANISH: 'optavia-success-system-spanish'
  },
  marketing_materials: {
    OPT_SUCCESS_SYSTEM: 'optavia-success-system',
    OPT_STEPS_TO_SUCCESS: 'optavia-coaching-guide',
    OPT_OPTIMAL_51_INTRO_GUIDE: 'optimal-51-client-intro-guide',
    OPT_MAP_NOTEPAD: 'optavia-map-pad',
    OPT_HEALTH_ASSESSMENT: 'health-assessment',
    OPT_INFO_CARDS: 'optavia-info-cards',
    OPT_COACHING_GUIDE: 'coaching-guide',
    OPT_MEETING_ROOM_BOOKING_SG: 'sg-meeting-booking',
    OPT_MEETING_ROOM_BOOKING_HK: 'hk-meeting-booking'
  },
  renewal_certification: {
    OPT_BUSINESS_RENEWAL: 'business-renewal'
  },
  wellness_credit: {
    OPT_WELLNESS_CREDIT: 'wellness-credit'
  },
  purposeful_hydration: {
    OPT_PUR_HYDRATION_HABITS_HEALTH: 'opt-purpose-hydration-habits-health',
    OPT_PUR_HYDRATION_KIT: 'PurposefulHydrationKit_Product'
  },
  shakes_smoothies: {
    OPT_CHOC_SHAKE: 'opt-choc-shake',
    OPT_VAN_SHAKE: 'opt-van-shake',
    OPT_CHIA_SMOOTHIE: 'opt-chia-smoothie',
    OPT_CHOC_CHERRY_SHAKE: 'opt-choc-cherry-shake',
    OPT_COCONUT_SHAKE: 'opt-coconut-shake',
    OPT_CAR_MACCHIATO_SHAKE: 'opt-car-macchiato-shake',
    OPT_MOCHA_SHAKE: 'opt-mocha-shake',
    OPT_STRAW_SHAKE: 'opt-straw-shake',
    OPT_TROPICAL_SMOOTHIE: 'opt-tropical-smoothie',
    OPT_CHOC_ANTIOX_SHAKE: 'opt-choc-antiox-shake',
    OPT_GREEN_TEA_SHAKE: 'opt-green-tea-shake'
  },
  snacks: {
    OPT_OLIVE_SALT_POPCORN: 'opt-olive-salt-popcorn',
    OPT_PUFF_RANCH_SNACKS: 'opt-puff-ranch-snacks',
    OPT_PUFF_SWEET_SALT_SNACKS: 'opt-puff-sweet-salt-snacks',
    OPT_SHARP_CHEDDAR_POPCORN: 'opt-cheddar-popcorn',
    OPT_ROSE_SALT_CRACKERS: 'rose-salt-crackers',
    OPT_MULTI_CRACKERS: 'multi-crackers',
    OPT_CHEDDAR_POPCORN: 'cheddar-popcorn',
    OPT_SEA_SALT_POPCORN: 'sea-salt-popcorn'
  },
  soups: {
    OPT_WILD_RICE_CHICKEN_SOUP: 'opt-wild-rice-chicken-soup',
    OPT_CHICKEN_NOODLE_SOUPS: 'opt-chicken-noodle-soup',
    OPT_BEEF_VEG_SOUP: 'opt-beef-veg-soup',
    OPT_EGG_MUSHROOM_SOUP: 'opt-egg-mushroom-soup',
    OPT_MUSHROOM_SOUP: 'opt-cream-mushroom-soup',
    OPT_HOMESTYLE_CHICKEN_NOODLE_SOUP: 'opt-homestyle-chicken-noodle-soup'
  },
  tools_accessories: {
    OPT_DISCOVER_OPTIMAL_HEALTH: 'discover-optimal-health',
    OPT_OPTAVIA_GUIDE: 'optavia-guide',
    OPT_OPTAVIA_DIABETES_GUIDE: 'optavia-diabetes-guide',
    OPT_SPANISH_DR_HABITS_HEALTH_SYSTEM: 'dra-habits-health-spanish',
    OPT_OPTIMAL_WEIGHT_4_2_1_PLAN_GUIDE: 'optimal-421-guide',
    OPT_OPTIMAL_HEALTH_3_3_PLAN_GUIDE: 'optimal-33-guide',
    OPT_DINING_OUT_GUIDE: 'optavia-dining-out-guide',
    OPT_SENIORS_GUIDE: 'optavia-seniors-guide',
    OPT_BLENDER_BOTTLE: 'optavia-blender-bottle',
    OPT_ARIA_WIFI_BLACK: 'fitbit-aria-black',
    OPT_PUR_HYDRATION_WATER_BOTTLE: 'ph-water-bottle',
    OPT_HABITS_HEALTH_SYSTEM: 'HabitsHealthSystemKit_Product',
    OPT_OPTAVIA_GUIDE_ENGLISH: 'opt-guide-english',
    OPT_OPTAVIA_GUIDE_CHINESE: 'opt-guide-chinese',
    OPT_OPTAVIA_GUIDE_SG: 'opt-guide',
    OPT_CLIENT_TIP_SHEET: 'client-tip-sheet',
    OPT_JOURNEY_KICKOFF_CARD: 'journey-kickoff-card'

  },
  infusers_tea: {
    OPT_LEMON_ENERGY_INFUSER: 'lemon-infuser',
    OPT_GREEN_TEA_LEMON_INFUSER: 'green-tea-lemon-infuser',
    OPT_STRAWBERRY_LEMON_INFUSER: 'straw-lemon-infuser',
    OPT_ORANGE_ENERGY_INFUSER: 'orange-infuser',
    OPT_WHITE_GRAPE_PEACH_INFUSER: 'white-grape-peach-infuser',
    OPT_RASPBERRY_ACAI_INFUSER: 'rasp-acai-infuser',
    OPT_MIXED_BERRY_INFUSER: 'mixed-berry-infuser'
  },
  upsell: {
    OPT_ULTIMATE_HYDRATION_PACK: 'opt-ultimate-hydration-pack',
    OPT_CHEESY_PACK: 'opt-cheesy-pack',
    OPT_HYDRATION_STARTER_PACK: 'opt-hydration-starter-pack',
    OPT_HABITS_HEALTH_SYSTEM: 'opt-habits-health-system',
    OPT_HEALTH_PRO_MARK_PACK: 'opt-hea-pro-mar-pac',
    OPT_COACH_BUSINESS_KIT_SPA: 'opt-coa-buss-kit-spa',
    OPT_FUELING_EXP_PACK: 'opt-fue-exp-pac'

  },
  naturally_flavored: {
    FRUIT_NUT_CRUNCH_BAR: 'fruit-nut-crunch-bar'
  }
}

export const productUOM = {
  BOX: ' (Box)',
  CASE: ' (Case)',
  KIT: ' Kit',
  NONE: ''
}

export const market = {
  US: 1,
  SG: 2,
  HK: 3
}

export const attb = {
  CATEGORY: 'A',
  SUBCATEGORY: 'B',
  SHORT_NAME: 'C',
  NAME: 'D',
  SKU: 'E',
  UNIT_PRICE: 'F',
  CASE_PRICE: 'G',
  DESCRIPTION: 'H'
}

export default {
  async getProductRow (productShortName, market) {
    return ExcelUtil.getRowNumberByGivenCellValue(excelFileName, market, 'C', productShortName)
  },

  PRODUCT_INFO: async function (attribute: string, product: string, market: number, uom: string = '') {
    return await ExcelUtil.getCellData(excelFileName, market, `${attribute}${await this.getProductRow(product, market)}`) + (uom === '' && (attribute === attb.UNIT_PRICE || attribute === attb.CASE_PRICE) ? 0 : uom)
  },

  PRODUCT_LIST_INFO: async function (attribute: string, products: [], market: number) {
    let list = []
    for (const item of products) {
      list.push(await ExcelUtil.getCellData(excelFileName, market, `${attribute}${await this.getProductRow(item.prod, market)}`) + (item.uom === undefined && (attribute === attb.UNIT_PRICE || attribute === attb.CASE_PRICE) ? 0 : (attribute === attb.NAME && item.uom !== undefined ? item.uom : '')))
    }
    return list
  }

}
